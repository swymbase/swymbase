# SWYMbase

A knowledge base interface for saying what you mean.

## Contents

- [dev](/dev) : Scripts for development tasks
- [packages](/packages) : The deployable units (Node modules) defined by this
  project

## Prerequisites

This project requires Node version 12 or higher.

You will also need access to a SPARQL database. See the “[Database](#database)”
section below for notes about how to run one locally.

### NPM workspaces

This project uses
[NPM workspaces](https://docs.npmjs.com/cli/v7/using-npm/workspaces). NPM 7 is
required.

To install all dependencies required for working on the project, run

```sh
dev/configure-npm.sh
npm install
```

The `configure-npm` script copies an included `.npmrc` file to each package
directory, since some packages require non-public dependencides.

## Quick start

You can get up and running in one step by using the
[quickstart.sh](/dev/quickstart.sh) script:

```sh
dev/quickstart.sh
```

The quick start script performs a number of one-time steps:

- Create a default [configuration](#configuration)

- Set up a local [database](#database)

The script will also build the project and run the tests.

> See [the AVA notes](doc/ava.md) if you encounter trouble installing the AVA
> package.

Note that the quick start script currently assumes that Docker is installed.

The `quickstart` script will start a database on the default port of 3030. To
use a different port, pass a port number as the first argument to the script.

## Configuration

The application recognizes certain externally-defined settings.

In local environments, these settings are read from an `.env.local` file in the
root of this repository.

## Lerna

This project uses [Lerna](https://github.com/lerna/lerna) to manage the
publication of multiple packages to an NPM registry.

However, Lerna is used only from the CI. **You do not need Lerna for local
development.**

## Database

Most development tasks require the use of a database. This project defines a
high-level interface to a graph database. At a low level, the graph database is
interfaced through the standard RDF
[SPARQL 1.1 protocols](https://www.w3.org/TR/sparql11-protocol/). As a result,
the application can be used with any compliant SPARQL protocol service.

To configure the environment for database access, export the `SPARQL_ENDPOINT`
variable in your environment configuration:

```sh
export SPARQL_ENDPOINT='http://localhost:3030/swymbase'
```

### AWS X-Ray

You will want to include the following in your environment configuration in
order to prevent X-Ray instrumentation from crashing tests that use X-Ray
tracing:

```sh
export AWS_XRAY_CONTEXT_MISSING="LOG_ERROR"
export AWS_XRAY_LOG_LEVEL="silent"
```

### Engine compatibility

Not all SPARQL providers are identical. Compliant providers can and do extend
the specified behaviors by defining extension functions and even new syntax.
Engines also differ in their support for various entailment regimes.

This project assumes only the functionality required by the
[SPARQL specifications](https://www.w3.org/TR/sparql11-overview/) and therefore
should be usable with any compliant implementation.

### Supported engines

Currently, the application has been tested with two providers:

- [Apache Jena](https://jena.apache.org/). This is the default engine used for
  testing. See the [dev/jena](/dev/jena) directory for details on how to run a
  Jena instance locally.

- [Amazon Neptune](https://aws.amazon.com/neptune/). While Neptune cannot be run
  locally, it can be used both in production and development by pointing to its
  `SPARQL_ENDPOINT` (or `CLUSTER_ENDPOINT`).

#### Neptune optimization

If you are targeting a Neptune instance, you can opt into Neptune-specific
behavior by passing `neptune` as the `sparql_engine` option to the knowledge
base interface. (See [sparql-rest](packages/sparql-rest/src/api.ts) for
details.)

## Workflow

Once the one-time setup is done, you can iterate on the system by running the
watcher:

```sh
dev/watch.sh
```

Note that this will run both the TypeScript build watcher and the AVA test
watcher in the same shell. See [watch.sh](dev/watch.sh) for details.

With that, you should be able to observe the effects of changes to modules and
their tests as you save changes.

## Code formatting

This project uses [Prettier](https://prettier.io/) as a formatter for several
file types. This formatting is a requirement before code can be merged.

Most editors support format-on-save integration with Prettier, and this is the
recommended approach. To manually format code at any point, run the script

```sh
dev/format-code.sh
```
