// Rollup JS bundler configuration.  https://rollupjs.org/#javascript-api

/** Modules built in this project. */
const modules = [
  ["api-gateway-interop"],
  ["cdk"],
  ["cdk-image-builder"],
  ["cdk-jena-fuseki"],
  ["jsonapi"],
  ["jsonapi-client"],
  ["media-interop"],
  ["media-processing"],
  ["metatype-core"],
  ["metatype-jsonapi"],
  ["sparql-ast"],
  ["sparql-protocol-client"],
  ["sparql-rest"],
];

const externals = [
  "aws-xray-sdk-core",
  "ffmpeg-static", // for media processing
  "jsonld",
  "node-fetch",
  "tslib",
  "uuid",
  "@mindgrub/cdk-jena-fuseki",
  // Node built-ins
  "assert",
  "util",
  "child_process",
];

// In effect, all scoped packages (including ones defined by this project) are
// externals.  I just prefer to be explicit.
const is_external = id =>
  externals.includes(id) ||
  id.startsWith("@aws-sdk/") ||
  id.startsWith("@aws-cdk/") ||
  id.startsWith("@swymbase/");

const bundle = ([name, { node_only, ...config } = {}]) => ({
  input: `packages/${name}/dist/src/index.js`,
  external: is_external,
  output: {
    name: `@swymbase/${name}`,
    format: "commonjs",
    file: `packages/${name}/dist/index.js`,
    sourcemap: true,
  },
  plugins: [
    {
      name: "redirect-modules",
      resolveId(id) {
        if (id.startsWith("@swymbase/")) {
          const name = id.split("/", 2)[1];
          return `packages/${name}/dist/src/index.js`;
        }
        return null;
      },
    },
  ],
});

export default [
  // SPECIAL CASE for providing Jena image builder
  {
    input: `packages/cdk/dist/src/db/jena-image-builder.js`,
    external: is_external,
    output: {
      format: "commonjs",
      file: `packages/cdk/dist/jena-image-builder.js`,
    },
  },
  ...modules.map(bundle),
];
