// Types and functions for JSON-based descriptions of API-Lambda integrations.

export type ApiPathSpec = Record<string, ApiResourceSpec>;

export interface ApiMethodSpec {
  /** Relative path from the CDK synthesis location to the package's code. */
  readonly lambda_code_path: string;

  /** Entry point of the Lambda function in the form `{module}.{function}`. */
  readonly handler: string;

  /** Indicates that the integration should not include an authorizer. */
  readonly bypass_authorizer?: boolean;
}

export interface ApiResourceSpec {
  GET?: ApiMethodSpec;
  POST?: ApiMethodSpec;
  PATCH?: ApiMethodSpec;
  DELETE?: ApiMethodSpec;
  CHILDREN?: ApiPathSpec;
}

type Entry<T, K = keyof T> = K extends keyof T ? [K, T[K]] : never;
type Entries<T> = ReadonlyArray<Entry<Required<T>>>;

const entries = <T extends object>(o: T): Entries<T> =>
  Object.entries(o) as unknown as Entries<T>;

// Flattened form of integration record
export interface IntegrationInfo {
  // Blank if this is a top-level resource.
  readonly parent: string;
  readonly path: string;
  // This segment of the path, which may have placeholders.
  readonly name: string;
  readonly implementation?: ApiMethodSpec & { readonly http_method: string };
}

export function* walk_integrations(
  node: ApiPathSpec,
  parent_path: readonly string[] = []
): IterableIterator<IntegrationInfo> {
  for (const [name, resource] of entries(node)) {
    const path_steps = [...parent_path, name];
    const { CHILDREN, ...methods } = resource;
    const parent = parent_path.join("/");
    const path = path_steps.join("/");

    // Create a node for this path, even if it has no methods.
    yield { parent, path, name };

    for (const [http_method, spec] of entries(methods))
      yield { parent, path, name, implementation: { http_method, ...spec } };

    if (CHILDREN) yield* walk_integrations(CHILDREN, path_steps);
  }
}
