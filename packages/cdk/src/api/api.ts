/**
 * Configuration properties for the optional media subsystem.
 */
export interface MediaConfiguration {
  /**
   * When true, indicates that the media subsystem should be used.
   */
  readonly enabled: boolean;

  /**
   * Specify the AWS S3 bucket in which to store media assets interfaced through
   * the API.
   *
   * If the media subsystem is enabled and this is not provided, a default name
   * is used.
   *
   * This value is provided to Lambda environments that may need to interact
   * with S3.
   */
  readonly s3_default_bucket: string;

  /**
   * The location of the directory containing the code to ship with the Lambda
   * function.
   */
  readonly processor_code_path: string;

  // Defaults to `index.handler`
  readonly processor_entry_point?: string;
}
