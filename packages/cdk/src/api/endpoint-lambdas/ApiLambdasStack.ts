import * as aws_ec2 from "@aws-cdk/aws-ec2";
import * as aws_iam from "@aws-cdk/aws-iam";
import * as core from "@aws-cdk/core";
import type { MediaConfiguration } from "../api";
import type { ApiPathSpec } from "../api-specs";
import { Endpoints } from "./Endpoints";
import { SharedResources } from "./SharedResources";

export interface ApiLambdasStackProps extends core.StackProps {
  readonly vpc: aws_ec2.IVpc;
  readonly sparql_endpoint: string;
  readonly primary_graph: string;
  readonly user_pool_id: string;
  readonly web_domains: string;
  readonly api_endpoint: string;
  readonly endpoints_spec: ApiPathSpec;
  readonly media?: MediaConfiguration;
}

export class ApiLambdasStack extends core.Stack {
  readonly lambda_execution_role: aws_iam.Role;
  readonly lambda_security_group: aws_ec2.ISecurityGroup;

  constructor(scope: core.Construct, id: string, props: ApiLambdasStackProps) {
    super(scope, id, props);

    const { vpc } = props;

    // Shared Resources
    const { lambda_execution_role } = new SharedResources(this, "shared", {
      vpc,
    });

    // Endpoints
    const { lambda_security_group } = new Endpoints(this, "endpoints", {
      ...props,
      lambda_execution_role,
    });

    this.lambda_security_group = lambda_security_group;
    this.lambda_execution_role = lambda_execution_role;
  }
}
