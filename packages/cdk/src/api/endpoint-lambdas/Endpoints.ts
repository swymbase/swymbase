import * as aws_ec2 from "@aws-cdk/aws-ec2";
import * as aws_iam from "@aws-cdk/aws-iam";
import * as aws_lambda from "@aws-cdk/aws-lambda";
import * as aws_logs from "@aws-cdk/aws-logs";
import * as aws_ssm from "@aws-cdk/aws-ssm";
import * as core from "@aws-cdk/core";
import assert from "assert";
import type { MediaConfiguration } from "../api";
import type { ApiPathSpec } from "../api-specs";
import { walk_integrations } from "../api-specs";
import { FollowMode } from "@aws-cdk/assets";

interface EndpointsProps {
  readonly endpoints_spec: ApiPathSpec;
  readonly lambda_execution_role: aws_iam.Role;

  readonly vpc: aws_ec2.IVpc;

  readonly user_pool_id: string;
  readonly api_endpoint: string;
  readonly web_domains: string;
  readonly sparql_endpoint: string;
  readonly primary_graph: string;
  readonly media?: MediaConfiguration;
}

export class Endpoints extends core.Construct {
  // May end up passing this from the outside instead.  For now, this is where
  // it's created.
  readonly lambda_security_group: aws_ec2.ISecurityGroup;

  constructor(scope: core.Construct, id: string, props: EndpointsProps) {
    super(scope, id);

    // Stage is provided to the Lambda function environment.
    const stage = this.node.tryGetContext("stage");
    assert(stage, "Stage name is required.");

    // For constructing a repeatable SSM parameter name (see below)
    const app_name = this.node.tryGetContext("app_name");
    assert(app_name, "App name is required.");
    const region = core.Stack.of(this).region;
    const account_id = core.Stack.of(this).account;

    const {
      vpc,
      lambda_execution_role,
      endpoints_spec,
      sparql_endpoint,
      primary_graph,
      user_pool_id,
      api_endpoint,
      web_domains,
      media,
    } = props;

    const env_vars = {
      SWYMBASE_APP_ENV: stage,
      SWYMBASE_SPARQL_ENDPOINT: sparql_endpoint,
      SWYMBASE_PRIMARY_GRAPH: primary_graph,
      SWYMBASE_COGNITO_USER_POOL_ID: user_pool_id,
      SWYMBASE_API_ENDPOINT: api_endpoint,
      SWYMBASE_WEB_DOMAINS: web_domains,
      ...(media?.s3_default_bucket && {
        SWYMBASE_S3_DEFAULT_BUCKET: media?.s3_default_bucket,
      }),
    };

    // Lambda Security Group
    const lambda_sec_group = new aws_ec2.SecurityGroup(
      this,
      "register-lambda-sg",
      { vpc }
    );

    // Create Lambdas from specs

    // Dictionary of integration info by path
    const entries = [...walk_integrations(endpoints_spec)];

    for (const entry of entries) {
      const { path, implementation } = entry;

      if (!implementation) continue;
      const { http_method, handler, lambda_code_path } = implementation;

      const id = `${path}-${http_method}`;
      const stripped_name = id.replace(/[/]/g, "-").replace(/[.{}]/g, "");
      const function_name = `${app_name}--${stage}--${stripped_name}`;
      const parameter_name = `/${function_name}-arn`;

      const lambda = new aws_lambda.Function(this, `${id}-lambda`, {
        code: aws_lambda.Code.fromAsset(lambda_code_path),
        runtime: aws_lambda.Runtime.NODEJS_12_X,
        handler,
        // layers:[layer],
        vpc,
        securityGroup: lambda_sec_group,
        timeout: core.Duration.seconds(300),
        environment: { ...env_vars },
        role: lambda_execution_role,
        functionName: function_name,
      });

      new aws_logs.LogGroup(this, `${id}-lambda-logs`, {
        logGroupName: "/aws/lambda/" + lambda.functionName,
        removalPolicy: core.RemovalPolicy.DESTROY,
        retention: aws_logs.RetentionDays.ONE_MONTH,
      });

      lambda.addPermission(`${id}-permission`, {
        principal: new aws_iam.ServicePrincipal("apigateway.amazonaws.com"),
        action: "lambda:InvokeFunction",
        sourceArn: `arn:aws:execute-api:${region}:${account_id}:*/*/*/*`,
      });

      new aws_ssm.StringParameter(this, `param-${id}-arn`, {
        description: `${this.node.path} ${id} Lambda ARN`,
        parameterName: parameter_name,
        stringValue: lambda.functionArn,
      });
    }

    this.lambda_security_group = lambda_sec_group;
  }
}
