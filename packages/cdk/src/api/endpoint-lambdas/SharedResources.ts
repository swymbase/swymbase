import * as aws_iam from "@aws-cdk/aws-iam";
import * as aws_ssm from "@aws-cdk/aws-ssm";
import * as core from "@aws-cdk/core";

interface SharedResourcesProps {}

/**
 * Create an execution role for Lambda with necessary permissions.
 */
export class SharedResources extends core.Construct {
  readonly lambda_execution_role: aws_iam.Role;

  constructor(scope: core.Construct, id: string, props: SharedResourcesProps) {
    super(scope, id);

    const {} = props;

    // Lambda Execution Role
    const lambda_execution_role = new aws_iam.Role(this, "lambda-exec-role", {
      assumedBy: new aws_iam.ServicePrincipal("lambda.amazonaws.com"),
      managedPolicies: [
        aws_iam.ManagedPolicy.fromAwsManagedPolicyName("AWSLambdaExecute"),
        aws_iam.ManagedPolicy.fromManagedPolicyArn(
          this,
          "lambda-vpc-managed-policy",
          "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
        ),
      ],
      inlinePolicies: {
        s3: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: ["s3:*"],
              resources: ["*"],
            }),
          ],
        }),
        sqs: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: ["sqs:*"],
              resources: ["*"],
            }),
          ],
        }),
        cognito: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: ["cognito-idp:*"],
              resources: ["*"],
            }),
          ],
        }),
        lambda: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: ["lambda:InvokeFunction", "lambda:InvokeAsync"],
              resources: ["*"],
            }),
          ],
        }),
      },
    });

    new aws_ssm.StringParameter(this, "param-lambda-role-arn", {
      description: `${this.node.path} Lambda IAM Role ARN`,
      // I think we can get away with this here as this is not referenced by
      // name from any other construct
      parameterName: `/${this.node.path}/lambda-role-arn`,
      stringValue: lambda_execution_role.roleArn,
    });

    this.lambda_execution_role = lambda_execution_role;
  }
}
