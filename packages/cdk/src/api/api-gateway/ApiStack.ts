import * as core from "@aws-cdk/core";
import type { ApiPathSpec } from "../api-specs";
import { ApiAuthorizers } from "./ApiAuthorizers";
import { ApiCore, ApiCoreProps } from "./ApiCore";
import { ApiIntegrations } from "./ApiIntegrations";
import { ApiModels } from "./ApiModels";

export interface ApiStackProps extends core.StackProps, ApiCoreProps {
  readonly user_pool_arn: string;
  readonly endpoints_spec: ApiPathSpec;
}

export class ApiStack extends core.Stack {
  constructor(scope: core.Construct, id: string, props: ApiStackProps) {
    super(scope, id, props);

    const { user_pool_arn, endpoints_spec } = props;

    // API Gateway Core Components
    const { api } = new ApiCore(this, "api", props);

    // API Gateway Authorizer
    const { authorizer } = new ApiAuthorizers(
      this,
      "api-authorizer-construct",
      { user_pool_arn, api }
    );

    // API Gateway Core Components
    new ApiModels(this, "api-models", { api });

    // API Gateway Integrations with Lambda Proxies
    new ApiIntegrations(this, "endpoint-integrations", {
      api,
      authorizer,
      endpoints_spec,
    });
  }
}
