import * as aws_apigateway from "@aws-cdk/aws-apigateway";
import * as aws_iam from "@aws-cdk/aws-iam";
import * as core from "@aws-cdk/core";

interface ApiAuthorizersProps {
  // Assuming this type based on expected prop
  readonly api: aws_apigateway.RestApi;
  readonly user_pool_arn: string;
}

export class ApiAuthorizers extends core.Construct {
  readonly authorizer: aws_apigateway.CfnAuthorizer;

  constructor(scope: core.Construct, id: string, props: ApiAuthorizersProps) {
    super(scope, id);

    const { api, user_pool_arn } = props;

    // Construct a safe prefix according to the rules for authorizer names.
    const namespace = this.node.path.replace(/[^-_A-Za-z0-9 ]/g, "--");

    const authorizer_role = new aws_iam.Role(this, "authorizer-role", {
      assumedBy: new aws_iam.ServicePrincipal("apigateway.amazonaws.com"),
      inlinePolicies: {
        cognito: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: ["cognito:*"],
              resources: ["*"],
            }),
          ],
        }),
        apigateway: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: ["apigateway:*"],
              resources: ["*"],
            }),
          ],
        }),
      },
    });

    // Cognito Authorizer
    const authorizer = new aws_apigateway.CfnAuthorizer(this, "authorizer", {
      restApiId: api.restApiId,
      type: "COGNITO_USER_POOLS",
      authorizerCredentials: authorizer_role.roleArn,
      authorizerResultTtlInSeconds: 300,
      identitySource: "method.request.header.Authorization",
      name: `${namespace}-cognito-authorizer`,
      providerArns: [user_pool_arn],
    });

    this.authorizer = authorizer;
  }
}
