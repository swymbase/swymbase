import * as aws_apigateway from "@aws-cdk/aws-apigateway";
import * as aws_lambda from "@aws-cdk/aws-lambda";
import * as aws_ssm from "@aws-cdk/aws-ssm";
import * as core from "@aws-cdk/core";
import assert from "assert";
import type { ApiPathSpec } from "../api-specs";
import { walk_integrations } from "../api-specs";

interface ApiIntegrationsProps {
  readonly endpoints_spec: ApiPathSpec;
  readonly api: aws_apigateway.RestApi;
  readonly authorizer: aws_apigateway.CfnAuthorizer;
}

/**
 * Construct API endpoints from a JSON-compatible description.  Creates
 * Authorizer and Lambda integrations.
 */
export class ApiIntegrations extends core.Construct {
  constructor(scope: core.Construct, id: string, props: ApiIntegrationsProps) {
    super(scope, id);

    const { api, authorizer, endpoints_spec } = props;

    // Dictionary of integration info by path
    const entries = [...walk_integrations(endpoints_spec)];
    const integrations = Object.fromEntries(entries.map((_) => [_.path, _]));

    // Map of resources by path
    const resources = new Map<string, aws_apigateway.IResource>();

    const app_name = this.node.tryGetContext("app_name");
    assert(app_name, "App name is required.");
    const stage = this.node.tryGetContext("stage");
    assert(stage, "Stage name is required.");

    // Get the resource with the given path, creating it if necessary.
    const ensure_resource = (path: string): aws_apigateway.IResource => {
      if (!resources.has(path)) {
        const info = integrations[path];
        const parent = info.parent ? ensure_resource(info.parent) : api.root;
        const resource = parent.addResource(info.name);
        resources.set(path, resource);
      }
      return resources.get(path)!;
    };

    for (const entry of entries) {
      const { path, implementation } = entry;
      if (!implementation) continue;

      const { http_method, bypass_authorizer } = implementation;

      const id = `${path}-${http_method}`;
      const stripped_name = id.replace(/[/]/g, "-").replace(/[.{}]/g, "");
      const function_name = `${app_name}--${stage}--${stripped_name}`;
      const parameter_name = `/${function_name}-arn`;

      const resource = ensure_resource(path);

      const arn_param = aws_ssm.StringParameter.valueForStringParameter(
        this,
        parameter_name
      );

      const lambda = aws_lambda.Function.fromFunctionArn(
        this,
        `${id}-from-arn`,
        arn_param
      );

      const integration = new aws_apigateway.LambdaIntegration(lambda, {
        requestTemplates: { "application/json": '{ "statusCode": "200" }' },
      });

      // Support opt-out of default authorizer.
      const use_auth = !bypass_authorizer;

      let authorization_type: aws_apigateway.AuthorizationType | undefined =
        undefined;
      let authorization_scopes: string[] | undefined = undefined;
      if (use_auth) {
        authorization_type = aws_apigateway.AuthorizationType.COGNITO;
        authorization_scopes = ["aws.cognito.signin.user.admin"];
      }

      const method = resource.addMethod(http_method, integration, {
        authorizationType: authorization_type,
        authorizationScopes: authorization_scopes,
      });

      if (use_auth) {
        const cfn_method = method.node.defaultChild as aws_apigateway.CfnMethod;
        cfn_method.addOverride("Properties.AuthorizerId", authorizer.ref);
      }
    }
  }
}
