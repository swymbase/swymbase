import * as aws_apigateway from "@aws-cdk/aws-apigateway";
import * as core from "@aws-cdk/core";

interface ApiModelsProps {
  readonly api: aws_apigateway.IRestApi;
}

export class ApiModels extends core.Construct {
  readonly models: {
    readonly response_model: aws_apigateway.Model;
    readonly error_response_model: aws_apigateway.Model;
  };

  constructor(scope: core.Construct, id: string, props: ApiModelsProps) {
    super(scope, id);

    const { api } = props;

    const response_model = new aws_apigateway.Model(this, "response-model", {
      schema: {
        schema: aws_apigateway.JsonSchemaVersion.DRAFT4,
        title: "responseModel",
        type: aws_apigateway.JsonSchemaType.OBJECT,
        properties: {
          state: {
            type: aws_apigateway.JsonSchemaType.STRING,
          },
          message: {
            type: aws_apigateway.JsonSchemaType.STRING,
          },
        },
      },
      contentType: "application/json",
      description: "response model",
      modelName: "ResponseModel",
      restApi: api,
    });

    const error_response_model = new aws_apigateway.Model(
      this,
      "error-response-model",
      {
        schema: {
          schema: aws_apigateway.JsonSchemaVersion.DRAFT4,
          title: "errorResponse",
          type: aws_apigateway.JsonSchemaType.OBJECT,
          properties: {
            state: {
              type: aws_apigateway.JsonSchemaType.STRING,
            },
            message: {
              type: aws_apigateway.JsonSchemaType.STRING,
            },
          },
        },
        contentType: "application/json",
        description: "error response model",
        modelName: "ErrorResponseModel",
        restApi: api,
      }
    );

    this.models = {
      response_model,
      error_response_model,
    };
  }
}
