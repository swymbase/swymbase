import * as aws_apigateway from "@aws-cdk/aws-apigateway";
import * as aws_certificatemanager from "@aws-cdk/aws-certificatemanager";
import * as aws_cloudfront from "@aws-cdk/aws-cloudfront";
import type * as aws_ec2 from "@aws-cdk/aws-ec2";
import * as aws_logs from "@aws-cdk/aws-logs";
import * as aws_route53 from "@aws-cdk/aws-route53";
import * as aws_route53_targets from "@aws-cdk/aws-route53-targets";
import * as core from "@aws-cdk/core";
import assert from "assert";

export interface ApiCoreProps {
  readonly vpc: aws_ec2.IVpc;
  /**
   * The domain name (without protocol) where the API will be accessed.
   */
  readonly domain_name: string;

  readonly additional_domains: readonly string[];

  /**
   * The domain to be used in the case that we're creating a DNS record for the
   * API.  This is also the domain used to validate a certificate if we are
   * creating a cert.
   */
  readonly hosted_zone?: string;

  /**
   * If a certificate was manually created, its ARN can be used.  If none is
   * provided, then one will be created based on the hosted zone.
   */
  readonly existing_cert_arn?: string;

  /**
   * A comma-separated list of domains where the API may be accessed (for CORS).
   */
  readonly web_domains: string;
}

export class ApiCore extends core.Construct {
  readonly api: aws_apigateway.RestApi;

  constructor(scope: core.Construct, id: string, props: ApiCoreProps) {
    super(scope, id);

    // Stage is a thing in API Gateway.  That said, since we generally create a
    // separate API for each stage, there's no particular need for the stage
    // name to match the “actual” stage name.
    const stage = this.node.tryGetContext("stage");
    assert(stage, "Stage name is required.");

    const {
      domain_name,
      hosted_zone,
      existing_cert_arn,
      web_domains,
      additional_domains,
    } = props;

    // Certificate for API Gateway Custom Domain

    let zone: aws_route53.IHostedZone | undefined = undefined;
    if (hosted_zone) {
      zone = aws_route53.HostedZone.fromLookup(this, "zone", {
        domainName: hosted_zone,
      });
    }

    let api_cert: aws_certificatemanager.ICertificate;
    if (existing_cert_arn) {
      api_cert = aws_certificatemanager.Certificate.fromCertificateArn(
        this,
        "api-acmcert",
        existing_cert_arn
      );
    } else {
      if (!zone)
        throw new Error(
          "At least one of hosted_zone or existing_cert_arn must be provided!"
        );

      api_cert = new aws_certificatemanager.DnsValidatedCertificate(
        this,
        "api-acmcert",
        {
          domainName: domain_name,
          hostedZone: zone,
        }
      );
    }

    // API Gateway

    // Construct a safe prefix according to the rules for export names.
    const namespace = this.node.path.replace(/[^-_A-Za-z0-9 ]/g, "--");

    const api_gateway_logs = new aws_logs.LogGroup(this, "api-gateway-logs", {
      logGroupName: `${this.node.path}-api-gateway-logs`,
      removalPolicy: core.RemovalPolicy.DESTROY,
      retention: aws_logs.RetentionDays.ONE_MONTH,
    });

    const api = new aws_apigateway.RestApi(this, "api-gateway", {
      // May need to sanitize this
      restApiName: `${this.node.path}-api`,
      description: `${this.node.path} API`,
      defaultCorsPreflightOptions: {
        allowOrigins: ["*"],
      },
      deploy: true,
      deployOptions: {
        stageName: stage,
        metricsEnabled: true,
        dataTraceEnabled: true,
        tracingEnabled: true,
        loggingLevel: aws_apigateway.MethodLoggingLevel.INFO,
        accessLogDestination: new aws_apigateway.LogGroupLogDestination(
          api_gateway_logs
        ),
        accessLogFormat: aws_apigateway.AccessLogFormat.jsonWithStandardFields({
          caller: true,
          httpMethod: true,
          ip: true,
          protocol: true,
          requestTime: true,
          resourcePath: true,
          responseLength: true,
          status: true,
          user: true,
        }),
      },
      endpointExportName: `${namespace}-api`,
      endpointTypes: [aws_apigateway.EndpointType.REGIONAL],
    });

    const responses = [
      aws_apigateway.ResponseType.ACCESS_DENIED,
      aws_apigateway.ResponseType.API_CONFIGURATION_ERROR,
      aws_apigateway.ResponseType.AUTHORIZER_CONFIGURATION_ERROR,
      aws_apigateway.ResponseType.AUTHORIZER_FAILURE,
      aws_apigateway.ResponseType.BAD_REQUEST_BODY,
      aws_apigateway.ResponseType.BAD_REQUEST_PARAMETERS,
      aws_apigateway.ResponseType.DEFAULT_4XX,
      aws_apigateway.ResponseType.DEFAULT_5XX,
      aws_apigateway.ResponseType.EXPIRED_TOKEN,
      aws_apigateway.ResponseType.INTEGRATION_FAILURE,
      aws_apigateway.ResponseType.INTEGRATION_TIMEOUT,
      aws_apigateway.ResponseType.INVALID_API_KEY,
      aws_apigateway.ResponseType.INVALID_SIGNATURE,
      aws_apigateway.ResponseType.MISSING_AUTHENTICATION_TOKEN,
      aws_apigateway.ResponseType.QUOTA_EXCEEDED,
      aws_apigateway.ResponseType.REQUEST_TOO_LARGE,
      aws_apigateway.ResponseType.RESOURCE_NOT_FOUND,
      aws_apigateway.ResponseType.THROTTLED,
      aws_apigateway.ResponseType.UNAUTHORIZED,
      aws_apigateway.ResponseType.UNSUPPORTED_MEDIA_TYPE,
      aws_apigateway.ResponseType.WAF_FILTERED,
    ];

    responses.forEach((response_type, index) => {
      api.addGatewayResponse(`${this.node.path}-gateway-response-${index}`, {
        type: response_type,
        responseHeaders: {
          "access-control-allow-origin": `'${web_domains}'`,
          "access-control-allow-headers":
            "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,X-Amz-User-Agent'",
          "access-control-allow-methods":
            "'OPTIONS,GET,PUT,POST,DELETE,PATCH,HEAD'",
          "content-type": "'application/json'",
        },
      });
    });

    // TS: Should be readonly, but receiving property is mutable
    const error_config: aws_cloudfront.CfnDistribution.CustomErrorResponseProperty[] =
      [
        {
          errorCode: 500,
          errorCachingMinTtl: 0,
        },
        {
          errorCode: 501,
          errorCachingMinTtl: 0,
        },
        {
          errorCode: 502,
          errorCachingMinTtl: 0,
        },
        {
          errorCode: 503,
          errorCachingMinTtl: 0,
        },
        {
          errorCode: 504,
          errorCachingMinTtl: 0,
        },
      ];

    const distribution = new aws_cloudfront.CloudFrontWebDistribution(
      this,
      "api-distro",
      {
        aliasConfiguration: {
          acmCertRef: api_cert.certificateArn,
          // TS: target property is mutable
          names: additional_domains as string[],
          sslMethod: aws_cloudfront.SSLMethod.SNI,
          securityPolicy: aws_cloudfront.SecurityPolicyProtocol.TLS_V1_1_2016,
        },
        errorConfigurations: error_config,
        originConfigs: [
          {
            originPath: `/${stage}`,
            customOriginSource: {
              // What's going on here?
              domainName: api.url.split("/")[2],
              allowedOriginSSLVersions: [
                aws_cloudfront.OriginSslPolicy.TLS_V1_2,
              ],
              httpsPort: 443,
              originKeepaliveTimeout: core.Duration.seconds(5),
              originReadTimeout: core.Duration.seconds(60),
              originProtocolPolicy:
                aws_cloudfront.OriginProtocolPolicy.HTTPS_ONLY,
            },
            behaviors: [
              {
                isDefaultBehavior: true,
                forwardedValues: {
                  queryString: true,
                  cookies: {
                    forward: "all",
                  },
                  headers: ["Authorization"],
                },
                allowedMethods: aws_cloudfront.CloudFrontAllowedMethods.ALL,
                compress: true,
                // default_ttl:core.Duration.seconds(86400),
                // max_ttl:core.Duration.seconds(31536000),
                defaultTtl: core.Duration.seconds(0),
                maxTtl: core.Duration.seconds(0),
                minTtl: core.Duration.seconds(0),
              },
            ],
          },
        ],
      }
    );

    // DNS Record for Custom Domain for API Gateway
    if (zone) {
      new aws_route53.ARecord(this, "api-dns-alias", {
        recordName: domain_name,
        target: aws_route53.RecordTarget.fromAlias(
          new aws_route53_targets.CloudFrontTarget(distribution)
        ),
        zone,
      });
    }

    this.api = api;
  }
}
