import * as aws_ec2 from "@aws-cdk/aws-ec2";
import * as aws_iam from "@aws-cdk/aws-iam";
import * as aws_lambda from "@aws-cdk/aws-lambda";
import * as aws_logs from "@aws-cdk/aws-logs";
import * as aws_s3 from "@aws-cdk/aws-s3";
import * as aws_s3_notifications from "@aws-cdk/aws-s3-notifications";
import * as core from "@aws-cdk/core";
import assert from "assert";
import type { MediaConfiguration } from "../api";

interface MediaStackProps extends core.StackProps {
  readonly vpc: aws_ec2.IVpc;
  // The same security group that is used for the API endpoints
  readonly lambda_security_group: aws_ec2.ISecurityGroup;
  // The same role that is used for the API endpoints
  readonly lambda_execution_role: aws_iam.Role;
  readonly config: MediaConfiguration;

  // Needed for environment of trigger Lambda that talks with KB
  readonly sparql_endpoint: string;
  readonly primary_graph: string;
}

/**
 * Stack to create resources required for the media subsystem if enabled.
 *
 * This stack provides functionality that is common to all API instances.
 */
export class MediaStack extends core.Stack {
  constructor(scope: core.Construct, id: string, props: MediaStackProps) {
    super(scope, id, props);

    const {
      vpc,
      config,
      lambda_execution_role,
      lambda_security_group,
      primary_graph,
      sparql_endpoint,
    } = props;

    const {
      enabled,
      s3_default_bucket: bucket_name,
      processor_code_path,
      processor_entry_point,
    } = config;

    // Don't do anything if the subsystem isn't enabled.
    if (!enabled) return;

    // For constructing a bucket name.  Technically, this is only necessary if a
    // bucket name isn't provided outright.
    const app_name = this.node.tryGetContext("app_name");
    assert(app_name, "App name is required.");

    // Stage is:
    //
    // - used to create distinct bucket names
    //
    // - provided to the trigger lambda, in the event that there is any
    //   environment-specific processing.
    const stage = this.node.tryGetContext("stage");
    assert(stage, "Stage name is required.");

    // Create S3 bucket for media storage.
    const bucket = new aws_s3.Bucket(this, "media-bucket", {
      bucketName: bucket_name,
      publicReadAccess: false,
      versioned: true,
      encryption: aws_s3.BucketEncryption.S3_MANAGED,
      cors: [
        {
          allowedMethods: [
            aws_s3.HttpMethods.GET,
            aws_s3.HttpMethods.POST,
            aws_s3.HttpMethods.HEAD,
            aws_s3.HttpMethods.PUT,
            aws_s3.HttpMethods.DELETE,
          ],
          allowedOrigins: ["*"],
          allowedHeaders: ["*"],
        },
      ],
    });

    // Create S3 trigger

    const function_name = `${app_name}--${stage}--new-media`;

    const media_lambda = new aws_lambda.Function(this, "new-media-handler", {
      vpc,
      code: aws_lambda.Code.fromAsset(processor_code_path),
      runtime: aws_lambda.Runtime.NODEJS_12_X,
      handler: processor_entry_point ?? "index.handler",
      securityGroup: lambda_security_group,
      timeout: core.Duration.seconds(300),
      memorySize: 1024,
      functionName: function_name,
      environment: {
        SWYMBASE_APP_ENV: stage,
        SWYMBASE_SPARQL_ENDPOINT: sparql_endpoint,
        SWYMBASE_PRIMARY_GRAPH: primary_graph,
        SWYMBASE_S3_DEFAULT_BUCKET: bucket_name,
        // TODO: Eventually do want this
        // SWYMBASE_SPARQL_ENGINE: props.sparql_engine,
        //
        // use SWYMBASE_ prefix as and when these are revived
        // CLUSTER_ENDPOINT: params["neptune_cluster_endpoint_encrypt"],
        // PORT: params["neptune_port_encrypt"],
        // READER_ENDPOINT: params["neptune_reader_endpoint_encrypt"],
        // SQS_URL_EMAIL: params["sqs_url_email"],
        // SQS_URL_SMS: params["sqs_url_sms"],
        // SQS_URL_NOTIFY: params["sqs_url_notify"],
        // SQS_URL_SOCKETS: params["sqs_url_sockets"],
      },
      role: lambda_execution_role,
    });

    new aws_logs.LogGroup(this, "new-media-handler-logs", {
      logGroupName: "/aws/lambda/" + media_lambda.functionName,
      removalPolicy: core.RemovalPolicy.DESTROY,
      retention: aws_logs.RetentionDays.ONE_MONTH,
    });

    const bucket_policy = new aws_iam.PolicyStatement({
      actions: ["s3:*"],
      resources: [bucket.bucketArn, bucket.bucketArn + "/*"],
    });
    bucket_policy.addArnPrincipal(lambda_execution_role.roleArn);
    bucket.addToResourcePolicy(bucket_policy);

    // Add Event to Bucket
    const lambda_dest = new aws_s3_notifications.LambdaDestination(
      media_lambda
    );

    bucket.addObjectCreatedNotification(lambda_dest);
  }
}
