// Main entry point for Swymbase CDK App.
import * as core from "@aws-cdk/core";
import assert from "assert";
import type { SwymbaseAppConfig } from "./api";
import type { MediaConfiguration } from "./api/api";
import { ApiLambdasStack, ApiStack } from "./api/index";
import { MediaStack } from "./api/media/MediaStack";
import type { JenaFusekiInstanceStackConfig } from "./db/index";
import {
  BastionStack,
  JenaFusekiInstanceStack,
  LoadBalancerStack,
} from "./db/index";
import { VpcStack } from "./network/index";

// When memory is unspecified, we use a more specific default based on instance
// size.
const JENA_MEMORY = {
  "m5.xlarge": 15,
};

const DEFAULT_JENA_CONFIG: JenaFusekiInstanceStackConfig = {
  // This value is compatible with the name used by `jena-image-builder`, and
  // thus should not normally need to be provided by apps.
  machineNamePattern: "jena-fuseki*",
  // I'm not even 100% sure that the Jena Fuseki image would work correctly with
  // a different port number.
  databasePort: 3030,
  instanceSize: "t3.medium",
  jenaMemory: 4,
};

/**
 * Extend a given app with constructs required for a given swymbase config.
 */
export function extend_app(app: core.App, config: SwymbaseAppConfig): void {
  const get_required_context = (key: string, fallback?: string): string => {
    const value = app.node.tryGetContext(key) ?? fallback;
    assert(value, `“${key}” is required in context.`);
    return value;
  };

  const app_name = get_required_context("app_name");
  const stage = get_required_context("stage");
  const bastion_ami = get_required_context("bastion_ami");

  // Standard practice across apps:
  // - `env` should be passed explicitly into stacks
  //   - (assuming they are environment agnostic)
  // - hardcode target account and region rather than taking from environment
  //   - CDK docs recommend this
  const { account, region } = config;
  const env: core.Environment = { account, region };

  const namespace = `${app_name}-${stage}`;

  // === Network

  const { vpc } = new VpcStack(app, `${namespace}-network`, {
    env,
    ...config.network,
  });

  // === Database

  const { db } = config;

  if (db.engine === "jena") {
    const { default_zone_name } = config;
    const db_domain = `data.${default_zone_name}`;
    const effective_db_config: JenaFusekiInstanceStackConfig = {
      ...DEFAULT_JENA_CONFIG,
      jenaMemory:
        JENA_MEMORY[db.instanceSize ?? DEFAULT_JENA_CONFIG.instanceSize] ??
        DEFAULT_JENA_CONFIG.jenaMemory,
      ...db,
    };
    const { databasePort } = effective_db_config;

    const { autoScalingGroup } = new JenaFusekiInstanceStack(
      app,
      `${namespace}-db-jena`,
      { env, vpc, ...effective_db_config }
    );

    new BastionStack(app, `${namespace}-db-bastion`, {
      env,
      vpc,
      bastion_ami,
      db_url: db_domain,
    });

    new LoadBalancerStack(app, `${namespace}-db-loadbalancer`, {
      env,
      vpc,
      zone_name: default_zone_name,
      domain_name: db_domain,
      database_port: databasePort,
      auto_scaling_group: autoScalingGroup,
    });
  } else {
    throw new Error(`Unsupported database engine: ‘${db.engine}’`);
  }

  // === API

  const media_config: MediaConfiguration = {
    s3_default_bucket:
      config.media.s3_default_bucket ?? `${app_name}-${stage}-media`,
    ...config.media,
  };

  const lambdas_stack = new ApiLambdasStack(app, `${namespace}-api-lambdas`, {
    env,
    vpc,
    media: media_config,
    ...config.api,
  });

  const api_stack = new ApiStack(app, `${namespace}-api`, {
    env,
    vpc,
    ...config.api,
  });
  api_stack.addDependency(lambdas_stack);

  const { lambda_execution_role, lambda_security_group } = lambdas_stack;

  const media_stack = new MediaStack(app, `${namespace}-media`, {
    env,
    vpc,
    lambda_execution_role,
    lambda_security_group,
    config: media_config,
    // TODO: this shouldn't be coming from client app config.  For Neptune at
    // least, it will have to be an output of the db stack.  Even for Jena, it
    // should have a default constructed from other info.
    sparql_endpoint: config.api.sparql_endpoint,
    primary_graph: config.api.primary_graph,
  });
}
