import type { MediaConfiguration } from "./api/api";
import type * as api from "./api/index";
import type * as db from "./db/index";
import type * as network from "./network/index";

/**
 * Swymbase configuration properties for a Jena Fuseki database.
 *
 * `database_port` defaults to 3030.  It is not public facing, so apps generally
 * won't care.
 *
 * `machine_name_pattern` defaults to a value compatible with the name used by
 * `jena-image-builder`.
 */
type JenaConfig = Partial<db.JenaFusekiInstanceStackConfig>;

type DatabaseConfig =
  | ({ engine: "jena" } & JenaConfig)
  // PROVISIONAL: Neptune is not yet supported via this CDK package
  | { engine: "neptune" };

type WithOptional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

/**
 * Configuration properties needed to deploy the application.
 *
 * This structure should be JSON-serializable.
 */
export interface SwymbaseAppConfig {
  readonly account: string;
  readonly region: string;
  // Also known as “non-prod zone name”, this is the domain root used as the
  // basis for other ad-hoc domain names within the app.
  readonly default_zone_name: string;

  // Configuration of stacks.
  readonly network: network.VpcStackProps;
  readonly db: DatabaseConfig;
  readonly api: Omit<api.ApiStackProps & api.ApiLambdasStackProps, "vpc">;
  readonly media: WithOptional<MediaConfiguration, "s3_default_bucket">;
}
