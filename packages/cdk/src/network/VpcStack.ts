import * as aws_ec2 from "@aws-cdk/aws-ec2";
import * as aws_logs from "@aws-cdk/aws-logs";
import * as core from "@aws-cdk/core";

// Fun, but not necessarily amenable to metatype.
type CIDR = `${number}.${number}.${number}.${number}/${number}`;

/**
 * Configuration required to create VPC stack.
 */
export interface VpcStackProps extends core.StackProps {
  // TODO: document this
  readonly cidr: CIDR;

  // TODO: document this
  readonly nat_count: number;
}

/**
 * A default VPC created for swymbase when an existing one is not provided.
 */
export class VpcStack extends core.Stack {
  readonly vpc: aws_ec2.IVpc;

  constructor(scope: core.Construct, id: string, props: VpcStackProps) {
    super(scope, id, props);

    const { cidr, nat_count } = props;

    const flow_logs = new aws_logs.LogGroup(this, "flow-logs", {
      logGroupName: `${this.node.path}-flow-logs`,
      removalPolicy: core.RemovalPolicy.DESTROY,
      retention: aws_logs.RetentionDays.ONE_MONTH,
    });

    const vpc = new aws_ec2.Vpc(this, "vpc", {
      cidr,
      natGatewayProvider: aws_ec2.NatProvider.gateway(),
      natGateways: nat_count,
      maxAzs: 2,
      natGatewaySubnets: { subnetType: aws_ec2.SubnetType.PUBLIC },
      subnetConfiguration: [
        {
          name: `${this.node.path}-public`,
          subnetType: aws_ec2.SubnetType.PUBLIC,
        },
        {
          name: `${this.node.path}-private`,
          subnetType: aws_ec2.SubnetType.PRIVATE,
        },
      ],
      flowLogs: {
        All: {
          destination: aws_ec2.FlowLogDestination.toCloudWatchLogs(flow_logs),
          trafficType: aws_ec2.FlowLogTrafficType.ALL,
        },
      },
    });

    new core.CfnOutput(this, "vpc_id", {
      value: vpc.vpcId,
      // IS USING node path instead of app env name going to make this a pain to
      // use?
      exportName: `${this.node.path}-vpc-id`,
    });

    this.vpc = vpc;
  }
}
