# Swymbase CDK database stack

This module defines CDK constructs and resources for deploying a swymbase
database stack.

The database stack consists primarily of the database engine and related
networking components.

## Jena Image Builder

Swymbase supports [Apache Jena](https://jena.apache.org/) as a storage engine.
The CDK constructs defined in this package include special support for deploying
a [Jena Fuseki](https://jena.apache.org/documentation/fuseki2/index.html) server
as the application's SPARQL provider. The Fuseki service uses a custom-built
image. The recipe for that image is defined in `@swymbase/cdk-jena-fuseki`.

Currently, the creation of an image based on that recipe is not included as part
of the main CDK application. A standalone app for deploying it is provided in
the this package at [jena-image-builder.ts](jena-image-builder.ts). (See also
the [bundler config](../../../../rollup.config.js)).

You can deploy it using normal `cdk` operations. Once that is deployed, you must
execute the the image builder pipeline to create the image.

## Bastion server

The KB stack includes a bastion server which provides controlled access to the
database. The bastion server is not required by the application, but is often
needed for development. To access the bastion server, you must start a session
(using AWS SSM) and tunnel to the instance.
