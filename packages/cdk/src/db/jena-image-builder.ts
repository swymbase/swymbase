// A standalone CDK app for building a JenaFuseki image-builder pipeline.
//
// This is an environment-agnostic app.
import * as core from "@aws-cdk/core";
import { JenaFusekiImageBuilderStack } from "@swymbase/cdk-jena-fuseki";

// As opposed to 'm5.xlarge', which we have also used
const DEFAULT_SIZE = "t3.medium";

export function main() {
  const app = new core.App();

  const get_required_context = (key: string): string => {
    const value = app.node.tryGetContext(key);
    if (!value) throw new Error(`“${key}” is required in context.`);
    return value;
  };

  const app_name = get_required_context("app_name");
  const instance_size = app.node.tryGetContext("instance_size") ?? DEFAULT_SIZE;

  new JenaFusekiImageBuilderStack(app, `${app_name}-jena-fuseki-imagebuilder`, {
    stackName: `${app_name}-jena-fuseki-imagebuilder`,
    archetype: "jena-fuseki",
    instanceSizeList: [instance_size],
  });
}

main();
