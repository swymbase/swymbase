import * as aws_ec2 from "@aws-cdk/aws-ec2";
import * as aws_elasticloadbalancingv2 from "@aws-cdk/aws-elasticloadbalancingv2";
import * as aws_route53 from "@aws-cdk/aws-route53";
import * as aws_route53_targets from "@aws-cdk/aws-route53-targets";
import * as core from "@aws-cdk/core";

export interface LoadBalancerStackProps extends core.StackProps {
  readonly vpc: aws_ec2.IVpc;
  // Does this need to be the zone that is associated with `domain_name?
  readonly zone_name: string;
  readonly domain_name: string;
  // The outward-facing port used by the database.
  readonly database_port: number;
  readonly auto_scaling_group: aws_elasticloadbalancingv2.IApplicationLoadBalancerTarget;
}

// Load balancer for the database.
//
// Questions:
//
// - Is this used for Neptune, too, or just Jena?
//
// - Does this need to be a stack?
export class LoadBalancerStack extends core.Stack {
  constructor(
    scope: core.Construct,
    id: string,
    props: LoadBalancerStackProps
  ) {
    super(scope, id, props);

    const { vpc, zone_name, domain_name, database_port, auto_scaling_group } =
      props;

    const zone = aws_route53.HostedZone.fromLookup(this, "zone", {
      domainName: zone_name,
    });

    const sec_group = new aws_ec2.SecurityGroup(this, "alb-sec-group", {
      vpc,
      description: `${this.node.path} load balancer security group`,
      allowAllOutbound: true,
    });
    sec_group.addIngressRule(
      aws_ec2.Peer.ipv4(vpc.vpcCidrBlock),
      aws_ec2.Port.allTcp(),
      "vpc-in"
    );

    const load_balancer =
      new aws_elasticloadbalancingv2.ApplicationLoadBalancer(this, "alb", {
        http2Enabled: true,
        idleTimeout: core.Duration.seconds(120),
        vpc,
        internetFacing: false,
        securityGroup: sec_group,
        loadBalancerName: `${this.node.path}-alb`,
        vpcSubnets: { subnets: vpc.privateSubnets },
      });

    const http_listener = load_balancer.addListener(
      `${this.node.path}-alb-http-listener`,
      {
        port: 80,
        protocol: aws_elasticloadbalancingv2.ApplicationProtocol.HTTP,
      }
    );

    new aws_route53.ARecord(this, "dns-alias", {
      recordName: domain_name,
      target: aws_route53.RecordTarget.fromAlias(
        new aws_route53_targets.LoadBalancerTarget(load_balancer)
      ),
      zone,
    });

    // Add Auto Scaling Group to ALB Target Group

    const target_group = new aws_elasticloadbalancingv2.ApplicationTargetGroup(
      this,
      "tg",
      {
        port: database_port,
        protocol: aws_elasticloadbalancingv2.ApplicationProtocol.HTTP,
        healthCheck: {
          enabled: true,
          healthyHttpCodes: "200,202",
          healthyThresholdCount: 5,
          path: "/",
          port: database_port.toString(),
          protocol: aws_elasticloadbalancingv2.Protocol.HTTP,
        },
        targetGroupName: this.node.path,
        vpc,
        targets: [auto_scaling_group],
      }
    );

    http_listener.addTargetGroups(`${this.node.path}-attach-tg`, {
      targetGroups: [target_group],
    });
  }
}
