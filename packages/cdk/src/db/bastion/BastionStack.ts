import * as core from "@aws-cdk/core";
import * as aws_ec2 from "@aws-cdk/aws-ec2";
import * as aws_logs from "@aws-cdk/aws-logs";
import * as aws_iam from "@aws-cdk/aws-iam";
import * as aws_autoscaling from "@aws-cdk/aws-autoscaling";

const BASTION_SIZE = "t2.micro";

export interface BastionStackProps extends core.StackProps {
  readonly vpc: aws_ec2.IVpc;
  // AMI id
  readonly bastion_ami: string;
  readonly db_url: string;
}

export class BastionStack extends core.Stack {
  constructor(scope: core.Construct, id: string, props: BastionStackProps) {
    super(scope, id, props);

    const { vpc, bastion_ami, db_url } = props;

    const bastion_sec_group = new aws_ec2.SecurityGroup(
      this,
      "bastion-sec-group",
      { vpc }
    );

    bastion_sec_group.addIngressRule(
      aws_ec2.Peer.ipv4(vpc.vpcCidrBlock),
      aws_ec2.Port.allTcp(),
      "vpc-in"
    );

    const bastion_logs = new aws_logs.LogGroup(this, "bastion-logs", {
      logGroupName: `${this.node.path}-bastion-logs`,
      removalPolicy: core.RemovalPolicy.DESTROY,
      retention: aws_logs.RetentionDays.ONE_MONTH,
    });

    const bastion_role = new aws_iam.Role(this, "inst-role", {
      assumedBy: new aws_iam.ServicePrincipal("ec2.amazonaws.com"),
      managedPolicies: [
        aws_iam.ManagedPolicy.fromAwsManagedPolicyName(
          "AmazonSSMManagedInstanceCore"
        ),
        aws_iam.ManagedPolicy.fromManagedPolicyArn(
          this,
          "EC2InstanceConnect-policy-arn",
          "arn:aws:iam::aws:policy/EC2InstanceConnect"
        ),
      ],
      inlinePolicies: {
        logs: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams",
              ],
              resources: [bastion_logs.logGroupArn],
            }),
          ],
        }),
      },
    });

    const bastion_image = aws_ec2.MachineImage.genericLinux({
      "us-east-1": bastion_ami,
    });

    // Egress firewall
    // HTTP/HTTPS = 80, 443
    // DNS = 53
    // DHCP = 67, 68
    // NTP = 123
    // NFS = 2049
    // SSH = 22
    const vpc_cidr_block = vpc.vpcCidrBlock;
    const inbound_tcp_ports = [80, 22];
    const inbound_udp_ports = [68];
    const inbound_tcp_ports_joined = inbound_tcp_ports.join(",");
    const inbound_udp_ports_joined = inbound_udp_ports.join(",");
    const outbound_tcp_ports = [80, 443, 53];
    const outbound_udp_ports = [53, 67, 123];
    const outbound_tcp_ports_joined = outbound_tcp_ports.join(",");
    const outbound_udp_ports_joined = outbound_udp_ports.join(",");

    const user_data = aws_ec2.UserData.forLinux();
    user_data.addCommands(
      "apt-get update -y",
      "apt-get install ec2-instance-connect haproxy -y",
      'echo "frontend database" >> /etc/haproxy/haproxy.cfg',
      'echo "        bind :80" >> /etc/haproxy/haproxy.cfg',
      'echo "        mode http" >> /etc/haproxy/haproxy.cfg',
      'echo "        timeout client 60m" >> /etc/haproxy/haproxy.cfg',
      'echo "        default_backend database" >> /etc/haproxy/haproxy.cfg',
      'echo "backend database" >> /etc/haproxy/haproxy.cfg',
      'echo "        mode http" >> /etc/haproxy/haproxy.cfg',
      'echo "        option forwardfor" >> /etc/haproxy/haproxy.cfg',
      'echo "        option httpclose" >> /etc/haproxy/haproxy.cfg',
      'echo "        timeout server 60m" >> /etc/haproxy/haproxy.cfg',
      'echo "        balance roundrobin" >> /etc/haproxy/haproxy.cfg',
      `echo "        server database ${db_url}:80 verify none weight 1 check inter 10000" >> /etc/haproxy/haproxy.cfg`,
      "service haproxy restart",
      "iptables -A INPUT -i lo -j ACCEPT",
      "iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT",
      `iptables -A INPUT -p tcp -s ${vpc_cidr_block} --match multiport --dports 0:65535 -j ACCEPT`,
      `iptables -A INPUT -p tcp --match multiport --dports ${inbound_tcp_ports_joined} -j ACCEPT`,
      `iptables -A INPUT -p udp --match multiport --dports ${inbound_udp_ports_joined} -j ACCEPT`,
      "iptables -A INPUT -j REJECT",
      "iptables -A OUTPUT -o lo -p all -j ACCEPT",
      "iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT",
      `iptables -A OUTPUT -p tcp -d ${vpc_cidr_block} -j ACCEPT`,
      `iptables -A OUTPUT -p tcp --match multiport --dports ${outbound_tcp_ports_joined} -j ACCEPT`,
      `iptables -A OUTPUT -p udp --match multiport --dports ${outbound_udp_ports_joined} -j ACCEPT`,
      "iptables -A OUTPUT -p tcp --match multiport --dports 2049-j ACCEPT",
      "iptables -A OUTPUT -p icmp -j ACCEPT",
      "iptables -A OUTPUT -j REJECT",
      "iptables-save"
    );

    const bastion = new aws_autoscaling.AutoScalingGroup(this, "bastion", {
      instanceType: new aws_ec2.InstanceType(BASTION_SIZE),
      vpc,
      vpcSubnets: { subnets: vpc.privateSubnets },
      machineImage: bastion_image,
      minCapacity: 1,
      maxCapacity: 1,
      desiredCapacity: 1,
      role: bastion_role,
      securityGroup: bastion_sec_group,
      userData: user_data,
    });

    // These tags help scripts identify the relevant instance.
    core.Tags.of(bastion).add("Group", "Bastion");

    // TODO: consider tagging resources by stage universally?
    const stage = this.node.tryGetContext("stage");
    if (stage) core.Tags.of(bastion).add("Stage", stage);
  }
}
