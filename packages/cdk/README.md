# Swymbase CDK

This package contains resources for deploying a swymbase application via the
CDK.

## Media trigger

This package depends on `@swymbase/media-processing`. That package provides a
handler function to be used with an S3 event trigger. So while this package
doesn't import anything from `@swymbase/media-processing` directly, the Media
stack assumes that it's installed so that its code can be referenced as an
asset.
