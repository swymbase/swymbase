# Swymbase/metatype JSON-LD interop

Status: PROVISIONAL

This directory contains interop code for JSON-LD that was removed from other
packages.

JSON-LD provides for a kind of record-graph mapping that is similar to what
metatype defines. But as the latter has evolved, their models are no longer
isomorphic. So while initially JSON-LD was used in marshaling in and out
records, that approach was replaced with special marshaling functions.

The core swymbase packages will thus not take a dependency on JSON-LD.

The code here is retained in the event that some kind of JSON-LD interop were
wanted, e.g. to construct a `@context` or framing that would align with defined
record schemas.
