import { InterfacesFrom, metatypes } from "@swymbase/metatype-core";
import { jsonld_header, normalize } from "@swymbase/sparql-rest";
import test from "ava";
import * as jsonld from "jsonld";
import { strip_jsonld_context } from "./lib/jsonld-helpers";

const TYPES = metatypes({
  User: {
    "@type": "https://dev.thread.org/vocab/User",
    properties: {
      firebaseId: {
        term: "https://dev.thread.org/vocab/firebaseUid",
        type: "string",
        multivalued: false,
      },
      name: {
        term: "https://dev.thread.org/vocab/firstName",
        type: "string",
        multivalued: false,
      },
      lucky_numbers: {
        term: "https://dev.thread.org/vocab/has_lucky_number",
        type: "number",
        multivalued: true,
      },
      mentor: {
        term: "https://dev.thread.org/vocab/mentor",
        type: "object",
        multivalued: false,
      },
      has_friend: {
        term: "https://dev.thread.org/vocab/hasFriend",
        type: "object",
        multivalued: true,
      },
    },
  },
});

type Types = InterfacesFrom<typeof TYPES>;

type User = Types["User"];

test("roundtrip typed record through JSON-LD", async (t) => {
  const metadata = TYPES.User;
  const record: User = {
    name: "Joe",
    firebaseId: "abcde",
    mentor: { id: "http://example.com/entities/AliceColtrane" },
    lucky_numbers: [33, 19, 17, -4],
    has_friend: [
      { id: "http://example.com/entities/JohnColtrane" },
      { id: "http://example.com/entities/AliceColtrane" },
    ],
  };
  const header = jsonld_header(metadata);

  const json_ld = await jsonld.expand({ ...header, ...record });
  t.is(json_ld.length, 1, "Expected one object in graph");

  const [expanded] = json_ld;

  // Confirm that expanded JSON-LD matches expectations based on spec
  for (const [key, spec] of Object.entries(metadata.properties)) {
    t.log({ key, spec });
    const { term, type } = spec;
    t.assert(term in expanded, `Expected ${term} to be in ${expanded}`);

    if (spec.multivalued) {
      t.true(Array.isArray(record[key]));
      for (const item of record[key]) t.is(typeof item, type);
    } else t.is(typeof record[key], type);

    // Assumes singular values (currently only kind  supported)
    const values = expanded[term];
    // t.log("VALUES", values);
    if (spec.multivalued) {
      // Multi-valued types
      t.is(values.length, record[key].length);

      for (const value of record[key]) {
        if (type === "object") {
          // Multi-valued object types
          t.log("Multi-valued object types", values, value);
          t.assert(values.some((obj) => obj["@id"] === value["id"]));
        } else {
          // Multi-valued value types
          t.assert(values.some((obj) => obj["@value"] === value));
        }
      }
    } else {
      // Single-valued types
      t.is(values.length, 1, `Expected one value for property ${key}`);

      const [value] = values;
      if (type === "object") {
        // Single-valued object types
        t.assert("@id" in value, `Expected @id in ${JSON.stringify(value)}`);
        t.is(value["@id"], value["@id"]);
      } else {
        // Single-valued value types
        t.assert(
          "@value" in value,
          `Expected @value in ${JSON.stringify(value)}`
        );
        const expanded_value = value["@value"];
        t.is(expanded_value, record[key]);
      }
    }
  }

  // The tests on the expanded form are nice, but the normalized form is the one
  // that we make guarantees about.
  const roundtripped = await normalize(metadata, expanded);
  t.log("ROUNDTRIPPED", roundtripped);
  t.deepEqual(strip_jsonld_context(roundtripped), {
    ...record,
    type: TYPES.User["@type"],
  });
});
