import type { InterfaceFrom } from "@swymbase/metatype-core";
import { metatypes } from "@swymbase/metatype-core";
import { jsonld_header, normalize } from "@swymbase/sparql-rest";
import test from "ava";
import type { JsonLdArray } from "jsonld/jsonld-spec";
import { strip_jsonld_header } from "./lib/jsonld-helpers";

const META = metatypes({
  Person: {
    "@type": "https://example.com/vocab#User",
    properties: {
      name: {
        term: "https://example.com/vocab#name",
        type: "string",
      },
      age: {
        term: "https://example.com/vocab#age",
        type: "number",
      },
      is_earthling: {
        term: "https://example.com/vocab#isEarthling",
        type: "boolean",
      },
    },
  },
  Planet: {
    "@type": "https://example.com/vocab#Planet",
    properties: {
      name: {
        term: "https://example.com/vocab#name",
        type: "string",
      },
      age: {
        term: "https://example.com/vocab#age",
        type: "number",
      },
      is_dwarf: {
        term: "https://example.com/vocab#isDwarf",
        type: "boolean",
      },
      orbits: {
        term: "https://example.com/vocab#orbits",
        type: "object",
      },
      has_satellite: {
        term: "https://example.com/vocab#hasSatellite",
        type: "object",
        multivalued: true,
      },
    },
  },
});

test("creates a JSON-LD context entry for each key in a metatype spec", async (t) => {
  for (const [spec_name, type_spec] of Object.entries(META)) {
    const header = jsonld_header(type_spec);
    const context = header["@context"];
    t.assert(context);
    t.is(typeof context, "object");
    t.log(header);
    for (const [key] of Object.entries(type_spec.properties)) {
      t.assert(
        context.hasOwnProperty(key),
        `Context for ${spec_name} should have property ${key}`
      );
    }
  }
});

// This is now covered in more detail in metatype tests
test("can canonicalize a record", async (t) => {
  const spec = META.Person;
  const entity: InterfaceFrom<typeof spec> = {
    name: "Ellis Marsalis",
    age: 85,
    is_earthling: true,
  };

  const normalized = await normalize(spec, entity);

  t.deepEqual(strip_jsonld_header(normalized), entity);
});

test("can canonicalize a document with a value object", async (t) => {
  const spec = META.Person;

  const canonical: InterfaceFrom<typeof spec> = {
    name: "Ornette Coleman",
    age: 193,
    is_earthling: false,
  };

  const expanded: JsonLdArray = [
    {
      "https://example.com/vocab#name": [{ "@value": "Ornette Coleman" }],
      "https://example.com/vocab#age": [{ "@value": 193 }],
      "https://example.com/vocab#isEarthling": [{ "@value": false }],
    },
  ];

  const normalized = await normalize(spec, expanded);
  t.deepEqual(strip_jsonld_header(normalized), canonical);
});

test("can canonicalize a document with a value object with a stringified primitive", async (t) => {
  const spec = META.Person;

  const canonical: InterfaceFrom<typeof spec> = {
    name: "Ornette Coleman",
    age: 193,
    is_earthling: false,
  };

  const expanded: JsonLdArray = [
    {
      "https://example.com/vocab#name": [{ "@value": "Ornette Coleman" }],
      "https://example.com/vocab#age": [{ "@value": "193" }],
      "https://example.com/vocab#isEarthling": [{ "@value": "false" }],
    },
  ];

  const normalized = await normalize(spec, expanded);
  t.deepEqual(strip_jsonld_header(normalized), canonical);
});

test("can canonicalize a document that includes JSON-LD `@type` keyword", async (t) => {
  const spec = META.Person;

  const canonical: InterfaceFrom<typeof spec> = {
    type: "https://example.com/vocab#User",
    name: "Ornette Coleman",
    age: 193,
    is_earthling: false,
  };

  const expanded: JsonLdArray = [
    {
      "@type": ["https://example.com/vocab#User"],
      "https://example.com/vocab#name": [{ "@value": "Ornette Coleman" }],
      "https://example.com/vocab#age": [{ "@value": 193 }],
      "https://example.com/vocab#isEarthling": [{ "@value": false }],
    },
  ];

  const normalized = await normalize(spec, expanded);
  t.deepEqual(strip_jsonld_header(normalized), canonical);
});

test("can canonicalize a document that includes rdf:type IRI", async (t) => {
  const spec = META.Person;

  const canonical: InterfaceFrom<typeof spec> = {
    type: "https://example.com/vocab#User",
    name: "Ornette Coleman",
    age: 193,
    is_earthling: false,
  };

  const expanded: JsonLdArray = [
    {
      // Neptune sometimes returns results with this instead of `@type`
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": [
        "https://example.com/vocab#User",
      ],
      "https://example.com/vocab#name": [{ "@value": "Ornette Coleman" }],
      "https://example.com/vocab#age": [{ "@value": 193 }],
      "https://example.com/vocab#isEarthling": [{ "@value": false }],
    },
  ];

  const normalized = await normalize(spec, expanded);
  t.deepEqual(strip_jsonld_header(normalized), canonical);
});

// TODO: This makes no sense.  Need separate normalize functions for one and many.
test("can normalize an empty array", async (t) => {
  const spec = META.Person;
  const expanded = [];
  const normalized = await normalize(spec, expanded);
  t.log("NORMALIZED", normalized);
  t.deepEqual(strip_jsonld_header(normalized), {});
});
