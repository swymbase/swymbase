// JSON-LD interop is being removed, or at least being removed to a standalone
// package.  Core swymbase will not take a dependency on any JSON-LD
// implementation.
import type { MetaInterface } from "@swymbase/metatype-core";
import * as jsonld from "jsonld";
import type { JsonLdArray } from "jsonld/jsonld-spec";

const RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

const DATATYPE_BY_RUNTIME_TYPE = {
  string: "http://www.w3.org/2001/XMLSchema#string",
  boolean: "http://www.w3.org/2001/XMLSchema#boolean",
  number: "http://www.w3.org/2001/XMLSchema#float",
};

/**
 * Create a JSON-LD context based on an interface definition.
 *
 * Note that you cannot set the `@type` of a node through the context.  See
 * https://github.com/w3c/json-ld-syntax/issues/76#issuecomment-427195612
 */
export const jsonld_header = (spec: MetaInterface) => {
  const context = {
    /**
     * Always map `@id` to id and `@type` to `type` (though regarding `@type`,
     * see other notes).
     *
     * Bikeshedders of the world, unite!
     * https://github.com/schemaorg/schemaorg/issues/854
     */
    id: "@id",
    type: "@type",

    /**
     * Records should always represent RDF type using an array of strings as
     * their `type` property.  The Editor's draft of the spec says that
     *
     * > Values of `@type`, or an alias of `@type`, may now have their
     * > `@container` set to `@set` to ensure that `@type` entries are always
     * > represented as an array
     *
     * [JSON-LD 1.1, “Changes since JSON-LD Community Group Final Report”]
     * (https://w3c.github.io/json-ld-syntax/#changes-from-cg)
     *
     * However, none of the following avails in current versions of `jsonld`, so
     * we use manual preprocessing as seen below.
     */
    //"@type": { "@container": "@set" },
    // [RDF_TYPE]: { "@container": "@set" },
  };

  for (const [key, prop] of Object.entries(spec.properties)) {
    context[key] = { "@id": prop.term };

    /**
     * Container support
     *
     * According to the [JSON-LD
     * spec](https://w3c.github.io/json-ld-syntax/#using-set-with-type), it
     * should be possible to use context to ensure that properties designated as
     * sets are always compacted to arrays.  However, it appears that the
     * described functionality has not been implemented, as any non-string value
     * for `@type` currently throws an error.
     *
     * See also https://github.com/json-ld/json-ld.org/issues/512
     */
    // if (prop.multivalued) context[key]["@type"] = { "@container": "@set" };
    if (prop.multivalued) context[key]["@container"] = "@set";

    // Datatype support
    const datatype = DATATYPE_BY_RUNTIME_TYPE[prop.type];
    if (datatype) context[key]["@type"] = datatype;
  }
  return { "@context": context, "@type": spec["@type"] };
};

// Also in get-group-list
const ensure_array = <T>(t: T | readonly T[]): readonly T[] =>
  Array.isArray(t) ? t : t == null ? [] : [t];

// Mutates an expanded JSON-LD document in place
// To normalize values according to a spec
const preprocess = (spec: MetaInterface, expanded: JsonLdArray): void => {
  const meta = Object.values(spec.properties);
  // Visit all values
  for (const node of ensure_array(expanded)) {
    /**
     * Recognize expanded term for `rdf:type`.
     *
     * JSON-LD context can associate `@type` with a given key, and it can
     * associate the IRI of `rdf:type` with a given key; but it can't associate
     * both with the same key.  Meanwhile, we do not control which form is used
     * in query results indicated as JSON-LD, and have seen both forms used.
     */
    if (node[RDF_TYPE]) {
      node["type"] = node[RDF_TYPE];
      delete node[RDF_TYPE];
    }

    for (const [term, values] of Object.entries(node)) {
      const type = meta.find((prop) => term === prop.term)?.type;
      if (type) {
        for (const value_object of ensure_array(values)) {
          // Ensure that expanded form includes corresponding type
          if (!value_object["@type"] && type in DATATYPE_BY_RUNTIME_TYPE)
            value_object["@type"] = DATATYPE_BY_RUNTIME_TYPE[type];

          // Convert all string values where type should be primitive
          const value = value_object?.["@value"];
          if (typeof value === "string") {
            if (type === "boolean" || type === "number")
              value_object["@value"] = JSON.parse(value);
          }
        }
      }
    }
  }
};

/**
 * Make a given JSON-LD object conform to a given spec.
 */
// DEPRECATED!  This will be removed soon.  It is referenced only by tests and
// should not be used by any client code.
export async function normalize(spec: MetaInterface, document: object) {
  const jsonld_context = jsonld_header(spec);
  const expanded = await jsonld.expand(document, {
    expandContext: jsonld_context,
  });
  preprocess(spec, expanded);
  const compacted = await jsonld.compact(expanded, jsonld_context, {
    skipExpansion: true,
  });

  for (const node of ensure_array(compacted)) {
    /**
     * Ensure that type is always represented as an array.  See note above.
     *
     * It would be preferable to do this while preprocessing the expanded form,
     * which is more predictable than the compacted form.  The issue here is
     * that the compaction algorithm would not know to leave these arrays alone.
     */
    const spec_type = jsonld_context["@type"];
    if (node["type"]) {
      // not ready for this yet
      // node["types"] = ensure_array(node["type"]);
      if (ensure_array(node["type"]).includes(spec_type)) {
        node["type"] = spec_type;
      } else {
        delete node["type"];
      }
    }

    for (const [key, prop] of Object.entries(spec.properties)) {
      // Opportunity to apply defaults
      if (!node.hasOwnProperty(key)) {
        // WORKAROUND: ensure that properties designated as multi-valued are
        // returned as arrays.  See note to `jsonld_header`
        if (prop.multivalued) node[key] = [];
      }
    }
  }

  return compacted;
}
