# Meta types

This package defines a simple vocabulary for describing record structures and
operations for bridging values between JSON, TypeScript, and RDF.

This is a type-only package, except for the `metatypes` helper constructor,
which is an identity function.

## Motivation

TypeScript applications that use an RDF backing store need a single source of
truth that supports the following use cases:

- documention
- mapping to RDF
- mapping from RDF
- mapping to JSON (output marshaling)
- mapping from JSON (input marshaling)
- mapping to TypeScript interfaces
- mapping to JSON Schema (if you're into that sort of thing)

Non-goals:

- validation (arbitrary predicates)
- support dictionaries, which have no corollary in RDF

## Optionality

It's tempting to try to support “required” properties, but in practice

1. optionality is always contextual (see
   [“Maybe Not”](https://www.youtube.com/watch?v=YR5WdGrpoug)), and

2. graph databases do not make such guarantees in general. At most, a “validate”
   operation could report that expectations were violated (though clients would
   still have to deal with missing values).
