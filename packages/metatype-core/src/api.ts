import type { TypeFrom, ValueSpec } from "./specs/api";

// Represents a contextualized property
export interface MetaProperty {
  // The IRI of this property in an RDF vocabulary
  // MAY split this into namespace and term
  readonly term: string;

  // Note that `object` means the property value is an IRI reference.
  readonly type:
    | "string"
    | "number"
    | "boolean"
    | "object"
    // Have to nest this at the moment since literals are allowed in VSD.  I
    // keep saying I'm going to require that they be boxed, but haven't brought
    // myself to do it yet.
    | { readonly spec: ValueSpec };

  // rdfs:comment
  readonly comment?: string;

  // rdfs:label
  // Though note that rdfs:label can be multivalued
  readonly label?: string;

  /** Indicates whether this is a non-functional property (i.e. can have more
   * than one value).  Default is single-valued. */
  readonly multivalued?: boolean;

  /** Indicates whether the property describes a relationship to another entity
   *  Default is false, meaning the property is an attribute. */
  readonly relationship?: boolean;

  /** An IRI of a converse relationship property. */
  readonly converse_property?: string;

  /**
   * Optional indicator of the type or types to which the related item belongs.
   *
   * This is intended to be used with relationship type properties.
   *
   * The use of multiple types is provisional and not currently supported.
   * In practice, multiple values would comprise a Set.
   */
  // readonly range?: IRI | readonly IRI[];
  readonly range?: string | readonly string[];

  /**
   * Metadata for virtual properties that provide media download URL's.
   */
  // TODO: app-specific.  This interface should be extensible by clients.
  readonly media?: {
    /** The property on this record that will serve the url. */
    url_property: string;

    /** Time in seconds before generated URL will expire. */
    expiration: number;

    /** Array of processor ids, to be defined and handled by a third-party. */
    processors?: string[];
  };
}

export type MetaProperties = { [name: string]: MetaProperty };

// Represents a dictionary of property information.
export interface MetaInterface {
  // The IRI of an RDF class associated with instances of this interface.
  "@type": string;

  // rdfs:comment
  // Though note that this can be multivalued
  readonly comment?: string;

  // rdfs:label
  // Though note that rdfs:label can be multivalued
  readonly label?: string;

  // Prefix for IRI's used by things of this type.  Can be used to mint
  // identifiers for new instances as an alternative to blank node identifiers.
  readonly instance_namespace?: string;

  // The known properties having this type in their domain.
  readonly properties: MetaProperties;
}

export type MetaInterfaces = { [name: string]: MetaInterface };

// construct a type from metadata
type PrimitiveFrom<T> = T extends "string"
  ? string
  : T extends "number"
  ? number
  : T extends "boolean"
  ? boolean
  : T extends "object" // Type is required in some contexts
  ? { id: string; type?: string }
  : unknown;

// Use wrapping trick to prevent “excessively deep type instantiation” in some
// inference sites.
export type ValueTypeFrom<T> = [T] extends [{ spec: infer S }]
  ? TypeFrom<S>
  : PrimitiveFrom<T>;

export type PropertyFrom<T extends MetaProperty> = T["multivalued"] extends true
  ? readonly ValueTypeFrom<T["type"]>[]
  : ValueTypeFrom<T["type"]>;

type PropertiesFrom<T extends MetaInterface> = {
  readonly [K in keyof T["properties"]]?: PropertyFrom<T["properties"][K]>;
}; /* & {
  readonly types?: string[];
}*/

// Convert an interface description into a TypeScript type.  Note that all
// records are readonly and partial.  Always considers `id` and `type` as
// optional parts of type.
export type InterfaceFrom<T extends MetaInterface> = {
  readonly id?: string;
  readonly type?: T["@type"];
  // readonly types?: readonly string[];
} & PropertiesFrom<T>;

// PROVISIONAL: variant of `InterfaceFrom` where id is required
// TODO: requiring `type` breaks compile on some tests
export type EntityFrom<T extends MetaInterface> = {
  readonly id: string;
  readonly type?: T["@type"];
  // readonly types?: readonly string[];
} & PropertiesFrom<T>;

// Convert a dictionary of type descriptions into a dictionary of types.
export type InterfacesFrom<T extends Record<string, MetaInterface>> = {
  [K in keyof T]: InterfaceFrom<T[K]>;
};

// Convert a dictionary of type descriptions into a dictionary of types.
export type EntitiesFrom<T extends Record<string, MetaInterface>> = {
  [K in keyof T]: EntityFrom<T[K]>;
};

/**
 * Helper constructor for metatype definitions.  Provides argument with both a
 * `const`-like context (e.g. for use with `InterfacesFrom`) and contextual
 * awareness (completion as `MetaInterfaces`).
 *
 * See also https://github.com/Microsoft/TypeScript/issues/30680
 */
export const metatypes = <T extends MetaInterfaces>(input: T): T => input;
