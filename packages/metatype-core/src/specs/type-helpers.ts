// https://github.com/microsoft/TypeScript/issues/30680
export type Narrowable =
  | string
  | number
  | boolean
  | symbol
  | object
  | undefined
  | void
  | null
  | {};
