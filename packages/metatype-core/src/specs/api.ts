export type ValueSpec<S = {} /* = def.Specs */> =
  // Literals represent themselves
  | boolean
  | number
  | string
  | PrimitiveSpec
  | IntersectionSpec<S>
  | UnionSpec<S>
  | ArraySpec<S>
  | DictionarySpec<S>
  | TupleSpec<S>
  | RecordSpec<S>
  | KeyofSpec<S>
  | PartialSpec<S>
  | AliasSpec<S>
  // Anything beyond this point is for other targets (not type generation)
  | { readonly $pred: readonly [(...args: any[]) => boolean, ...any[]] };

/**
 * Represents a value space corresponding to a primitive type.
 */
export interface PrimitiveSpec {
  readonly $spec: "Primitive";
  readonly $of: "boolean" | "number" | "string" | "undefined";
}

/**
 * Represents an intersection of value spaces.
 *
 * Values in this space conform to all of the descriptions in the `$of` array.
 */
export interface IntersectionSpec<S> {
  readonly $spec: "Intersection";
  readonly $of: readonly ValueSpec<S>[];
}

/**
 * Represents a union of value spaces.
 *
 * Values in this space conform to at least one of the descriptions in the `$of`
 * list.
 */
export interface UnionSpec<S> {
  readonly $spec: "Union";
  readonly $of: readonly ValueSpec<S>[];
}

/**
 * Represents a value space consisting of a homogeneous array.
 *
 * Values in this space are arrays of the type described by `$of`.
 */
export interface ArraySpec<S> {
  readonly $spec: "Array";
  readonly $of: ValueSpec<S>;
}

/**
 * Represents a value space consisting of a homogeneous map with string keys.
 *
 * Values in this space are arrays of the type described by `$of`.
 */
export interface DictionarySpec<S> {
  readonly $spec: "Dictionary";
  readonly $of: ValueSpec<S>;
}

/**
 * Represents a tuple value space.
 *
 * Values in this space are arrays whose members conform to the corresponding
 * description in `$of`.
 */
export interface TupleSpec<S> {
  readonly $spec: "Tuple";
  readonly $of: readonly ValueSpec<S>[];
}

/**
 * Represents a record value space.
 *
 * Values in this space are dictionaries with the keys in `$of` and values
 * conforming to the corresponding descriptions.
 */
export interface RecordSpec<S> {
  readonly $spec: "Record";
  readonly $of: { readonly [key: string]: ValueSpec<S> };
}

/**
 * Represents a value space consisting of the keys of another value space
 * description.
 *
 * This corresponds to TypeScript's `keyof` operator.
 */
export interface KeyofSpec<S> {
  readonly $spec: "Keyof";
  readonly $of: ValueSpec<S>;
}

/**
 * Represents a value space consisting of a partial projection of another space.
 *
 * Values in this space conform to the description in `$of` but with all keys
 * made optional.
 *
 * This corresponds to TypeScript's built-in `Partial` type.
 */
export interface PartialSpec<S> {
  readonly $spec: "Partial";
  readonly $of: ValueSpec<S>;
}

/**
 * Represents a value space looked up by name in a given schema.
 */
export interface AliasSpec<S /* = def.Specs */> {
  readonly $spec: "Alias";
  readonly $of: keyof S;
}

// We can't define this type as such because it would need to reference itself
// export type ValueSpecSchema = Record<string, ValueSpec<K>>;

// ====== Type generation

/**
 * Map a dictionary of value space descriptions to a dictionary of types, using
 * it (or an alternate context) for alias lookup.
 */
export type TypesFrom<S, C = S> = {
  readonly [K in keyof S]: TypeFrom<S[K], C>;
};

/**
 * Map a value space description to a type, with schema for alias lookup.
 */
// prettier-ignore
export type TypeFrom<T, S = {}/* = def.Specs */> =
  T extends string | number | boolean ? T
  : T extends { $spec: "Primitive", $of: any } ? PrimitiveFrom<T["$of"]>
  : T extends { $spec: "Intersection", $of: any } ? IntersectionFrom<T["$of"], S>
  : T extends { $spec: "Union", $of: any } ? UnionFrom<T["$of"], S>
  : T extends { $spec: "Array", $of: any } ? readonly TypeFrom<T["$of"], S>[]
  : T extends { $spec: "Dictionary", $of: any } ? { readonly [key: string]: TypeFrom<T["$of"], S> }
  : T extends { $spec: "Tuple", $of: any } ? TypesFrom<T["$of"], S>
  : // Identical to tuple except that inlining produces cleaner types
    T extends { $spec: "Record", $of: any } ? { readonly [K in keyof T["$of"]]: TypeFrom<T["$of"][K], S> }
  : T extends { $spec: "Keyof", $of: any } ? KeyFrom<T["$of"], S>
  : T extends { $spec: "Partial", $of: any } ? Partial<TypeFrom<T["$of"], S>>
  : T extends { $spec: "Alias", $of: any } ? Aliased<S, T["$of"]>
  : T extends { $pred: any } ? never // predicates are ignored for purposes of type generation
  // This is useful, but is causing assignability problems for type in some cases.
  : unknown // { $error: "UnrecognizedType", type: T};

// ====== Special helpers

// A type (as opposed to its spec) can be registered *directly* using `__type`.
// See `def.spec.aliased`.
export type Aliased<S, K> = K extends keyof S
  ? S[K] extends { __type: infer T }
    ? T
    : TypeFrom<S[K], S>
  : { $error: "SpecNotFound"; type: K };

type KeyFrom<T, S> = keyof TypeFrom<T, S>;

type IntersectionFrom<T, S> = IntersectionFromTuple<TypesFrom<T, S>>;

// map all union members to generated types in the given schema
// could use the same technique as intersection from
// this gives what amounts to the same result
type UnionFrom<T, S> = T extends readonly (infer U)[]
  ? TypeFrom<U, S>
  : unknown;

// ====== General helpers

// typeof -> type
// prettier-ignore
export type PrimitiveFrom<T> =
  T extends "string" ? string
  : T extends "number" ? number
  : T extends "boolean" ? boolean
  : T extends "undefined" ? undefined
  : unknown;

type IntersectionFromTuple<T> = T extends readonly [infer F, ...infer R]
  ? [] extends R
    ? F
    : F & IntersectionFromTuple<R>
  : unknown;
