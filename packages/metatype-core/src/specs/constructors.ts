// Expression constructors.  The “Narrowable” business is for making sure that
// we infer literals wherever possible.
import type { Narrowable } from "./type-helpers";

const $literal = <T extends undefined | boolean | number | string>(t: T): T =>
  t;

const $primitive = <T extends "undefined" | "boolean" | "number" | "string">(
  t: T
): { $spec: "Primitive"; $of: T } => ({ $spec: "Primitive", $of: t });

const $or = <
  N extends Narrowable,
  M extends N | { [k: string]: N | M | [] },
  T extends M[]
>(
  ...t: T
): { $spec: "Union"; $of: T } => ({ $spec: "Union", $of: t });

const $and = <
  N extends Narrowable,
  M extends N | { [k: string]: N | M | [] },
  T extends M[]
>(
  ...t: T
): { $spec: "Intersection"; $of: T } => ({ $spec: "Intersection", $of: t });

const $record = <N extends Narrowable, T extends { [k: string]: N | T | [] }>(
  t: T
): { $spec: "Record"; $of: T } => ({ $spec: "Record", $of: t });

const $tuple = <
  N extends Narrowable,
  M extends N | { [k: string]: N | M | [] },
  T extends M[]
>(
  ...t: T
): { $spec: "Tuple"; $of: T } => ({ $spec: "Tuple", $of: t });

const $array = <
  N extends Narrowable,
  T extends N | { [k: string]: N | T | [] }
>(
  t: T
): { $spec: "Array"; $of: T } => ({ $spec: "Array", $of: t });

const $dictionary = <
  N extends Narrowable,
  T extends N | { [k: string]: N | T | [] }
>(
  t: T
): { $spec: "Dictionary"; $of: T } => ({ $spec: "Dictionary", $of: t });

const $keyof = <N extends Narrowable, T extends { [k: string]: N | T | [] }>(
  t: T
): { $spec: "Keyof"; $of: T } => ({ $spec: "Keyof", $of: t });

const $partial = <N extends Narrowable, T extends { [k: string]: N | T | [] }>(
  t: T
): { $spec: "Partial"; $of: T } => ({ $spec: "Partial", $of: t });

const $spec = <K extends string>(k: K): { $spec: "Alias"; $of: K } => ({
  $spec: "Alias",
  $of: k,
});

// Constructor
//
// TODO: the $spec values use title case... these don't.  One should change.
export const Specs = {
  literal: $literal,
  primitive: $primitive,
  // Yes, these objects are re-used every time.  Might want to use Object.freeze
  // in the constructors.
  string: $primitive("string"),
  number: $primitive("number"),
  boolean: $primitive("boolean"),
  undefined: $primitive("undefined"),
  or: $or,
  and: $and,
  tuple: $tuple,
  array: $array,
  dictionary: $dictionary,
  record: $record,
  spec: $spec,
  partial: $partial,
  keyof: $keyof,
};
