// Kitchen-sink front-end for record-oriented RDF interface
import { SparqlResponse } from "@swymbase/sparql-protocol-client";
import type {
  MetaInterfaces,
  InterfaceFrom,
  EntityFrom,
} from "@swymbase/metatype-core";
import type { KBInterfaceOptions, KBReadOptions } from "./api";
import * as rec from "./records/index";
import { Filter } from "./filter/api";

// A sparse dictionary from type alias to sparse lists of their property names.
// The list values are expected (though not strictly required) to be distinct.
export type Fieldsets<S extends MetaInterfaces> = {
  [K in keyof S]?: readonly (string & keyof S[K]["properties"])[];
};

// Need prettier update for this
// export type PropertyReference<T extends keyof S, S extends MetaInterfaces> =
//   `${T}:${keyof S[T]["properties"]}`
export type PropertyReference<S extends MetaInterfaces> = string;

/**
 * Represents a sequence of property references within the schema, starting from
 * a known type.
 */
export type SchemaPath<S extends MetaInterfaces, T extends keyof S> =
  | (string & keyof S[T]["properties"])
  | readonly [string & keyof S[T]["properties"], ...PropertyReference<S>[]];

export interface RecordFetchOptions<
  S extends MetaInterfaces,
  T extends keyof S
> {
  /**
   * An optional list of schema paths to process for inclusion in the response
   * value.  Resources reachable from the matched record by one of the paths
   * will be added to the included records in the response.
   *
   * The paths are interpreted as follows:
   *
   * - the first path step is a property name assumed to be resolved against the
   *   type relevant to the operation.
   *
   * - remaining path steps are expected to be qualified with the alias of their
   *   type, separated by a color (e.g. `User:name`).
   */
  // TODO: Rename to include_paths?
  readonly include_related?: readonly SchemaPath<S, T>[];
  readonly fields?: Fieldsets<S>;
}

export interface ListRecordOptions<S extends MetaInterfaces, T extends keyof S>
  extends RecordFetchOptions<S, T> {
  readonly filter_old?: { [path: string]: string | string[] };
  readonly filter?: Filter | null;
  readonly order_by?: readonly { property: string; descending?: boolean }[];
  readonly offset?: number;
  readonly limit?: number;
  readonly ignore_permissions?: boolean;
  readonly include_count?: boolean;
}

export interface KBRecordInterface<S extends MetaInterfaces, C = any> {
  record_exists<T extends keyof S>(
    type: T,
    id: string
  ): ReturnType<typeof rec.record_exists>;

  get_record<T extends keyof S>(
    type: T,
    id: string,
    options?: KBReadOptions & RecordFetchOptions<S, T>
  ): Promise<{
    readonly context: SparqlResponse;
    readonly value: EntityFrom<S[T]> | undefined;
    readonly included_values: EntityFrom<any>[] | undefined;
  }>;

  list_records<T extends keyof S>(
    type: T,
    context: C,
    options?: KBReadOptions & ListRecordOptions<S, T>
  ): Promise<{
    readonly context: SparqlResponse;
    readonly value: readonly EntityFrom<S[T]>[] | undefined;
    readonly included_values: readonly EntityFrom<any>[] | undefined;
    readonly total_result_count: number | undefined;
    readonly limit: number;
    readonly offset: number;
  }>;

  post_record<T extends keyof S>(
    type: T,
    record: InterfaceFrom<S[T]>,
    context: C
  ): ReturnType<typeof rec.post_record>;

  put_record<T extends keyof S>(
    type: T,
    record: EntityFrom<S[T]>
  ): ReturnType<typeof rec.put_record>;

  patch_record<T extends keyof S>(
    type: T,
    record: EntityFrom<S[T]>,
    context: C
  ): ReturnType<typeof rec.patch_record>;

  delete_record<T extends keyof S>(
    type: T,
    id: string,
    context: C
  ): ReturnType<typeof rec.delete_record>;

  // TEMP: For use in raw queries until interface supports necessary methods
  endpoint: string;
  named_graph?: string;
}

export const make_kb_interface = <S extends MetaInterfaces, C>(
  endpoint: string,
  types: S,
  options?: Partial<KBInterfaceOptions<S, C>>
): KBRecordInterface<S, C> => {
  return {
    record_exists: (type, id) =>
      rec.record_exists(types, type, id, endpoint, options),
    get_record: (type, id, get_options) =>
      rec.get_record(types, type, id, endpoint, {
        ...options,
        ...get_options,
      }),
    list_records: (type, context, list_options) =>
      rec.list_records(
        types,
        type,
        endpoint,
        { ...options, ...list_options },
        context
      ),
    post_record: (type, record, context: C) =>
      rec.post_record(types, type, record, endpoint, options, context),
    put_record: (type, record) =>
      rec.put_record(types, type, record, endpoint, options),
    patch_record: (type, record, context) =>
      rec.patch_record(types, type, record, endpoint, options, context),
    delete_record: (type, id, context) =>
      rec.delete_record(types, type, id, endpoint, options, context),

    endpoint,
    named_graph: options?.named_graph,
  };
};
