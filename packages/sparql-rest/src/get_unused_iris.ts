import { v4 as uuid } from "uuid";

/**
 * Generate a set of IRI's based on a given namespace and new UUID's.  Assumes
 * that the given prefix is already URI-encoded.
 *
 * This function does not check whether the resulting IRI's already exist within
 * any database.  Doing so becomes prohibitively costly for large numbers of
 * ID's.  See `get_unused_iris_checked.ts`.  The function is effectively
 * synchronous and has no explicit failure modes, but it retains its present
 * signature for compatibility with implementations that do perform availability
 * checks.
 */
export const get_unused_iris = async (
  count: number,
  _endpoint: string, // Not used in this implementation.
  prefix: string,
  _named_graph?: string // Not used in this implementation.
): Promise<
  | { readonly success: true; readonly result: readonly string[] }
  | {
      readonly success: false;
      readonly error: { readonly code: string; readonly context: object };
    }
> => {
  const result: string[] = [];
  for (let i = 0; i < count; i++) {
    result.push(`${prefix}${uuid()}`);
  }
  await new Promise((resolve) => setTimeout(resolve, 100));
  return { success: true, result };
};
