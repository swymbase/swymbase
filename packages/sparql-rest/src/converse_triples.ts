import { SparqlResponse, send } from "@swymbase/sparql-protocol-client";
import type { MetaInterfaces } from "@swymbase/metatype-core";
import { KBInterfaceOptions } from "./api";
import { serialize, maybe_contextualize } from "./simple";

export const CONVERSE_TERM = "http://www.w3.org/2002/07/owl#inverseOf";

const scope = (expr: string, named_graph?: string) =>
  named_graph ? `GRAPH <${named_graph}> { ${expr} }` : expr;

export const generate_converse_triples = (types: MetaInterfaces): string[] => {
  const triples: Set<string> = new Set();

  for (const type_spec of Object.values(types)) {
    for (const term_spec of Object.values(type_spec.properties)) {
      if (term_spec.converse_property) {
        triples.add(
          `<${term_spec.term}> <${CONVERSE_TERM}> <${term_spec.converse_property}>`
        );
      }
    }
  }

  return [...triples];
};

export const write_converse_triples = (
  types: MetaInterfaces,
  endpoint: string,
  options?: KBInterfaceOptions<any>
): Promise<SparqlResponse> => {
  const data = generate_converse_triples(types).join(" .\n");

  const update = serialize(
    maybe_contextualize({ type: "insert_data", data }, options?.named_graph)
  );

  return send({ endpoint, operation: { update } });
};

export const delete_converse_triples = (
  endpoint: string,
  options?: KBInterfaceOptions<any>
): Promise<SparqlResponse> => {
  const where = scope(`?s <${CONVERSE_TERM}> ?o`, options?.named_graph);

  const update = serialize({ type: "delete_where", where });

  return send({ endpoint, operation: { update } });
};
