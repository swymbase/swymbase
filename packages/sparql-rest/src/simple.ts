// Minimal representation of a special subset of SPARQL language.

interface Statement<T> {
  type: T;
  // For base & prefixes
  preamble?: string;
}

export type SimpleSparqlStatement =
  | (Statement<"select"> & {
      vars: string;
      where: string;
      from?: string;
      limit?: number;
    })
  | (Statement<"describe"> & {
      vars: string;
      where?: string;
      from?: string;
      limit?: number;
    })
  | (Statement<"construct"> & {
      pattern?: string;
      where: string;
      from?: string;
      limit?: number;
    })
  | (Statement<"ask"> & { where: string; from?: string })
  | (Statement<"delete_data"> & { data: string })
  | (Statement<"delete_where"> & { what?: string; where: string })
  | (Statement<"delete_insert"> & {
      what: string;
      data: string;
      where: string;
    })
  | (Statement<"insert_data"> & { data: string });

export const serialize = (stmt: SimpleSparqlStatement): string => {
  const from = () => (stmt["from"] ? `FROM <${stmt["from"]}>` : "");
  const limit = () => (stmt["limit"] ? `LIMIT ${stmt["limit"]}` : "");
  const where = () => (stmt["where"] ? `WHERE { ${stmt["where"]} }` : "");

  return (
    (stmt.preamble ? stmt.preamble + "\n" : "") +
    (stmt.type === "ask"
      ? `ASK ${from()} ${where()}`
      : stmt.type === "describe"
      ? `DESCRIBE ${stmt.vars} ${from()} ${where()} ${limit()}`
      : stmt.type === "construct"
      ? `CONSTRUCT ${
          stmt.pattern ? `{ ${stmt.pattern} }` : ""
        } ${from()} ${where()} ${limit()}`
      : stmt.type === "select"
      ? `SELECT ${stmt.vars} ${from()} ${where()} ${limit()}`
      : stmt.type === "delete_data"
      ? `DELETE DATA  { ${stmt.data} }`
      : stmt.type === "delete_where"
      ? `DELETE ${stmt.what ? `{ ${stmt.what} }` : ""} WHERE { ${stmt.where} }`
      : stmt.type === "delete_insert"
      ? `DELETE { ${stmt.what} } INSERT { ${stmt.data} } WHERE { ${stmt.where} }`
      : stmt.type === "insert_data"
      ? `INSERT DATA { ${stmt.data} }`
      : "")
  );
};

// Given a statement that is agnostic of named graphs, modify it to target the
// given named graph.  For simple statement representation, we assume that the
// contents don't mention (named or default) graphs.
export const contextualize = (
  statement: SimpleSparqlStatement,
  named_graph: string
): SimpleSparqlStatement => {
  return {
    ...statement,
    ...(statement.type === "ask"
      ? { from: named_graph }
      : statement.type === "select"
      ? { from: named_graph }
      : statement.type === "describe"
      ? { from: named_graph }
      : statement.type === "construct"
      ? { from: named_graph }
      : statement.type === "delete_data"
      ? { data: `GRAPH <${named_graph}> { ${statement.data} }` }
      : statement.type === "delete_where"
      ? { where: `GRAPH <${named_graph}> { ${statement.where} }` }
      : statement.type === "delete_insert"
      ? {
          what: `GRAPH <${named_graph}> { ${statement.what} }`,
          data: `GRAPH <${named_graph}> { ${statement.data} }`,
          where: `GRAPH <${named_graph}> { ${statement.where} }`,
        }
      : statement.type === "insert_data"
      ? { data: `GRAPH <${named_graph}> { ${statement.data} }` }
      : {}),
  };
};

// Given a statement that is agnostic of named graphs, modify it to target the
// given named graph.  For simple statement representation, we assume that the
// contents don't mention (named or default) graphs.
export const maybe_contextualize = (
  statement: SimpleSparqlStatement,
  named_graph?: string
): SimpleSparqlStatement =>
  named_graph ? contextualize(statement, named_graph) : statement;
