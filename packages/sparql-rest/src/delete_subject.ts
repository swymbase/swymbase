import { serialize, maybe_contextualize } from "./simple";
import { send } from "@swymbase/sparql-protocol-client";

// Note that this deleters the *resource*, not just the “record”.  It will wipe
// out all properties of the resource, whether or not they're defined on the
// record.
//
// This is *subject-oriented*: it does not touch facts where this resource is
// the object.
export async function delete_subject(
  id: string,
  endpoint: string,
  named_graph?: string
): ReturnType<typeof send> {
  const update = serialize(
    maybe_contextualize(
      { type: "delete_where", where: `<${id}> ?p ?o` },
      named_graph
    )
  );
  return send({ endpoint, operation: { update } });
}
