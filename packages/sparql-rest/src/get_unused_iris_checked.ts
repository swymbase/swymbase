import { formatted } from "@swymbase/sparql-protocol-client";
import { maybe_contextualize, serialize } from "./simple";

const escape_apostrophe = (s: string) => s.replace(/'/g, "\\'");

/**
 * Generate a set of IRI's that do not exist in the database as a subject or
 * object.  Combines a given prefix with a UUID.  Assumes that the given prefix
 * is already URI-encoded.
 *
 * This function is no longer used.  Given a large enough `count`, the resulting
 * SPARQL crashes Jena.  Performing the checks in batches would require multiple
 * database requests, which would be expensive and introduce race
 * conditions. The degree of safety achieved by checking the database is low
 * when weighed against the prospect of an UUID collision.
 */
export const get_unused_iris_checked = async (
  count: number,
  endpoint: string,
  prefix: string,
  named_graph?: string
): Promise<
  | { readonly success: true; readonly result: readonly string[] }
  | {
      readonly success: false;
      readonly error: { readonly code: string; readonly context: object };
    }
> => {
  if (count < 1) return { success: true, result: [] };
  const keys = [...new Array(count).keys()];
  const where = keys
    .map(
      (n) => `BIND(CONCAT('${escape_apostrophe(prefix)}', STRUUID()) as ?u${n})
      FILTER NOT EXISTS { ?u${n} ?p1 ?o }
      FILTER NOT EXISTS { ?s ?p2 ?u${n} }`
    )
    .join("\n");

  const query = serialize(
    maybe_contextualize({ type: "select", vars: "*", where })
  );

  let i = 10;
  while (i--) {
    const result = await formatted("select_results", {
      endpoint,
      operation: { query },
    });

    // If the database request failed outright, report this directly and
    // immediately to caller.  The retry mechanism here is meant only for
    // dealing with collisions.
    if (!result.context.success) {
      return {
        success: false,
        error: { code: "SelectFailed", context: result },
      };
    }

    const binding = result.value?.results.bindings[0];
    if (binding) {
      const candidate_iris = keys
        .map((n) => binding[`u${n}`].value)
        .filter((x) => !!x);
      if (candidate_iris?.length >= count)
        return { success: true, result: candidate_iris };
    }
  }
  throw new Error(
    `Could not generate ${count} ID’s for host ‘${endpoint}’ ${
      named_graph ? `in graph ‘${named_graph}’ ` : ``
    }with prefix ‘${prefix}’ after several attempts`
  );
};
