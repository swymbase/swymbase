import { SparqlResponse, send } from "@swymbase/sparql-protocol-client";
import { serialize } from "./simple";

/*
  Delete all values of the given properties for the identified resource.

  If the keys in the record/schema are ":name", ":knows" and ":blah", this will
  generate something like the following:

DELETE  {
  :Alice :name ?o0 .
  :Alice :knows ?o1 .
  :Alice :blah ?o2 .
}
WHERE {
  OPTIONAL { :Alice :name ?o0 }
  OPTIONAL { :Alice :knows ?o1 }
  OPTIONAL { :Alice :blah ?o2 }
}

  The following form would also have the same effect.

DELETE WHERE  { :Alice :name ?o  } ;
DELETE WHERE  { :Alice :knows ?o  } ;
DELETE WHERE  { :Alice :blah ?o  }

   This form is less redundant, but the engine *may* not guarantee
   transactionality for multiple queries.  The first form is safer.

   See https://www.w3.org/TR/2013/REC-sparql11-update-20130321/#updateServices

   > Each request SHOULD be treated atomically by a SPARQL 1.1 Update
   > service. The term 'atomically' means that a single request will result in
   > either no effect or a complete effect, regardless of the number of
   > operations that may be present in the request.

*/
export async function delete_properties(
  id: string,
  // Expected to be a ready-to-use list of IRI's
  properties: readonly string[],
  endpoint: string,
  named_graph?: string
): Promise<SparqlResponse> {
  const scope = (expr: string) =>
    named_graph ? `GRAPH <${named_graph}> { ${expr} }` : expr;
  const patterns = properties.map((p, i) => scope(`<${id}> <${p}> ?o${i}`));
  const what = patterns.join(" .\n");
  const where = patterns.map((pattern) => `OPTIONAL { ${pattern} }`).join("\n");
  const update = serialize({ type: "delete_where", what, where });
  return send({ endpoint, operation: { update } });
}
