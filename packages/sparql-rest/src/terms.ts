const RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
const XSD = "http://www.w3.org/2001/XMLSchema#";

export const rdf = {
  type: `${RDF}type`,
  JSON: `${RDF}JSON`,
};
export const xsd = {
  boolean: `${XSD}boolean`,
  float: `${XSD}float`,
  string: `${XSD}string`,
  decimal: `${XSD}decimal`,
  double: `${XSD}double`,
  integer: `${XSD}integer`,
};
