import { get_unused_iris } from "./get_unused_iris";

/**
 * Generate an IRI that does not exist in the database as a subject or object.
 * Combines a given prefix with a new UUID.  Assumes that the given prefix is
 * already URI-encoded.
 */
export const get_unused_iri = async (
  endpoint: string,
  prefix: string,
  named_graph?: string
): Promise<
  | { readonly success: true; result: string }
  | {
      readonly success: false;
      readonly error: { readonly code: string; readonly context: object };
    }
> => {
  const response = await get_unused_iris(1, endpoint, prefix, named_graph);
  if (!response.success) return response;
  return { success: true, result: response.result[0] };
};
