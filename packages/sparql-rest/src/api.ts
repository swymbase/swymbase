import type {
  MetaInterfaces,
  MetaInterface,
  InterfaceFrom,
  EntityFrom,
} from "@swymbase/metatype-core";

// An IRI would make sense here, if the vendors defined terms for their own
// products.  However, this doesn't appear to be the case.  For example,
// although Neptune uses the `http://aws.amazon.com/neptune/vocab/v01/` prefix
// for certain well-known entities, these are not part of any formal vocabulary
// (such as might include a term referring to the Neptune engine itself).  As
// long as we are inventing the terms used here, unqualified names will do.
export type RecognizedSparqlEngine = "neptune";

/* Permissions */

// `C` is the Context
interface PermissionSpec<C> {
  /**
   * Required natural-language description of the rule implemented by this
   * permission.
   */
  readonly description: string;

  /**
   * Returns a SPARQL phrase expressing a condition in a context.  Certain
   * variables will be defined in the context in which the expression is used.
   */
  where(thing_var: string, context: C): string;

  /** Optional development note. */
  readonly comment?: string;
}

// `C` is the Context
export interface RecordPermissions<C> {
  readonly create?: PermissionSpec<C>;
  readonly read?: PermissionSpec<C>;
  readonly update?: PermissionSpec<C>;
  readonly delete?: PermissionSpec<C>;
}

/**
 * Configuration for type-specific filters to be used unconditionally with
 * record operations.
 */
export interface SchemaPermissions<S extends MetaInterfaces, Context> {
  /**
   * Map a given context to SPARQL binding clauses.  Use to define variables
   * that can be referenced in filter clauses.  The returned value is an object
   * whose keys are the variable names and whose values are SPARQL expressions
   * to be bound to those variables.
   */
  bindings: (context: Context) => Record<string, string>;

  /** Optional per-type definitions of filters to use with record operations. */
  types: {
    readonly [K in keyof S]?: RecordPermissions<Context>;
  };
}

/* Record Observers */

export type RecordObserverEvents =
  | "retrieved"
  | "creating"
  | "created"
  | "updating"
  | "updated"
  | "deleting"
  | "deleted";

type RecordObserverEvent<
  M extends MetaInterface,
  Context,
  T extends InterfaceFrom<M> | EntityFrom<M>
> = (record: T, context: Context | undefined, type_spec: M) => T | Promise<T>;

export type RecordObserver<M extends MetaInterface, Context> = {
  readonly [event in Exclude<
    RecordObserverEvents,
    "creating"
  >]?: RecordObserverEvent<M, Context, EntityFrom<M>>;
} &
  {
    readonly [event in Extract<
      RecordObserverEvents,
      "creating"
    >]?: RecordObserverEvent<M, Context, InterfaceFrom<M>>;
  };

export type RecordObservers<S extends MetaInterfaces, Context> = {
  readonly ["*"]?: RecordObserver<any, Context>;
} & {
  readonly [K in keyof S]?: RecordObserver<S[K], Context>;
};

/* Interface options */

export interface KBInterfaceOptions<S extends MetaInterfaces, Context = any> {
  /**
   * An alternate endpoint for query-only operations.  If specified, this
   * endpoint will be used for read requests.  Otherwise, the main endpoint will
   * always be used.
   *
   * Note that the use of this property may be overridden on individual
   * operations by the `no_read_endpoint` option.
   */
  readonly read_endpoint?: string;

  /**
   * A named graph to target in all operations.  If not specified, the default
   * graph will be used.
   */
  readonly named_graph?: string;

  /**
   * Optional indicator of the SPARQL engine servicing the endpoints.  This can
   * be used to opt-in to optimizations or extended features.  It is not
   * required for correct functioning on any compliant engine.
   */
  readonly sparql_engine?: RecognizedSparqlEngine;

  /**
   * Optional per-type filters to apply to records during operations.
   */
  readonly permissions?: SchemaPermissions<S, Context>;

  /**
   * Optional per-type observers/transformers.
   */
  readonly observers?: RecordObservers<S, Context>;
}

/**
 * Options applicable to individual read operations on the knowledge base.
 */
export interface KBReadOptions {
  /**
   * When set, indicates that a read-only endpoint should not be used for an
   * operation even when available.  This may be needed for reading back the
   * most current data after a write, in which case a replica may not reflect
   * the update immediately.
   */
  readonly no_read_endpoint?: boolean;
}
