import type {
  MetaInterface,
  MetaInterfaces,
  MetaProperty,
} from "@swymbase/metatype-core";
import type { RecordPermissions, SchemaPermissions } from "../api";
import { SchemaPath } from "../interface";

const defined = <T>(x: T | undefined): x is T => x !== undefined;

export const ensure_array = <T>(t: T | readonly T[] | undefined): T[] =>
  Array.isArray(t) ? t : t == null ? [] : [t];

/**
 * Map the given property reference to its definition in the given schema.  If
 * the reference is qualified (like `prefix:suffix`), the prefix is expected to
 * be the name of a type in the schema and the suffix a property for that type.
 * If the reference is unqualified, the given default type is assumed.
 */
const resolve_property = (
  types: MetaInterfaces,
  default_type: string,
  reference: string
): MetaProperty | undefined => {
  const [type, property] = reference.includes(":")
    ? reference.split(":", 2)
    : [default_type, reference];

  return types[type]?.properties?.[property];
};

/**
 * Map the given property path to a sequence of SPARQL path steps, using the
 * given schema and starting type.  Steps referring to converse properties are
 * mapped to inverse property steps against the converse term.
 */
export const resolve_property_path = (
  types: MetaInterfaces,
  start_type: string,
  path: SchemaPath<any, any>
): string[] | undefined => {
  if (typeof path === "string") {
    return resolve_property_path(types, start_type, [path]);
  }
  const terms = path.map((part) => resolve_property(types, start_type, part));
  if (terms.every(defined))
    return terms.map((_) =>
      _.converse_property ? `^<${_.converse_property}>` : `<${_.term}>`
    );
};

/**
 * Given a list of schema property paths, return a list of corresponding SPARQL
 * property paths, where the properties could all be resolved to terms in the
 * given schema.  Note that the properties are not required to be relationships.
 */
export const get_include_paths = (
  types: MetaInterfaces,
  start_type: string,
  paths: readonly SchemaPath<any, any>[]
): string[] => {
  return paths
    .map((path) => resolve_property_path(types, start_type, path))
    .filter(defined)
    .map((path) => path.join("/"));
};

/**
 * Convert an object representing binding clauses to zero or more SPARQL BIND
 * expressions.  Keys of input object are variable names and values are assumed
 * to already be SPARQL expressions.
 */
export const to_sparql_bindings = (variables: Record<string, string>) =>
  Object.entries(variables)
    .map(([name, expr]) => `BIND (${expr} as ?${name})`)
    .join("\n");

const not_scope = (pattern: string, not: boolean) =>
  not && pattern ? `filter not exists {\n${pattern}\n}\n` : pattern;

const construct_filter_block = <S extends MetaInterfaces, T extends keyof S>(
  object_var: string,
  types: S,
  parent_type: T,
  components: string[],
  objects: any[]
): string => {
  const triples: string[] = [];
  const preamble_triples: string[] = [];
  let component = components.shift();

  if (component) {
    const [type, term] = component.includes(":")
      ? component.split(":")
      : // TS: keyof includes symbol
        [parent_type as string, component];

    const spec = types[type];
    if (!spec) throw new Error(`No metadata for type: ${type}`);

    const term_spec = spec.properties?.[term];
    if (!term_spec) throw new Error(`No metadata for property: ${component}`);

    // Recursively add patterns.
    if (components.length) {
      // Replace object with related.
      let new_object_var = `?${term}_ref`;

      preamble_triples.push(
        term_spec.converse_property
          ? `${new_object_var} <${term_spec.converse_property}> ${object_var}`
          : `${object_var} <${term_spec.term}> ${new_object_var}`
      );

      preamble_triples.push(
        construct_filter_block(new_object_var, types, type, components, objects)
      );
    } else {
      // Relationships
      if (Array.isArray(objects) && term_spec.relationship) {
        for (const o of objects) {
          triples.push(
            term_spec.converse_property
              ? `<${o}> <${term_spec.converse_property}> ${object_var}`
              : `${object_var} <${term_spec.term}> <${o}>`
          );
        }
      }

      // Booleans.
      if (term_spec.type === "boolean") {
        for (const o of objects) {
          triples.push(
            `${object_var} <${term_spec.term}> ${
              ["true", "1"].includes(o) ? "true" : "false"
            }`
          );
        }
      }

      // Numbers.
      if (term_spec.type === "number") {
        for (const o of objects) {
          triples.push(`${object_var} <${term_spec.term}> ${Number(o)}`);
        }
      }

      // Strings.
      if (term_spec.type === "string") {
        let i = 0;
        for (const o of objects) {
          const s = `?${term}_text_search_${i}`;
          triples.push(`
            ${object_var} <${term_spec.term}> ${s} .
            filter (regex(str(${s}), "${o}", "i"))
          `);
          i++;
        }
      }
    }
  }

  return (
    preamble_triples.join(". \n") +
    triples.map((t) => `{ ${t} }`).join("\nunion\n")
  );
};

export const construct_filter_blocks = <
  S extends MetaInterfaces,
  T extends keyof S
>(
  object_var: string,
  types: S,
  parent_type: T,
  filter: { [path: string]: string | string[] }
): string[] => {
  const filter_blocks: string[] = [];

  for (let pattern of Object.keys(filter)) {
    const not = pattern.charAt(0) === "!";

    const path = not ? pattern.substr(1) : pattern;
    const components = path.split(".");
    const objects = ensure_array(filter[pattern]);

    const patterns = construct_filter_block(
      object_var,
      types,
      parent_type,
      components,
      objects
    );

    filter_blocks.push(not_scope(patterns, not));
  }

  return filter_blocks;
};

export const construct_order_by = (
  object_var: string,
  spec: MetaInterface,
  order_by: readonly { property: string; descending?: boolean }[]
) => {
  const ordering = order_by.filter(({ property }) => spec.properties[property]);

  const patterns: string[] = [];
  const comparators: string[] = [];

  for (const by of ordering) {
    const prop_spec = spec.properties[by.property];

    if (prop_spec) {
      const variable = `?${by.property}`;
      const order = by.descending ? "desc" : "asc";

      patterns.push(`${object_var} <${prop_spec.term}> ${variable}`);
      comparators.push(`${order}(${variable})`);
    }
  }

  return {
    patterns,
    comparators,
  };
};

export const construct_permissions_block = <
  S extends MetaInterfaces,
  T extends keyof S,
  C extends object,
  P extends SchemaPermissions<S, C>
>(
  thing_var: string,
  types: S,
  type: T,
  context: C,
  permissions: P,
  method: keyof RecordPermissions<C>
): string => {
  const permission = permissions?.types[type];
  const method_permission = permission?.[method];

  if (permission && method_permission) {
    if (method === "read") {
      const bindings = permissions.bindings(context);
      const binding_clauses = to_sparql_bindings(bindings);
      const where_clauses = method_permission.where(thing_var, context);

      return `{
        ${binding_clauses}
        ${where_clauses}
      }`;
    }

    // TODO: Extend for other methods...
  }

  return "";
};

export const construct_related_permissions_block = <
  S extends MetaInterfaces,
  C
>(
  thing_var: string,
  types: S,
  permissions: SchemaPermissions<S, C>,
  context: C
): string => {
  const blocks: string[] = [];

  // A branch for each type is required since included values'
  // type cannot be explictly known at execution time.
  for (const [type, spec] of Object.entries(types)) {
    if (permissions.types[type]?.read) {
      const type_permissions = construct_permissions_block(
        thing_var,
        types,
        type,
        context ?? {},
        permissions,
        "read"
      );

      // Build branchs that will short-circuit if type pattern does not match.
      blocks.push(`
      {
        ${thing_var} a <${spec["@type"]}>
        ${type_permissions}
      }
    `);
    }
  }

  return blocks.join("\nunion\n");
};
