import type {
  MetaInterfaces,
  InterfaceFrom,
  EntityFrom,
} from "@swymbase/metatype-core";
import { RecordObservers, RecordObserverEvents, RecordObserver } from "../api";

export const handle_observer_event = async <
  S extends MetaInterfaces,
  T extends keyof S,
  R =
    | InterfaceFrom<S[T]>
    | EntityFrom<S[T]>
    | InterfaceFrom<S[T]>[]
    | EntityFrom<S[T]>[],
  C = any,
  O = RecordObservers<S, C>
>(
  event: RecordObserverEvents,
  types: S,
  type: T,
  records: R,
  record_observers: O,
  context?: C
): Promise<R> => {
  // TS: `as any` unifies call signatures
  const observers: RecordObserver<S[typeof type], C>[] = [
    record_observers["*"],
    record_observers[type as any],
  ];

  const spec = types[type];
  let processed = records;

  for (const obverser of observers) {
    const obverser_event = obverser?.[event];

    if (obverser_event) {
      if (Array.isArray(processed)) {
        // @ts-expect-error: TS cannot mix InterfaceFrom and EntityFrom.
        processed = await Promise.all(
          processed.map((rec) => obverser_event(rec, context, spec))
        );
      } else {
        // @ts-expect-error: TS cannot mix InterfaceFrom and EntityFrom.
        processed = await obverser_event(processed, context, spec);
      }
    }
  }

  return processed;
};
