import { send } from "@swymbase/sparql-protocol-client";
import { create_new_named_graph } from "./create_new_named_graph";

/**
 * Call the given function with the IRI of a graph that will be dropped
 * afterwards.
 */
export const with_temp_named_graph = async (
  endpoint: string,
  operation: (graph_iri: string) => Promise<void>
): Promise<void> => {
  let graph_iri: string | undefined = undefined;
  try {
    const result = await create_new_named_graph(endpoint);

    if (typeof result !== "string") {
      console.log("Create graph result", result);
      throw new Error(`Create graph failed: ${result.error.code}`);
    }

    graph_iri = result;
    await operation(graph_iri);
  } finally {
    if (graph_iri !== undefined) {
      const drop = `DROP GRAPH <${graph_iri}>`;
      const drop_result = await send({ endpoint, operation: { update: drop } });
      if (!drop_result.success) {
        throw new Error(
          `Cleanup failed for ${graph_iri}: ${drop_result.error}`
        );
      }
    }
  }
};
