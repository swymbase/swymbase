import { send } from "@swymbase/sparql-protocol-client";
import { get_unused_iri } from "../../../src/get_unused_iri";

const DEFAULT_PREFIX = "http://example.com/temp_resource/";

interface Args {
  endpoint: string;
  // Not sure every call should have to provide this
  prefix?: string;
}

/**
 * Call the given function with the IRI of an unused resource that will be
 * deleted afterwards.
 *
 * Only available for the default graph.
 */
export const with_temp_resource = async (
  { endpoint, prefix }: Args,
  operation: (graph_iri: string) => Promise<void>
): Promise<void> => {
  let resource_iri: string | undefined = undefined;
  try {
    const response = await get_unused_iri(endpoint, prefix || DEFAULT_PREFIX);
    if (!response.success) {
      throw new Error(`Get new ID failed: ${response.error.code}`);
    }
    const resource_iri = response.result;
    await operation(resource_iri);
  } finally {
    if (resource_iri !== undefined) {
      const update = `DELETE WHERE { <${resource_iri}> ?p ?o }`;
      const delete_result = await send({ endpoint, operation: { update } });
      if (!delete_result.success) {
        throw new Error(
          `Cleanup failed for ${resource_iri}: ${delete_result.error}`
        );
      }
    }
  }
};
