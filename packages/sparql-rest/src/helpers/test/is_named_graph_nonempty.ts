import { formatted } from "@swymbase/sparql-protocol-client";

/**
 * Tell whether a given named graph contains any triples at the given SPARQL
 * endpoint.
 */
export const is_named_graph_nonempty = async (
  endpoint: string,
  graph_iri: string
): ReturnType<typeof formatted> =>
  formatted("boolean", {
    endpoint,
    operation: { query: `ASK WHERE { GRAPH <${graph_iri}> { ?s ?p ?o } }` },
  });
