import { send } from "@swymbase/sparql-protocol-client";
import { is_named_graph_nonempty } from "./is_named_graph_nonempty";

const TEST_GRAPH_NAMESPACE = "https://example.com/create_new_named_graph/";
const DUMMY_PREDICATE = `${TEST_GRAPH_NAMESPACE}exists`;

/**
 * Return the IRI of a new named graph that did not previously exist in the
 * dataset at the given SPARQL endpoint.  The resulting graph will not be empty
 * (see following note), but it will *have been* empty prior to the call.
 *
 * The objective here is to “block” a named graph against use by others,
 * assuming that a standard method is used to determine whether a graph exists.
 * Stores are not required to record the existence of empty graphs (according to
 * [https://www.w3.org/TR/sparql11-update/#create](the SPARQL 1.1 Update spec)),
 * and as such there is no built-in check for whether a graph exists
 * independently of any content.  This method therefore writes a “dummy” triple
 * to the store (rather than bothering with a `CREATE GRAPH` statement.)
 */
export const create_new_named_graph = async (
  endpoint: string
): Promise<
  | string
  | { readonly error: { readonly code: string; readonly context: object } }
> => {
  let i = 10;
  while (i--) {
    const graph_iri = `${TEST_GRAPH_NAMESPACE}${Math.round(
      Math.random() * 1e12
    )}`;

    const exists_response = await is_named_graph_nonempty(endpoint, graph_iri);

    if (!exists_response.context.success) {
      return {
        error: { code: "CheckNonEmptyFailed", context: exists_response },
      };
    }

    if (exists_response.value === false) {
      const update = `INSERT DATA { GRAPH <${graph_iri}> { <${graph_iri}> <${DUMMY_PREDICATE}> true } }`;

      const insert_result = await send({ endpoint, operation: { update } });

      if (!insert_result.success) {
        return {
          error: { code: "InsertMarkerFailed", context: insert_result },
        };
      }

      return graph_iri;
    }
  }
  throw new Error(`Could not create new named graph after several attempts`);
};
