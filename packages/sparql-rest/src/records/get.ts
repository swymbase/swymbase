import type { EntityFrom, MetaInterfaces } from "@swymbase/metatype-core";
import { formatted, SparqlResponse } from "@swymbase/sparql-protocol-client";
import type { KBInterfaceOptions, KBReadOptions } from "../api";
import { CONVERSE_TERM } from "../converse_triples";
import { handle_observer_event } from "../helpers/handle-observer-event";
import { get_include_paths } from "../helpers/query-helpers";
import type { RecordFetchOptions } from "../interface";
import { maybe_contextualize, serialize } from "../simple";
import { TOP_GRAPH_IRI } from "./basis";
import { fieldset_helpers } from "./internal/fieldset-helpers";
import { marshal_out_records } from "./marshal-out-records";

/**
 * Return a conformant record representing the identified resource, if possible.
 */
export async function get_record<S extends MetaInterfaces, T extends keyof S>(
  types: S,
  type: T,
  id: string,
  endpoint: string,
  options?: KBReadOptions & KBInterfaceOptions<S> & RecordFetchOptions<S, T>
): Promise<{
  readonly context: SparqlResponse;
  readonly value: EntityFrom<S[T]> | undefined;
  readonly included_values: EntityFrom<any>[] | undefined;
}> {
  const spec = types[type];
  if (!spec) throw new Error(`No metadata for type: ${type}`);

  const include_paths = get_include_paths(
    types,
    type as string,
    options?.include_related ?? []
  );

  const vars = `
    ?thing ?p ?o
    ?converse ?referer
    ?included_thing ?rp ?ro
    ?included_type
`;

  const fieldset = fieldset_helpers(types, options?.fields);
  const main_type_iri = spec["@type"];

  const where = `
    bind (<${id}> as ?thing)
    {
      {
        ?thing ?p ?o 
        ${fieldset.filter_type(main_type_iri, "?p")}
      }
      union {
        ?referer ?reversible ?thing .
		    ?converse <${CONVERSE_TERM}> ?reversible .
        ${fieldset.filter_type(main_type_iri, "?converse")}
      }
    }
    ${
      include_paths.length === 0
        ? ""
        : `union {
      {
        {
          ?thing ${include_paths.join("|")} ?included_thing .
          ?included_thing ?rp ?ro .
          ?included_thing a ?included_type .
          ${fieldset.filter("?included_type", "?rp")}
        }
      }
    }`
    }
  `;

  const query = serialize(
    maybe_contextualize({ type: "select", vars, where }, options?.named_graph)
  );

  const { context, value: document } = await formatted("select_results", {
    endpoint:
      (!options?.no_read_endpoint && options?.read_endpoint) || endpoint,
    operation: { query },
  });

  const bindings = document?.results?.bindings;
  if (bindings) {
    const grouped_graphs = marshal_out_records(types, types[type], bindings);

    // Retreived observer event.
    if (options?.observers) {
      for (const key in grouped_graphs) {
        const group_type =
          key === TOP_GRAPH_IRI
            ? type
            : Object.keys(types).find(t => types[t]["@type"] === key);

        if (group_type) {
          grouped_graphs[key] = await handle_observer_event(
            "retrieved",
            types,
            group_type,
            grouped_graphs[key],
            options.observers
          );
        }
      }
    }

    const { [TOP_GRAPH_IRI]: top, ...included } = grouped_graphs;

    const value = top?.[0];
    const included_values = Object.values(included).flat();

    fieldset.prune(value);
    included_values.forEach(fieldset.prune);

    return { context, value, included_values };
  }
  return { context, value: undefined, included_values: undefined };
}
