// Functions for mapping records into RDF according to schema.
//
// These functions are exported from package for testing only.
import { rdf } from "../terms";
import { serialize_string, serialize_iri } from "@swymbase/sparql-ast";
import type { MetaInterface } from "@swymbase/metatype-core";
import type { RDFTerm } from "@swymbase/sparql-protocol-client";
import { get_marshal_in } from "./marshaling/in/get-marshal-in";

const RDF_TYPE = rdf.type;

/**
 * Map the given record to the predicate-object pairs imputed by the record
 * against the given type.
 *
 * Note that facts arising from the assertion of a record will only apply to a
 * single subject.
 */
export const record_to_predicate_objects = (
  type_spec: MetaInterface,
  record: object
): readonly (readonly [predicate: RDFTerm, object: RDFTerm])[] => {
  const tuples: [RDFTerm, RDFTerm][] = [];

  if (type_spec["@type"]) {
    tuples.push([
      { type: "uri", value: RDF_TYPE },
      { type: "uri", value: type_spec["@type"] },
    ]);
  }

  for (const [key, prop_spec] of Object.entries(type_spec.properties)) {
    // TS: Prevent excessively deep type instantiation below, and anyway reflect
    // the fact that we don't want to make assumptions about incoming values.
    // Unlike `unknown`, this signature supports some CFA.
    const value = record[key] as null | undefined | Object;

    // Don't include in record if no value exists
    if (value == null) continue;

    // Processing a single item should work exactly the same as processing the
    // same value for a single-valued (but otherwise equivalent) property.
    //
    // By getting the marshaling function before iterating, we avoid having to
    // test attributes of the spec on each iteration.

    // Not that this will throw on an unrecognized schema even if the record
    // contains no value for the property.
    const marshal_to_term = get_marshal_in(prop_spec);

    const marshal = (value: any) => {
      const object = marshal_to_term(value, prop_spec);
      if (object !== undefined) {
        tuples.push([predicate, object]);
      }
      // Silently ignore values that couldn't be marshaled
    };

    // Note that this object may be reused in multiple output tuples.
    // We assume that the returned values will not be mutated.
    const predicate: RDFTerm = { type: "uri", value: prop_spec.term };

    if (prop_spec.multivalued) {
      if (!Array.isArray(value)) {
        // TODO: add this to errors output
        // throw new Error(`Expected array value for ${key}`);
        continue;
      }
      for (const item of value) {
        marshal(item);
      }
    } else {
      marshal(value);
    }
  }

  return tuples;
};

const serialize_rdf_term = (term: RDFTerm) => {
  if (term.type === "uri") {
    return serialize_iri(term.value);
  }

  if (term.type === "bnode") {
    return `_:${term.value}`;
  }

  if (term.type === "literal") {
    const language = term["xml:lang"];
    const quoted = serialize_string(term.value);
    if (language) {
      return `${quoted}@${language}`;
    }
    const { datatype } = term;
    if (datatype) {
      return `${quoted}^^<${datatype}>`;
    }
    return quoted;
  }
};

/**
 * Given a record and a corresponding schema, return equivalent data in a
 * SPARQL/N-Quads compatible format.
 */
export const marshal_in_record = (
  type_spec: MetaInterface,
  record: object,
  subject: RDFTerm // assume this is a named node or a URI.
): string => {
  return record_to_predicate_objects(type_spec, record)
    .map(([predicate, object]) => {
      return (
        [subject, predicate, object].map(serialize_rdf_term).join(" ") + " ."
      );
    })
    .join("\n");
};
