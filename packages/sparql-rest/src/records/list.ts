import type { EntityFrom, MetaInterfaces } from "@swymbase/metatype-core";
import { serialize_group_graph_pattern_part } from "@swymbase/sparql-ast";
import type {
  SparqlResponse,
  SparqlSelectResults,
} from "@swymbase/sparql-protocol-client";
import { read_brtr, send } from "@swymbase/sparql-protocol-client";
import { getSegment } from "aws-xray-sdk-core";
import type { KBInterfaceOptions, KBReadOptions } from "../api";
import { CONVERSE_TERM } from "../converse_triples";
import { filter_to_sparql } from "../filter/filter-to-sparql";
import { map_terms } from "../filter/property-references";
import { handle_observer_event } from "../helpers/handle-observer-event";
import {
  construct_filter_blocks,
  construct_order_by,
  construct_permissions_block,
  get_include_paths,
} from "../helpers/query-helpers";
import type { ListRecordOptions } from "../interface";
import { maybe_contextualize, serialize } from "../simple";
import { TOP_GRAPH_IRI } from "./basis";
import { DEFAULT_LIMIT, DEFAULT_OFFSET } from "./internal-defaults";
import { fieldset_helpers } from "./internal/fieldset-helpers";
import { marshal_out_records } from "./marshal-out-records";

const NEPT = "http://aws.amazon.com/neptune/vocab/v01/";
const NEPTUNE_JOIN_ORDER_HINT = `<${NEPT}QueryHints#Group> <${NEPT}QueryHints#joinOrder> "Ordered" .`;

// TODO: Comparator is not meaningful in all cases (e.g. objects).  Need to
// ensure that we're providing a consistent ordering for all value spaces.
const order_value = (
  value: EntityFrom<any>[],
  order_by: readonly { property: string; descending?: boolean }[]
) =>
  [...value].sort((a, b) => {
    for (const by of order_by) {
      if (a[by.property] && b[by.property]) {
        // TS: see above
        const av = a[by.property] as any;
        const bv = b[by.property] as any;

        if (av < bv) return by.descending ? 1 : -1;
        if (av > bv) return by.descending ? -1 : 1;
      }
    }

    return 0;
  });

/**
 * Resolve to an array of conformant records of the selected type, if possible.
 */
export const list_records = async <
  S extends MetaInterfaces,
  T extends keyof S,
  C
>(
  types: S,
  type: T,
  endpoint: string,
  options?: KBReadOptions & KBInterfaceOptions<S> & ListRecordOptions<S, T>,
  context?: C
): Promise<{
  readonly context: SparqlResponse;
  readonly value: readonly EntityFrom<S[T]>[] | undefined;
  readonly included_values: readonly EntityFrom<any>[] | undefined;
  readonly total_result_count: number | undefined;
  readonly limit: number;
  readonly offset: number;
}> => {
  const segment = getSegment()?.addNewSubsegment("list_records");

  const spec = types[type];
  if (!spec) throw new Error(`No metadata for type: ${type}`);

  const type_iri = spec["@type"];
  if (!type_iri) throw new Error(`Metatype for type ${type} has no type IRI`);

  const limit = options?.limit ?? DEFAULT_LIMIT;
  const offset = options?.offset ?? DEFAULT_OFFSET;

  const permissions_block = (subject_var: string) =>
    options?.permissions && !options.ignore_permissions
      ? construct_permissions_block(
          `?${subject_var}`,
          types,
          type,
          context ?? {},
          options.permissions,
          "read"
        )
      : "";

  // TODO: Combine these two into a single block that can match both?
  const related_permissions_block = "";
  // options?.permissions && !options.ignore_permissions
  //   ? construct_related_permissions_block(
  //       "?included_thing",
  //       types,
  //       options.permissions,
  //       context
  //     )
  //   : "";

  const related_converse_permissions_block = "";
  // options?.permissions && !options.ignore_permissions
  //   ? construct_related_permissions_block(
  //       "?included_converse",
  //       types,
  //       options.permissions,
  //       context
  //     )
  //   : "";

  const filter_sparql = subject_var => {
    const filter = options?.filter;
    if (Array.isArray(filter)) {
      const resolved = filter.map(clause => map_terms(clause, types));
      const ast = filter_to_sparql(resolved, { subject_var });
      return ast.map(serialize_group_graph_pattern_part).join("\n");
    }

    return construct_filter_blocks(
      `?${subject_var}`,
      types,
      type,
      options?.filter_old ?? {}
    ).join("\n");
  };

  const order_by = construct_order_by("?thing", spec, options?.order_by ?? []);

  const count_sparql = `
    {
      select (count(distinct ?all_things) as ?total_result_count)
      where {
        ?all_things a <${type_iri}> .
        ${permissions_block("all_things")}
        ${filter_sparql("all_things")}
      }
    }
  `;

  const vars = `
    ?thing ?p ?o
    ?converse ?referer
    ?included_thing ?rp ?ro
    ?included_type
    ?total_result_count 
  `;

  const include_paths = get_include_paths(
    types,
    type as string,
    options?.include_related ?? []
  );

  const fieldset = fieldset_helpers(types, options?.fields);
  const main_type_iri = spec["@type"];

  // OPTIMIZATION: The Neptune-specific hint causes the top-level group to be
  // processed in the order written.  This is critical for ensuring that the
  // patterns for included resources are only evaluated after `?thing` is bound
  // to actual results, since they are fully open patterns and would otherwise
  // match everything in the graph.  Note that `Group` join order is specified
  // rather than `Query`.  This allows Neptune to optimize the subquery itself
  // normally, which is also critical.
  //
  // See https://docs.aws.amazon.com/neptune/latest/userguide/sparql-query-hints-joinOrder.html
  const is_neptune = options?.sparql_engine === "neptune";
  const neptune_join_order_hint = is_neptune ? NEPTUNE_JOIN_ORDER_HINT : "";
  const where = `
    ${neptune_join_order_hint}
    {
      select distinct ?thing
      where {
        ?thing a <${type_iri}>
        optional {
          ${order_by.patterns.join(" .\n")}
        }
        ${permissions_block("thing")}
        ${filter_sparql("thing")}
      }
      ${
        order_by.comparators.length
          ? `order by ${order_by.comparators.join(" ")}`
          : ""
      }
      limit ${limit}
      offset ${offset}
    }
    {
      { 
        ?thing ?p ?o 
        ${fieldset.filter_type(main_type_iri, "?p")}
      }
      union {
        ?referer ?reversible ?thing .
		    ?converse <${CONVERSE_TERM}> ?reversible .
        ${fieldset.filter_type(main_type_iri, "?converse")}
      }
    }
    ${
      include_paths.length === 0
        ? ""
        : `union {
      {
        ?thing ${include_paths.join("|")} ?included_thing .
        ?included_thing ?rp ?ro .
        ?included_thing a ?included_type .
        ${fieldset.filter("?included_type", "?rp")}
        {
          ${related_permissions_block}
        }
      }
    }`
    }
    ${options?.include_count ? count_sparql : ""}
  `;

  const query = serialize(
    maybe_contextualize({ type: "select", vars, where }, options?.named_graph)
  );

  const query_segment = segment?.addNewSubsegment("query");
  query_segment?.addMetadata("query", query);
  const response_context = await send({
    // Prefer binary format when available.
    accept: [
      "application/x-binary-rdf-results-table",
      "application/sparql-results+json",
    ],
    endpoint:
      (!options?.no_read_endpoint && options?.read_endpoint) || endpoint,
    operation: { query },
  });
  query_segment?.close();

  if (response_context.success === false) {
    return {
      context: response_context,
      value: undefined,
      included_values: undefined,
      total_result_count: undefined,
      limit,
      offset,
    };
  }
  let bindings: SparqlSelectResults["results"]["bindings"];
  const { response } = response_context;
  if (response.format === "brtr") {
    const read_brtr_segment = segment?.addNewSubsegment("read_brtr");
    const data = response.value;
    try {
      // TODO: Support lazy read.  This materializes the thing, which we don't
      // need.
      const parsed = read_brtr(data);
      bindings = parsed.results.bindings;
    } catch (error) {
      console.log("ReadBrtrFailed", data, error);
      throw new Error("ReadBrtrFailed");
    }
    read_brtr_segment?.close();
  } else if (response.format === "results") {
    if ("boolean" in response.value) {
      throw new Error("UnexpectedResultsFormat");
    }
    bindings = response.value.results.bindings;
  } else {
    console.log("UnsupportedResultsFormat", response_context.response);
    throw new Error("UnsupportedResultsFormat");
  }

  if (bindings) {
    const marshal_segment = segment?.addNewSubsegment("marshal_out");

    // Provide total result count when requested.  Note that the binding
    // including the total will also contain other data.
    let total_result_count: number | undefined = undefined;
    if (options?.include_count) {
      // If no valid binding is found but a count was requested, return zero as
      // the count.
      total_result_count = 0;
      for (const binding of bindings) {
        const term = binding["total_result_count"];
        // We expect there to be exactly one binding for the total, containing a
        // numeric (RDF term) value.  Stil, if we see a non-empty binding that
        // contains an invalid value, we go on to check the other bindings.
        if (term?.type === "literal") {
          const value = parseInt(term.value, 10);
          if (!isNaN(value)) {
            total_result_count = value;
            break;
          }
        }
      }
    }

    const grouped_graphs = marshal_out_records(types, types[type], bindings);
    marshal_segment?.close();

    // Retrieved observer event.
    if (options?.observers) {
      const observers_segment = segment?.addNewSubsegment("observers");
      for (const key in grouped_graphs) {
        const group_type =
          key === TOP_GRAPH_IRI
            ? type
            : Object.keys(types).find(t => types[t]["@type"] === key);

        if (group_type) {
          grouped_graphs[key] = await handle_observer_event(
            "retrieved",
            types,
            group_type,
            grouped_graphs[key],
            options.observers
          );
        }
      }
      observers_segment?.close();
    }

    const { [TOP_GRAPH_IRI]: top, ...included } = grouped_graphs;

    const value = top ?? [];
    const included_values = Object.values(included).flat();

    /** We need to manually order constructed results due to
     *  unreliable normalization */
    const sort_segment = segment?.addNewSubsegment("sort");
    const ordered_value = order_value(value, options?.order_by ?? []);
    sort_segment?.close();

    const prune_segment = segment?.addNewSubsegment("prune");
    value.forEach(fieldset.prune);
    included_values.forEach(fieldset.prune);
    prune_segment?.close();

    segment?.close();
    return {
      context: response_context,
      value: ordered_value,
      included_values,
      total_result_count,
      limit,
      offset,
    };
  }

  segment?.close();
  return {
    context: response_context,
    value: undefined,
    included_values: undefined,
    total_result_count: undefined,
    limit,
    offset,
  };
};
