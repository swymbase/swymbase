import type { MetaInterfaces, EntityFrom } from "@swymbase/metatype-core";
import { KBInterfaceOptions } from "../api";
import { write_record } from "./write";
import { delete_subject } from "../delete_subject";

/**
 * Request to completely replace a certain resource with a given representation.
 * Existing properties of the given record *type* are first deleted for the
 * identified resource.  Assumes that the given record has an `id`.
 *
 * See HTTP `PUT`
 * https://tools.ietf.org/html/draft-ietf-httpbis-p2-semantics-21#section-5.3.4
 */
export async function put_record<S extends MetaInterfaces, T extends keyof S>(
  types: S,
  type: T,
  record: EntityFrom<S[T]>,
  endpoint: string,
  options?: KBInterfaceOptions<S>
): ReturnType<typeof write_record> {
  const spec = types[type];
  if (!spec) throw new Error(`No metadata for type: ${type}`);

  const id = record.id;
  if (!id) throw new Error("Put requires record to have an ID");

  // Delete *all* properties
  const delete_response = await delete_subject(
    id,
    endpoint,
    options?.named_graph
  );
  if (!delete_response.success) return delete_response;
  return write_record(spec, record, endpoint, options);
}
