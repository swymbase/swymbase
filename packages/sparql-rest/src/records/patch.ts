import { SparqlResponse } from "@swymbase/sparql-protocol-client";
import type { MetaInterfaces, EntityFrom } from "@swymbase/metatype-core";
import { KBInterfaceOptions } from "../api";
import { delete_record } from "./delete";
import { write_record } from "./write";
import { get_record } from "./get";
import { handle_observer_event } from "../helpers/handle-observer-event";

/**
 * Update the properties of a resource based on those in the given record.
 * Existing properties of the given *record* are first deleted for the
 * identified resource.
 *
 * See HTTP `PATCH` https://tools.ietf.org/html/rfc5789
 */
export async function patch_record<
  S extends MetaInterfaces,
  T extends keyof S,
  C
>(
  types: S,
  type: T,
  record: EntityFrom<S[T]>,
  endpoint: string,
  options?: KBInterfaceOptions<S>,
  context?: C
): Promise<SparqlResponse> {
  const spec = types[type];
  if (!spec) throw new Error(`No metadata for type: ${type}`);

  const id = record.id;
  if (!id) throw new Error("Patch requires record to have an ID");

  let to_write = record;

  // Updating observer event.
  if (options?.observers) {
    to_write = await handle_observer_event(
      "updating",
      types,
      type,
      to_write,
      options.observers,
      context
    );
  }

  // First delete keys that are in this record (i.e. that are being replaced).
  const delete_result = await delete_record(
    types,
    type,
    id,
    endpoint,
    {
      keys: Object.keys(to_write),
      named_graph: options?.named_graph,
    },
    context
  );
  if (!delete_result.success) return delete_result;

  // Now write the new values
  const write_response = await write_record(spec, to_write, endpoint, options);

  if (!write_response.success) return write_response;

  // Updated observer event.
  if (options?.observers) {
    const get = await get_record(types, type, id, endpoint, {
      ...options,
      no_read_endpoint: true,
    });

    if (get.value) {
      await handle_observer_event(
        "updated",
        types,
        type,
        get.value,
        options.observers,
        context
      );
    }
  }

  return write_response;
}
