import type { MetaProperty } from "@swymbase/metatype-core";
import type { RDFTerm } from "@swymbase/sparql-protocol-client";

// Basically, anything except undefined.
// Using this type as a placeholder, but if I keep it, it should go with metatype record
type RecordValue = Object;

export interface MarshalToTerm {
  (value: RecordValue, spec: MetaProperty): RDFTerm | undefined;
}
