import { xsd } from "../../../terms";
import type { MarshalToTerm } from "./api";

const XSD_FLOAT = xsd.float;

export const marshal_in_number: MarshalToTerm = (value) => {
  if (typeof value !== "number") {
    // Unexpected type provided
    return undefined;
  }

  return { type: "literal", value: value.toString(), datatype: XSD_FLOAT };
};
