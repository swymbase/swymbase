import { xsd } from "../../../terms";
import type { MarshalToTerm } from "./api";

const XSD_STRING = xsd.string;

export const marshal_in_string: MarshalToTerm = (value) => {
  if (typeof value !== "string") {
    // Unexpected type provided
    return undefined;
  }

  return { type: "literal", value, datatype: XSD_STRING };
};
