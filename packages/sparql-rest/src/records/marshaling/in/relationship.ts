import type { MarshalToTerm } from "./api";

// TS: this value could have come from anywhere
export const marshal_in_relationship: MarshalToTerm = (value) => {
  const target = value?.["id"];
  if (typeof target !== "string") {
    // Invalid value.  Could also throw here.
    return undefined;
  }

  return { type: "uri", value: target };
};
