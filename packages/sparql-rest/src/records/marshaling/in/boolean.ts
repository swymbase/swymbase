import { xsd } from "../../../terms";
import type { MarshalToTerm } from "./api";

const XSD_BOOLEAN = xsd.boolean;

export const marshal_in_boolean: MarshalToTerm = (value) => {
  if (typeof value !== "boolean") {
    // Unexpected type provided
    return undefined;
  }

  return { type: "literal", value: value.toString(), datatype: XSD_BOOLEAN };
};
