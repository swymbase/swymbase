import type { MetaProperty } from "@swymbase/metatype-core";
import type { MarshalToTerm } from "./api";
import { marshal_in_boolean } from "./boolean";
import { marshal_in_number } from "./number";
import { marshal_in_relationship } from "./relationship";
import { marshal_in_string } from "./string";
import { marshal_in_value_spec } from "./value-spec";

// Return the appropriate marshaling-in function for on a property spec.
export const get_marshal_in = (spec: MetaProperty): MarshalToTerm => {
  if (spec.relationship) return marshal_in_relationship;
  if (spec.type === "string") return marshal_in_string;
  if (spec.type === "number") return marshal_in_number;
  if (spec.type === "boolean") return marshal_in_boolean;
  if (typeof spec.type === "object") return marshal_in_value_spec;
  throw new Error("Unsupported property spec");
};
