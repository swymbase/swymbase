import { rdf } from "../../../terms";
import type { MarshalToTerm } from "./api";

const RDF_JSON = rdf.JSON;

export const marshal_in_value_spec: MarshalToTerm = (value) => {
  // TODO: first conform the value to the spec
  // const { spec } = prop_spec.type;
  const json = JSON.stringify(value);

  return { type: "literal", value: json, datatype: RDF_JSON };
};
