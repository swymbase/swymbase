# Marshal-in functions

This directory contains functions and a dispatcher for mapping from values in a
record to terms in a graph (specifically object values).

The functions deal only with individual values. Multi-valued properties simply
marshal each of the values separately.
