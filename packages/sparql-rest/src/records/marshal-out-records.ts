// Functions for mapping the results of read operations into records.
import type {
  EntityFrom,
  MetaInterface,
  MetaInterfaces,
  MetaProperty,
} from "@swymbase/metatype-core";
import type { RDFTerm } from "@swymbase/sparql-protocol-client";
import { rdf, xsd } from "../terms";
import { TOP_GRAPH_IRI } from "./basis";

type RecordGroups = { [basis_iri: string]: EntityFrom<any>[] };
type RecordValue = { readonly id: string } | string | number | boolean | Object;
// See below.  Including `Object` because of structured values.
type InternalValue = symbol | string | number | boolean | Object;

const RDF_JSON = rdf.JSON;
const BOOLEAN = xsd.boolean;
const DECIMAL = xsd.decimal;
const FLOAT = xsd.float;
const DOUBLE = xsd.double;
const INTEGER = xsd.integer;
const STRING = xsd.string;

// There are other ones with specific ranges, but this suffices for interop with
// literals.
const NUMERIC_TYPES = new Set([DECIMAL, FLOAT, DOUBLE, INTEGER]);

const is_boolean = (term: RDFTerm): boolean =>
  term.type === "literal" && term.datatype === BOOLEAN;

const is_numeric = (term: RDFTerm): boolean =>
  // TS: datatype will be undefined if this is a language-tagged string, but ok.
  term.type === "literal" && NUMERIC_TYPES.has(term.datatype!);

const is_string = (term: RDFTerm): boolean =>
  term.type === "literal" && (!term.datatype || term.datatype === STRING);

const is_json = (term: RDFTerm): boolean =>
  term.type === "literal" && term.datatype === RDF_JSON;

const try_parse = (text: string): Object | undefined => {
  try {
    return JSON.parse(text);
  } catch {}
};

/**
 * These fields correspond to the variables used in the constructed `SELECT`
 * query used in `get_record`.  They are interpreted as forming the following
 * graph pattern:
 *
 *     ?thing ?p ?o
 *     ; ?converse ?referer
 *     ; <${BASIS}> <${TOP_GRAPH_IRI}> .
 *
 *     ?included_thing ?rp ?ro
 *     ; <${BASIS}> ?included_type .
 */
interface GetBindings {
  readonly thing?: RDFTerm;
  readonly p?: RDFTerm;
  readonly o?: RDFTerm;
  readonly converse?: RDFTerm;
  readonly referer?: RDFTerm;
  readonly included_thing?: RDFTerm;
  readonly rp?: RDFTerm;
  readonly ro?: RDFTerm;
  readonly included_type?: RDFTerm;
}

// Intermediate structure for mapping select bindings into records
type BindingMap = Map<
  // The resource's IRI
  string,
  {
    // The set of IRI's on the basis of which this resource is included.  In
    // practice, we only expect there to be one _type_ associated with each
    // record, but it is possible to be in both the main set (which uses a
    // well-known IRI) and an included set (by type).
    readonly bases: Set<string>;
    // A map from predicate IRI to all of its RDF values for this resource.
    readonly props: Map<string, RDFTerm[]>;
  }
>;

// Map bindings into the intermediate structure just described.
const map_from_bindings = (bindings: Iterable<GetBindings>): BindingMap => {
  const map: BindingMap = new Map();

  const assert = (basis: string, sub: string, pred: string, value: RDFTerm) => {
    let rec = map.get(sub);
    if (!rec) {
      rec = { bases: new Set(), props: new Map() };
      map.set(sub, rec);
    }
    rec.bases.add(basis);

    let values = rec.props.get(pred);
    if (!values) {
      values = [];
      rec.props.set(pred, values);
    }
    values.push(value);
  };

  for (const binding of bindings) {
    const { thing, included_thing } = binding;

    if (thing) {
      const { p, o, converse, referer } = binding;
      if (p && o) {
        assert(TOP_GRAPH_IRI, thing.value, p.value, o);
      }
      if (converse && referer) {
        assert(TOP_GRAPH_IRI, thing.value, converse.value, referer);
      }
    }

    if (included_thing) {
      const { rp, ro, included_type } = binding;
      if (rp && ro && included_type) {
        assert(included_type.value, included_thing.value, rp.value, ro);
      }
    }
  }

  return map;
};

// Part of strategy to quickly deduplicate multivalued properties.
const unsymbol = (value: InternalValue): RecordValue =>
  typeof value === "symbol" ? { id: Symbol.keyFor(value)! } : value;

// All record construction occurs here.
const record_from_props = (
  id: string,
  type_spec: MetaInterface,
  props: Map<string, RDFTerm[]>
): EntityFrom<any> => {
  const record = { id, type: type_spec["@type"] };

  // We're primarily interested in the record shape as defined by the schema.
  // We can construct a conformant record with no facts whatsoever.
  for (const [key, prop_spec] of Object.entries(type_spec.properties)) {
    // Hash of the distinct values collected for the property.
    const uniques = prop_spec.multivalued ? new Set() : undefined;

    // At this point there should be at least one value available
    for (const value of props.get(prop_spec.term) ?? []) {
      const conformant_value = marshal_out_value(value, prop_spec);

      // Ignore values that could not be interpreted according to the schema.
      if (conformant_value === undefined) {
        continue;
      }

      // All property value assignment occurs here
      if (prop_spec.multivalued) {
        uniques!.add(conformant_value);
      } else {
        record[key] = unsymbol(conformant_value);

        // UNDEFINED BEHAVIOR: If this is a single-valued property, ignore any
        // remaining values.  If multiple values are in fact stored for a
        // single-valued property, we take the first one with a conformant
        // value.  This is “undefined behavior” since the values can occur in
        // any order and may yield different results each time.
        break;
      }
    }

    // _Always_ use an array for multivalued properties.
    if (prop_spec.multivalued) {
      record[key] = Array.from(uniques!, unsymbol);
    }
  }

  // TODO: See note to `TypeFrom` definition.  This is not assignable when
  // `$error` type is in union.
  return record;
};

/**
 * Map an RDF term to an individual value based on a property spec.  Not
 * concerned with whether the property is multivalued.
 */
const marshal_out_value = (
  term: RDFTerm,
  prop_spec: MetaProperty
): InternalValue | undefined => {
  if (prop_spec.relationship) {
    // Symbol allows us to hash distinct values for all (currently supported) kinds.
    return term.type === "uri" ? Symbol.for(term.value) : undefined;
  }

  if (prop_spec.type === "string") {
    return is_string(term) ? term.value : undefined;
  }

  if (prop_spec.type === "boolean") {
    return is_boolean(term) ? term.value === "true" : undefined;
  }

  if (prop_spec.type === "number") {
    return is_numeric(term) ? parseFloat(term.value) : undefined;
  }

  if (prop_spec.type?.["spec"]) {
    // All spec-ed values are expected to have JSON datatype
    // (even though some actual values may be primitives).
    if (is_json(term)) {
      // We would prefer a Record here (with value semantics).
      // For the moment, we're assuming that we won't get duplicates.
      return try_parse(term.value);
    }
    return undefined;
  }

  // Invalid prop spec: ignore
};

/**
 * Given a set of `GetBindings`, return a set of records grouped by the IRI of
 * their type.  The type of a record is determined based on provided bindings.
 * All records are normalized to the indicated type based on the given schema.
 */
export const marshal_out_records = (
  types: MetaInterfaces,
  top_type_spec: MetaInterface,
  bindings: Iterable<GetBindings>
): RecordGroups => {
  const map = map_from_bindings(bindings);

  const groups: RecordGroups = Object.create(null);

  // TODO: memoize this.  Schema never changes at runtime.
  const type_map = new Map(
    Object.values(types).map((spec) => [spec["@type"], spec])
  );

  for (const [id, { bases, props }] of map) {
    for (const basis of bases) {
      const group = (groups[basis] ??= []);

      let type_spec = top_type_spec;

      if (basis !== TOP_GRAPH_IRI) {
        type_spec = type_map.get(basis)!;

        // If we match data that doesn't have a corresponding type definition in
        // the schema for any reason, ignore it.
        if (!type_spec) {
          continue;
        }
      }

      group.push(record_from_props(id, type_spec, props));
    }
  }

  return groups;
};
