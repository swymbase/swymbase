import type { MetaInterface } from "@swymbase/metatype-core";
import { send, SparqlResponse } from "@swymbase/sparql-protocol-client";
import { KBInterfaceOptions } from "../api";
import { maybe_contextualize, serialize } from "../simple";
import { marshal_in_record } from "./marshal-in-records";

// Write the given properties without respect to any pre-existing values.
// Assumes any relevant deletion has already been done.  Assumes that the record
// has an `id`.  Else I believe the resource will be written with a blank node.
export async function write_record(
  spec: MetaInterface,
  record: { id: string } & object,
  endpoint: string,
  options?: KBInterfaceOptions<any>
): Promise<SparqlResponse> {
  const data = marshal_in_record(spec, record, {
    type: "uri",
    value: record.id,
  });
  const update = serialize(
    maybe_contextualize({ type: "insert_data", data }, options?.named_graph)
  );
  return send({ endpoint, operation: { update } });
}
