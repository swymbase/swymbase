import {
  SparqlResponse,
  FailureResponse,
} from "@swymbase/sparql-protocol-client";
import type { MetaInterfaces, InterfaceFrom } from "@swymbase/metatype-core";
import type { KBInterfaceOptions } from "../api";
import { get_unused_iri } from "../get_unused_iri";
import { write_record } from "./write";
import { handle_observer_event } from "../helpers/handle-observer-event";

/**
 * Requests to store the given record as the given type and return its
 * identifier.  Each successful call results in the creation of a new entity.
 *
 * See HTTP `POST`
 * https://tools.ietf.org/html/draft-ietf-httpbis-p2-semantics-21#section-5.3.3
 */
export async function post_record<
  S extends MetaInterfaces,
  T extends keyof S,
  C
>(
  types: S,
  type: T,
  record: InterfaceFrom<S[T]>,
  endpoint: string,
  options?: KBInterfaceOptions<S>,
  context?: C
): Promise<({ new_id: string } & SparqlResponse) | FailureResponse> {
  const spec = types[type];
  if (!spec) throw new Error(`No metadata for type '${type}'`);

  if (!spec.instance_namespace)
    throw new Error(`No instance namespace for type '${type}'`);

  let to_write = record;

  // Creating observer event.
  if (options?.observers) {
    to_write = await handle_observer_event(
      "creating",
      types,
      type,
      to_write,
      options.observers,
      context
    );
  }

  const new_id_result = await get_unused_iri(
    options?.read_endpoint || endpoint,
    spec.instance_namespace,
    options?.named_graph
  );

  if (!new_id_result.success) {
    throw new Error(`Getting new ID failed: ${new_id_result.error.code}`);
  }
  const new_id = new_id_result.result;

  const write_response = await write_record(
    spec,
    { id: new_id, ...to_write },
    endpoint,
    options
  );
  if (!write_response.success) return write_response;

  // Created observer event.
  if (options?.observers) {
    await handle_observer_event(
      "created",
      types,
      type,
      { id: new_id, ...to_write },
      options.observers,
      context
    );
  }

  return { ...write_response, new_id };
}
