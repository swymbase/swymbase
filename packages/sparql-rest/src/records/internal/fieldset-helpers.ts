import type { MetaInterfaces } from "@swymbase/metatype-core";
import { serialize_iri } from "@swymbase/sparql-ast";
import type { Fieldsets } from "../../interface";
import { rdf } from "../../terms";

const iri_list = (iris: Iterable<string>) =>
  `(${Array.from(iris, serialize_iri).join(", ")})`;

/**
 * Return a set of bound helper functions for supporting fieldset selections in
 * record query operations.
 *
 * The interface provides three methods: `filter`, `filter_type`, and `prune`.
 *
 * The `filter*` methods are used to generate SPARQL clauses that can be
 * integrated into a query to restrict the output bindings to the desired
 * fields.  This is the most effective way to reduce the workload, *before*
 * unwanted facts are matched in the first place.
 *
 * The `prune` method removes unwanted fields from records after they've been
 * constructed.  The marshaling step and the observers may have added fields
 * that were not specified in the expected field list.  We could provide the
 * field list to those operations so that they may avoid any unneeded work in
 * the first place, but at least in the case of observers, we wouldn't count on
 * their doing so, and would thus have to do this anyway.
 *
 * Note that in the generated SPARQL, `rdf:type` is always included as a
 * recognized predicate.  Otherwise, some valid records would produce no
 * bindings.
 */
export const fieldset_helpers = <S extends MetaInterfaces>(
  types: S,
  fields: Fieldsets<S> | undefined
) => {
  // Map from type IRI to fieldset information.  Maps type and property aliases
  // to IRI's, throwing out unrecognized terms.
  const projections = new Map(
    Object.entries(fields || {})
      .filter(([alias]) => types[alias])
      .map(([alias, props]) => {
        const spec = types[alias];
        const predicates = (props || [])
          .map((prop) => spec.properties[prop]?.term)
          .filter((x) => !!x);
        return [spec["@type"], { predicates, fields: new Set(props) }];
      })
  );

  /**
   * Construct a graph pattern filter that restricts matches to selected fields
   * for the given type, if a fieldset was specified.
   */
  const filter_type = (type_iri: string, predicate_var: string): string => {
    const it = projections.get(type_iri);
    if (!it) return "";
    return `filter (${predicate_var} in ${iri_list([
      rdf.type,
      ...it.predicates,
    ])})`;
  };

  /**
   * Construct a graph pattern filter that restricts matches to selected fields
   * where applicable.
   *
   * A filter clause is used to limit the predicates to any fieldset that was
   * specified for the matched type.  Otherwise, the predicate is unrestricted
   * (and thus could include values not belonging to the schema).
   */
  const filter = (type_var: string, predicate_var: string): string =>
    projections.size === 0
      ? ""
      : `filter (
            ${predicate_var} = ${serialize_iri(rdf.type)}
            || ${type_var} not in ${iri_list(projections.keys())}
            || ${Array.from(
              projections,
              ([type_iri, _]) =>
                `(${type_var} = ${serialize_iri(type_iri)}
                    && ${predicate_var} in ${iri_list(_.predicates)})`
            ).join(" || ")}
          )`;

  /**
   * **Mutate** the given record to remove unwanted fields.
   */
  const prune = (record: any) => {
    if (record) {
      const fieldset = projections.get(record.type)?.fields;
      if (fieldset) {
        for (const key of Object.keys(record)) {
          if (key !== "id" && key !== "type" && !fieldset.has(key)) {
            // Yes, those who mutate get what they deserve
            delete record[key];
          }
        }
      }
    }
  };

  return { filter_type, filter, prune };
};
