import type { MetaInterfaces } from "@swymbase/metatype-core";
import { SparqlResponse, formatted } from "@swymbase/sparql-protocol-client";
import { KBInterfaceOptions } from "../api";
import { serialize, maybe_contextualize } from "../simple";

export async function record_exists<
  S extends MetaInterfaces,
  T extends keyof S
>(
  types: S,
  type: T,
  id: string,
  endpoint: string,
  options?: KBInterfaceOptions<S>
): Promise<{
  readonly value: boolean | undefined;
  readonly context: SparqlResponse;
}> {
  const spec = types[type];
  if (!spec) throw new Error(`No metadata for type: ${type}`);

  const type_iri = spec["@type"];
  if (!type_iri) throw new Error(`Metatype for type ${type} has no type IRI`);

  const statement = maybe_contextualize(
    { type: "ask", where: `<${id}> ?p ?o ; a <${type_iri}>` },
    options?.named_graph
  );
  const query = serialize(statement);
  return formatted("boolean", {
    endpoint: options?.read_endpoint || endpoint,
    operation: { query },
  });
}
