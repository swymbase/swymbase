import type { MetaInterfaces } from "@swymbase/metatype-core";
import { KBInterfaceOptions } from "../api";
import { delete_properties } from "../delete_properties";
import { handle_observer_event } from "../helpers/index";
import { get_record } from "./get";

// Unlike `delete_subject`, this deletes only properties defined by the schema
// (and `rdf:type`).
const RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
export async function delete_record<
  S extends MetaInterfaces,
  T extends keyof S,
  C
>(
  types: S,
  type: T,
  id: string,
  endpoint: string,
  // You can limit deletion to the given keys
  options?: KBInterfaceOptions<S> & { keys?: readonly string[] },
  context?: C
): ReturnType<typeof delete_properties> {
  const spec = types[type];
  if (!spec) throw new Error(`No metadata for type: ${type}`);
  const keys = options?.keys;
  const properties_to_delete = [
    RDF_TYPE,
    ...Object.entries(spec.properties)
      .filter(([key]) => !keys || keys.includes(key))
      .map(([, prop]) => prop.term)
      // HACK? see metatypes
      .filter((term) => !!term && term !== "@id"),
  ];

  const get = options?.observers
    ? await get_record(types, type, id, endpoint, options)
    : undefined;

  // Deleting observer event.
  if (options?.observers && get?.value) {
    await handle_observer_event(
      "deleting",
      types,
      type,
      get.value,
      options.observers,
      context
    );
  }

  const result = await delete_properties(
    id,
    properties_to_delete,
    endpoint,
    options?.named_graph
  );

  // Deleted observer event.
  if (options?.observers && get?.value) {
    await handle_observer_event(
      "deleted",
      types,
      type,
      get.value,
      options.observers,
      context
    );
  }

  return result;
}
