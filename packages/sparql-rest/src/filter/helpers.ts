// ALSO in sparql-ast
export function assert_unreachable(_: never, name?: string): never {
  throw new Error(
    name
      ? `Unrecognized ${name}: ${JSON.stringify(_)}.`
      : `Assert failed: unreachable code reached.`
  );
}
