import type * as SPARQL from "@swymbase/sparql-ast";
import type * as DSL from "../../src/filter/api";
import { assert_unreachable } from "./helpers";

/**
 * Stateful (!), mutable (!) context used throughout transformation of filter
 * expressions into SPARQL.  `mint` is stateful and mutable because it counts
 * used variables.
 */
export interface FilterToSparqlContext {
  /** Name of the variable to substitute into `this` expressions. */
  readonly subject_var: string;
  /** Factory to return a new variable. */
  readonly mint: () => SPARQL.VariableTerm;
}

/**
 * Simple visitor for mapping terminal strings to terminal form `["id", id]`
 */
const map_property_path = (path: DSL.PropertyPath): SPARQL.PropertyPath => {
  if (typeof path === "string") return ["id", path];
  const [op, ...items] = path;
  // @ts-expect-error: union assignability
  return [op, ...items.map(map_property_path)];
};

/**
 * Create a monotonically-increasing variable name generator, starting with
 * letter `a`.  This scheme is currently assumed in tests.
 */
const sparql_var_mint = (): (() => SPARQL.VariableTerm) => {
  let id = 0;
  return () => ["var", String.fromCharCode(97 + id++)];
};

/**
 * Implements the “`this` expressions” transformation described in the README.
 *
 * Given an arbitrary value expression (from the DSL), returns a variable
 * binding and pattern expression (if such is needed); else just returns the
 * SPARQL expression that would go here
 */
const resolve_this_expr = (
  expr: DSL.ValueExpression,
  { subject_var, mint }: FilterToSparqlContext
): { sub: SPARQL.ValueExpression; pattern?: SPARQL.TriplePattern } => {
  // Literals and ["id"] nodes are the same in both grammars.
  if (typeof expr !== "object" || expr[0] === "id") return { sub: expr };

  // This-expression substitution.
  if (expr[0] === "this") {
    const [, path] = expr;
    // A “this reference” is just the variable
    if (!path) return { sub: ["var", subject_var] };

    // A “this path” requires a pattern
    const v = mint();
    return {
      sub: v,
      pattern: ["pattern", ["var", subject_var], map_property_path(path), v],
    };
  }

  assert_unreachable(expr, "value expression");
};

/**
 * Rewrite a filter description as a SPARQL Group graph pattern.
 *
 * @param context.subject_var - The name of the variable binding to assume for
 * the contextual subject.
 *
 * @param context.mint - A factory for generating unused SPARQL variables.
 */
export const filter_to_sparql = (
  filter: DSL.Filter,
  options?: Partial<FilterToSparqlContext>
): readonly SPARQL.GroupGraphPatternPart[] => {
  const context: FilterToSparqlContext = {
    subject_var: options?.subject_var ?? "this",
    mint: options?.mint ?? sparql_var_mint(),
  };

  return filter.map((clause) => filter_clause_to_sparql(clause, context));
};

export const negatable_to_sparql = (
  expr: DSL.NegatableExpression,
  context: FilterToSparqlContext
): Iterable<SPARQL.BasicGraphPatternPart> => {
  if (expr[0] === "not") {
    const [, sub] = expr;
    return [
      ["not-exists", ["basic", ...boolean_expression_to_sparql(sub, context)]],
    ];
  }

  return boolean_expression_to_sparql(expr, context);
};

export const clause_or_conjunction_to_sparql = (
  expr: DSL.ClauseOrConjunction,
  context: FilterToSparqlContext
): SPARQL.GroupGraphPatternPart => {
  if (expr[0] === "and") {
    const [, ...clauses] = expr;
    return [
      "group",
      ...clauses.map(
        (clause) =>
          ["basic", ...boolean_clause_to_sparql(clause, context)] as const
      ),
    ];
  }

  return ["basic", ...boolean_clause_to_sparql(expr, context)];
};

export const filter_clause_to_sparql = (
  expr: DSL.FilterClause,
  options?: Partial<FilterToSparqlContext>
): SPARQL.GroupGraphPatternPart => {
  const context: FilterToSparqlContext = {
    subject_var: options?.subject_var ?? "this",
    mint: options?.mint ?? sparql_var_mint(),
  };

  // Unions
  if (expr[0] === "or") {
    const [, ...clauses] = expr;
    return [
      "union",
      ...clauses.map(
        (c) => ["group", clause_or_conjunction_to_sparql(c, context)] as const
      ),
    ];
  }

  return ["basic", ...boolean_clause_to_sparql(expr, context)];
};

export const boolean_clause_to_sparql = (
  expr: DSL.BooleanClause,
  context: FilterToSparqlContext
): Iterable<SPARQL.BasicGraphPatternPart> => {
  const { mint, subject_var } = context;

  // Exists is implemented as a simple pattern, which has the same effect as a
  // `FILTER EXISTS`
  if (expr[0] === "exists") {
    const [, [, path]] = expr;
    // Throwaway var.  An empty blank node ([]) would also work here.
    // TODO: add empty blank node to sparql-ast and replace this
    return [["pattern", ["var", subject_var], map_property_path(path), mint()]];
  }

  return negatable_to_sparql(expr, context);
};

export const boolean_expression_to_sparql = (
  expr: DSL.BooleanExpression,
  context: FilterToSparqlContext
): Iterable<SPARQL.BasicGraphPatternPart> => {
  if (
    expr[0] === "=" ||
    expr[0] === "<" ||
    expr[0] === ">" ||
    expr[0] === "<=" ||
    expr[0] === ">="
  ) {
    const [op, lhs, rhs] = expr;
    const L = resolve_this_expr(lhs, context);
    const R = resolve_this_expr(rhs, context);
    return [
      ...(L.pattern ? [L.pattern] : []),
      ...(R.pattern ? [R.pattern] : []),
      ["filter", [op, L.sub, R.sub]],
    ];
  }

  if (expr[0] === "in") {
    const [, val, list] = expr;
    const { sub, pattern } = resolve_this_expr(val, context);

    return [...(pattern ? [pattern] : []), ["filter", ["in", sub, ...list]]];
  }

  if (expr[0] === "match") {
    const [, val, pat, opts] = expr;
    const { sub, pattern } = resolve_this_expr(val, context);

    return [
      ...(pattern ? [pattern] : []),
      ["filter", ["call", "REGEX", sub, pat, ...(opts ? [opts] : [])]],
    ];
  }

  assert_unreachable(expr, "boolean expression");
};
