# Filtering in SPARQL-REST

This module defines a small language for expressing filter conditions on
graph-based resource selections.

## Background

The `sparql-rest` package provides a record-oriented interface to an RDF data
source. All operations on this interface are performed against a given _type_;
as such, all queries implicitly include at least a filter on type. Other
mechanisms (such as a permissions model) can be used to further constrain
responses based on metadata.

The interface also supports ad-hoc filtering of specific requests. Callers can
specify windowing parameters (limit and offset) to get paged results. Callers
can also specify arbitrary filter criteria to be evaluated against each
resource, with knowledge of the record-mapping metadata. The facilities in this
module support that requirement.

## Design

This section describes the design goals and motivates the design choices.

### Goal

The goal of the filter language is to expose as much of the underlying query
engine's power as possible without conflicting with the interface's other
invariants. Incoming filter expressions must carry sufficient information to be
consistently, unambiguously, and correctly integrated into larger query
expressions.

### Constraints

The filter language is constrained by several hard requirements, which are
described in this section.

#### JSON syntax

Expressions are fully JSON roundtrippable. The language does not introduce any
syntax affecting the structure of expressions. (A syntax is used for mapping
between record properties and RDF terms, but this mapping does not change
expression structure.)

#### JSON API interop

While this module (and `sparql-rest` generally) is strictly a JavaScript
interface and is not concerned with the transmission of requests and responses,
it is expected to be used with linked-data-oriented protocols such as JSON API.

The [JSON API spec](https://jsonapi.org/format/#fetching-filtering) reserves a
single query parameter (`filter`) for expressing filter criteria in incoming
requests, though it is “agnostic about the strategies supported.”

For compatibility with such usage, it is essential that a complete filter
criteria always be representable as a _single_ JSON value.

#### TypeScript interop

Valid expression trees can be described by TypeScript interfaces. This serves a
number of purposes. The types constitute a formal grammar definition, including
documentation, for which the compiler can act as a checker. Functions operating
on specific node types can also be checked (including for exhaustiveness), which
is valuable since the module is primarily a term-rewriting tool.

#### SPARQL interop

Expressions are translatable to standard SPARQL (given a binding for the
subject). “Standard” SPARQL here means SPARQL that is compatible with major
implementations (currently tested with Neptune and Jena.)

#### SPARQL injection story

Expressions are translatable into _secure_ SPARQL. This primarily means that no
user-supplied text is interpolated directly into SPARQL queries without the
possibility of being sanitized. This may also include the ability to limit the
overall complexity of filter expressions (to prevent, e.g. disruption of service
through compute-intensive queries).

#### SPARQL-REST interop

Filters are restrictive only. User-supplied filters will never cause items to be
selected that don't also match other criteria imposed by the interface. In other
words, filters can be implemented in a way that is compatible with the
`sparql-rest` interface's other invariants, notably:

- ordering and paging

- pseudo converse properties

- included resources

- single-graph worldview. The `sparql-rest` interface does not expose a notion
  of named graphs within the context of its operations. All operations take
  place implicitly within the context of a single graph (named or default). No
  explicit graph references should be emitted in the implementation of filter
  clauses.

- permissions model, which may forbid certain properties from being available in
  certain request contexts for certain data conditions.

- No dependency on special entailment regimes (e.g. RDFS/OWL). This is a
  constraint of `sparql-rest` generally.

### Desiderata

This section describes the “soft constraints”: properties that are desirable but
not formal requirements.

The language should

- be concise (used in query arguments)

- be self-contained (not introduce module dependencies)

### Tradeoffs

This section lists conscious tradeoffs that were made in the interest of the
priority objectives.

#### Isolated grammar

The main tradeoff is that the grammatical form is not fully self-descriptive.
That is, filter expressions are not themselves interoperable as linked data.
This is because the grammar uses tuples instead of maps for representing nodes
in the expression tree, and there is no standard mechanism for mapping tuple
trees into annotated structures. A map-based language, such as
[Mongo's operator DSL](https://docs.mongodb.com/manual/reference/operator/), is
suceptible to description by a JSON-LD context capable of associating semantics
with each node; but JSON-LD has no concept of tuples. For similar reasons, the
structures cannot be described by a [metatype](../../../metatype) schema.

So why use tuples? For one thing, since not every part needs to be labeled, it's
more concise. But it's also critical to the leverage of structural types for
defining an unambiguous grammar. TypeScript will create a discriminated union
from `["foo", string] | ["bar", number]` but not from
`{foo: string} | {bar: number}`.

This terseness is mitigated in some contexts by the ability to
[label tuple members](https://github.com/microsoft/TypeScript/pull/38234) in
TypeScript. Documentation and development environments can benefit from this
additional information where type alone might not have made the intent of a
structure clear.

In general, though, this limitation is mitigated by the fact that filter
expressions tend to be ad-hoc in practice. Moreover, since they are
serializable, they can still be transported in a structure-preserving (if not
semantics-preserving) way.

#### Potential for ambiguity

Most constructs in the grammar are “tagged”; that is, they begin with a
distinguishing token:

```json
[
  "or",
  ["in", ["this", "User:alias"], ["sudo", "su"]],
  ["match", ["this", "User:name"], "root"],
  ["=", ["this", ["/", "User:supervisor", "User:is_admin"]], true]
]
```

However, not _all_ nodes are tagged. In certain places, literals can be used
directly.

While this makes expressions more concise, care must be taken in the grammar to
avoid ambiguities. I don't know of an automatic way to detect that situation.

#### Encoding requirements

Although this module deals only in runtime values, it is expected that typical
usage will involve multiple levels of encoding on the client's part. For
example, when transmitted as a query argument, filters will require JSON
encoding plus URL encoding, making values impractical to type directly. No
effort is made in the language to accomodate this kind of usage.

## Filter expressions

A filter definition is an unordered collection of filter clauses. (In JSON terms
it is an array where order is not significant).

A filter clause is a list-based expression tree using a prefix notation for
identifying (operator) node types (similar to
[S-expressions](https://en.wikipedia.org/wiki/S-expression)).

The [type definitions](./api.ts) in this module include documentation of all
allowed node types, starting with the root element `Filter`.

A variety of usage examples can be found in the
[tests](../../test/filter/list-filter.ts).

## Concepts

In general, the filter language builds on concepts defined elsewhere. This
section describes concepts that are specific to the filtering language.

### Property references

`sparql-rest` supports a record-oriented interface to an RDF based data store.
Records have properties with local names, like `title` and `category`, as in the
following example:

```json
{
  "title": "Much Ado About Nothing",
  "category": "Comedy"
}
```

In RDF, all predicates are IRI's. Therefore, terms like `title` and `category`
must first be resolved to IRI's before they can be used. In general, the
`sparql-rest` interface performs this mapping implicitly.

However, filter expressions may leave the context of the original item by
traversing through one or more properties. For example, the following “sequence
path”

```json
["/", "has_part", "name"]
```

says, first find all matches for `has_part`, then, _from there_ find the `name`
property. The problem for us is that we don't know where we are at that point.
Since the `metatype` schema used by `sparql-rest` does not expect related
entities to have a specific type, the system cannot assume anything about the
resources that might be matched by the `has_part` expression. It is possible (if
not ideal practice) that the `name` property maps to different terms on
different matched entities.

For this reason, it is not possible in general to determine what predicate a
local name in a filter expression intends to target.

As a result, references to properties must be _qualified_. A qualified name has
the following form:

```
{type}:{property}
```

where `{type}` is the name of the type in the record schema and `{property}` is
the local name of a property defined on that type.

(But see [“Default namespace”](#default-namespace)) in roadmap.

Property references that cannot be resolved to a defined term in the associated
schema will result in an error.

### `this` expressions

Filter expressions do not address the graph at large; they address a specific
subject. The structure of filter expressions reflects this.

Specifically, the filter language includes a “`this` expression”. A `this`
expression can take two forms: a `this` reference and a `this` path.

A `this` _reference_ is simply a reference to the implied subject.

For example,

```json
[
  "in",
  ["this"],
  [
    ["id", "http://example.org/accept"],
    ["id", "http://example.org/reject"]
  ]
]
```

creates a filter like the following

```sparql
FILTER (?this IN (<http://example.org/accept>, <http://example.org/reject>))
```

A `this` _path_ combines a property (traversal) path with the implied subject.

For example, you can say

```json
["match", ["this", "User:name"], "john"]
```

This translates to SPARQL like the following:

```sparql
?this th:name ?gen1
FILTER REGEX(?gen1, "john")
```

The form thus evaluates to any object reachable from the subject by way of the
described path.

These forms are recognized in most places where a value is expected, though see
the grammar for details.

Multiple `this` expressions can be used in a filter. For example, you can
compare two results reached from the context item:

```json
[
  "lt",
  ["this", "Expense:requested_amount"],
  ["this", "Expense:reimbursed_amount"]
]
```

which is equivalent to the SPARQL:

```sparql
?this th:requested amount ?gen_req .
?this th:reimbursed_amount ?gen_reimb .
filter(?gen_req < ?gen_reimb)
```

You can use these forms in combination to test a resource against its related
resources. For example, this filter expression

```json
["=", ["this"], ["this", "Person:emergencyContact"]]
```

will match Person records that are listed as their own emergency contact.

## Future work

This section describes expected directions for future development.

### SPARQL AST

**UPDATE**: A preliminary [`sparql-ast`](../../../sparql-ast) package has been
added.

It would be preferable to target a full-fledged SPARQL AST. A full, structured
representation of SPARQL expressions would have several obvious applications
throughout the `sparql-rest` package. In particular for this module, such a
module could support the (secure) serialization of SPARQL expressions. Rather
than trafficing in strings, this module should be implemented as a pure
term-rewriting from the filter grammar to SPARQL AST fragments.

### Default namespace

As noted in the [“Property references”](#property-references) section, it is
currently necessary to namespace-qualify all property references with a prefix
indicating the relevant type.

Requiring the namespace is verbose and avoidable in some cases.

One way of avoiding this would be to “figure out” what the appropriate property
must be based on the expression and context. In the basic case of unary
predicate paths, it is possible to disambiguate unqualified (non-namespaced)
property references based on context. However, in compound expressions the
context becomes unclear, and a rule cannot be applied consistently, to know that
the elision is safe in some position. In no case do we want to allow
unverifiable elisions using a "best guess" heuristic.

Another approach would be to _always_ allow elision, and simply supply _any_
missing qualifier using the context type. The advantage of such a rule is that
it can be applied consistently regardless of the particular schema. There is no
implication that the elision is correct; and a mismatch will still throw an
error (as if the qualifier had been explicit). This “danger” would also give
such filters some portability across types, where common properties happen to be
defined. (And if you don't want this effect, you can always just keep the
qualifier).

### Subqueries

Some filters that are feasible in plain SPARQL cannot be described by the DSL as
currently defined. In particular, counts and other aggregates of related items
cannot be expressed.

The language is designed to support future modification that includes the use of
subqueries and bindings, which would enable filtering on more complex
conditions.

For example, a filter expression like the following could be used to select
groups that are either private or have more than five members:

```json
[
  [
    "or",
    ["eq", ["this", "Group:private"], true],
    ["gt", ["var", "member_count"], 5]
  ],
  [
    "bind"
    /* whatever it takes to bind member_count */
  ]
]
```

Note that this would necessecitate the use of explicit bindings and variable
references, which are not currently included in the DSL. The current
implementation avoids the need for explicit bindings (even where they are
necessary in the SPARQL form) by allowing the embedding of `this` expressions
where (single-use) variables would go.

Note that the introduction of bindings would also mean that top-level filter
clauses are no longer mutually independent (which they currently are).

Currently, the language represents a set of closed expressions. _It would be
nice_ if any extension introducing open variable references were to leave the
current form intact. That would likely require parameterizing the existing
types.

Also, any such extension must ensure that filter clauses not leak variables into
the containing scope. A proper set of (typed) term-rewriting functions for
SPARQL would be a prerequisite for this, and the filter grammar would need
extension into some subset of the general query language (projections, etc).

### Permissions interop

A permissions model for attribute visibility is still under development. Such
considerations will necessarily interact with user-defined filters, since
filters can indirectly expose users to facts even when those facts are not
returned directly.

See [permissions](/doc/permissions.md) for (WIP) notes on interaction between
permissions and filtering.

## Related work

MongoDB defines a
[JSON-based grammar](https://docs.mongodb.com/manual/reference/operator/) for a
wide variety of operations, including
[filter specifications](https://docs.mongodb.com/manual/reference/operator/aggregation/filter/).

The Mongo grammar mixes domain keys (field names) and keywords. This makes
"leaf" expressions more concise in some cases, but it makes the grammar less
suceptible to static typing.
