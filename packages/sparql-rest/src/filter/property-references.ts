// Implements the “Property references” transformation described in the README.
import type { MetaInterfaces } from "@swymbase/metatype-core";
import type * as DSL from "../../src/filter/api";
import { assert_unreachable } from "../../src/filter/helpers";

/**
 * Return the resolved path expression for with the given `type:propery` form,
 * or throw.
 *
 * This mapping does two things:
 *
 * - finds the qualified IRI for the given property reference
 *
 * - inverts the path expression if it refers to a pseudo-converse property
 */
export const map_term = (
  term: string,
  schema: MetaInterfaces
): DSL.PropertyPath => {
  // Assume a two-part string
  const [type_name, property] = term.split(":");

  const type_spec = schema[type_name];
  if (!type_spec)
    throw new RangeError(`Type ${type_name} is not in given schema.`);

  const property_spec = type_spec.properties[property];
  if (!property_spec)
    throw new RangeError(
      `Property ${property} is not defined for type ${type_name}.`
    );

  // If this is a pseudo-converse property, return an inverted path expression
  if (property_spec.converse_property)
    return ["^", property_spec.converse_property];

  return property_spec.term;
};

export const map_clause_or_conjunction = (
  expr: DSL.ClauseOrConjunction,
  schema: MetaInterfaces
): DSL.ClauseOrConjunction => {
  if (expr[0] === "and") {
    const [op, ...clauses] = expr;
    // @ts-expect-error: first element in clauses is required, may be fixed in TS4
    return [op, ...clauses.map((clause) => map_boolean_clause(clause, schema))];
  }

  return map_boolean_clause(expr, schema);
};

export const map_boolean_clause = (
  expr: DSL.BooleanClause,
  schema: MetaInterfaces
): DSL.BooleanClause => {
  if (expr[0] === "exists") {
    const [op, clause] = expr;
    return [op, map_this_path(clause, schema)];
  }
  return map_negatable_expression(expr, schema);
};

export const map_negatable_expression = (
  expr: DSL.NegatableExpression,
  schema: MetaInterfaces
): DSL.NegatableExpression => {
  if (expr[0] === "not") {
    const [op, clause] = expr;
    return [op, map_boolean_expression(clause, schema)];
  }
  return map_boolean_expression(expr, schema);
};

export const map_boolean_expression = (
  expr: DSL.BooleanExpression,
  schema: MetaInterfaces
): DSL.BooleanExpression => {
  if (
    expr[0] === "=" ||
    expr[0] === "<" ||
    expr[0] === ">" ||
    expr[0] === "<=" ||
    expr[0] === ">="
  ) {
    const [op, lhs, rhs] = expr;
    return [op, map_value_expr(lhs, schema), map_value_expr(rhs, schema)];
  }

  if (expr[0] === "in") {
    const [op, val, list] = expr;
    return [op, map_value_expr(val, schema), list];
  }

  if (expr[0] === "match") {
    const [op, val, ...rest] = expr;
    return [op, map_value_expr(val, schema), ...rest];
  }

  assert_unreachable(expr, "boolean expression");
};

export const map_terms = (
  expr: DSL.FilterClause,
  schema: MetaInterfaces
): DSL.FilterClause => {
  if (expr[0] === "or") {
    const [op, ...clauses] = expr;
    // @ts-expect-error: first element in clauses is required, may be fixed in TS4
    return [op, ...clauses.map((c) => map_clause_or_conjunction(c, schema))];
  }

  return map_boolean_clause(expr, schema);
};

const map_this_expr = (
  expr: DSL.ThisExpression,
  schema: MetaInterfaces
): DSL.ThisExpression => {
  const [op, path] = expr;
  if (path) return [op, map_path_expr(path, schema)];
  return [op];
};

const map_this_path = (
  expr: DSL.ThisPath,
  schema: MetaInterfaces
): DSL.ThisPath => {
  const [op, path] = expr;
  return [op, map_path_expr(path, schema)];
};

const map_path_expr = (
  expr: DSL.PropertyPath,
  schema: MetaInterfaces
): DSL.PropertyPath => {
  if (typeof expr === "string") return map_term(expr, schema);
  const [op, ...elts] = expr;
  // @ts-expect-error. doesn't unify different arities. May check in TS4
  return [op, ...elts.map((elt) => map_path_expr(elt, schema))];
};

const map_value_expr = (
  expr: DSL.ValueExpression,
  schema: MetaInterfaces
): DSL.ValueExpression => {
  if (typeof expr === "object" && expr[0] === "this")
    return map_this_expr(expr, schema);

  return expr;
};
