/*
 * This module defines a set of types which comprise a DSL for expressing
 * constraints on (typed) resources.
 */

/**
 * An unordered collection of zero or more clauses representing a set of
 * constraints on a resource selection (where *all* constraints apply).
 *
 * The space of expressions is similar to that of a SPARQL group graph pattern,
 * except that a subject is implicit throughout (@see `ThisExpression`).
 */
export type Filter = readonly FilterClause[];

/**
 * Represents a constraint on a resource selection.
 *
 * Note: This type may be expanded in the future to include, in particular, full
 * predicates, binding expressions, and subqueries.
 */
export type FilterClause = OrClause | BooleanClause;

/**
 * Represents a collection of alternative constraints.  The result is the union
 * of everything matching any of the clauses.
 */
export type OrClause = readonly [
  "or",
  ClauseOrConjunction,
  ...ClauseOrConjunction[]
];

/**
 * Represents either a constraint or a conjunction of constraints.
 */
export type ClauseOrConjunction = AndClause | BooleanClause;

/**
 * Represents a collection of constraints to treat as an intersection operation.
 */
export type AndClause = readonly ["and", BooleanClause, ...BooleanClause[]];

/**
 * A clause that can be evaluated for truth or falsehood.
 */
export type BooleanClause = ExistsClause | NegatableExpression;

/**
 * Represents an assertion of the existence of any value for a given property
 * path on the implied subject.
 */
export type ExistsClause = readonly ["exists", ThisPath];

/**
 * Represents either a boolean expression or a negated boolean expression.
 *
 * Note that this is not recursive: negation is currently only allowed at the
 * top level of boolean expressions.
 */
export type NegatableExpression = NotExpression | BooleanExpression;

/**
 * Represents a constraint that passes when a given condition is not met.
 */
export type NotExpression = readonly ["not", BooleanExpression];

export type BooleanExpression =
  | ComparisonExpression
  | InListExpression
  | FunctionCallExpression;

/**
 * Represents a reference that is interpreted relative to the context subject.
 *
 * This form is “contagious”: any node that allows `ThisExpression` as a
 * descendant node assumes a contextual subject.
 */
export type ThisExpression = ThisReference | ThisPath;

/**
 * Represents a reference to the contextual subject.
 *
 * In SPARQL terms, this form is equivalent to the variable that is bound to the
 * ID of the resource being considered.
 */
export type ThisReference = readonly ["this"];

/**
 * Represents the application of a property path to a subject supplied by the
 * context.
 *
 * For any given _subject_ (`this`, which comes from the context) the expression
 * evaluates to every _object_ connected with that subject by the property path.
 *
 * In SPARQL terms, this form implies a partial graph pattern and possibly
 * a `FILTER` clause.  (Simple existence and equality checks only need pattern
 *  matching).
 *
 * This form is “contagious”: any node that allows `ThisPath` as a descendant
 * node assumes a contextual subject.
 */
export type ThisPath = readonly ["this", PropertyPath];

/**
 * Represents an equality or ordinal comparison test between two value
 * expressions.
 */
// This construction narrows better than embedding the operator union in 1 tuple
export type ComparisonExpression =
  | readonly ["=", ValueExpression, ValueExpression]
  | readonly ["<", ValueExpression, ValueExpression]
  | readonly [">", ValueExpression, ValueExpression]
  | readonly ["<=", ValueExpression, ValueExpression]
  | readonly [">=", ValueExpression, ValueExpression];

/**
 * Represents a constraint where value must equal at least one value from a
 * fixed set.
 */
export type InListExpression = readonly [
  "in",
  ValueExpression,
  readonly LiteralExpression[]
];

/**
 * Represents a simple value.
 *
 * Note that since literals can be plain strings, all other members of the
 * resolved union must be tagged in order to avoid ambiguity.
 *
 * Note: This type may be expanded in the future to include variable references.
 */
export type ValueExpression = LiteralExpression | ThisExpression;

/**
 * Represents an explicit, concrete value in the graph data model.
 *
 * Note: This type may be expanded in the future to include language tags or
 * datatyped literals.
 */
export type LiteralExpression = boolean | number | string | NamedNodeReference;

/**
 * Represents a complete node identifier.
 */
export type NamedNodeReference = readonly ["id", string];

/**
 * Represent a subset of SPARQL property path expressions
 *
 * See §9.1 “Property Paths” https://www.w3.org/TR/sparql11-query/#propertypaths
 */
export type PropertyPath = PredicatePath | CompositePath;

/**
 * Ground value for path expressions.  All path expressions terminate with this.
 *
 * Note that the values provided here are not assumed to be IRI's.  If terms
 * need to first be resolved, it is up to the caller to do that mapping prior to
 * conversion to SPARQL.
 */
export type PredicatePath = string;

export type CompositePath = InversePath | SequencePath | AlternativePath;

/**
 * Path element relating subject with object (*versus* object with subject).
 */
export type InversePath = readonly ["^", PropertyPath];

/**
 * Path elements to be applied in succession.
 */
export type SequencePath = readonly ["/", PropertyPath, ...PropertyPath[]];

/**
 * A set of path elements any which will satisfy the criteria.
 */
export type AlternativePath = readonly ["|", PropertyPath, ...PropertyPath[]];

export type FunctionCallExpression = RegexExpression;

/**
 * Represents a regular expression match of the input expression against a given
 * pattern.
 */
export type RegexExpression = readonly ["match", ValueExpression, string, "i"?];
