import {
  assert_success,
  formatted,
  send,
} from "@swymbase/sparql-protocol-client";
import {
  is_named_graph_nonempty,
  with_temp_named_graph,
} from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "../lib/config";

test("creates a graph and destroys it after operation", async (t) => {
  let graph_iri: string | undefined = undefined;

  // === OPERATION
  await with_temp_named_graph(endpoint, async (temp_graph_iri) => {
    graph_iri = temp_graph_iri;

    const EX = `http://example.com/`;

    const triples = `
<${EX}Alice> <${EX}name> "Alice" .
<${EX}Bob> <${EX}name> "Bob" ; <${EX}loves> <${EX}Alice> .
`;

    const update = `INSERT DATA { GRAPH <${graph_iri}> { ${triples} } }`;
    const insert_result = await send({ endpoint, operation: { update } });
    assert_success(t, insert_result, "insert test triples");

    // Confirm that triples were written to graph
    const query = `ASK WHERE { GRAPH <${graph_iri}> { ${triples} } }`;
    const ask_result = await formatted("boolean", {
      endpoint,
      operation: { query },
    });
    assert_success(t, ask_result.context, "confirm test triples");

    t.true(ask_result.value);
  });

  // === POSTCONDITIONS

  t.assert(graph_iri);
  if (!graph_iri) throw new Error("Assert fail");

  const is_nonempty_result = await is_named_graph_nonempty(endpoint, graph_iri);
  assert_success(t, is_nonempty_result.context, "postcondition: graph empty");

  t.false(is_nonempty_result.value);
});
