import { assert_success, send } from "@swymbase/sparql-protocol-client";
import { is_named_graph_nonempty } from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "../lib/config";

// Random enough for our purposes
const random = () => Math.round(Math.random() * 1e12);
const pseudo_uuid = () => `${random()}-${random()}-${random()}-${random()}`;
const presumably_unused_graph_name = () => `http://test.net/${pseudo_uuid()}`;

/*
 * These tests have to assume that the randomly-named graphs don't previously
 * exist in the database, since a check for their existence is what's being
 * tested here.
 */

test("tells when a graph is empty", async (t) => {
  // === SETUP
  const graph_iri = presumably_unused_graph_name();

  // === OPERATION
  const result = await is_named_graph_nonempty(endpoint, graph_iri);
  assert_success(t, result.context, "main operation");

  // === POSTCONDITIONS
  t.false(result.value, "Expected graph to be reported as empty");

  // === CLEANUP (none)
  // (No triples, so no cleanup)
});

test("tells when a graph is not empty", async (t) => {
  // === SETUP
  const graph_iri = presumably_unused_graph_name();
  const triples = `<http://example.com/thing> <http://example.com/vocab/dummy> true`;

  // Assert at least one fact to the named graph
  const insert = `INSERT DATA { GRAPH <${graph_iri}> { ${triples} } }`;
  const insert_result = await send({ endpoint, operation: { update: insert } });
  assert_success(t, insert_result, "precondition: insert facts");

  try {
    // === OPERATION
    const result = await is_named_graph_nonempty(endpoint, graph_iri);
    assert_success(t, result.context, "main operation");

    // === POSTCONDITIONS
    t.true(result.value, "Expected graph to be reported as non-empty");
  } finally {
    // === CLEANUP
    // Delete any triples that were asserted
    const cleanup = `DELETE DATA { GRAPH <${graph_iri}> { ${triples} } }`;

    const delete_result = await send({
      endpoint,
      operation: { update: cleanup },
    });
    assert_success(t, delete_result, "cleanup: delete asserted triples");
  }
});
