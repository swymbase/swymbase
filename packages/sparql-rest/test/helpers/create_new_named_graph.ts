import { assert_success, send } from "@swymbase/sparql-protocol-client";
import {
  create_new_named_graph,
  is_named_graph_nonempty,
} from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "../lib/config";

/*
 * The only thing we can test about this function is whether it marks a graph as
 * unavailable vis-a-vis the check that we'll use elsewhere for a graph's
 * availability.
 */

test("makes a verifiably-used graph", async (t) => {
  // === PRECONDITIONS (none)
  // Since we don't know what the IRI is going to be, we can't test now whether
  // the graph exists beforehand.
  let graph_iri: string | undefined = undefined;

  try {
    // === OPERATION
    const result = await create_new_named_graph(endpoint);

    if (typeof result === "object") {
      throw new Error("Create new graph failed");
    }

    const graph_iri = result;

    // === POSTCONDITIONS
    const nonempty_before = await is_named_graph_nonempty(endpoint, graph_iri);
    assert_success(t, nonempty_before.context, "post: determine emptiness");

    t.true(nonempty_before.value);
  } finally {
    // === CLEANUP
    if (graph_iri !== undefined) {
      const drop = `DROP GRAPH <${graph_iri}>`;
      const drop_result = await send({ endpoint, operation: { update: drop } });
      assert_success(t, drop_result, "cleanup: delete asserted triples");

      // === POST-POSTCONDITIONS
      const nonempty_after = await is_named_graph_nonempty(endpoint, graph_iri);
      assert_success(t, nonempty_after.context, "post-postcondition");

      t.false(nonempty_after.value);
    }
  }
});
