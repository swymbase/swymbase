// Tests for included resources on list and get operations.
//
// All tests use the same test schema and data.
import { EntitiesFrom, metatypes, Specs as $ } from "@swymbase/metatype-core";
import test, { ExecutionContext } from "ava";
import {
  assert_equal_records,
  assert_equal_record_sets,
} from "./lib/record-helpers";
import { with_seeded_kb } from "./lib/with-seeded-kb";

interface GetCase<T extends keyof Types> {
  readonly type: T;
  readonly id: string;
  readonly include?: readonly (
    | keyof Metatypes[T]["properties"]
    | [keyof Metatypes[T]["properties"], ...string[]]
  )[];
  readonly expect: {
    readonly record: Types[T];
    readonly included?: readonly object[];
  };
}

interface ListCase<T extends keyof Types> {
  readonly type: T;
  readonly include?: readonly (
    | keyof Metatypes[T]["properties"]
    | [keyof Metatypes[T]["properties"], ...string[]]
  )[];
  readonly expect: {
    readonly records: readonly Types[T][];
    readonly included?: readonly object[];
  };
}

const XSD = "http://www.w3.org/2001/XMLSchema#";
const RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

const V = "http://example.com/vocab#";
const E = "http://example.com/entities/";

const TYPES = metatypes({
  Media: {
    "@type": `${V}Media`,
    properties: {
      small: {
        term: `${V}small`,
        type: "string",
      },
    },
  },
  Person: {
    "@type": `${V}Person`,
    properties: {
      name: { term: `${V}name`, type: "string" },
      alias: { term: `${V}alias`, type: "string", multivalued: true },
      avatar: {
        term: `${V}avatar`,
        type: "object",
        relationship: true,
      },
      mother: { term: `${V}mother`, type: "object", relationship: true },
      mother_of: {
        term: `${V}mother_of`,
        type: "object",
        relationship: true,
        multivalued: true,
        converse_property: `${V}mother`,
      },
      sender_of: {
        term: `${V}sender_of`,
        type: "object",
        relationship: true,
        multivalued: true,
        converse_property: `${V}from`,
      },
      recipient_of: {
        term: `${V}recipient_of`,
        type: "object",
        relationship: true,
        multivalued: true,
        converse_property: `${V}to`,
      },
      father: { term: `${V}father`, type: "object", relationship: true },
      loves: {
        term: `${V}loves`,
        type: "object",
        relationship: true,
        multivalued: true,
      },
      member_of: {
        term: `${V}member_of`,
        type: "object",
        relationship: true,
        multivalued: true,
        converse_property: `${V}member`,
      },
    },
  },
  Group: {
    "@type": `${V}Group`,
    properties: {
      name: { term: `${V}name`, type: "string" },
      passcode: { term: `${V}passcode`, type: "number" },
      members: {
        term: `${V}member`,
        type: "object",
        relationship: true,
        multivalued: true,
      },
    },
  },
  Message: {
    "@type": `${V}Message`,
    properties: {
      from: { term: `${V}from`, type: "object", relationship: true },
      to: { term: `${V}to`, type: "object", relationship: true },
      headers: {
        term: `${V}headers`,
        type: { spec: $.dictionary($.or($.string, $.number)) },
      },
      body: { term: `${V}body`, type: "string" },
      is_read: { term: `${V}is_read`, type: "boolean" },
    },
  },
});

type Metatypes = typeof TYPES;
type Types = EntitiesFrom<Metatypes>;

const TEST_DATA = `
<Claudius> a :Person
 ; :name "Claudius"
 ; :avatar <ClaudiusAvatar> .
<Gertrude> a :Person
 ; :name "Gertrude"
 ; :avatar <GertrudeAvatar>
 ; :alias "Queen Gertrude" .
<Laertes> a :Person
 ; :name "Laertes" 
 ; :avatar <LaertesAvatar>
 ; :father <Polonius> 
 ; :loves <Ophelia>, <Polonius> .
<Ophelia> a :Person
 ; :name "Ophelia"
 ; :avatar <OpheliaAvatar>
 ; :father <Polonius>
 ; :loves <Hamlet>, <Polonius>, <Laertes> .
<Polonius> a :Person
 ; :name "Polonius"
 ; :avatar <PoloniusAvatar> .
<Hamlet> a :Person
 ; :name "Hamlet"
 ; :avatar <HamletAvatar>
 ; :alias "Prince of Denmark"
 ; :father <KingHamlet>
 ; :mother <Gertrude>
 ; :loves <Ophelia>, <KingHamlet>, <Horatio> .
<KingHamlet> a :Person ; :name "Hamlet" ; :alias "Ghost of Hamlet" .
<Horatio> a :Person ; :name "Horatio" .
<Rosencrantz> a :Person ; :name "Rosencrantz" .

<ClaudiusAvatar> a :Media ; :small "https://example.com/Claudius-sm.png" .
<GertrudeAvatar> a :Media ; :small "https://example.com/Gertrude-sm.png" .
<LaertesAvatar> a :Media ; :small "https://example.com/Laertes-sm.png" .
<OpheliaAvatar> a :Media ; :small "https://example.com/Ophelia-sm.png" .
<PoloniusAvatar> a :Media ; :small "https://example.com/Polonius-sm.png" .
<HamletAvatar> a :Media ; :small "https://example.com/Hamlet-sm.png" .

<TheCourt> a :Group ; :name "The court of Denmark"
  ; :passcode "1234"^^xsd:float
  ; :member <Claudius>, <Gertrude>, <Hamlet>, <Polonius> .
<Students> a :Group ; :name "Wittenbergers"
  ; :member <Hamlet>, <Horatio>, <Laertes>, <Rosencrantz> .

<YourFather> a :Message ; :from <Horatio> ; :to <Hamlet> 
  ; :body "the king your father" ; :is_read true .

<Murther> a :Message ; :from <KingHamlet> ; :to <Hamlet>
  ; :headers """
{
  "Content-type": "text/plain",
  "Content-length": 17
}
"""^^rdf:JSON
  ; :body "murther most foul" .

<DoubtThat> a :Message ; :from <Hamlet> ; :to <Ophelia> 
  ; :body "doubt that the sun doth move" .
`;

const preamble = `BASE <${E}>
PREFIX : <${V}>
PREFIX xsd: <${XSD}>
PREFIX rdf: <${RDF}>
`;

const get = <T extends keyof Types>(t: ExecutionContext, props: GetCase<T>) =>
  with_seeded_kb(t, preamble, TEST_DATA, TYPES, async (kb) => {
    const { type, id, expect, include } = props;

    // === OPERATION
    const response = await kb.get_record(type, id, {
      // TS: `include` is PropertyKey for some reason
      include_related: <any>include,
    });

    // === POSTCONDITIONS
    if (!response.value) {
      t.log("RESPONSE", response);
      throw new Error("Result of operation was undefined");
    }
    t.log("INCLUDED", response.included_values);

    assert_equal_records(t, response.value, expect.record);
    assert_equal_record_sets(t, response.included_values, expect.included);
  });

const list = <T extends keyof Types>(t: ExecutionContext, props: ListCase<T>) =>
  with_seeded_kb(t, preamble, TEST_DATA, TYPES, async (kb) => {
    const { type, expect, include } = props;

    // === OPERATION

    const response = await kb.list_records(
      type,
      {},
      // TS: `include` is PropertyKey for some reason
      { include_related: <any>include, include_count: true }
    );

    // === POSTCONDITIONS
    if (!response.value) {
      t.log("RESPONSE", response);
      throw new Error("Result of operation was undefined");
    }
    t.log("INCLUDED", response.included_values);

    t.is(response.total_result_count, expect.records.length, "Result count");
    assert_equal_record_sets(t, response.value, expect.records);
    assert_equal_record_sets(t, response.included_values, expect.included);
  });

// It appears that empty arrays are not being returned for multivalued
// properties on included records.
const PERSON_DEFAULTS: Partial<Types["Person"]> = {
  alias: [],
  loves: [],
  member_of: [],
  mother_of: [],
  sender_of: [],
  recipient_of: [],
};

const person = (record: Types["Person"]) => ({
  ...PERSON_DEFAULTS,
  ...record,
});

const KING_HAMLET_INCL = person({
  id: `${E}KingHamlet`,
  type: `${V}Person`,
  name: "Hamlet",
  alias: ["Ghost of Hamlet"],
});
const KING_HAMLET: Types["Person"] = {
  ...KING_HAMLET_INCL,
  sender_of: [{ id: `${E}Murther` }],
};

// Re the _INCL here and below, converse properties are not supported on
// included records.
const CLAUDIUS_INCL = person({
  id: `${E}Claudius`,
  type: `${V}Person`,
  name: "Claudius",
  avatar: { id: `${E}ClaudiusAvatar` },
});
const CLAUDIUS: Types["Person"] = {
  ...CLAUDIUS_INCL,
  member_of: [{ id: `${E}TheCourt` }],
};

const GERTRUDE_INCL = person({
  id: `${E}Gertrude`,
  type: `${V}Person`,
  name: "Gertrude",
  alias: ["Queen Gertrude"],
  avatar: { id: `${E}GertrudeAvatar` },
});
const GERTRUDE: Types["Person"] = {
  ...GERTRUDE_INCL,
  mother_of: [{ id: `${E}Hamlet` }],
  member_of: [{ id: `${E}TheCourt` }],
};

const POLONIUS_INCL = person({
  id: `${E}Polonius`,
  type: `${V}Person`,
  name: "Polonius",
  avatar: { id: `${E}PoloniusAvatar` },
});
const POLONIUS: Types["Person"] = {
  ...POLONIUS_INCL,
  member_of: [{ id: `${E}TheCourt` }],
};

const HAMLET_INCL = person({
  id: `${E}Hamlet`,
  type: `${V}Person`,
  name: "Hamlet",
  alias: ["Prince of Denmark"],
  avatar: { id: `${E}HamletAvatar` },
  father: { id: `${E}KingHamlet` },
  mother: { id: `${E}Gertrude` },
  loves: [
    { id: `${E}KingHamlet` },
    { id: `${E}Ophelia` },
    { id: `${E}Horatio` },
  ],
});
const HAMLET: Types["Person"] = {
  ...HAMLET_INCL,
  member_of: [{ id: `${E}Students` }, { id: `${E}TheCourt` }],
  sender_of: [{ id: `${E}DoubtThat` }],
  recipient_of: [{ id: `${E}Murther` }, { id: `${E}YourFather` }],
};

const OPHELIA_INCL = person({
  id: `${E}Ophelia`,
  type: `${V}Person`,
  name: "Ophelia",
  avatar: { id: `${E}OpheliaAvatar` },
  father: { id: `${E}Polonius` },
  loves: [{ id: `${E}Hamlet` }, { id: `${E}Polonius` }, { id: `${E}Laertes` }],
});
const OPHELIA: Types["Person"] = {
  ...OPHELIA_INCL,
  recipient_of: [{ id: `${E}DoubtThat` }],
};

const LAERTES_INCL = person({
  id: `${E}Laertes`,
  type: `${V}Person`,
  name: "Laertes",
  avatar: { id: `${E}LaertesAvatar` },
  father: { id: `${E}Polonius` },
  loves: [{ id: `${E}Ophelia` }, { id: `${E}Polonius` }],
});
const LAERTES: Types["Person"] = {
  ...LAERTES_INCL,
  member_of: [{ id: `${E}Students` }],
};

const HORATIO_INCL = person({
  id: `${E}Horatio`,
  type: `${V}Person`,
  name: "Horatio",
});
const HORATIO: Types["Person"] = {
  ...HORATIO_INCL,
  member_of: [{ id: `${E}Students` }],
  sender_of: [{ id: `${E}YourFather` }],
};

const ROSENCRANTZ_INCL = person({
  id: `${E}Rosencrantz`,
  type: `${V}Person`,
  name: "Rosencrantz",
});
const ROSENCRANTZ: Types["Person"] = {
  ...ROSENCRANTZ_INCL,
  member_of: [{ id: `${E}Students` }],
};

const COURT: Types["Group"] = {
  id: `${E}TheCourt`,
  type: `${V}Group`,
  name: "The court of Denmark",
  passcode: 1234,
  members: [
    { id: `${E}Claudius` },
    { id: `${E}Gertrude` },
    { id: `${E}Hamlet` },
    { id: `${E}Polonius` },
  ],
};

const STUDENTS: Types["Group"] = {
  id: `${E}Students`,
  type: `${V}Group`,
  name: "Wittenbergers",
  members: [
    { id: HAMLET.id },
    { id: HORATIO.id },
    { id: LAERTES.id },
    { id: ROSENCRANTZ.id },
  ],
};

const PEOPLE = [
  CLAUDIUS,
  GERTRUDE,
  POLONIUS,
  HAMLET,
  HORATIO,
  OPHELIA,
  KING_HAMLET,
  ROSENCRANTZ,
  LAERTES,
];

const YOUR_FATHER: Types["Message"] = {
  id: `${E}YourFather`,
  type: `${V}Message`,
  from: { id: HORATIO.id },
  to: { id: HAMLET.id },
  body: "the king your father",
  is_read: true,
};

const MURTHER: Types["Message"] = {
  id: `${E}Murther`,
  type: `${V}Message`,
  from: { id: KING_HAMLET.id },
  to: { id: HAMLET.id },
  headers: {
    "Content-type": "text/plain",
    "Content-length": 17,
  },
  body: "murther most foul",
};

const DOUBT_THAT: Types["Message"] = {
  id: `${E}DoubtThat`,
  type: `${V}Message`,
  from: { id: `${E}Hamlet` },
  to: { id: `${E}Ophelia` },
  body: "doubt that the sun doth move",
};

for (const p of PEOPLE)
  test(`get ${p.id}`, (t) =>
    get(t, { type: "Person", id: p.id, expect: { record: p } }));

test("get Court", (t) =>
  get(t, { type: "Group", id: COURT.id, expect: { record: COURT } }));

test("get Hamlet with loves", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Hamlet`,
    include: ["loves"],
    expect: {
      record: HAMLET,
      included: [KING_HAMLET_INCL, OPHELIA_INCL, HORATIO_INCL],
    },
  }));

test("get Polonius with group memberships", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Polonius`,
    include: ["member_of"],
    expect: {
      record: POLONIUS,
      included: [COURT],
    },
  }));

test("get Hamlet with group memberships", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Hamlet`,
    include: ["member_of"],
    expect: {
      record: HAMLET,
      included: [COURT, STUDENTS],
    },
  }));

test("get Hamlet with loves & group memberships", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Hamlet`,
    include: ["member_of", "loves"],
    expect: {
      record: HAMLET,
      included: [COURT, STUDENTS, KING_HAMLET_INCL, OPHELIA_INCL, HORATIO_INCL],
    },
  }));

test("get message from Ghost", (t) =>
  get(t, { type: "Message", id: `${E}Murther`, expect: { record: MURTHER } }));

test("get message from Ghost & sender", (t) =>
  get(t, {
    type: "Message",
    id: `${E}Murther`,
    include: ["from"],
    expect: { record: MURTHER, included: [KING_HAMLET_INCL] },
  }));

test("get message from Ghost & recipient", (t) =>
  get(t, {
    type: "Message",
    id: `${E}Murther`,
    include: ["to"],
    // TODO: This is weird and wrong: Hamlet is included with an empty array for
    // member of, not just a missing value.
    expect: { record: MURTHER, included: [HAMLET_INCL] },
  }));

test("get message from Ghost & sender & recipient", (t) =>
  get(t, {
    type: "Message",
    id: `${E}Murther`,
    include: ["from", "to"],
    expect: {
      record: MURTHER,
      included: [KING_HAMLET_INCL, HAMLET_INCL],
    },
  }));

test("get Hamlet with loves & mother & group memberships", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Hamlet`,
    include: ["member_of", "loves", "mother"],
    expect: {
      record: HAMLET,
      included: [
        COURT,
        STUDENTS,
        KING_HAMLET_INCL,
        OPHELIA_INCL,
        HORATIO_INCL,
        GERTRUDE_INCL,
      ],
    },
  }));

test("get with two-step include (members with fathers)", (t) =>
  get(t, {
    type: "Group",
    id: `${E}Students`,
    include: [["members", "Person:father"]],
    expect: {
      record: STUDENTS,
      included: [KING_HAMLET_INCL, POLONIUS_INCL],
    },
  }));

test("get with two-step include (member avatars)", (t) =>
  get(t, {
    type: "Group",
    id: `${E}TheCourt`,
    include: [["members", "Person:avatar"]],
    expect: {
      record: COURT,
      included: ["Claudius", "Gertrude", "Polonius", "Hamlet"].map((name) => ({
        id: `${E}${name}Avatar`,
        type: `${V}Media`,
        small: `https://example.com/${name}-sm.png`,
      })),
    },
  }));

test("get with one and two-step include (members & member avatars)", (t) =>
  get(t, {
    type: "Group",
    id: `${E}TheCourt`,
    include: ["members", ["members", "Person:avatar"]],
    expect: {
      record: COURT,
      included: [
        CLAUDIUS_INCL,
        GERTRUDE_INCL,
        POLONIUS_INCL,
        HAMLET_INCL,
        ...["Claudius", "Gertrude", "Polonius", "Hamlet"].map((name) => ({
          id: `${E}${name}Avatar`,
          type: `${V}Media`,
          small: `https://example.com/${name}-sm.png`,
        })),
      ],
    },
  }));

test("get with two-step include (mother of sender)", (t) =>
  get(t, {
    type: "Message",
    id: `${E}DoubtThat`,
    include: [["from", "Person:mother"]],
    expect: {
      record: DOUBT_THAT,
      included: [GERTRUDE_INCL],
    },
  }));

test("get with two-step include, one converse (mother-of -> father)", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Gertrude`,
    include: [["mother_of", "Person:father"]],
    expect: {
      record: GERTRUDE,
      included: [KING_HAMLET_INCL],
    },
  }));

test("get with two-step include, both converse (mother-of -> sender-of)", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Gertrude`,
    include: [["mother_of", "Person:sender_of"]],
    expect: {
      record: GERTRUDE,
      included: [DOUBT_THAT],
    },
  }));

test("get with two two-step includes (mother-of -> father & sender-of)", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Gertrude`,
    include: [
      ["mother_of", "Person:sender_of"],
      ["mother_of", "Person:father"],
    ],
    expect: {
      record: GERTRUDE,
      included: [KING_HAMLET_INCL, DOUBT_THAT],
    },
  }));

test("get with mixed includes (mother-of & -> father & sender-of)", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Gertrude`,
    include: [
      ["mother_of"],
      ["mother_of", "Person:sender_of"],
      ["mother_of", "Person:father"],
    ],
    expect: {
      record: GERTRUDE,
      included: [HAMLET_INCL, KING_HAMLET_INCL, DOUBT_THAT],
    },
  }));

test("get with two-step include, both converse (mother-of -> member-of)", (t) =>
  get(t, {
    type: "Person",
    id: `${E}Gertrude`,
    include: [["mother_of", "Person:member_of"]],
    expect: {
      record: GERTRUDE,
      included: [COURT, STUDENTS],
    },
  }));

test("list People", (t) =>
  list(t, { type: "Person", expect: { records: PEOPLE } }));

test("list People & include mothers", (t) =>
  list(t, {
    type: "Person",
    include: ["mother"],
    expect: {
      records: PEOPLE,
      included: [GERTRUDE],
    },
  }));

test("list Groups", (t) =>
  list(t, { type: "Group", expect: { records: [COURT, STUDENTS] } }));

test("list Groups & include members", (t) =>
  list(t, {
    type: "Group",
    include: ["members"],
    expect: {
      records: [COURT, STUDENTS],
      included: [
        CLAUDIUS_INCL,
        GERTRUDE_INCL,
        POLONIUS_INCL,
        HAMLET_INCL,
        HORATIO_INCL,
        ROSENCRANTZ_INCL,
        LAERTES_INCL,
      ],
    },
  }));

test("list with two-step include (member avatars)", (t) =>
  list(t, {
    type: "Group",
    include: [["members", "Person:avatar"]],
    expect: {
      records: [COURT, STUDENTS],
      included: ["Claudius", "Gertrude", "Polonius", "Hamlet", "Laertes"].map(
        (name) => ({
          id: `${E}${name}Avatar`,
          type: `${V}Media`,
          small: `https://example.com/${name}-sm.png`,
        })
      ),
    },
  }));

test("list with one and two-step include (members & member avatars)", (t) =>
  list(t, {
    type: "Group",
    include: ["members", ["members", "Person:avatar"]],
    expect: {
      records: [COURT, STUDENTS],
      included: [
        CLAUDIUS_INCL,
        GERTRUDE_INCL,
        POLONIUS_INCL,
        HAMLET_INCL,
        HORATIO_INCL,
        ROSENCRANTZ_INCL,
        LAERTES_INCL,
        ...["Claudius", "Gertrude", "Polonius", "Hamlet", "Laertes"].map(
          (name) => ({
            id: `${E}${name}Avatar`,
            type: `${V}Media`,
            small: `https://example.com/${name}-sm.png`,
          })
        ),
      ],
    },
  }));

test("list with two-step include, one converse (sender member-of)", (t) =>
  list(t, {
    type: "Message",
    include: [["from", "Person:member_of"]],
    expect: {
      records: [YOUR_FATHER, MURTHER, DOUBT_THAT],
      included: [COURT, STUDENTS],
    },
  }));

test("list with three-step include, two converse (member -> recipient -> sender)", (t) =>
  list(t, {
    type: "Group",
    include: [["members", "Person:recipient_of", "Message:from"]],
    expect: {
      records: [COURT, STUDENTS],
      included: [HORATIO_INCL, KING_HAMLET_INCL],
    },
  }));

test("list with three-step include, with one intermediate", (t) =>
  list(t, {
    type: "Group",
    include: [
      ["members", "Person:recipient_of"],
      ["members", "Person:recipient_of", "Message:from"],
    ],
    expect: {
      records: [COURT, STUDENTS],
      included: [MURTHER, YOUR_FATHER, HORATIO_INCL, KING_HAMLET_INCL],
    },
  }));

test("list with three-step include, with both intermediates", (t) =>
  list(t, {
    type: "Group",
    include: [
      ["members"],
      ["members", "Person:recipient_of"],
      ["members", "Person:recipient_of", "Message:from"],
    ],
    expect: {
      records: [COURT, STUDENTS],
      included: [
        CLAUDIUS_INCL,
        GERTRUDE_INCL,
        POLONIUS_INCL,
        HAMLET_INCL,
        HORATIO_INCL,
        ROSENCRANTZ_INCL,
        LAERTES_INCL,
        MURTHER,
        YOUR_FATHER,
        HORATIO_INCL,
        KING_HAMLET_INCL,
      ],
    },
  }));
