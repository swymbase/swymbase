import { formatted, send } from "@swymbase/sparql-protocol-client";
import {
  contextualize,
  serialize,
  with_temp_named_graph,
} from "@swymbase/sparql-rest";
import test from "ava";
import * as jsonld from "jsonld";
import { endpoint } from "./lib/config";

interface ExampleDomainType {
  "@id"?: string;
  first_name: string;
  last_name: string;
}

const EXAMPLE_CONTEXT = {
  schema: "http://schema.org/",
  first_name: "schema:firstName",
  last_name: "schema:lastName",
};

test("roundtrip an entity from runtime", async (t) => {
  await with_temp_named_graph(endpoint, async (graph_iri) => {
    // Create an instance of the entity
    const example1: ExampleDomainType = {
      "@id": "https://example.com/JerryMcguire",
      first_name: "Don",
      last_name: "Knuth",
    };

    // Contextualize the instance into an RDF model
    const contextualized1 = { "@context": EXAMPLE_CONTEXT, ...example1 };

    // Ensure that it's what we think it is
    const [expanded1, ...rest] = await jsonld.expand(contextualized1);
    t.assert(expanded1);
    t.is(rest.length, 0);
    t.log(expanded1);
    t.deepEqual(expanded1, {
      "@id": "https://example.com/JerryMcguire",
      "http://schema.org/firstName": [{ "@value": "Don" }],
      "http://schema.org/lastName": [{ "@value": "Knuth" }],
    });

    // Write the JSON to a store
    // Serialize in a format that the SPARQL client can take
    const nquads = await jsonld.toRDF(expanded1, {
      format: "application/n-quads",
    });

    t.log("N-quads", nquads);

    const update = `
      INSERT DATA { 
        graph <${graph_iri}> { ${nquads} }
      }
    `;

    t.log(update);

    const inserted = await send({ endpoint, operation: { update } });
    if (!inserted.success) {
      t.log(inserted.code, inserted.error);
      t.log(inserted.http_response?.headers);
      t.fail("Couldn't run insert");
    }
    t.assert(inserted.success);

    // Now get the object back
    const query = `
      select ?s ?p ?o
      where { graph <${graph_iri}> { <https://example.com/JerryMcguire> ?p ?o } } 
      limit 10
    `;

    const selected = await send({ endpoint, operation: { query } });

    if (!selected.success) {
      t.log(selected.code, selected.error);
      throw new Error("Couldn't select object back");
    }

    t.assert(selected.success);

    const { response } = selected;
    if (response.format !== "results")
      t.fail("Expected `results` to be response type");
    else if (!("results" in response.value)) t.fail("Expected SELECT results");
    else {
      // Now get it into JSON
      const {
        results: { bindings },
      } = response.value;

      t.log(bindings);

      // This
      t.is(bindings.length, 2);

      // TODO: Now marshal it back into a form like the original (get it into JSON-LD)
    }
  });
});

test("named graph operations", async (t) => {
  await with_temp_named_graph(endpoint, async (graph) => {
    const iri = "http://test/S";

    // Insert some data for that endpoint
    const insert_data = serialize(
      contextualize(
        {
          type: "insert_data",
          data: `<${iri}> <http://test/p> "hello"`,
        },
        graph
      )
    );
    const inserted = await send({
      endpoint,
      operation: { update: insert_data },
    });
    t.assert(inserted.success);

    // Now confirm that it's there by fetching it back
    const read_back = serialize(
      contextualize(
        {
          type: "select",
          vars: "*",
          where: `<${iri}> ?p ?o`,
          limit: 10,
        },
        graph
      )
    );
    const select_result = await send({
      endpoint,
      operation: { query: read_back },
    });
    t.assert(select_result.success);
    if (!select_result.success) {
      t.log(
        "SELECT ERROR",
        select_result.error,
        select_result.request.operation
      );
      t.fail();
    } else {
      const { response } = select_result;
      t.assert(response);
      if (response.format === "results" && !("boolean" in response.value)) {
        t.log(response.value.results.bindings);
        t.is(response.value.results.bindings.length, 1);
        const [binding] = response.value.results.bindings;
        t.assert(binding);
        t.deepEqual(binding, {
          p: { type: "uri", value: "http://test/p" },
          o: { type: "literal", value: "hello" },
        });
      } else t.fail();
    }

    // Do the same using CONSTRUCT
    const reconstruct = serialize(
      contextualize(
        {
          type: "construct",
          pattern: `<${iri}> ?p ?o`,
          where: "?s ?p ?o",
          limit: 10,
        },
        graph
      )
    );
    const construct_result = await formatted("json_ld", {
      endpoint,
      operation: { query: reconstruct },
    });
    if (!construct_result.context.success) {
      t.log(construct_result);
      throw new Error("CONSTRUCT query failed");
    }
    t.log("CONSTRUCT result", construct_result.value);
    const reconstructed = { ...construct_result.value };
    const expanded = await jsonld.expand(reconstructed);
    t.log("EXPANDED", expanded);
    t.is(expanded[0]["@id"], iri);
    t.deepEqual(expanded[0]["http://test/p"], [
      {
        "@value": "hello",
      },
    ]);
  });
});

test.skip("list all things of a given type", async (t) => {
  const type_iri = "https://dev.thread.org/vocab/User";
  const select = `SELECT * WHERE { ?s a <${type_iri}> }`;
  const result = await send({ endpoint, operation: { query: select } });
  if (result.success) {
    t.log("RESULT RESPONSE", result.response);
  }
  t.fail("WIP");
});

test("json ld operations", async (t) => {
  // save and restore
  const touchpoints = [
    {
      id: 1,
      participants: [
        {
          id: 3,
          first_name: "Rob",
          last_name: "Koch",
        },
        {
          id: 4,
          first_name: "Marcus",
          last_name: "Edwards",
        },
      ],
    },
  ];
  const rdf_agnostic_object = {
    first_name: "Joe",
    last_name: "Ezeh",
    email: "jezeh@mindgrub.com",
    role: "Staff",
    touchpoints,
  };

  const person_context = {
    "@base": "https://app.qa.mindgrb.io/",
    "@vocab": "",
    thread: "https://app.dev.thread.mindgrb.io/",
    entities: "entities/",
    tp: "activity/",
    user: "users/",
    participants: "hasParticipant",
  };
  const touchpoint_context = person_context;

  const person_with_context = {
    "@context": person_context,
    ...rdf_agnostic_object,
  };
  const touchpoint_with_context = {
    "@context": touchpoint_context,
    ...rdf_agnostic_object,
  };

  const person_expanded = await jsonld.expand(person_with_context);
  t.log(`expanded`, person_expanded);

  const person_compacted = await jsonld.compact(
    person_expanded,
    person_context
  );

  // make test data
  // with an entity of some type
  const test_jsonld = {};
  const test_jsonld_context = {};

  t.pass("OKAYY");
});
