// Remove JSON-LD keywords from a compacted JSON-LD object.
// This is a convenience function for test methods.
export const strip_jsonld_header = <T extends object>(compacted: T): T =>
  Object.fromEntries(
    Object.entries(compacted).filter(
      ([key]) => key !== "@context" && key !== "@type"
    )
  ) as T;

export const strip_jsonld_context = <T extends object>(compacted: T): T =>
  Object.fromEntries(
    Object.entries(compacted).filter(([key]) => key !== "@context")
  ) as T;
