import * as assert from "assert";
import type { ExecutionContext } from "ava";

/**
 * Assert that a given record value is equivalent to an expected value.
 *
 * Multivalued properties are compared without respect to order.
 */
export const assert_equal_records = (
  t: ExecutionContext,
  got: object,
  expect: object
) => {
  try {
    assert.deepStrictEqual(
      new Set(Object.keys(got)),
      new Set(Object.keys(expect)),
      "Expected same key sets"
    );
  } catch (error) {
    t.log("EXPECT", expect, "GOT", got);
    throw error;
  }

  for (const [key, value] of Object.entries(expect)) {
    const got_value = got[key];
    if (Array.isArray(value)) {
      if (!Array.isArray(got_value)) {
        t.log("EXPECT PROPERTY", got["id"], key, value, "GOT", got_value);
        t.fail("Expected array");
        continue;
      }
      if (value.length !== got_value.length) {
        t.log("EXPECT PROPERTY", got["id"], key, value, "GOT", got_value);
        throw new Error(`Expected arrays of same length`);
      }
      assert.deepStrictEqual(new Set(got_value), new Set(value));
    } else {
      t.deepEqual(value, got[key]);
    }
  }
};

/**
 * Assert that a given record list of records is equivalent to an expected list.
 *
 * `undefined` values are treated as empty lists.  The lists can be in any
 * order.  Matching is done by `id` property.
 *
 * Comparison of individual records is then done as in `assert_equal_records`.
 */
export const assert_equal_record_sets = (
  t: ExecutionContext,
  got: readonly any[] | undefined,
  expect: readonly any[] | undefined
) => {
  got ??= [];
  expect ??= [];

  assert.deepStrictEqual(
    new Set(got.map((_) => _.id)),
    new Set(expect.map((_) => _.id)),
    "Expected same ID's"
  );

  for (const record of expect) {
    const id = record.id;
    const corresponding = got.find((_) => _.id === id);
    if (!corresponding) {
      t.fail(`No record corresponding to ${id}`);
      continue;
    }
    assert_equal_records(t, corresponding, record);
  }
};
