import { formatted } from "@swymbase/sparql-protocol-client";
import { maybe_contextualize, serialize } from "@swymbase/sparql-rest";

export const is_node_in = async (
  iri: string,
  endpoint: string,
  named_graph?: string
): Promise<boolean | undefined> => {
  // This function was crashing Jena during tests.
  return false;
  return (
    await formatted("boolean", {
      endpoint,
      operation: {
        query: serialize(
          maybe_contextualize(
            {
              type: "ask",
              where: `{ <${iri}> ?p1 ?o } UNION { ?s ?p2 <${iri}> }`,
            },
            named_graph
          )
        ),
      },
    })
  ).value;
};
