const { SPARQL_ENDPOINT } = process.env;

if (!SPARQL_ENDPOINT)
  throw new Error("Tests require SPARQL_ENDPOINT to be set in environment.");

export const endpoint = SPARQL_ENDPOINT;
