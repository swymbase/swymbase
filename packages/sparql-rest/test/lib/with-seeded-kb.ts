import type { MetaInterfaces } from "@swymbase/metatype-core";
import { send } from "@swymbase/sparql-protocol-client";
import {
  contextualize,
  generate_converse_triples,
  KBRecordInterface,
  make_kb_interface,
  serialize,
  with_temp_named_graph,
} from "@swymbase/sparql-rest";
import type { ExecutionContext } from "ava";
import { endpoint } from "./config";

// Create a sandbox knowledge base with the given schema and initial data and
// execute the given callback against it.
//
// Assumes the configured endpoint is used.
export const with_seeded_kb = async (
  t: ExecutionContext,
  preamble: string,
  test_data: string,
  schema: MetaInterfaces,
  fn: (kb: KBRecordInterface<MetaInterfaces>) => Promise<void>
) =>
  with_temp_named_graph(endpoint, async (named_graph) => {
    const inverses = generate_converse_triples(schema);
    const data =
      inverses.join(".\n") + (inverses.length ? ".\n" : "") + test_data;
    const statement = contextualize(
      { type: "insert_data", preamble, data },
      named_graph
    );
    const update = serialize(statement);
    const response = await send({ endpoint, operation: { update } });
    if (!response.success) {
      t.log("SEND RESPONSE", response.http_response);
      throw new Error(`precondition failed writing: ${response.code}`);
    }

    const kb = make_kb_interface(endpoint, schema, { named_graph });
    await fn(kb);
  });
