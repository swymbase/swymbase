import { InterfacesFrom, metatypes } from "@swymbase/metatype-core";
import { assert_success, formatted } from "@swymbase/sparql-protocol-client";
import { with_temp_named_graph, write_record } from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "./lib/config";

const TEST_TYPES = metatypes({
  Group: {
    "@type": "https://example.com/vocab/Group",
    instance_namespace: `https://example.com/entities/Group/`,
    properties: {
      name: {
        term: "https://example.com/vocab/name",
        type: "string",
      },
      public: {
        term: "https://example.com/vocab/public",
        type: "boolean",
      },
    },
  },
});

type TestTypes = InterfacesFrom<typeof TEST_TYPES>;

test("creates a record with the given ID and type and properties in a named graph", async (t) => {
  await with_temp_named_graph(endpoint, async (graph_iri) => {
    const given /*: TestTypes["Group"]*/ = {
      id: `${TEST_TYPES.Group.instance_namespace}TheCharlatans`,
      name: "The Charlatans",
      public: false,
    };

    // === OPERATION
    const result = await write_record(TEST_TYPES.Group, given, endpoint, {
      named_graph: graph_iri,
    });
    assert_success(t, result, "main operation");
    if (!result.success) return; // TODO: see note in assert_success

    // === POSTCONDITIONS

    // Assert that the graph now contains the records
    const triples = `
GRAPH <${graph_iri}> {
<${given.id}> a <${TEST_TYPES.Group["@type"]}> ;
  <${TEST_TYPES.Group.properties.name.term}> "The Charlatans" ;
  <${TEST_TYPES.Group.properties.public.term}> false
}
`;
    const ask = `ASK WHERE { ${triples} }`;
    const ask_result = await formatted("boolean", {
      endpoint,
      operation: { query: ask },
    });
    assert_success(t, ask_result.context, "postcondition: check triples");

    t.true(ask_result.value);
  });
});
