import { EntitiesFrom, metatypes } from "@swymbase/metatype-core";
import {
  assert_success,
  formatted,
  send,
} from "@swymbase/sparql-protocol-client";
import {
  get_record,
  with_temp_named_graph,
  with_temp_resource,
  delete_record,
  record_exists,
} from "@swymbase/sparql-rest";
import * as assert from "assert";
import test, { ExecutionContext } from "ava";
import { endpoint } from "./lib/config";

const XSD = "http://www.w3.org/2001/XMLSchema#";

const TEST_TYPES = metatypes({
  Play: {
    "@type": "https://example.com/vocab/Play",
    instance_namespace: `https://example.com/entities/Play/`,
    properties: {
      title: {
        term: "https://example.com/vocab/title",
        type: "string",
      },
      subtitle: {
        term: "https://example.com/vocab/subtitle",
        type: "string",
      },
      year: {
        term: "https://example.com/vocab/year",
        type: "number",
        comment: "Approximate year of composition",
      },
      roles: {
        term: "https://example.com/vocab/hasRole",
        multivalued: true,
        type: "object",
        relationship: true,
      },
    },
  },
  Role: {
    "@type": "https://example.com/vocab/Role",
    instance_namespace: `https://example.com/entities/Role/`,
    properties: {
      label: {
        term: "https://example.com/vocab/label",
        type: "string",
        multivalue: true,
      },
    },
  },
});

type TestTypes = EntitiesFrom<typeof TEST_TYPES>;

const delete_test = async (
  t: ExecutionContext,
  iris: {
    play_iri: string;
    role1_iri: string;
    role2_iri: string;
    graph_iri?: string;
  }
) => {
  const { play_iri, role1_iri, role2_iri, graph_iri } = iris;
  t.log({ play_iri, role1_iri, role2_iri });
  const scope = (expr) =>
    graph_iri ? `GRAPH <${graph_iri}> { ${expr} }` : expr;

  // === SETUP

  // We will write these triples outright.  This is equivalent to what we
  // would expect if a record had been written using `post_record`
  const triples = `
<${role1_iri}> a :Role ; :label "Rosalind" .
<${role2_iri}> a :Role ; :label "Orlando" .
<${play_iri}> a :Play ;
  :title "As You Like It" ;
  :subtitle "Or, What you will" ;
  :year "1599"^^<${XSD}float> ;
  :hasRole <${role1_iri}> ;
  :hasRole <${role2_iri}> 
.
`;

  // === PRECONDITIONS
  const update = `prefix : <https://example.com/vocab/>
INSERT DATA { ${scope(triples)} }`;
  const write_result = await send({ endpoint, operation: { update } });
  assert_success(t, write_result, "precondition: write triples");

  // Assert that the graph now contains the records
  const get_before_result = await get_record(
    TEST_TYPES,
    "Play",
    play_iri,
    endpoint,
    { named_graph: graph_iri }
  );
  assert_success(t, get_before_result.context, "precondition: get test play");
  if (!get_before_result.value) return; // TODO: see note in assert_success
  const { roles, ...got_play } = get_before_result.value;
  delete got_play["@context"];
  t.deepEqual(got_play, {
    id: play_iri,
    type: TEST_TYPES.Play["@type"],
    title: "As You Like It",
    subtitle: "Or, What you will",
    year: 1599,
  });
  assert.deepEqual(
    new Set(roles),
    new Set([{ id: role1_iri }, { id: role2_iri }])
  );

  // === OPERATION

  const result = await delete_record(
    TEST_TYPES,
    "Play",
    play_iri,
    endpoint,
    {
      named_graph: graph_iri,
    },
    {}
  );
  assert_success(t, result, "main operation");

  // === POSTCONDITIONS

  // Assert that the graph now contains the updated record
  const subject_facts = `SELECT * WHERE { ${scope(`<${play_iri}> ?p ?o`)} }`;
  const select_after_result = await formatted("select_results", {
    endpoint,
    operation: { query: subject_facts },
  });
  assert_success(t, select_after_result.context, "postcondition: select");
  if (!select_after_result.value) return; // TODO: see note in assert_success

  const expect_after = [] as const;
  t.log("EXPECTED AFTER", expect_after);
  t.log("SELECTED AFTER", select_after_result.value.results.bindings);
  assert.deepStrictEqual(
    new Set(select_after_result.value.results.bindings),
    new Set(expect_after)
  );
  // The roles themselves should still exist
  t.true(
    (
      await record_exists(TEST_TYPES, "Role", role1_iri, endpoint, {
        named_graph: graph_iri,
      })
    ).value
  );
  t.true(
    (
      await record_exists(TEST_TYPES, "Role", role2_iri, endpoint, {
        named_graph: graph_iri,
      })
    ).value
  );
};

test("deletes a record with the given ID and type in the default graph", (t) =>
  with_temp_resource({ endpoint }, (play_iri) =>
    with_temp_resource({ endpoint }, (role1_iri) =>
      with_temp_resource({ endpoint }, (role2_iri) =>
        delete_test(t, { play_iri, role1_iri, role2_iri })
      )
    )
  ));

test("deletes a record with the given ID and type in a named graph", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    delete_test(t, {
      play_iri: `${TEST_TYPES.Play.instance_namespace}AsYouLikeIt`,
      role1_iri: `${TEST_TYPES.Role.instance_namespace}Rosalind`,
      role2_iri: `${TEST_TYPES.Role.instance_namespace}Orlando`,
      graph_iri,
    })
  ));
