import { InterfacesFrom, metatypes } from "@swymbase/metatype-core";
import {
  assert_success,
  formatted,
  send,
} from "@swymbase/sparql-protocol-client";
import { post_record, with_temp_named_graph } from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "./lib/config";

const TEST_TYPES = metatypes({
  Person: {
    "@type": "https://example.com/vocab/Person",
    instance_namespace: `https://example.com/entities/Person/`,
    properties: {
      name: {
        term: "https://example.com/vocab/name",
        type: "string",
      },
      optimist: {
        term: "https://example.com/vocab/optimist",
        type: "boolean",
      },
    },
  },
});

type TestTypes = InterfacesFrom<typeof TEST_TYPES>;

// NEED TO TEST:
// - non-multivalued properties are treated as such

test("creates a record with the given type and properties in the default graph", async (t) => {
  const given_person: TestTypes["Person"] = {
    name: "Wiley",
    optimist: true,
  };

  // === OPERATION
  const result = await post_record(
    TEST_TYPES,
    "Person",
    given_person,
    endpoint
  );
  assert_success(t, result, "main operation");
  if (!result.success) return; // TODO: see note in assert_success

  // === POSTCONDITIONS

  const record_iri = result.new_id;

  // Assert that the graph now contains the records
  const triples = `
<${record_iri}> a <${TEST_TYPES.Person["@type"]}> ;
  <${TEST_TYPES.Person.properties.name.term}> "Wiley" ;
  <${TEST_TYPES.Person.properties.optimist.term}> true
`;
  const ask = `ASK WHERE { ${triples} }`;
  const ask_result = await formatted("boolean", {
    endpoint,
    operation: { query: ask },
  });
  assert_success(t, ask_result.context, "postcondition: check triples");

  t.true(ask_result.value);

  // === CLEANUP
  const cleanup = `DELETE DATA { ${triples} }`;
  const cleanup_result = await send({
    endpoint,
    operation: { update: cleanup },
  });
  assert_success(t, cleanup_result, "cleanup: delete triples");
});

test("creates a record with the given type and properties in a named graph", async (t) => {
  await with_temp_named_graph(endpoint, async (graph_iri) => {
    const given_person: TestTypes["Person"] = {
      name: "Lollie",
      optimist: false,
    };

    // === OPERATION
    const result = await post_record(
      TEST_TYPES,
      "Person",
      given_person,
      endpoint,
      { named_graph: graph_iri }
    );
    assert_success(t, result, "main operation");
    if (!result.success) return; // TODO: see note in assert_success

    // === POSTCONDITIONS

    const record_iri = result.new_id;

    // Assert that the graph now contains the records
    const triples = `
GRAPH <${graph_iri}> {
<${record_iri}> a <${TEST_TYPES.Person["@type"]}> ;
  <${TEST_TYPES.Person.properties.name.term}> "Lollie" ;
  <${TEST_TYPES.Person.properties.optimist.term}> false
}
`;
    const ask = `ASK WHERE { ${triples} }`;
    const ask_result = await formatted("boolean", {
      endpoint,
      operation: { query: ask },
    });
    assert_success(t, ask_result.context, "postcondition: check triples");

    t.true(ask_result.value);
  });
});
