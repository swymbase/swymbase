import { FilterClause, map_terms } from "@swymbase/sparql-rest";
import test, { ExecutionContext } from "ava";
import { RDFS_LABEL, TEST_TYPES } from "./lib/test-schema";

interface Case {
  given: FilterClause;
  expect: FilterClause;
}

const test_predicate_mapping = (t: ExecutionContext, test_case: Case) => {
  const { given, expect } = test_case;
  t.log("GIVEN FILTER", given);
  t.log("EXPECT FILTER", expect);
  const got = map_terms(given, TEST_TYPES);
  t.log("GOT FILTER", got);
  t.deepEqual(got, expect);
};

test("no effect on closed expressions", test_predicate_mapping, {
  given: ["<", 1, 2],
  expect: ["<", 1, 2],
});

test("no effect on node references", test_predicate_mapping, {
  given: ["=", ["id", "Person:fake_id"], ["id", "preserved"]],
  expect: ["=", ["id", "Person:fake_id"], ["id", "preserved"]],
});

test("maps over less than", test_predicate_mapping, {
  given: ["<", ["this", "Person:name"], "Joe"],
  expect: ["<", ["this", "http://example.com/v/name"], "Joe"],
});

test("maps over equals", test_predicate_mapping, {
  given: ["=", ["this", "Person:rating"], 49],
  expect: ["=", ["this", "http://example.com/v/rating"], 49],
});

test("writes properties with converse normally", test_predicate_mapping, {
  given: ["=", ["this", "Invitation:sender"], ["id", "Joe"]],
  expect: ["=", ["this", "http://example.com/v/sender"], ["id", "Joe"]],
});

test("writes converse properties as inverse path", test_predicate_mapping, {
  given: ["=", ["this", "Person:sent"], ["id", "invite1"]],
  expect: [
    "=",
    ["this", ["^", "http://example.com/v/sender"]],
    ["id", "invite1"],
  ],
});

test("maps over 'exists'", test_predicate_mapping, {
  given: ["exists", ["this", "Invitation:sender"]],
  expect: ["exists", ["this", "http://example.com/v/sender"]],
});

test("maps over logical 'or'", test_predicate_mapping, {
  given: [
    "or",
    ["exists", ["this", "Person:name"]],
    ["exists", ["this", "Person:label"]],
  ],
  expect: [
    "or",
    ["exists", ["this", "http://example.com/v/name"]],
    ["exists", ["this", RDFS_LABEL]],
  ],
});

test("maps over logical 'or' with nested 'and'", test_predicate_mapping, {
  given: [
    "or",
    ["exists", ["this", "Person:name"]],
    [
      "and",
      ["exists", ["this", "Person:first_name"]],
      ["exists", ["this", "Person:last_name"]],
    ],
  ],
  expect: [
    "or",
    ["exists", ["this", "http://example.com/v/name"]],
    [
      "and",
      ["exists", ["this", "https://schema.org/givenName"]],
      ["exists", ["this", "https://schema.org/familyName"]],
    ],
  ],
});

test("maps over negated expression", test_predicate_mapping, {
  given: ["not", ["=", ["this", "Person:is_admin"], true]],
  expect: ["not", ["=", ["this", "http://example.com/v/is_admin"], true]],
});

test("maps over sequence paths", test_predicate_mapping, {
  given: [
    "=",
    ["this", ["/", "Person:loves", "Person:likes"]],
    ["id", "ex:Bob"],
  ],
  expect: [
    "=",
    ["this", ["/", "http://example.com/v/loves", "http://example.com/v/likes"]],
    ["id", "ex:Bob"],
  ],
});

test("maps over `in` expressions", test_predicate_mapping, {
  given: ["in", ["this", "Person:name"], ["Person:name", "trick"]],
  expect: [
    "in",
    ["this", "http://example.com/v/name"],
    ["Person:name", "trick"],
  ],
});

test("`this` reference is unchanged", test_predicate_mapping, {
  given: [
    "in",
    ["this"],
    [
      ["id", "http://example.com/one"],
      ["id", "http://example.com/two"],
    ],
  ],
  expect: [
    "in",
    ["this"],
    [
      ["id", "http://example.com/one"],
      ["id", "http://example.com/two"],
    ],
  ],
});

test.todo("error when term is undefined");
