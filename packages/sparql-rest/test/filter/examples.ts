// No real tests, just some examples to ensure that types check.
import { Filter } from "@swymbase/sparql-rest";
import test from "ava";

// AVA will complain if there's no test in this module.
test("examples typecheck", (t) => t.pass());

export const examples: Record<string, Filter> = {
  "name is John": [["=", ["this", "User:name"], "John"]],

  "name matches 'jo'": [["match", ["this", "User:name"], "jo"]],

  "name matches 'jo' case-insensitively": [
    ["match", ["this", "User:name"], "jo", "i"],
  ],

  "first name or last name matches 'jo'": [
    ["match", ["this", ["|", "User:first_name", "User:last_name"]], "jo"],
  ],

  "reimbursed amount is less than requested amount": [
    [
      "<",
      ["this", "Expense:reimbursed_amount"],
      ["this", "Expense:requested_amount"],
    ],
  ],

  // TODO: not-exists is currently not supported
  // "has no connections": [
  //   ["not", ["exists", ["this", ["^", "Connection:user"]]]],
  // ],

  "is connected to Alice": [
    [
      "=",
      ["this", ["/", ["^", "Connection:user"], "Connection:user"]],
      ["id", "http://ex/Alice"],
    ],
  ],

  "is connected to Alice, Bob, or Carol": [
    [
      "in",
      ["this", ["/", ["^", "Connection:user"], "Connection:user"]],
      [
        ["id", "http://ex/Alice"],
        ["id", "http://ex/Bob"],
        ["id", "http://ex/Carol"],
      ],
    ],
  ],

  // Verbose alternative to in list
  "is connected to Alice or Bob": [
    [
      "or",
      [
        "=",
        ["this", ["/", ["^", "Connection:user"], "Connection:user"]],
        ["id", "http://ex/Alice"],
      ],
      [
        "=",
        ["this", ["/", ["^", "Connection:user"], "Connection:user"]],
        ["id", "http://ex/Bob"],
      ],
    ],
  ],

  // Not so sure about this one
  "is connected to Alice or *not* connected to Bob": [
    [
      "or",
      [
        "=",
        ["this", ["/", ["^", "Connection:user"], "Connection:user"]],
        ["id", "http://ex/Alice"],
      ],
      [
        "not",
        [
          "=",
          ["this", ["/", ["^", "Connection:user"], "Connection:user"]],
          ["id", "http://ex/Bob"],
        ],
      ],
    ],
  ],
};
