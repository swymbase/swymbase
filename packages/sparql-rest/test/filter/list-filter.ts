import test from "ava";
import { filter_test } from "./lib/filter";
import { COMMON_FACTS } from "./lib/test-data";
import { EN, TEST_TYPES } from "./lib/test-schema";

import * as util from "util";
util.inspect.defaultOptions.depth = 10;

test("empty filter has no effect", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people
  select_type: "Person",
  filter: null,
  expect: [`${EN}Alice`, `${EN}Bob`, `${EN}Carol`],
});

test("closed true clause has no effect", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people where 0 = 0
  select_type: "Person",
  filter: [["=", 0, 0]],
  expect: [`${EN}Alice`, `${EN}Bob`, `${EN}Carol`],
});

test("closed false clause eliminates all records", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people where 0 = 1
  select_type: "Person",
  filter: [["=", 0, 1]],
  expect: [],
});

test("this reference equals literal", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people whose identity is Alice
  select_type: "Person",
  filter: [["=", ["this"], ["id", `${EN}Alice`]]],
  expect: [`${EN}Alice`],
});

test("this reference equals property reference", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who trust themselves
  select_type: "Person",
  filter: [["=", ["this"], ["this", "Person:trusts"]]],
  expect: [`${EN}Alice`],
});

test("this reference not equals property reference", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who do not trust themselves
  select_type: "Person",
  filter: [["not", ["=", ["this"], ["this", "Person:trusts"]]]],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

test("property exists", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who like anyone
  select_type: "Person",
  filter: [["exists", ["this", "Person:likes"]]],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

test("inverse property exists", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who are the sender of an invitation
  select_type: "Person",
  filter: [["exists", ["this", ["^", "Invitation:sender"]]]],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

test("pseudo-converse property exists", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who have sent an invitation
  select_type: "Person",
  filter: [["exists", ["this", "Person:sent"]]],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

test("property sequence exists", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who love anyone who has a rating
  select_type: "Person",
  filter: [["exists", ["this", ["/", "Person:loves", "Person:rating"]]]],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

// TODO: may add not-exists to grammar
test.skip("property not exists", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all users who do not like anyone
  select_type: "Person",
  // filter: [["not", ["exists", ["this", "Person:likes"]]]],
  filter: [["exists", ["this", "Person:likes"]]],
  expect: [`${EN}Alice`],
});

test("property equals id", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who love Alice
  select_type: "Person",
  filter: [["=", ["this", "Person:loves"], ["id", `${EN}Alice`]]],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

test("property greater than scalar", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people with a rating greater than 30
  select_type: "Person",
  filter: [[">", ["this", "Person:rating"], 30]],
  expect: [`${EN}Alice`],
});

test("property less than scalar", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people with a rating less than 42
  select_type: "Person",
  filter: [["<", ["this", "Person:rating"], 42]],
  expect: [`${EN}Bob`],
});

test("property less than property", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people whose rating is less than their target rating
  select_type: "Person",
  filter: [["<", ["this", "Person:rating"], ["this", "Person:target_rating"]]],
  expect: [`${EN}Alice`],
});

test("property in list of scalars", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people whose name is ‘Alice’,  ‘Bob’ or ‘Joe’
  select_type: "Person",
  filter: [["in", ["this", "Person:name"], ["Alice", "Bob", "Joe"]]],
  expect: [`${EN}Alice`, `${EN}Bob`],
});

test("property in list of ids", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who like either Bob or Carol
  select_type: "Person",
  filter: [
    [
      "in",
      ["this", "Person:likes"],
      [
        ["id", `${EN}Bob`],
        ["id", `${EN}Carol`],
      ],
    ],
  ],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

test("property matches literal", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people whose name begins with A or C
  select_type: "Person",
  filter: [["match", ["this", "Person:name"], "^[AC]"]],
  expect: [`${EN}Alice`, `${EN}Carol`],
});

test("property matches literal with escaped control character", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people whose alias begins with O+S
  select_type: "Person",
  filter: [["match", ["this", "Person:alias"], "^O\\+S"]],
  expect: [`${EN}Bob`],
});

test("property not equal to id", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who do not love Alice
  select_type: "Person",
  filter: [["not", ["=", ["this", "Person:loves"], ["id", `${EN}Alice`]]]],
  expect: [`${EN}Alice`],
});

test("property not less than value (sparse)", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who do not have a rating less than 50
  select_type: "Person",
  filter: [["not", ["<", ["this", "Person:rating"], 50]]],
  expect: [`${EN}Alice`, `${EN}Carol`],
});

test("property not greater than value (sparse)", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who do not have a rating greater than 50
  select_type: "Person",
  filter: [["not", [">", ["this", "Person:rating"], 50]]],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

test("property not greater than value", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who do not have a target rating greater than 50
  select_type: "Person",
  filter: [["not", [">", ["this", "Person:target_rating"], 50]]],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

test("inverse property equals id", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who are loved by Alice
  select_type: "Person",
  filter: [["=", ["this", ["^", "Person:loves"]], ["id", `${EN}Alice`]]],
  expect: [`${EN}Carol`],
});

test("sequence path equals id", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who love someone who likes Bob
  select_type: "Person",
  filter: [
    ["=", ["this", ["/", "Person:loves", "Person:likes"]], ["id", `${EN}Bob`]],
  ],
  expect: [`${EN}Alice`],
});

test("sequence path with inversion step equals id", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who love someone who is liked by Bob
  select_type: "Person",
  filter: [
    [
      "=",
      ["this", ["/", "Person:loves", ["^", "Person:likes"]]],
      ["id", `${EN}Bob`],
    ],
  ],
  expect: [`${EN}Alice`],
});

test("alternative path equals id", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  select_type: "Person",
  // Get all people who love or like Carol
  filter: [
    [
      "=",
      ["this", ["|", "Person:loves", "Person:likes"]],
      ["id", `${EN}Carol`],
    ],
  ],
  expect: [`${EN}Alice`, `${EN}Bob`],
});

test("multiple filters are applied conjunctively", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who love Alice and have an alias
  select_type: "Person",
  filter: [
    ["exists", ["this", "Person:alias"]],
    ["=", ["this", "Person:loves"], ["id", `${EN}Alice`]],
  ],
  expect: [`${EN}Bob`],
});

test("complex path does not equal id", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who have not sent Bob an invitation
  select_type: "Person",
  filter: [
    [
      "not",
      [
        "=",
        ["this", ["/", ["^", "Invitation:sender"], "Invitation:recipient"]],
        ["id", `${EN}Bob`],
      ],
    ],
  ],
  expect: [`${EN}Alice`, `${EN}Bob`],
});

test("complex path does not equal id (part 2)", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people to whom Bob has not sent an invitation
  select_type: "Person",
  filter: [
    [
      "not",
      [
        "=",
        ["this", ["/", ["^", "Invitation:recipient"], "Invitation:sender"]],
        ["id", `${EN}Bob`],
      ],
    ],
  ],
  expect: [`${EN}Bob`, `${EN}Carol`],
});

test(
  "conjunction of two clauses where complex path does not equal id (Bob)",
  filter_test,
  {
    given_facts: COMMON_FACTS,
    types: TEST_TYPES,
    // Get all people who have not sent or received an invitation from Bob
    select_type: "Person",
    filter: [
      [
        "not",
        [
          "=",
          ["this", ["/", ["^", "Invitation:sender"], "Invitation:recipient"]],
          ["id", `${EN}Bob`],
        ],
      ],
      [
        "not",
        [
          "=",
          ["this", ["/", ["^", "Invitation:recipient"], "Invitation:sender"]],
          ["id", `${EN}Bob`],
        ],
      ],
    ],
    expect: [`${EN}Bob`],
  }
);

test(
  "conjunction of two clauses where complex path does not equal id (Alice)",
  filter_test,
  {
    given_facts: COMMON_FACTS,
    types: TEST_TYPES,
    // Get all people who have not sent or received an invitation from Alice
    select_type: "Person",
    filter: [
      [
        "not",
        [
          "=",
          ["this", ["/", ["^", "Invitation:sender"], "Invitation:recipient"]],
          ["id", `${EN}Alice`],
        ],
      ],
      [
        "not",
        [
          "=",
          ["this", ["/", ["^", "Invitation:recipient"], "Invitation:sender"]],
          ["id", `${EN}Alice`],
        ],
      ],
    ],
    expect: [`${EN}Alice`, `${EN}Carol`],
  }
);

test("conjunction of three complex clauses with negation", filter_test, {
  given_facts: COMMON_FACTS,
  types: TEST_TYPES,
  // Get all people who have not sent or received an invitation from Alice and are not in a group with Alice
  select_type: "Person",
  filter: [
    [
      "not",
      [
        "=",
        ["this", ["/", ["^", "Group:has_member"], "Group:has_member"]],
        ["id", `${EN}Alice`],
      ],
    ],
    [
      "not",
      [
        "=",
        ["this", ["/", ["^", "Invitation:sender"], "Invitation:recipient"]],
        ["id", `${EN}Alice`],
      ],
    ],
    [
      "not",
      [
        "=",
        ["this", ["/", ["^", "Invitation:recipient"], "Invitation:sender"]],
        ["id", `${EN}Alice`],
      ],
    ],
  ],
  expect: [],
});
