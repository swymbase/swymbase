import { metatypes } from "@swymbase/metatype-core";

export const RDFS_LABEL = "http://www.w3.org/2000/01/rdf-schema#";
export const VO = "http://example.com/v/"; // vocabulary
export const EN = "http://example.com/e/"; // instances

export const TEST_TYPES = metatypes({
  Person: {
    "@type": `${VO}Person`,
    properties: {
      label: {
        term: RDFS_LABEL,
        type: "string",
      },
      name: {
        term: `${VO}name`,
        type: "string",
      },
      first_name: {
        term: "https://schema.org/givenName",
        type: "string",
      },
      last_name: {
        term: "https://schema.org/familyName",
        type: "string",
      },
      alias: {
        term: `${VO}alias`,
        type: "string",
      },
      trusts: {
        term: `${VO}trusts`,
        type: "object",
        relationship: true,
        multivalued: true,
      },
      rating: {
        term: `${VO}rating`,
        type: "number",
      },
      target_rating: {
        term: `${VO}target_rating`,
        type: "number",
      },
      loves: {
        term: `${VO}loves`,
        type: "string",
        multivalued: true,
        relationship: true,
      },
      likes: {
        term: `${VO}likes`,
        type: "string",
        multivalued: true,
        relationship: true,
      },
      member_of: {
        term: `${VO}member_of`,
        type: "object",
        relationship: true,
        converse_property: `${VO}has_member`,
      },
      sent: {
        term: `${VO}sent`,
        type: "object",
        relationship: true,
        converse_property: `${VO}sender`,
      },
      received: {
        term: `${VO}received`,
        type: "object",
        relationship: true,
        converse_property: `${VO}recipient`,
      },
      is_admin: {
        term: `${VO}is_admin`,
        type: "boolean",
      },
    },
  },
  Group: {
    "@type": `${VO}Group`,
    properties: {
      name: {
        term: RDFS_LABEL,
        type: "string",
      },
      has_member: {
        term: `${VO}has_member`,
        type: "object",
        relationship: true,
      },
      exclusive: {
        term: `${VO}exclusive`,
        type: "boolean",
      },
    },
  },
  Invitation: {
    "@type": `${VO}Invitation`,
    properties: {
      sender: {
        term: `${VO}sender`,
        type: "object",
        relationship: true,
      },
      recipient: {
        term: `${VO}recipient`,
        type: "object",
        relationship: true,
      },
    },
  },
});
