import type { MetaInterfaces } from "@swymbase/metatype-core";
import { serialize_group_graph_pattern_part } from "@swymbase/sparql-ast";
import { assert_success, send } from "@swymbase/sparql-protocol-client";
import {
  Filter,
  filter_to_sparql,
  make_kb_interface,
  with_temp_named_graph,
} from "@swymbase/sparql-rest";
import * as assert from "assert";
import type { ExecutionContext } from "ava";
import { endpoint } from "../../lib/config";
import { EN, TEST_TYPES, VO } from "./test-schema";

interface FilterTestCase<S extends MetaInterfaces, K = keyof S> {
  // Initial data to query again, in Turtle format with assumed base and prefix.
  readonly given_facts: string;

  // Schema to use for the test.  If not provided, a default is used
  readonly types?: S;

  // One of the types defined in the meta interfaces
  readonly select_type: K;

  readonly filter: Filter | null;

  // Set of ID's of records expected to be included in the filter
  // For filter, only interested in *which* records are selected, not the complete
  // values that were returned.
  readonly expect: readonly string[];
}

export const filter_test = async <S extends MetaInterfaces, K = keyof S>(
  t: ExecutionContext,
  test: FilterTestCase<S, K>
): Promise<void> => {
  const { expect, filter, given_facts, select_type, types = TEST_TYPES } = test;
  await with_temp_named_graph(endpoint, async (named_graph) => {
    t.log("GIVEN FILTER:", filter);
    t.log("EXPECT MATCHES:", expect);

    const kb = make_kb_interface(endpoint, types, { named_graph });

    // === SETUP
    const update = `
base <${EN}>
prefix : <${VO}>
insert data {
  graph <${named_graph}> {
    ${given_facts}
  }
}`;
    const write_response = await send({ endpoint, operation: { update } });
    assert_success(t, write_response, "error writing preconditions");

    // Do some of the work that list does, for debug
    const rewritten = filter_to_sparql(filter || [], { subject_var: "thing" });
    t.log("REWRITTEN", rewritten);

    const sparql = rewritten.map(serialize_group_graph_pattern_part);
    t.log("SPARQL", sparql);

    // === OPERATION
    // TS: list_records expects type to be pegged to types defined on KB
    const list_response = await kb.list_records(select_type as any, undefined, {
      filter,
    });
    assert_success(t, list_response.context, "error listing records");
    t.log("OPERATION", list_response.context.request.operation);

    // === POSTCONDITIONS
    const actual = list_response.value!.map((_) => _.id);
    // Order-independent compare
    assert.deepStrictEqual(new Set(actual), new Set(expect));

    // If we got here, we passed (Node) assertion
    t.pass("OK!");
  });
};
