// Use in conjunction with `test-schema`
export const COMMON_FACTS = `
<Alice> a :Person
 ; :name "Alice"
 ; :loves <Carol> 
 ; :trusts <Alice>
 ; :rating 99 
 ; :target_rating 100 
.
<Bob> a :Person 
 ; :name "Bob"
 ; :alias "Beto"
 ; :alias "O+Shea"
 ; :loves <Alice>
 ; :likes <Carol> 
 ; :rating 30 
 ; :target_rating 30 
.
<Carol> a :Person
 ; :name "Carol"
 ; :loves <Alice>
 ; :trusts <Alice>
 ; :likes <Bob> 
 ; :target_rating 10
.
<SecretSociety> a :Group
 ; :has_member <Alice>, <Carol>
.
<BobAliceInvite> a :Invitation
 ; :sender <Bob>
 ; :recipient <Alice>
.
<CarolBobInvite> a :Invitation
 ; :sender <Carol>
 ; :recipient <Bob>
.
`;
