/*
 * Tests functions for rewriting filter expressions as SPARQL AST's.
 *
 * Technically, these tests target an implementation detail, in that there are
 * often several “correct” ways the expression can be rewritten.  This was
 * somewhat a scaffold to get to the actual correctness tests (chiefly,
 * `list-filter`); but they are somewhat helpful as an intermediate reference.
 *
 * Currently, tests only target individual filter clauses, since the clauses are
 * trivially aggregated into a filter (and they do not affect one another).
 *
 * “Open” expressions are those that include at least one `this` expression.
 *
 * “Closed” expressions have no `this` expression, and are generally not
 * meaningful as filters.  However they still exercise significant parts of the
 * rewrite logic and are thus tested before similar open variants.
 */
import type * as SPARQL from "@swymbase/sparql-ast";
import { FilterClause, filter_clause_to_sparql } from "@swymbase/sparql-rest";
import test, { ExecutionContext } from "ava";
import * as util from "util";

util.inspect.defaultOptions.depth = 10;

interface ClauseCase {
  readonly given: FilterClause;
  readonly expect: SPARQL.GroupGraphPatternPart;
}

const test_clause_rewrite = (t: ExecutionContext, test_case: ClauseCase) => {
  const { given, expect } = test_case;
  t.log("GIVEN FILTER", given);
  t.log("EXPECT SPARQL", expect);
  const got = filter_clause_to_sparql(given);
  t.log("GOT SPARQL", got);
  t.deepEqual(got, expect);
};

test("equals (closed, literal numbers)", test_clause_rewrite, {
  given: ["=", 0, 1],
  expect: ["basic", ["filter", ["=", 0, 1]]],
});

test("less than (closed, mixed literals)", test_clause_rewrite, {
  given: ["<", 400, "bar"],
  expect: ["basic", ["filter", ["<", 400, "bar"]]],
});

test("less than (open)", test_clause_rewrite, {
  given: ["<", ["this", "ex:value"], 42],
  expect: [
    "basic",
    ["pattern", ["var", "this"], ["id", "ex:value"], ["var", "a"]],
    ["filter", ["<", ["var", "a"], 42]],
  ],
});

test("equals with this reference", test_clause_rewrite, {
  given: ["=", ["this"], ["id", "ex:alpha"]],
  expect: ["basic", ["filter", ["=", ["var", "this"], ["id", "ex:alpha"]]]],
});

test("greater than (open, two this expressions)", test_clause_rewrite, {
  given: [">", ["this", "ex:min"], ["this", "ex:max"]],
  expect: [
    "basic",
    ["pattern", ["var", "this"], ["id", "ex:min"], ["var", "a"]],
    ["pattern", ["var", "this"], ["id", "ex:max"], ["var", "b"]],
    ["filter", [">", ["var", "a"], ["var", "b"]]],
  ],
});

test("equal (open, two this expressions)", test_clause_rewrite, {
  given: ["=", ["this", "ex:min"], ["this", "ex:max"]],
  expect: [
    "basic",
    ["pattern", ["var", "this"], ["id", "ex:min"], ["var", "a"]],
    ["pattern", ["var", "this"], ["id", "ex:max"], ["var", "b"]],
    ["filter", ["=", ["var", "a"], ["var", "b"]]],
  ],
});

// Yeah the binding isn't needed when the test is equality against a literal, but who cares
test.skip("equal (yields pattern only for literal)", test_clause_rewrite, {
  given: ["=", ["this", "ex:answer"], 42],
  expect: ["basic", ["pattern", ["var", "this"], ["id", "ex:answer"], 42]],
});

test("exists (basic property)", test_clause_rewrite, {
  given: ["exists", ["this", "ex:badge"]],
  expect: [
    "basic",
    ["pattern", ["var", "this"], ["id", "ex:badge"], ["var", "a"]],
  ],
});

test("exists (compound property)", test_clause_rewrite, {
  given: ["exists", ["this", ["|", "ex:badge", "ex:gun"]]],
  expect: [
    "basic",
    [
      "pattern",
      ["var", "this"],
      ["|", ["id", "ex:badge"], ["id", "ex:gun"]],
      ["var", "a"],
    ],
  ],
});

test("or (single clause)", test_clause_rewrite, {
  given: [
    "or",
    ["exists", ["this", "ex:badge"]],
    ["=", ["this", "ex:name"], "root"],
  ],
  expect: [
    "union",
    [
      "group",
      ["basic", ["pattern", ["var", "this"], ["id", "ex:badge"], ["var", "a"]]],
    ],
    [
      "group",
      [
        "basic",
        ["pattern", ["var", "this"], ["id", "ex:name"], ["var", "b"]],
        ["filter", ["=", ["var", "b"], "root"]],
      ],
    ],
  ],
});

test("negated comparision expression", test_clause_rewrite, {
  given: ["not", ["<", ["this", "Person:age"], 42]],
  expect: [
    "basic",
    [
      "not-exists",
      [
        "basic",
        ["pattern", ["var", "this"], ["id", "Person:age"], ["var", "a"]],
        ["filter", ["<", ["var", "a"], 42]],
      ],
    ],
  ],
});

test("negated equals expression", test_clause_rewrite, {
  given: ["not", ["=", ["this", "Person:is_admin"], true]],
  expect: [
    "basic",
    [
      "not-exists",
      [
        "basic",
        ["pattern", ["var", "this"], ["id", "Person:is_admin"], ["var", "a"]],
        ["filter", ["=", ["var", "a"], true]],
      ],
    ],
  ],
});

test("match (closed)", test_clause_rewrite, {
  given: ["match", "a", "aa"],
  expect: ["basic", ["filter", ["call", "REGEX", "a", "aa"]]],
});

test("match with option (closed)", test_clause_rewrite, {
  given: ["match", "a", "aa", "i"],
  expect: ["basic", ["filter", ["call", "REGEX", "a", "aa", "i"]]],
});

test("match (open)", test_clause_rewrite, {
  given: ["match", ["this", "ex:name"], "aa"],
  expect: [
    "basic",
    ["pattern", ["var", "this"], ["id", "ex:name"], ["var", "a"]],
    ["filter", ["call", "REGEX", ["var", "a"], "aa"]],
  ],
});

test("in list (closed)", test_clause_rewrite, {
  given: ["in", 0, [1, 2, 3]],
  expect: ["basic", ["filter", ["in", 0, 1, 2, 3]]],
});

test("in list (open, mixed literals)", test_clause_rewrite, {
  given: ["in", ["this", "ex:color"], ["red", 420, ["id", "green"]]],
  expect: [
    "basic",
    ["pattern", ["var", "this"], ["id", "ex:color"], ["var", "a"]],
    ["filter", ["in", ["var", "a"], "red", 420, ["id", "green"]]],
  ],
});

test("equals test with sequence path", test_clause_rewrite, {
  given: [
    "=",
    ["this", ["/", "Person:loves", "Person:likes"]],
    ["id", "ex:Bob"],
  ],
  expect: [
    "basic",
    [
      "pattern",
      ["var", "this"],
      ["/", ["id", "Person:loves"], ["id", "Person:likes"]],
      ["var", "a"],
    ],
    ["filter", ["=", ["var", "a"], ["id", "ex:Bob"]]],
  ],
});
