import { metatypes } from "@swymbase/metatype-core";
import { assert_success, send } from "@swymbase/sparql-protocol-client";
import { record_exists, with_temp_named_graph } from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "./lib/config";

const TEST_TYPES = metatypes({
  User: {
    "@type": "https://example.com/vocab/User",
    instance_namespace: `https://example.com/entities/User/`,
    properties: {
      name: {
        term: "https://example.com/vocab/name",
        type: "string",
      },
    },
  },
});

test("tells whether a record exists in the default graph", async (t) => {
  const record_iri = `http://example.com/test/record_exists`;

  // === PRECONDITIONS
  const exists_before = await record_exists(
    TEST_TYPES,
    "User",
    record_iri,
    endpoint
  );
  assert_success(t, exists_before.context, "check exists before");

  t.false(exists_before.value);

  // === SETUP
  // Write a record with that ID
  const triples = ` 
<${record_iri}> a <${TEST_TYPES.User["@type"]}> ;
  <${TEST_TYPES.User.properties.name.term}> "Alberta"`;
  const update = `INSERT DATA { ${triples} }`;
  const update_result = await send({ endpoint, operation: { update } });
  assert_success(t, update_result, "setup: write test resource");

  try {
    // === OPERATION
    const exists_after = await record_exists(
      TEST_TYPES,
      "User",
      record_iri,
      endpoint
    );
    assert_success(t, exists_after.context, "operation: check record exists");

    // === POSTCONDITIONS
    t.true(exists_after.value);
  } finally {
    // === CLEANUP
    // TBD: add a corresponding with-temp wrapper for namespaces in default graph
    const cleanup = `DELETE DATA { ${triples} }`;
    const cleanup_result = await send({
      endpoint,
      operation: { update: cleanup },
    });
    assert_success(t, cleanup_result, "cleanup: delete test resource");
  }
});

test("tells whether a record exists in a named graph", async (t) => {
  await with_temp_named_graph(endpoint, async (graph_iri) => {
    const record_iri = `http://example.com/test/record_exists`;

    // === PRECONDITIONS
    const exists_before = await record_exists(
      TEST_TYPES,
      "User",
      record_iri,
      endpoint,
      { named_graph: graph_iri }
    );
    assert_success(t, exists_before.context, "check exists before");

    t.false(exists_before.value);

    // === SETUP
    // Write a record with that ID
    const triples = `
<${record_iri}> a <${TEST_TYPES.User["@type"]}> ;
  <${TEST_TYPES.User.properties.name.term}> "Alberta"`;
    const update = `INSERT DATA { GRAPH <${graph_iri}> { ${triples} } }`;
    const update_result = await send({ endpoint, operation: { update } });
    assert_success(t, update_result, "setup: write test resource");

    // === OPERATION
    const exists_after = await record_exists(
      TEST_TYPES,
      "User",
      record_iri,
      endpoint,
      { named_graph: graph_iri }
    );
    assert_success(t, exists_after.context, "operation: check record exists");

    // === POSTCONDITIONS
    t.true(exists_after.value);
  });
});
