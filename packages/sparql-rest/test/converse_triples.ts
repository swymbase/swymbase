import { metatypes } from "@swymbase/metatype-core";
import { assert_success, formatted } from "@swymbase/sparql-protocol-client";
import {
  contextualize,
  CONVERSE_TERM,
  delete_converse_triples,
  generate_converse_triples,
  serialize,
  with_temp_named_graph,
  write_converse_triples,
} from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "./lib/config";

const TEST_TYPES = metatypes({
  Person: {
    "@type": "https://example.com/vocab#Person",
    instance_namespace: `https://example.com/entities/Person/`,
    properties: {
      name: {
        term: "https://example.com/vocab#name",
        type: "string",
      },
      in_group: {
        term: "https://example.com/vocab#inGroup",
        type: "object",
        relationship: true,
        multivalued: true,
      },
      admin_of: {
        term: "https://example.com/vocab#adminOf",
        type: "object",
        relationship: true,
      },
    },
  },
  Group: {
    "@type": "https://example.com/vocab#Group",
    instance_namespace: `https://example.com/entities/Group/`,
    properties: {
      name: {
        term: "https://example.com/vocab#name",
        type: "string",
      },
      has_members: {
        term: "https://example.com/vocab#hasMembers",
        type: "object",
        converse_property: "https://example.com/vocab#inGroup",
        relationship: true,
        multivalued: true,
      },
      admin: {
        term: "https://example.com/vocab#admin",
        type: "object",
        converse_property: "https://example.com/vocab#adminOf",
      },
    },
  },
});

test("generate converse triples", async (t) => {
  const triples = generate_converse_triples(TEST_TYPES);

  const expected = [
    "<https://example.com/vocab#admin> <http://www.w3.org/2002/07/owl#inverseOf> <https://example.com/vocab#adminOf>",
    "<https://example.com/vocab#hasMembers> <http://www.w3.org/2002/07/owl#inverseOf> <https://example.com/vocab#inGroup>",
  ];

  t.deepEqual(triples.sort(), expected.sort());
});

test("round trip converse triples", async (t) => {
  await with_temp_named_graph(endpoint, async (named_graph) => {
    // === OPERATION
    const write1 = await write_converse_triples(TEST_TYPES, endpoint, {
      named_graph,
    });

    assert_success(t, write1, "Could not write converse triples 1.");

    const assert_converse_triple_count = async (count: number) => {
      const read_back = serialize(
        contextualize(
          {
            type: "select",
            vars: "*",
            where: `?s <${CONVERSE_TERM}> ?o`,
          },
          named_graph
        )
      );

      const select_result = await formatted("select_results", {
        endpoint,
        operation: { query: read_back },
      });

      assert_success(t, select_result.context, "Could not read back triples.");

      const bindings = select_result.value?.results.bindings!;

      t.assert(bindings);
      t.is(bindings.length, count);
    };

    // Assert two triples were written.
    await assert_converse_triple_count(2);

    const _delete = await delete_converse_triples(endpoint, { named_graph });

    // Assert all triples were deleted.
    await assert_converse_triple_count(0);

    // === OPERATION
    const write2 = await write_converse_triples(TEST_TYPES, endpoint, {
      named_graph,
    });

    assert_success(t, write2, "Could not write converse triples 2.");

    // Assert two triples were rewritten.
    await assert_converse_triple_count(2);
  });
});
