import { get_unused_iri, with_temp_named_graph } from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "./lib/config";
import { is_node_in } from "./lib/is_node_in";

// We cannot really prove that the IRI-generating functions avoid collisions
// with existing nodes, especially insofar as the graphs used by the tests are
// empty or nearly empty.  We can say that their failure is at least as unlikely
// as an UUID collision in the `uuid` implementation.

test.skip("Yields an IRI that is not used in the default graph", async (t) => {
  const PREFIX = "http://exemplification.com/";

  // === OPERATION
  const response = await get_unused_iri(endpoint, PREFIX);

  // === POSTCONDITIONS
  t.log("RESPONSE", response);
  if (!response.success)
    throw new Error(`operation failed: ${response.error.code}`);
  const iri = response.result;
  t.assert(iri.startsWith(PREFIX), "IRI should start with prefix");

  const exists = await is_node_in(iri, endpoint);
  t.false(exists, "IRI should not exist in graph");
});

test.skip("Yields an IRI that is not used in a named graph", async (t) =>
  with_temp_named_graph(endpoint, async (graph_iri) => {
    const PREFIX = "http://exemplification.com/";

    // === OPERATION
    const response = await get_unused_iri(endpoint, PREFIX, graph_iri);

    // === POSTCONDITIONS
    t.log("RESPONSE", response);
    if (!response.success)
      throw new Error(`operation failed: ${response.error.code}`);
    const iri = response.result;
    t.assert(iri.startsWith(PREFIX), "IRI should start with prefix");

    const exists = await is_node_in(iri, endpoint, graph_iri);
    t.false(exists, "IRI should not exist in graph");
  }));
