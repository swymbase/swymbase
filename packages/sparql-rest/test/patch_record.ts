import { EntitiesFrom, metatypes } from "@swymbase/metatype-core";
import {
  assert_success,
  formatted,
  send,
} from "@swymbase/sparql-protocol-client";
import {
  patch_record,
  with_temp_named_graph,
  with_temp_resource,
} from "@swymbase/sparql-rest";
import * as assert from "assert";
import test, { ExecutionContext } from "ava";
import { endpoint } from "./lib/config";

const RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
const XSD = "http://www.w3.org/2001/XMLSchema#";

const TEST_TYPES = metatypes({
  Play: {
    "@type": "https://example.com/vocab/Play",
    instance_namespace: `https://example.com/entities/Play/`,
    properties: {
      title: {
        term: "https://example.com/vocab/title",
        type: "string",
      },
      subtitle: {
        term: "https://example.com/vocab/subtitle",
        type: "string",
      },
      year: {
        term: "https://example.com/vocab/year",
        type: "number",
        comment: "Approximate year of composition",
      },
      roles: {
        term: "https://example.com/vocab/hasRole",
        multivalue: true,
        type: "object",
      },
    },
  },
  Role: {
    "@type": "https://example.com/vocab/Role",
    instance_namespace: `https://example.com/entities/Role/`,
    properties: {
      label: {
        term: "https://example.com/vocab/label",
        type: "string",
        multivalue: true,
      },
    },
  },
});

type TestTypes = EntitiesFrom<typeof TEST_TYPES>;

const patch_test = async (
  t: ExecutionContext,
  { record_iri, graph_iri }: { record_iri: string; graph_iri?: string }
) => {
  t.log("RECORD", record_iri);
  const scope = (expr) =>
    graph_iri ? `GRAPH <${graph_iri}> { ${expr} }` : expr;

  // === SETUP

  // We will write these triples outright.  This is equivalent to what we
  // would expect if a record had been written using `post_record`
  // Re float, record interface writes all numbers as float
  const triples = `
<${record_iri}> a <${TEST_TYPES.Play["@type"]}> ;
  <${TEST_TYPES.Play.properties.title.term}> "The Tragedy of Othello" ;
  <${TEST_TYPES.Play.properties.subtitle.term}> "The Moor of Venice" ;
  <${TEST_TYPES.Play.properties.year.term}> "1603"^^<${XSD}float>
`;

  // === PRECONDITIONS
  const update = `INSERT DATA { ${scope(triples)} }`;
  const write_result = await send({ endpoint, operation: { update } });
  assert_success(t, write_result, "precondition: write triples");

  // Assert that the graph now contains the records
  const subject_facts = `SELECT * WHERE { ${scope(`<${record_iri}> ?p ?o`)} }`;
  const select_before_result = await formatted("select_results", {
    endpoint,
    operation: { query: subject_facts },
  });
  assert_success(t, select_before_result.context, "precondition: select");
  if (!select_before_result.value) return; // TODO: see note in assert_success

  const expect_before = [
    {
      p: { type: "uri", value: RDF_TYPE },
      o: { type: "uri", value: TEST_TYPES.Play["@type"] },
    },
    {
      p: { type: "uri", value: TEST_TYPES.Play.properties.title.term },
      o: { type: "literal", value: "The Tragedy of Othello" },
    },
    {
      p: { type: "uri", value: TEST_TYPES.Play.properties.subtitle.term },
      o: { type: "literal", value: "The Moor of Venice" },
    },
    {
      p: { type: "uri", value: TEST_TYPES.Play.properties.year.term },
      o: { type: "literal", value: "1603", datatype: `${XSD}float` },
    },
  ] as const;
  t.log("EXPECTED BEFORE", expect_before);
  t.log("SELECTED BEFORE", select_before_result.value.results.bindings);
  // Unlike AVA's method, this compares sets unordered
  // https://nodejs.org/api/assert.html#assert_assert_deepstrictequal_actual_expected_message
  assert.deepStrictEqual(
    new Set(select_before_result.value.results.bindings),
    new Set(expect_before)
  );

  // === OPERATION

  const updated: TestTypes["Play"] = {
    id: record_iri,
    title: "Othello",
    year: 1604,
  };
  const result = await patch_record(TEST_TYPES, "Play", updated, endpoint, {
    named_graph: graph_iri,
  });
  assert_success(t, result, "main operation");

  // === POSTCONDITIONS

  // Assert that the graph now contains the updated record
  const select_after_result = await formatted("select_results", {
    endpoint,
    operation: { query: subject_facts },
  });
  assert_success(t, select_after_result.context, "postcondition: select");
  if (!select_after_result.value) return; // TODO: see note in assert_success

  const expect_after = [
    {
      p: { type: "uri", value: RDF_TYPE },
      o: { type: "uri", value: TEST_TYPES.Play["@type"] },
    },
    {
      p: { type: "uri", value: TEST_TYPES.Play.properties.title.term },
      o: { type: "literal", value: "Othello" },
    },
    {
      p: { type: "uri", value: TEST_TYPES.Play.properties.subtitle.term },
      o: { type: "literal", value: "The Moor of Venice" },
    },
    {
      p: { type: "uri", value: TEST_TYPES.Play.properties.year.term },
      o: { type: "literal", value: "1604", datatype: `${XSD}float` },
    },
  ] as const;
  t.log("EXPECTED AFTER", expect_after);
  t.log("SELECTED AFTER", select_after_result.value.results.bindings);

  assert.deepStrictEqual(
    new Set(select_after_result.value.results.bindings),
    new Set(expect_after)
  );

  t.pass("Completed with native assertions");
};

test("updates a record with the given ID and type and properties in the default graph", (t) =>
  with_temp_resource({ endpoint }, (record_iri) =>
    patch_test(t, { record_iri })
  ));

test("updates a record with the given ID and type and properties in a named graph", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    patch_test(t, {
      record_iri: `${TEST_TYPES.Play.instance_namespace}Othello`,
      graph_iri,
    })
  ));
