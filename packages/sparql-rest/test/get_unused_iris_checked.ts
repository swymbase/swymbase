// This function is not exported (for reasons noted there).
import {
  get_unused_iris_checked,
  with_temp_named_graph,
} from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "./lib/config";
import { is_node_in } from "./lib/is_node_in";

// TODO: It turns out that not everything in the protocol send method is
// trapped.  Indeed, the request is created immediately with no try/catch.  So
// this approach to forcing an error doesn't work as expected.
test.skip("Reports errors directly as errors", async (t) => {
  const PREFIX = "http://willnotbeused.com/";

  // === OPERATION
  // Use a non-existent endpoint to ensure a protocol error.
  const response = await get_unused_iris_checked(1, "not an endpoint", PREFIX);

  // === POSTCONDITIONS
  t.log("RESULT", response);
  if (response.success) throw new Error("Expected failure result");
  t.assert(response.error, "Expected error in result");
  t.is(typeof response.error.code, "string", "Expected error code");
  t.assert(response.error.context, "Expected error context");
  t.is(
    typeof response.error.context,
    "object",
    "Expected error context object"
  );
});

test("Yields an empty array for a count of zero", async (t) => {
  const PREFIX = "http://exemplification.com/";

  // === OPERATION
  const response = await get_unused_iris_checked(0, endpoint, PREFIX);

  // === POSTCONDITIONS
  t.log("RESULT", response);
  if (!response.success)
    throw new Error(`operation failed: ${response.error.code}`);
  const iris = response.result;
  t.deepEqual(iris, [], "Result should be an empty array");
});

test("Yields a set of IRI's that are not used in the default graph", async (t) => {
  const PREFIX = "http://exemplification.com/";
  const SIZE = 5;

  // === OPERATION
  const response = await get_unused_iris_checked(SIZE, endpoint, PREFIX);

  // === POSTCONDITIONS
  t.log("RESULT", response);
  if (!response.success)
    throw new Error(`operation failed: ${response.error.code}`);
  const iris = response.result;
  t.true(Array.isArray(iris), "Result should be an array");
  t.is(iris.length, SIZE, "Result should have the requested cardinality");
  t.is(iris.length, new Set(iris).size, "Result should be distinct IRI's");
  for (const iri of iris) {
    t.assert(iri.startsWith(PREFIX), "IRI should start with prefix");

    const exists = await is_node_in(iri, endpoint);
    t.false(exists, "IRI should not exist in graph");
  }
});

test("Yields a set of IRI's that are not used in a named graph", async (t) =>
  with_temp_named_graph(endpoint, async (graph_iri) => {
    const PREFIX = "http://exemplification.com/";
    const SIZE = 5;

    // === OPERATION
    const response = await get_unused_iris_checked(
      SIZE,
      endpoint,
      PREFIX,
      graph_iri
    );

    // === POSTCONDITIONS
    t.log("RESULT", response);
    if (!response.success)
      throw new Error(`operation failed: ${response.error.code}`);
    const iris = response.result;
    t.true(Array.isArray(iris), "Result should be an array");
    t.is(iris.length, SIZE, "Result should have the requested cardinality");
    t.is(iris.length, new Set(iris).size, "Result should be distinct IRI's");
    for (const iri of iris) {
      t.assert(iri.startsWith(PREFIX), "IRI should start with prefix");

      const exists = await is_node_in(iri, endpoint, graph_iri);
      t.false(exists, "IRI should not exist in graph");
    }
  }));
