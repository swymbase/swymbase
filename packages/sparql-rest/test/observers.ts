import { InterfaceFrom, metatypes } from "@swymbase/metatype-core";
import { assert_success } from "@swymbase/sparql-protocol-client";
import {
  delete_record,
  get_record,
  patch_record,
  post_record,
  RecordObservers,
  with_temp_named_graph,
} from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "./lib/config";

const TEST_TYPES = metatypes({
  Person: {
    "@type": "https://example.com/vocab#Person",
    instance_namespace: `https://example.com/entities/Person/`,
    properties: {
      name: {
        term: "https://example.com/vocab#name",
        type: "string",
      },
      student_id: {
        term: "https://example.com/vocab#studentID",
        type: "string",
      },
      age: {
        term: "https://example.com/vocab#age",
        type: "number",
      },
      created_at: {
        term: "https://example.com/vocab#createdAt",
        type: "string",
      },
      updated_at: {
        term: "https://example.com/vocab#updatedAt",
        type: "string",
      },
    },
  },
});

const observers: RecordObservers<typeof TEST_TYPES, any> = {
  Person: {
    retrieved: (person, context) => {
      if (person.created_at) {
        // Dynamically calculate age based on created_at.
        const age = new Date(person.created_at).getTime() % 100;

        return {
          ...person,
          age,
        };
      }

      return person;
    },

    creating: (person, context) => {
      const { system_time } = context;

      if (!system_time) {
        throw new Error("system_time missing from context.");
      }

      return {
        ...person,
        created_at: new Date(system_time).toISOString(),
        updated_at: new Date(system_time).toISOString(),
      };
    },

    created: async (person, context) => {
      const { created_cb } = context;

      if (typeof created_cb !== "function") {
        throw new Error("created_cb missing from context.");
      }

      await created_cb();

      return person;
    },

    updating: (person, context) => {
      const { system_time } = context;

      if (!system_time) {
        throw new Error("system_time missing from context.");
      }

      return {
        ...person,
        updated_at: new Date(system_time).toISOString(),
      };
    },

    updated: async (person, context) => {
      const { updated_cb } = context;

      if (typeof updated_cb !== "function") {
        throw new Error("created_cb missing from context.");
      }

      const person_keys = Object.keys(person);

      for (const key of ["id", "name", "student_id"]) {
        if (!person_keys.includes(key)) {
          throw new Error(`${key} is missing from updated person record.`);
        }
      }

      await updated_cb();

      return person;
    },

    deleting: async (person, context) => {
      const { deleting_cb } = context;

      if (typeof deleting_cb !== "function") {
        throw new Error("deleting_cb missing from context.");
      }

      await deleting_cb();

      return person;
    },

    deleted: async (person, context) => {
      const { deleted_cb } = context;

      if (typeof deleted_cb !== "function") {
        throw new Error("deleted_cb missing from context.");
      }

      await deleted_cb();

      return person;
    },
  },
};

test("observer events are called", async (t) => {
  await with_temp_named_graph(endpoint, async (named_graph) => {
    // === SETUP
    let created_cb_called = false;
    let updated_cb_called = false;
    let deleting_cb_called = false;
    let deleted_cb_called = false;

    const system_time_post = new Date().setDate(0);
    const system_time_patch = new Date().setDate(15);

    const created_cb = () => (created_cb_called = true);
    const updated_cb = () => (updated_cb_called = true);
    const deleting_cb = () => (deleting_cb_called = true);
    const deleted_cb = () => (deleted_cb_called = true);

    const context = {
      system_time: new Date().setDate(0),
      created_cb,
      updated_cb,
      deleting_cb,
      deleted_cb,
    };

    const person_given: InterfaceFrom<typeof TEST_TYPES["Person"]> = {
      name: "Billie French",
      student_id: "4123",
    };

    // === OPERATION
    const post_result = await post_record(
      TEST_TYPES,
      "Person",
      person_given,
      endpoint,
      { named_graph, observers },
      { ...context, system_time: system_time_post }
    );

    assert_success(t, post_result, "Could not post Person.");

    const post_get = await get_record(
      TEST_TYPES,
      "Person",
      post_result.new_id,
      endpoint,
      { named_graph, observers }
    );

    assert_success(t, post_get.context, "Could not get Person after posting.");

    context.system_time = new Date().setDate(1);

    const patch_result = await patch_record(
      TEST_TYPES,
      "Person",
      {
        id: post_result.new_id,
        student_id: "4124",
      },
      endpoint,
      { named_graph, observers },
      { ...context, system_time: system_time_patch }
    );

    assert_success(t, patch_result, "Could not patch Person.");

    const patch_get = await get_record(
      TEST_TYPES,
      "Person",
      post_result.new_id,
      endpoint,
      { named_graph, observers }
    );

    assert_success(t, post_get.context, "Could not get Person after patching.");

    const delete_result = await delete_record(
      TEST_TYPES,
      "Person",
      post_result.new_id,
      endpoint,
      { named_graph, observers },
      context
    );

    assert_success(t, delete_result, "Could not delete Person.");

    // === POSTCONDITIONS
    const post = post_get.value;
    const patch = patch_get.value;

    // Retrieved observer.
    t.is(post?.age, new Date(post?.created_at!).getTime() % 100);
    t.is(patch?.age, new Date(post?.created_at!).getTime() % 100);

    // Creating observer.
    t.is(post?.created_at, new Date(system_time_post).toISOString());
    t.is(post?.updated_at, new Date(system_time_post).toISOString());

    // Created observer.
    t.assert(created_cb_called);

    // Updating observer.
    t.is(patch?.created_at, new Date(system_time_post).toISOString());
    t.is(patch?.updated_at, new Date(system_time_patch).toISOString());

    // Updated observer.
    t.assert(updated_cb_called);

    // Deleting observer.
    t.assert(deleting_cb_called);

    // Deleted observer.
    t.assert(deleted_cb_called);
  });
});
