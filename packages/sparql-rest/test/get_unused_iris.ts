// Largely the same tests as `get_unused_iris_checked.ts`, but
// - database code is just for ceremony: the function doesn't use it
// - includes test of a larger batch size (that would not work in other version)
import { get_unused_iris, with_temp_named_graph } from "@swymbase/sparql-rest";
import test from "ava";
import { endpoint } from "./lib/config";
import { is_node_in } from "./lib/is_node_in";

test("Yields an empty array for a count of zero", async (t) => {
  const PREFIX = "http://exemplification.com/";

  // === OPERATION
  const response = await get_unused_iris(0, endpoint, PREFIX);

  // === POSTCONDITIONS
  t.log("RESULT", response);
  if (!response.success)
    throw new Error(`operation failed: ${response.error.code}`);
  const iris = response.result;
  t.deepEqual(iris, [], "Result should be an empty array");
});

test("Yields a set of IRI's that are not used in the default graph", async (t) => {
  const PREFIX = "http://exemplification.com/";
  const SIZE = 50;

  // === OPERATION
  const response = await get_unused_iris(SIZE, endpoint, PREFIX);

  // === POSTCONDITIONS
  t.log("RESULT", response);
  if (!response.success)
    throw new Error(`operation failed: ${response.error.code}`);
  const iris = response.result;
  t.true(Array.isArray(iris), "Result should be an array");
  t.is(iris.length, SIZE, "Result should have the requested cardinality");
  t.is(iris.length, new Set(iris).size, "Result should be distinct IRI's");
  for (const iri of iris) {
    t.assert(iri.startsWith(PREFIX), "IRI should start with prefix");

    const exists = await is_node_in(iri, endpoint);
    t.false(exists, "IRI should not exist in graph");
  }
});

test("Yields a set of IRI's that are not used in a named graph", async (t) =>
  with_temp_named_graph(endpoint, async (graph_iri) => {
    const PREFIX = "http://exemplification.com/";
    const SIZE = 50;

    // === OPERATION
    const response = await get_unused_iris(SIZE, endpoint, PREFIX, graph_iri);

    // === POSTCONDITIONS
    t.log("RESULT", response);
    if (!response.success)
      throw new Error(`operation failed: ${response.error.code}`);
    const iris = response.result;
    t.true(Array.isArray(iris), "Result should be an array");
    t.is(iris.length, SIZE, "Result should have the requested cardinality");
    t.is(iris.length, new Set(iris).size, "Result should be distinct IRI's");
    for (const iri of iris) {
      t.assert(iri.startsWith(PREFIX), "IRI should start with prefix");

      const exists = await is_node_in(iri, endpoint, graph_iri);
      t.false(exists, "IRI should not exist in graph");
    }
  }));

// Same as previous test with a larger size.
test.only("Yields a large set of IRI's that are not used in a named graph", async (t) =>
  with_temp_named_graph(endpoint, async (graph_iri) => {
    const PREFIX = "http://exemplification.com/";
    const SIZE = 2000;

    // === OPERATION
    const response = await get_unused_iris(SIZE, endpoint, PREFIX, graph_iri);

    // === POSTCONDITIONS
    t.log("RESULT", response);
    if (!response.success)
      throw new Error(`operation failed: ${response.error.code}`);
    const iris = response.result;
    t.true(Array.isArray(iris), "Result should be an array");
    t.is(iris.length, SIZE, "Result should have the requested cardinality");
    t.is(iris.length, new Set(iris).size, "Result should be distinct IRI's");
    for (const iri of iris) {
      t.assert(iri.startsWith(PREFIX), "IRI should start with prefix");

      const exists = await is_node_in(iri, endpoint, graph_iri);
      t.false(exists, "IRI should not exist in graph");
    }
  }));
