import {
  assert_success,
  formatted,
  send,
} from "@swymbase/sparql-protocol-client";
import {
  delete_properties,
  with_temp_named_graph,
  with_temp_resource,
} from "@swymbase/sparql-rest";
import * as assert from "assert";
import test, { ExecutionContext } from "ava";
import { endpoint } from "./lib/config";

const delete_test = async (
  t: ExecutionContext,
  { record_iri, graph_iri }: { record_iri: string; graph_iri?: string }
) => {
  const scope = (expr) =>
    graph_iri ? `GRAPH <${graph_iri}> { ${expr} }` : expr;

  const properties_to_delete = [
    "http://example.com/parent",
    "http://example.com/adverb",
  ];

  // === SETUP
  // Write some facts
  const triples = `
<${record_iri}>
  <http://example.com/name> "Quentin" ;
  <http://example.com/adverb> "literally" ;
  <http://example.com/parent> <http://example.com/Alice> ;
  <http://example.com/parent> <http://example.com/Bob> ;
  <http://example.com/child> <http://example.com/Carol> ;
  <http://example.com/child> <http://example.com/Dave> 
  .
`;
  const update_result = await send({
    endpoint,
    operation: { update: `INSERT DATA { ${scope(triples)} }` },
  });
  assert_success(t, update_result, "setup: write test triples");

  // === PRECONDITIONS
  // (Assume previous write succeeded)

  // === OPERATION
  const result = await delete_properties(
    record_iri,
    properties_to_delete,
    endpoint,
    graph_iri
  );
  assert_success(t, result, "main operation");

  // === POSTCONDITIONS
  // All values for the specified properties are gone
  // All values for properties not specified are as before
  //
  // TODO: Need a helper for “assert everything known about X”
  // See also put_record and patch_record
  const subject_facts = `SELECT * WHERE { ${scope(`<${record_iri}> ?p ?o`)} }`;
  const select_before_result = await formatted("select_results", {
    endpoint,
    operation: { query: subject_facts },
  });
  assert_success(t, select_before_result.context, "precondition: select");
  if (!select_before_result.value) return; // TODO: see note in assert_success

  const expect_before = [
    {
      p: { type: "uri", value: "http://example.com/name" },
      o: { type: "literal", value: "Quentin" },
    },
    {
      p: { type: "uri", value: "http://example.com/child" },
      o: { type: "uri", value: "http://example.com/Carol" },
    },
    {
      p: { type: "uri", value: "http://example.com/child" },
      o: { type: "uri", value: "http://example.com/Dave" },
    },
  ] as const;
  t.log("EXPECTED BEFORE", expect_before);
  t.log("SELECTED BEFORE", select_before_result.value.results.bindings);
  assert.deepStrictEqual(
    new Set(select_before_result.value.results.bindings),
    new Set(expect_before)
  );

  t.pass("Completed with native assertions");
};

test("deletes the given properties of the given resource in the default graph", (t) =>
  with_temp_resource({ endpoint }, (record_iri) =>
    delete_test(t, { record_iri })
  ));

test("deletes the given properties of the given resource in a named graph", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    delete_test(t, {
      record_iri: `http://example.com/delete_properties/Hamlet`,
      graph_iri,
    })
  ));
