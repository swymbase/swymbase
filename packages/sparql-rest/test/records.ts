import { InterfacesFrom, metatypes } from "@swymbase/metatype-core";
// For low-level confirmation of record
import {
  assert_success,
  formatted,
  send,
} from "@swymbase/sparql-protocol-client";
import {
  delete_record,
  delete_subject,
  get_record,
  patch_record,
  post_record,
  record_exists,
  serialize,
  with_temp_named_graph,
} from "@swymbase/sparql-rest";
import test from "ava";
import { v4 as uuid } from "uuid";
import { endpoint } from "./lib/config";
import { strip_jsonld_header } from "./lib/jsonld-helpers";

const TEST_TYPES = metatypes({
  User: {
    "@type": "https://example.com/vocab/User",
    instance_namespace: `https://example.com/entities/User/`,
    label: "user",
    properties: {
      // Not the resource IRI, but an identifier that can be used to tell
      // whether the record existed or not.
      uid: {
        term: "https://example.com/vocab/uid",
        type: "string",
      },
      firstName: {
        term: "https://example.com/vocab/firstName",
        type: "string",
      },
      dateOfBirth: {
        term: "https://example.com/vocab/dateOfBirth",
        type: "string",
        label: "birthday",
      },
      isEarthling: {
        term: "https://example.com/vocab/isEarthling",
        type: "boolean",
      },
      luckyNumber: {
        term: "https://example.com/vocab/luckyNumber",
        type: "number",
      },
    },
  },
  Group: {
    "@type": "https://dev.thread.org/vocab/Group",
    instance_namespace: `https://example.com/entities/Group/`,
    properties: {
      name: {
        term: "https://example.com/vocab/name",
        type: "string",
      },
      createdBy: {
        term: "https://example.com/vocab/createdBy",
        type: "object",
        relationship: true,
      },
      hasMember: {
        term: "https://example.com/vocab/hasmember",
        type: "object",
        multivalued: true,
        relationship: true,
      },
    },
  },
});

type ExampleTypes = InterfacesFrom<typeof TEST_TYPES>;

test("roundtrip a simple record through the knowledge base", async (t) => {
  const uid = uuid();

  // === PRECONDITIONS

  // Confirm that the test user doesn't exist already
  const ask = serialize({
    type: "ask",
    where: `?s <${TEST_TYPES.User.properties.uid.term}> "${uid}"`,
  });
  const pre_exists = await formatted("boolean", {
    endpoint,
    operation: { query: ask },
  });
  assert_success(t, pre_exists.context, "precondition: check user exists");
  t.false(pre_exists.value, `A resource with uid ${uid} already existed!`);

  // === OPERATIONS

  // POST
  const record: ExampleTypes["User"] = {
    uid,
    firstName: "Joe",
    dateOfBirth: "2010-07-19",
    isEarthling: true,
    luckyNumber: 19,
  };
  const post_result = await post_record(TEST_TYPES, "User", record, endpoint);
  assert_success(t, post_result, "post user");
  if (!post_result.success) return; // TODO: see note to assert_success
  t.is(typeof post_result.new_id, "string");
  t.not(post_result.new_id, "");

  // GET
  const iri = post_result.new_id;
  const get_result = await get_record(TEST_TYPES, "User", iri, endpoint);
  t.assert(get_result.value);
  if (get_result.value)
    t.deepEqual(
      { id: iri, type: TEST_TYPES.User["@type"], ...record },
      strip_jsonld_header(get_result.value)
    );
  else t.log(get_result.context);

  // PATCH
  const revised: typeof record = {
    firstName: "Joseph", // Change name from “Joe”,
    luckyNumber: 45, // Change lucky number from 19
  };
  const update_result = await patch_record(
    TEST_TYPES,
    "User",
    { id: iri, ...revised },
    endpoint
  );
  assert_success(t, update_result, "patch user record");

  // REREAD
  const reread_result = await get_record(TEST_TYPES, "User", iri, endpoint);
  t.assert(reread_result.value);
  if (reread_result.value)
    t.deepEqual(
      { id: iri, type: TEST_TYPES.User["@type"], ...record, ...revised },
      strip_jsonld_header(reread_result.value)
    );
  else t.log(reread_result.context);

  // DELETE
  const delete_result = await delete_record(TEST_TYPES, "User", iri, endpoint);
  assert_success(t, delete_result, "delete user record");

  // CONFIRM
  const post_exists = await record_exists(TEST_TYPES, "User", iri, endpoint);
  if (post_exists.value === true) {
    t.log("Resource still exists", iri);
    // The resource wasn't completely deleted, what's still here?
    const query = serialize({
      type: "select",
      vars: "*",
      where: `<${iri}> ?p ?o`,
    });
    const get_result = await formatted("select_results", {
      endpoint,
      operation: { query },
    });
    if (get_result.value) {
      t.log("STILL ON OBJECT", get_result.value.results.bindings);
    } else {
      t.log(get_result.context);
    }
  }
  t.false(post_exists.value);
});

test("roundtrip two related records through the knowledge base", async (t) => {
  // === HELPERS

  // A simple deep-equals check does not suffice because order doesn't matter in
  // the arrays.
  function compare_objects(given: object, roundtripped: object) {
    t.log("GIVEN", given, "ROUNDTRIPPED", roundtripped);
    t.deepEqual(Object.keys(roundtripped).sort(), Object.keys(given).sort());
    for (const [key, value] of Object.entries(roundtripped)) {
      const orig = given[key];
      if (Array.isArray(orig)) {
        t.true(Array.isArray(value));
        // TS doesn't know about assertion
        if (Array.isArray(value)) {
          t.is(value.length, orig.length);
          for (const item of orig)
            t.assert(
              value.some((x) =>
                typeof item === "object" ? x["@id"] === item["@id"] : x === item
              )
            );
        }
      } else t.deepEqual(orig, value);
    }
  }

  // === POST

  // Create users
  const user1: ExampleTypes["User"] = { firstName: "Geddy" };
  const user1_post_result = await post_record(
    TEST_TYPES,
    "User",
    user1,
    endpoint
  );
  assert_success(t, user1_post_result, "post user 1");
  if (!user1_post_result.success) return; // TODO: see note to assert_success
  const user1_iri = user1_post_result.new_id;

  const user2: ExampleTypes["User"] = { firstName: "Neil" };
  const user2_post_result = await post_record(
    TEST_TYPES,
    "User",
    user2,
    endpoint
  );
  assert_success(t, user2_post_result, "post user 2");
  if (!user2_post_result.success) return; // TODO: see note to assert_success
  const user2_iri = user2_post_result.new_id;

  // Create a group
  const group1: ExampleTypes["Group"] = {
    name: "Rush",
    createdBy: { id: user1_iri },
    hasMember: [],
    // These cases are covered during the patches below
    // hasMember: [{ "id": user1_iri }]
    // hasMember: [{ "id": user1_iri }, { "id": user2_iri }]
  };
  const group1_post_result = await post_record(
    TEST_TYPES,
    "Group",
    group1,
    endpoint
  );
  assert_success(t, group1_post_result, "post group 1");
  if (!group1_post_result.success) return; // TODO: see note to assert_success
  const group1_iri = group1_post_result.new_id;

  // === GET

  // Covers case of empty array

  const user1_get_result = await get_record(
    TEST_TYPES,
    "User",
    user1_iri,
    endpoint
  );
  assert_success(t, user1_get_result.context, "get user 1");
  t.log("USER1", user1_get_result);
  // @ts-expect-error: id is optional, so TS doesn't like this
  const { id: id1, ...user1_roundtripped } = user1_get_result.value;
  delete user1_roundtripped["@context"];
  t.deepEqual(user1_roundtripped, {
    type: TEST_TYPES.User["@type"],
    ...user1,
  });

  const user2_get_result = await get_record(
    TEST_TYPES,
    "User",
    user2_iri,
    endpoint
  );
  assert_success(t, user2_get_result.context, "get user 2");
  t.log("USER 2", user2_get_result);
  // @ts-expect-error: id is optional, so TS doesn't like this
  const { id: id2, ...user2_roundtripped } = user2_get_result.value;
  delete user2_roundtripped["@context"];
  t.deepEqual(user2_roundtripped, {
    type: TEST_TYPES.User["@type"],
    ...user2,
  });

  const group1_get_result = await get_record(
    TEST_TYPES,
    "Group",
    group1_iri,
    endpoint
  );
  assert_success(t, group1_get_result.context, "get group 1");
  const group1_roundtripped = { ...group1_get_result.value };
  delete group1_roundtripped["@context"];
  compare_objects(
    { id: group1_iri, type: TEST_TYPES.Group["@type"], ...group1 },
    group1_roundtripped
  );

  // === PATCH

  // Add members to group: covers case of multi-value array

  const group_with_several_members: typeof group1 = {
    hasMember: [{ id: user1_iri }, { id: user2_iri }],
  };
  const update1_result = await patch_record(
    TEST_TYPES,
    "Group",
    { id: group1_iri, ...group_with_several_members },
    endpoint
  );
  assert_success(t, update1_result, "patch group to add members");

  // REREAD
  const reread1_result = await get_record(
    TEST_TYPES,
    "Group",
    group1_iri,
    endpoint
  );
  t.assert(reread1_result.value);
  if (reread1_result.value) {
    t.log("REREAD 1", reread1_result.value);
    compare_objects(
      {
        id: group1_iri,
        type: TEST_TYPES.Group["@type"],
        ...group1,
        ...group_with_several_members,
      },
      strip_jsonld_header(reread1_result.value)
    );
  } else t.log(reread1_result.context);

  // Remove members from group: covers case of single-value array

  const group_with_one_member: typeof group1 = {
    hasMember: [{ id: user2_iri }],
  };
  const update2_result = await patch_record(
    TEST_TYPES,
    "Group",
    { id: group1_iri, ...group_with_one_member },
    endpoint
  );
  assert_success(t, update2_result, "patch group to have one member");

  // REREAD
  const reread2_result = await get_record(
    TEST_TYPES,
    "Group",
    group1_iri,
    endpoint
  );
  t.assert(reread2_result.value);
  if (reread2_result.value) {
    compare_objects(
      {
        id: group1_iri,
        type: TEST_TYPES.Group["@type"],
        ...group1,
        ...group_with_one_member,
      },
      strip_jsonld_header(reread2_result.value)
    );
  } else t.log(reread2_result.context);

  // === CLEANUP

  const user1_cleanup = await delete_subject(
    user1_post_result.new_id,
    endpoint
  );
  if (!user1_cleanup.success) {
    t.log(user1_cleanup);
    t.fail(`Couldn't cleanup user 1`);
  }

  const user2_cleanup = await delete_subject(
    user2_post_result.new_id,
    endpoint
  );
  if (!user2_cleanup.success) {
    t.log(user2_cleanup);
    t.fail(`Couldn't cleanup user 2`);
  }

  const group1_cleanup = await delete_subject(
    group1_post_result.new_id,
    endpoint
  );
  if (!group1_cleanup.success) {
    t.log(group1_cleanup);
    t.fail(`Couldn't cleanup group 1`);
  }
});

test("get a resource that is both a subject and an object", async (t) => {
  // === TEST SCOPE
  const user_iri = `${TEST_TYPES.User.instance_namespace}GRSOT_User`;
  const group_iri = `${TEST_TYPES.User.instance_namespace}GRSOT_Group`;

  await with_temp_named_graph(endpoint, async (graph) => {
    // === PRECONDITIONS
    const update = `
PREFIX vo: <https://example.com/vocab/>
PREFIX : <https://example.com/entities/>

INSERT DATA {
  GRAPH <${graph}> {
  <${user_iri}> a <${TEST_TYPES.User["@type"]}> ;
  <${TEST_TYPES.User.properties.firstName.term}> "Edgar" ;
  <${TEST_TYPES.User.properties.isEarthling.term}> false ;
  .

  <${group_iri}> a <${TEST_TYPES.Group["@type"]}> ;
  <${TEST_TYPES.Group.properties.name.term}> "The Edgartons" ;
  <${TEST_TYPES.Group.properties.createdBy.term}> <${user_iri}> ;
  <${TEST_TYPES.Group.properties.hasMember.term}> <${user_iri}> ;
  .

  }
}
`;

    // === OPERATION
    const insert_result = await send({ endpoint, operation: { update } });
    assert_success(t, insert_result, "write test graph");

    // Sanity check
    const query = `
PREFIX vo: <https://example.com/vocab/>
PREFIX : <https://example.com/entities/>

SELECT ?s ?p ?o
WHERE { GRAPH <${graph}> { ?s ?p ?o  } }
`;
    const query_result = await formatted("results", {
      endpoint,
      operation: { query },
    });
    assert_success(t, query_result.context, "query test graph");
    t.log("QUERIED", query_result.value);

    // === POSTCONDITIONS

    // === Get records using interfaces

    const got_user = await get_record(TEST_TYPES, "User", user_iri, endpoint, {
      named_graph: graph,
    });
    assert_success(t, got_user.context, "postcondition: get user");
    const roundtripped_user = { ...got_user.value };
    delete roundtripped_user["@context"];
    // Must match description above
    const described_user: ExampleTypes["User"] = {
      id: user_iri,
      type: TEST_TYPES.User["@type"],
      firstName: "Edgar",
      isEarthling: false,
    };
    t.log(got_user.value);
    t.deepEqual(roundtripped_user, described_user);

    const got_group = await get_record(
      TEST_TYPES,
      "Group",
      group_iri,
      endpoint,
      { named_graph: graph }
    );
    assert_success(t, got_group.context, "postcondition: get group");
    const roundtripped_group = { ...got_group.value };
    delete roundtripped_group["@context"];
    // Must match description above
    const described_group: ExampleTypes["Group"] = {
      id: group_iri,
      type: TEST_TYPES.Group["@type"],
      name: "The Edgartons",
      createdBy: { id: user_iri },
      hasMember: [{ id: user_iri }],
    };
    t.deepEqual(roundtripped_group, described_group);
  });
});
