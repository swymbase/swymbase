import { metatypes } from "@swymbase/metatype-core";
import { assert_success, send } from "@swymbase/sparql-protocol-client";
import {
  get_record,
  with_temp_named_graph,
  write_converse_triples,
} from "@swymbase/sparql-rest";
import test, { ExecutionContext } from "ava";
import { endpoint } from "./lib/config";

const TEST_TYPES = metatypes({
  Person: {
    "@type": "https://example.com/vocab#Person",
    instance_namespace: `https://example.com/entities/Person/`,
    properties: {
      student_id: {
        term: "https://example.com/vocab#studentID",
        type: "string",
      },
      in_group: {
        term: "https://example.com/vocab#inGroup",
        type: "object",
        relationship: true,
        multivalued: true,
      },
      best_friend: {
        term: "https://example.com/vocab#best_friend",
        type: "object",
        relationship: true,
      },
    },
  },
  Group: {
    "@type": "https://example.com/vocab#Group",
    instance_namespace: `https://example.com/entities/Group/`,
    properties: {
      name: {
        term: "https://example.com/vocab#name",
        type: "string",
      },
      objectives: {
        term: "https://example.com/vocab#objectives",
        type: "string",
        multivalued: true,
      },
      has_members: {
        term: "https://example.com/vocab#hasMembers",
        type: "object",
        relationship: true,
        multivalued: true,
        converse_property: "https://example.com/vocab#inGroup",
      },
    },
  },
});

const scope = (expr: string, graph_iri?: string) =>
  graph_iri ? `GRAPH <${graph_iri}> { ${expr} }` : expr;
const random = () => Math.round(Math.random() * 1e9);

const get_test = async (
  t: ExecutionContext,
  {
    person,
    graph_iri,
    include_related,
  }: {
    person: { iri: string; group_iris: string[]; best_friend: string };
    graph_iri?: string;
    include_related: (keyof typeof TEST_TYPES["Person"]["properties"])[];
  }
) => {
  // === SETUP
  const group_iris = person.group_iris;

  const group_triples = group_iris
    .map(
      (iri) => `
        <${iri}> a <${TEST_TYPES.Group["@type"]}>
        ; <${TEST_TYPES.Group.properties.name.term}> "${random()}" .
      `
    )
    .join("\n");

  const group_insert = `INSERT DATA { ${scope(group_triples, graph_iri)} }`;
  const group_write_result = await send({
    endpoint,
    operation: { update: group_insert },
  });
  assert_success(t, group_write_result, "precondition: write group triples");

  const bf_tripples = `
    <${person.best_friend}> a <${TEST_TYPES.Person["@type"]}>
    ; <${TEST_TYPES.Person.properties.student_id.term}> "${random()}"
    ${person.group_iris
      .map((gi) => `; <${TEST_TYPES.Person.properties.in_group.term}> <${gi}>`)
      .join("\n")} .
  `;

  const bf_insert = `INSERT DATA { ${scope(bf_tripples, graph_iri)} }`;
  const write_bf_result = await send({
    endpoint,
    operation: { update: bf_insert },
  });
  assert_success(t, write_bf_result, "precondition: write best friend triples");

  const person_tripples = `
    <${person.iri}> a <${TEST_TYPES.Person["@type"]}>
    ; <${TEST_TYPES.Person.properties.student_id.term}> "${random()}"
    ; <${TEST_TYPES.Person.properties.best_friend.term}> <${person.best_friend}>
    ${person.group_iris
      .map((gi) => `; <${TEST_TYPES.Person.properties.in_group.term}> <${gi}>`)
      .join("\n")} .
  `;

  const people_insert = `INSERT DATA { ${scope(person_tripples, graph_iri)} }`;
  const write_people_result = await send({
    endpoint,
    operation: { update: people_insert },
  });
  assert_success(t, write_people_result, "precondition: write person triples");

  await write_converse_triples(TEST_TYPES, endpoint, {
    named_graph: graph_iri,
  });

  // === OPERATION
  const result = await get_record(TEST_TYPES, "Person", person.iri, endpoint, {
    named_graph: graph_iri,
    include_related,
  });
  assert_success(t, result.context, "main operation");

  // Included values.
  const included_records = result.included_values!;

  if (include_related.length === 0) {
    t.deepEqual(include_related, []);
  }

  if (include_related.includes("in_group")) {
    for (const group_iri of group_iris) {
      const group = included_records.find((r) => r.id === group_iri)!;

      t.assert(group);
      t.assert(typeof group.name === "string");
    }
  }

  if (include_related.includes("best_friend")) {
    const bf = result.included_values?.find(
      (r) => r.id === person.best_friend
    )!;
    const bf_group_iris = (bf as any).in_group.map((g) => g.id);

    t.assert(bf);
    t.deepEqual(bf_group_iris.sort(), person.group_iris.sort());
  }

  // Converse properties.
  for (const group_iri of person.group_iris || []) {
    const get_group = await get_record(
      TEST_TYPES,
      "Group",
      group_iri,
      endpoint,
      { named_graph: graph_iri, include_related: ["has_members"] }
    );

    assert_success(t, get_group.context, `Could not get group ${group_iri}.`);

    const group = get_group.value!;
    const group_included = get_group.included_values!;

    t.assert(group);
    t.assert(group.has_members?.find((p) => p.id === person.iri));
    t.assert(group.has_members?.find((p) => p.id === person.best_friend));

    // Including converse properties.
    for (const member of group.has_members!) {
      const person = group_included.find((r) => r.id === member.id);

      t.assert(person);
      t.assert(typeof person!.student_id === "string");
    }
  }
};

test("returns correct included records", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    get_test(t, {
      person: {
        iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
        group_iris: [
          `${TEST_TYPES.Group.instance_namespace}Group1`,
          `${TEST_TYPES.Group.instance_namespace}Group2`,
        ],
        best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
      },
      graph_iri,
      include_related: ["in_group", "best_friend"],
    })
  ));

test("returns only requested included records", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    get_test(t, {
      person: {
        iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
        group_iris: [
          `${TEST_TYPES.Group.instance_namespace}Group1`,
          `${TEST_TYPES.Group.instance_namespace}Group2`,
        ],
        best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
      },
      graph_iri,
      include_related: ["in_group"],
    })
  ));

test("returns no included records", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    get_test(t, {
      person: {
        iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
        group_iris: [
          `${TEST_TYPES.Group.instance_namespace}Group1`,
          `${TEST_TYPES.Group.instance_namespace}Group2`,
        ],
        best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
      },
      graph_iri,
      include_related: [],
    })
  ));
