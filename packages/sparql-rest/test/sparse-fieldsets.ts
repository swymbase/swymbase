// Tests for sparse fieldsets on list and get operations.
//
// All tests use the same test schema and data.
import { EntitiesFrom, metatypes } from "@swymbase/metatype-core";
import type { Fieldsets, Filter, SchemaPath } from "@swymbase/sparql-rest";
import test, { ExecutionContext } from "ava";
import {
  assert_equal_records,
  assert_equal_record_sets,
} from "./lib/record-helpers";
import { with_seeded_kb } from "./lib/with-seeded-kb";

interface GetCase<T extends keyof Types> {
  readonly type: T;
  readonly id: string;
  readonly include?: readonly SchemaPath<Metatypes, T>[];
  readonly fields?: Fieldsets<Metatypes>;
  readonly properties: Omit<Types[T], "id" | "type">;
  readonly included?: readonly object[];
}

interface ListCase<T extends keyof Types> {
  readonly type: T;
  readonly include?: readonly SchemaPath<Metatypes, T>[];
  readonly filter?: Filter; // not under test here, but helps keep results small
  readonly fields?: Fieldsets<Metatypes>;
  readonly records: readonly Types[T][];
  readonly included?: readonly object[];
}

const V = "http://example.com/sparse/vocab#";
const E = "http://example.com/sparse/entities/";
const XSD = "http://www.w3.org/2001/XMLSchema#";
const RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
const RDFS = "http://www.w3.org/2000/01/rdf-schema#";

const TYPES = metatypes({
  Character: {
    "@type": `${V}Character`,
    properties: {
      givenName: { term: `${V}givenName`, type: "string" },
      familyName: { term: `${V}familyName`, type: "string" },
      alias: { term: `${V}alias`, type: "string", multivalued: true },
      adoptedBy: {
        term: `${V}adoptedBy`,
        type: "object",
        relationship: true,
        multivalued: true,
      },
      adopted: {
        term: `${V}adopted`,
        type: "object",
        relationship: true,
        multivalued: true,
        converse_property: `${V}adoptedBy`,
      },
      mother: { term: `${V}mother`, type: "object", relationship: true },
      father: { term: `${V}father`, type: "object", relationship: true },
      motherOf: {
        term: `${V}motherOf`,
        type: "object",
        relationship: true,
        multivalued: true,
        converse_property: `${V}mother`,
      },
      livedAt: {
        term: `${V}livedAt`,
        type: "object",
        relationship: true,
        multivalued: true,
        range: `${V}Location`, // INFORMATIONAL
      },
      lawyer: {
        term: `${V}lawyer`,
        type: "object",
        relationship: true,
        multivalued: true,
      },
      student: {
        term: `${V}student`,
        type: "object",
        relationship: true,
        multivalued: true,
      },
      married: {
        // should be symmetrical
        term: `${V}married`,
        type: "object",
        relationship: true,
        multivalued: true,
      },
    },
  },
  Location: {
    "@type": `${V}Location`,
    properties: {
      label: {
        term: `${RDFS}label`,
        type: "string",
      },
      hadResident: {
        term: `${V}hadResident`,
        type: "object",
        converse_property: `${V}livedAt`,
        relationship: true,
        multivalued: true,
      },
    },
  },
});

type Metatypes = typeof TYPES;
type Types = EntitiesFrom<Metatypes>;

const TEST_DATA = `
<Pip> a :Character 
 ; :givenName "Phillip"
 ; :familyName "Pirrip" 
 ; :alias "Pip" 
 ; :mother <Georgiana>
 ; :adoptedBy <MrsJoe>, <Joe>
 ; :livedAt <TheForge>, <BarnardsInn>, <TempleGardens>
.
<Joe> a :Character
 ; :givenName "Joe"
 ; :familyName "Gargery"
 ; :married <MrsJoe>, <Biddy>
 ; :alias "Jo"
 ; :livedAt <TheForge>
.
<MrsJoe> a :Character
 ; :mother <Georgiana>
 ; :familyName "Pirrip"
 ; :married <Joe>
 ; :livedAt <TheForge>
.
<Georgiana> a :Character
 ; :givenName "Georgiana"
.
<Biddy> a :Character 
 ; :married <Joe>
 ; :livedAt <TheForge>
.
<Drummle> a :Character 
 ; :married <Estella>
.
<Orlick> a :Character 
.
<Wemmick> a :Character 
 ; :givenName "John"
 ; :father <TheAged>
 ; :married <MissSkiffins>
 ; :livedAt <Walworth>
.
<Jaggers> a :Character 
.
<MissHavisham> a :Character 
 ; :familyName "Havisham"
 ; :lawyer <Jaggers>
 ; :livedAt <SatisHouse>
.
<Estella> a :Character 
 ; :father <Magwitch>
 ; :mother <Molly>
 ; :adoptedBy <MissHavisham>
 ; :married <Drummle>
 ; :livedAt <SatisHouse>
.
<Herbert> a :Character 
 ; :givenName "Herbert"
 ; :familyName "Pocket"
 ; :father <MrPocket>
 ; :mother <MrsPocket>
 ; :married <Clara>
 ; :livedAt <BarnardsInn>, <TempleGardens>
.
<Molly> a :Character
 ; :lawyer <Jaggers>
 ; :alias "a wild beast tamed"
.
<Magwitch> a :Character 
 ; :givenName "Abel"
 ; :familyName "Magwitch"
 ; :alias "Provis"
 ; :livedAt <TempleGardens>, <MillPondBank>
.
<Compeyson> a :Character 
.
<Wopsle> a :Character 
.
<Pumblechook> a :Character 
.
<MrPocket> a :Character 
 ; :married <MrsPocket>
 ; :student <Pip>, <Drummle>, <Startop>
.
<MrsPocket> a :Character 
 ; :married <MrPocket>
.
<SarahPocket> a :Character 
.
<Startop> a :Character 
.
<Clara> a :Character 
 ; :married <Herbert>
 ; :livedAt <MillPondBank>
.
<MissSkiffins> a :Character 
 ; :married <Wemmick>
 ; :livedAt <Walworth>
.
<TheAged> a :Character 
 ; :alias "the Aged parent", "Aged P"
 ; :livedAt <Walworth>
.
# Locations
<SatisHouse> a :Location ; rdfs:label "Satis House" .
<Walworth> a :Location ; rdfs:label "Walworth" .
<TheForge> a :Location ; rdfs:label "the forge" .
<Newgate> a :Location ; rdfs:label "Newgate" .
<BarnardsInn> a :Location ; rdfs:label "Barnard's Inn" .
<TempleGardens> a :Location ; rdfs:label "Temple Gardens" .
<MillPondBank> a :Location ; rdfs:label "Mill Pond Bank" .

`;

const preamble = `BASE <${E}>
PREFIX : <${V}>
PREFIX xsd: <${XSD}>
PREFIX rdf: <${RDF}>
PREFIX rdfs: <${RDFS}>
`;

// Helpers for making partial object projections
const pick =
  <K extends string[]>(...keys: K) =>
  (record: object): Record<K[any], any> => {
    const ret = {};
    for (const key of keys)
      if (record.hasOwnProperty(key)) ret[key] = record[key];
    return ret as any;
  };
const props = <K extends string[]>(...keys: K) => pick("id", "type", ...keys);

// Shorthand for multi-valued properties, which are always present by default.
const CHARACTER_DEFAULTS: Partial<Types["Character"]> = {
  type: `${V}Character`,
  alias: [],
  lawyer: [],
  student: [],
  married: [],
  adopted: [],
  adoptedBy: [],
  livedAt: [],
  motherOf: [],
};

const character = (id: string, record: Omit<Types["Character"], "id">) => ({
  ...CHARACTER_DEFAULTS,
  id,
  ...record,
});
const location = (id: string, record: Omit<Types["Location"], "id">) => ({
  id,
  hadResident: [],
  type: `${V}Location`,
  ...record,
});

const PIP = character(`${E}Pip`, {
  givenName: "Phillip",
  familyName: "Pirrip",
  alias: ["Pip"],
  mother: { id: `${E}Georgiana` },
  adoptedBy: [{ id: `${E}MrsJoe` }, { id: `${E}Joe` }],
  livedAt: [
    { id: `${E}TempleGardens` },
    { id: `${E}BarnardsInn` },
    { id: `${E}TheForge` },
  ],
});

// Re the _INCL here and below, converse properties are not supported on
// included records. (See also `include.ts` tests).
const GEORGIANA_INCL = character(`${E}Georgiana`, {
  givenName: "Georgiana",
});
const GEORGIANA: Types["Character"] = {
  ...GEORGIANA_INCL,
  motherOf: [{ id: `${E}Pip` }, { id: `${E}MrsJoe` }],
};

const MRS_JOE_INCL = character(`${E}MrsJoe`, {
  familyName: "Pirrip",
  married: [{ id: `${E}Joe` }],
  mother: { id: `${E}Georgiana` },
  livedAt: [{ id: `${E}TheForge` }],
});
const MRS_JOE: Types["Character"] = {
  ...MRS_JOE_INCL,
  adopted: [{ id: `${E}Pip` }],
};

const JOE_INCL = character(`${E}Joe`, {
  givenName: "Joe",
  familyName: "Gargery",
  married: [{ id: `${E}MrsJoe` }, { id: `${E}Biddy` }],
  alias: ["Jo"],
  livedAt: [{ id: `${E}TheForge` }],
});
const JOE: Types["Character"] = {
  ...JOE_INCL,
  adopted: [{ id: `${E}Pip` }],
};

const BIDDY = character(`${E}Biddy`, {
  married: [{ id: `${E}Joe` }],
  livedAt: [{ id: `${E}TheForge` }],
});

const MISS_HAVISHAM = character(`${E}MissHavisham`, {
  familyName: "Havisham",
  lawyer: [{ id: `${E}Jaggers` }],
  livedAt: [{ id: `${E}SatisHouse` }],
  adopted: [{ id: `${E}Estella` }],
});

const HERBERT = character(`${E}Herbert`, {
  givenName: "Herbert",
  familyName: "Pocket",
  married: [{ id: `${E}Clara` }],
  father: { id: `${E}MrPocket` },
  mother: { id: `${E}MrsPocket` },
  livedAt: [{ id: `${E}BarnardsInn` }, { id: `${E}TempleGardens` }],
});

const SATIS = location(`${E}SatisHouse`, {
  label: "Satis House",
  hadResident: [{ id: `${E}Estella` }, { id: `${E}MissHavisham` }],
});
const WALWORTH = location(`${E}Walworth`, {
  label: "Walworth",
  hadResident: [
    { id: `${E}Wemmick` },
    { id: `${E}MissSkiffins` },
    { id: `${E}TheAged` },
  ],
});
const FORGE = location(`${E}TheForge`, {
  label: "the forge",
  hadResident: "Joe MrsJoe Pip Biddy"
    .split(" ")
    .map((x) => ({ id: `${E}${x}` })),
});
const NEWGATE = location(`${E}Newgate`, { label: "Newgate" });
const BARNARDS = location(`${E}BarnardsInn`, {
  label: "Barnard's Inn",
  hadResident: [{ id: `${E}Pip` }, { id: `${E}Herbert` }],
});
const TEMPLE = location(`${E}TempleGardens`, {
  label: "Temple Gardens",
  hadResident: [
    { id: `${E}Pip` },
    { id: `${E}Herbert` },
    { id: `${E}Magwitch` },
  ],
});
const MILL_POND = location(`${E}MillPondBank`, {
  label: "Mill Pond Bank",
  hadResident: [{ id: `${E}Clara` }, { id: `${E}Magwitch` }],
});

const get = <T extends keyof Types>(t: ExecutionContext, spec: GetCase<T>) =>
  with_seeded_kb(t, preamble, TEST_DATA, TYPES, async (kb) => {
    const { type, id, fields, include, properties, included } = spec;
    // === OPERATION
    const response = await kb.get_record(type, id, {
      fields,
      include_related: include,
    });

    // === POSTCONDITIONS
    const got = response.value;
    if (!got) {
      t.log("RESPONSE", response);
      throw new Error("Result of operation was undefined");
    }

    const type_iri = TYPES[type]["@type"];
    assert_equal_records(t, got, { id, type: type_iri, ...properties });
    assert_equal_record_sets(t, response.included_values, included);
  });

const list = <T extends keyof Types>(t: ExecutionContext, spec: ListCase<T>) =>
  with_seeded_kb(t, preamble, TEST_DATA, TYPES, async (kb) => {
    const { type, fields, filter, include, records, included } = spec;
    // === OPERATION
    const response = await kb.list_records(
      type,
      {},
      { fields, filter, include_related: include }
    );

    // === POSTCONDITIONS
    const got = response.value;
    if (!got) {
      t.log("RESPONSE", response);
      throw new Error("Result of operation was undefined");
    }

    assert_equal_record_sets(t, got, records);
    assert_equal_record_sets(t, response.included_values, included);
  });

// ====== CASES

// === get default (no options)

for (const person of [
  PIP,
  MRS_JOE,
  JOE,
  MISS_HAVISHAM,
  GEORGIANA,
  BIDDY,
  HERBERT,
])
  test(`get: default case for ${person.id} (no fields specified)`, (t) =>
    get(t, { type: "Character", id: person.id, properties: person }));

// === get + fields

test("get: selecting no fields returns no properties (except id and type)", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    fields: { Character: [] },
    properties: {},
  }));

test("get: select one primitive field with fact", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    fields: { Character: ["givenName"] },
    properties: { givenName: "Phillip" },
  }));

test("get: select one multi-valued relationship, with no facts", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    fields: { Character: ["student"] },
    properties: { student: [] },
  }));

test("get: select multi-valued relationship, with facts", (t) =>
  get(t, {
    type: "Character",
    id: `${E}MissHavisham`,
    fields: { Character: ["lawyer"] },
    properties: { lawyer: [{ id: `${E}Jaggers` }] },
  }));

test("get: select multi-valued relationship, with no facts", (t) =>
  get(t, {
    type: "Character",
    id: `${E}MissHavisham`,
    fields: { Character: ["married"] },
    properties: { married: [] },
  }));

test("get: select multi-valued relationships, with and without facts", (t) =>
  get(t, {
    type: "Character",
    id: `${E}MissHavisham`,
    fields: { Character: ["married", "lawyer"] },
    properties: { married: [], lawyer: [{ id: `${E}Jaggers` }] },
  }));

test("get: select mixed fields on main type", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    fields: { Character: ["givenName", "mother"] },
    properties: { givenName: "Phillip", mother: { id: `${E}Georgiana` } },
  }));

test("get: selected single-valued fields missing a value are still absent", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    fields: { Character: ["givenName", "father"] },
    properties: { givenName: "Phillip" },
  }));

// === get + include

test("get: include property of same type, default case (no fields specified)", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["adoptedBy"],
    properties: PIP,
    included: [MRS_JOE_INCL, JOE_INCL],
  }));

// This is really an `include` test
test("get: ignores unrecognized included property", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["adoptedBy", "albatross" as any], // TS: this is the point
    fields: { Character: ["familyName"] },
    properties: props("familyName")(PIP),
    included: [MRS_JOE, JOE].map(props("familyName")),
  }));

test("get: ignores unrecognized type in fieldset", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["adoptedBy"],
    fields: {
      Character: ["familyName"],
      // @ts-expect-error: this is the point
      Novelist: [],
    },
    properties: props("familyName")(PIP),
    included: [MRS_JOE, JOE].map(props("familyName")),
  }));

test("get: ignores unrecognized property on main type in fieldset", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["adoptedBy"],
    fields: { Character: ["familyName", <any>"secretDreams"] }, // TS: the point
    properties: props("familyName")(PIP),
    included: [MRS_JOE, JOE].map(props("familyName")),
  }));

test("get: ignores unrecognized property on recognized type in fieldset", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["livedAt"],
    fields: { Character: [], Location: ["label", <any>"trapdoors"] }, // TS: the point
    properties: {},
    included: [FORGE, BARNARDS, TEMPLE].map(props("label")),
  }));

test("get: include property of same type, with selected primitive field", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["adoptedBy"],
    fields: { Character: ["familyName"] },
    properties: { familyName: "Pirrip" },
    included: [MRS_JOE, JOE].map(props("familyName")),
  }));

test("get: include property of same type, with selected fields", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["adoptedBy"],
    fields: { Character: ["givenName", "mother"] },
    properties: { givenName: "Phillip", mother: { id: `${E}Georgiana` } },
    included: [MRS_JOE, JOE].map(props("givenName", "mother")),
  }));

test("get: include property of a different type, default case (no fields specified)", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["livedAt"],
    properties: PIP,
    // TODO: here and below, hadResident should either not be returned at all, or should have values
    included: [FORGE, BARNARDS, TEMPLE].map((x) => ({ ...x, hadResident: [] })),
  }));

test("get: include property of a different type, fields specified on main type", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["livedAt"],
    fields: { Character: ["alias"] },
    properties: { alias: ["Pip"] },
    // TODO: see above
    included: [FORGE, BARNARDS, TEMPLE].map((x) => ({ ...x, hadResident: [] })),
  }));

test("get: include property of a different type, empty fields list specified on it", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["livedAt"],
    fields: { Location: [] },
    properties: PIP,
    included: [FORGE, BARNARDS, TEMPLE].map(props()),
  }));

test("get: include property of a different type, with one primitive field selected", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["livedAt"],
    fields: { Location: ["label"] },
    properties: PIP,
    included: [FORGE, BARNARDS, TEMPLE].map(props("label")),
  }));

test("get: include mixed types, one with fields selected", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["livedAt", "adoptedBy"],
    fields: { Location: ["label"] },
    properties: PIP,
    included: [
      ...[MRS_JOE_INCL, JOE_INCL],
      ...[FORGE, BARNARDS, TEMPLE].map(props("label")),
    ],
  }));

test("get: include mixed types, both with fields selected", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: ["livedAt", "adoptedBy"],
    fields: { Location: ["label"], Character: ["married", "alias"] },
    properties: { alias: ["Pip"], married: [] },
    included: [
      ...[MRS_JOE, JOE].map(props("alias", "married")),
      ...[FORGE, BARNARDS, TEMPLE].map(props("label")),
    ],
  }));

test("get: include converse property of same type, with one primitive field selected", (t) =>
  get(t, {
    type: "Character",
    id: `${E}MrsJoe`,
    include: ["adopted"],
    fields: { Character: ["familyName"] },
    properties: { familyName: "Pirrip" },
    included: [{ id: `${E}Pip`, type: `${V}Character`, familyName: "Pirrip" }],
  }));

test("get: include converse property of different type, with one primitive field selected", (t) =>
  get(t, {
    type: "Location",
    id: `${E}TheForge`,
    include: ["hadResident"],
    fields: { Character: ["familyName"] },
    properties: {
      label: "the forge",
      hadResident: [MRS_JOE, JOE, PIP, BIDDY].map(pick("id")),
    },
    included: [MRS_JOE, JOE, PIP, BIDDY].map(props("familyName")),
  }));

// === get + include paths

test("get: two-step include (adoptedBy -> livedAt)", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: [["adoptedBy", "Character:livedAt"]],
    fields: { Character: ["alias"], Location: ["label"] },
    properties: props("alias")(PIP),
    included: [FORGE].map(props("label")),
  }));

test("get: two-step include with intermediate (adoptedBy -> livedAt)", (t) =>
  get(t, {
    type: "Character",
    id: `${E}Pip`,
    include: [["adoptedBy"], ["adoptedBy", "Character:livedAt"]],
    fields: { Character: ["familyName", "givenName"], Location: ["label"] },
    properties: props("givenName", "familyName")(PIP),
    included: [
      ...[JOE, MRS_JOE].map(props("givenName", "familyName")),
      ...[FORGE].map(props("label")),
    ],
  }));

test("get: two-step include, one converse (hadResident -> adoptedBy)", (t) =>
  get(t, {
    type: "Location",
    id: `${E}SatisHouse`,
    include: [["hadResident", "Character:adoptedBy"]],
    fields: { Character: ["familyName"] },
    properties: SATIS,
    included: [MISS_HAVISHAM].map(props("familyName")),
  }));

test("get: two-step include, both converse (hadResident -> adopted)", (t) =>
  get(t, {
    type: "Location",
    id: `${E}TheForge`,
    include: [["hadResident", "Character:adopted"]],
    fields: { Character: ["alias"] },
    properties: FORGE,
    included: [PIP].map(props("alias")),
  }));

test("get: two-step include, with intermediate (hadResident -> adopted)", (t) =>
  get(t, {
    type: "Location",
    id: `${E}TheForge`,
    include: [["hadResident"], ["hadResident", "Character:adopted"]],
    fields: { Character: ["alias"] },
    properties: FORGE,
    included: [JOE, MRS_JOE, BIDDY, PIP].map(props("alias")),
  }));

// === list

test("list: default case (no fields specified)", (t) =>
  list(t, {
    type: "Location",
    records: [SATIS, WALWORTH, FORGE, NEWGATE, BARNARDS, TEMPLE, MILL_POND],
  }));

// === list + fields

test.skip("list: selecting no fields returns no properties (except id and type)", (t) =>
  list(t, {
    type: "Location",
    fields: { Location: [] },
    records: [SATIS, WALWORTH, FORGE, NEWGATE, BARNARDS, TEMPLE, MILL_POND].map(
      props()
    ),
  }));

test("list: select one primitive field", (t) =>
  list(t, {
    type: "Location",
    fields: { Location: ["label"] },
    records: [SATIS, WALWORTH, FORGE, NEWGATE, BARNARDS, TEMPLE, MILL_POND].map(
      props("label")
    ),
  }));

test("list: select one converse, multi-valued relationship field", (t) =>
  list(t, {
    type: "Location",
    fields: { Location: ["hadResident"] },
    records: [SATIS, WALWORTH, FORGE, NEWGATE, BARNARDS, TEMPLE, MILL_POND].map(
      props("hadResident")
    ),
  }));

// === list + include

test("list: include property of same type, default case (no fields specified)", (t) =>
  list(t, {
    type: "Character",
    include: ["mother"],
    filter: [["=", ["this", "Character:livedAt"], ["id", `${E}BarnardsInn`]]],
    records: [PIP, HERBERT],
    included: [
      GEORGIANA_INCL,
      {
        ...CHARACTER_DEFAULTS,
        id: `${E}MrsPocket`,
        type: `${V}Character`,
        married: [{ id: `${E}MrPocket` }],
      },
    ],
  }));

test("list: include property of different type, default case (no fields specified)", (t) =>
  list(t, {
    type: "Character",
    fields: { Location: [] },
    include: ["livedAt"],
    filter: [["=", ["this", "Character:livedAt"], ["id", `${E}TheForge`]]],
    records: [JOE, MRS_JOE, BIDDY, PIP],
    included: [FORGE, BARNARDS, TEMPLE].map(props()),
  }));

test("list: include property of same type, with selected relationship field", (t) =>
  list(t, {
    type: "Character",
    fields: { Character: ["mother"] },
    include: ["mother"],
    filter: [["=", ["this", "Character:livedAt"], ["id", `${E}BarnardsInn`]]],
    records: [PIP, HERBERT].map(props("mother")),
    included: [
      props("mother")(GEORGIANA),
      { id: `${E}MrsPocket`, type: `${V}Character` },
    ],
  }));
