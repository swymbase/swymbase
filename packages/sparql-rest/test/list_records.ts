import { metatypes } from "@swymbase/metatype-core";
import { assert_success, send } from "@swymbase/sparql-protocol-client";
import type { Filter, SchemaPermissions } from "@swymbase/sparql-rest";
import {
  list_records,
  with_temp_named_graph,
  with_temp_resource,
  write_converse_triples,
} from "@swymbase/sparql-rest";
import test, { ExecutionContext } from "ava";
import { endpoint } from "./lib/config";

const DEFAULT_LIMIT = 25;
const DEFAULT_OFFSET = 0;

const TEST_TYPES = metatypes({
  Person: {
    "@type": "https://example.com/vocab#Person",
    instance_namespace: `https://example.com/entities/Person/`,
    properties: {
      student_id: {
        term: "https://example.com/vocab#studentID",
        type: "string",
      },
      in_group: {
        term: "https://example.com/vocab#inGroup",
        type: "object",
        relationship: true,
        multivalued: true,
      },
      best_friend: {
        term: "https://example.com/vocab#bestFriend",
        type: "object",
        relationship: true,
      },
      is_admin: {
        term: "https://example.com/vocab#isAdmin",
        type: "boolean",
      },
      age: {
        term: "https://example.com/vocab#age",
        type: "number",
      },
      name: {
        term: "https://example.com/vocab#name",
        type: "string",
      },
      created_at: {
        term: "https://example.com/vocab#createdAt",
        type: "string",
      },
      has_connection: {
        term: "https://example.com/vocab#hasConnection",
        type: "object",
        relationship: true,
        multivalued: true,
        converse_property: "https://example.com/vocab#connection_users",
      },
    },
  },
  Group: {
    "@type": "https://example.com/vocab#Group",
    instance_namespace: `https://example.com/entities/Group/`,
    properties: {
      name: {
        term: "https://example.com/vocab#name",
        type: "string",
      },
      objectives: {
        term: "https://example.com/vocab#objectives",
        type: "string",
        multivalued: true,
      },
      has_members: {
        term: "https://example.com/vocab#hasMembers",
        type: "object",
        relationship: true,
        multivalued: true,
        converse_property: "https://example.com/vocab#inGroup",
      },
    },
  },
  Connection: {
    "@type": "https://example.com/vocab#Connection",
    instance_namespace: `https://example.com/entities/Connection/`,
    properties: {
      users: {
        term: "https://example.com/vocab#connection_users",
        type: "object",
        relationship: true,
        multivalued: true,
      },
    },
  },
});

const TEST_PERMISSIONS: SchemaPermissions<typeof TEST_TYPES, any> = {
  bindings: (_) => ({ user: `<${_.authenticated_user_id}>` }),
  types: {
    Group: {
      read: {
        description: "Can see group if user is a member.",
        where: (thing) => `
          ?user <${TEST_TYPES.Person.properties.in_group.term}> ${thing}
        `,
      },
    },
    Connection: {
      read: {
        description: "Can see connection if user is apart of it.",
        where: (thing) => `
          ${thing} <${TEST_TYPES.Connection.properties.users.term}> ?user
        `,
      },
    },
  },
};

const scope = (expr: string, graph_iri?: string) =>
  graph_iri ? `GRAPH <${graph_iri}> { ${expr} }` : expr;
const random = () => Math.round(Math.random() * 1e9);

const list_test = async (
  t: ExecutionContext,
  {
    people,
    graph_iri,
    order,
    limit,
    total_result_count,
    include_related,
    filter,
    filter_old,
    expected_people,
    ignore_permissions,
    authenticated_user_id,
    expected_connections,
    expected_groups,
  }: {
    people: {
      iri: string;
      group_iris?: string[];
      best_friend?: string;
      name?: string;
      is_admin?: boolean;
      age?: number;
    }[];
    graph_iri?: string;
    order?: { property: string; descending?: boolean }[];
    limit?: number;
    total_result_count?: number;
    include_related?: (keyof typeof TEST_TYPES["Person"]["properties"])[];
    filter?: Filter | null;
    filter_old?: Record<string, string>;
    expected_people?: string[];
    ignore_permissions?: boolean;
    authenticated_user_id?: string;
    expected_connections?: [string, string][];
    expected_groups?: string[];
  }
) => {
  // === SETUP
  const group_iris = [
    ...new Set(
      people.reduce<string[]>((ppl, p) => [...ppl, ...(p.group_iris ?? [])], [])
    ),
  ];

  const group_triples = group_iris
    .map(
      (iri) => `
        <${iri}> a <${TEST_TYPES.Group["@type"]}>
        ; <${TEST_TYPES.Group.properties.name.term}> "${iri.split("/").pop()}" .
      `
    )
    .join("\n");

  const group_insert = `INSERT DATA { ${scope(group_triples, graph_iri)} }`;
  const group_write_result = await send({
    endpoint,
    operation: { update: group_insert },
  });
  assert_success(t, group_write_result, "precondition: write group triples");

  const people_triples = people
    .map(
      (p) => `
        <${p.iri}> a <${TEST_TYPES.Person["@type"]}>
        ; <${TEST_TYPES.Person.properties.student_id.term}> "${random()}"
        ; <${TEST_TYPES.Person.properties.is_admin.term}> ${
        p.is_admin ? "true" : "false"
      }
        ; <${TEST_TYPES.Person.properties.age.term}> ${p.age ?? random()}
        ; <${TEST_TYPES.Person.properties.name.term}> "${p.name ?? random()}"
        ; <${TEST_TYPES.Person.properties.created_at.term}> "${new Date(
        new Date().setSeconds(random() % 1000)
      ).toISOString()}"
        ${(p.group_iris ?? [])
          .map(
            (gi) => `; <${TEST_TYPES.Person.properties.in_group.term}> <${gi}>`
          )
          .join("\n")} .
      ${
        p.best_friend
          ? `
          <${p.iri}> <${TEST_TYPES.Person.properties.best_friend.term}> <${
              p.best_friend
            }> .
          <${TEST_TYPES.Connection.instance_namespace}${random()}> a <${
              TEST_TYPES.Connection["@type"]
            }>
          ; <${TEST_TYPES.Connection.properties.users.term}> <${p.iri}>
          ; <${TEST_TYPES.Connection.properties.users.term}> <${
              p.best_friend
            }> .
        `
          : ""
      }
      `
    )
    .join("\n");

  const people_insert = `INSERT DATA { ${scope(people_triples, graph_iri)} }`;
  const write_people_result = await send({
    endpoint,
    operation: { update: people_insert },
  });
  assert_success(t, write_people_result, "precondition: write people triples");

  await write_converse_triples(TEST_TYPES, endpoint, {
    named_graph: graph_iri,
  });

  // === OPERATION
  const people_result = await list_records(
    TEST_TYPES,
    "Person",
    endpoint,
    {
      named_graph: graph_iri,
      limit,
      include_related,
      filter_old,
      // TEMP:
      filter: filter_old ? null : filter,
      order_by: order ?? [{ property: "created_at", descending: true }],
      permissions: TEST_PERMISSIONS,
      ignore_permissions: ignore_permissions ?? true,
      include_count: total_result_count != null,
    },
    { authenticated_user_id }
  );

  assert_success(t, people_result.context, "main operation, list people");

  const groups_result = await list_records(TEST_TYPES, "Group", endpoint, {
    named_graph: graph_iri,
  });

  assert_success(t, people_result.context, "main operation, list groups");

  // === POSTCONDITIONS
  const person_records = people_result.value!;
  const included_records = people_result.included_values!;

  const group_records = groups_result.value!;

  // Main document.
  t.log(
    "QUERY",
    ((people_result.context.request.operation as any) ?? {}).query
  );
  t.log("RECORDS", person_records);

  t.log("LIMIT", limit);
  t.log("EXPECTED PEOPLE", expected_people);
  t.log("PEOPLE", people);

  t.is(
    person_records.length,
    limit ?? expected_people?.length ?? people.length,
    "result count"
  );

  t.is(people_result.limit, limit ?? DEFAULT_LIMIT);
  t.is(people_result.offset, DEFAULT_OFFSET);

  for (const record of person_records!) {
    t.assert(record, "record exists");
    t.assert(record.id, "record id");
    t.is(typeof record.id, "string", "record id is a string");
    t.deepEqual(record.type, TEST_TYPES.Person["@type"], "matches given");
    t.assert(record.student_id, "student id");
    t.is(typeof record.student_id, "string", "student id is a string");
  }

  // Total count.
  if (total_result_count != null) {
    t.is(
      people_result.total_result_count,
      total_result_count,
      "total_result_count result is incorrect"
    );
  } else {
    t.is(people_result.total_result_count, undefined);
  }

  // Included values.
  if (include_related?.includes("in_group")) {
    if (expected_groups) {
      const groups = included_records.filter(
        (r) => r.type === TEST_TYPES.Group["@type"]
      );

      t.is(groups.length, expected_groups.length);

      for (const exp of expected_groups) {
        const group = groups.find((g) => g.id === exp);

        t.assert(group, "Group was not included.");
      }
    } else {
      for (const group_iri of group_iris) {
        const group = included_records.find((r) => r.id === group_iri)!;

        t.assert(group, "group");
        t.is(typeof group.name, "string", "group name is string");
      }
    }
  }

  if (include_related?.includes("has_connection")) {
    if (expected_connections) {
      const connections = included_records.filter(
        (r) => r.type === TEST_TYPES.Connection["@type"]
      );

      t.is(connections.length, expected_connections.length);

      for (const exp of expected_connections) {
        const connection = connections.find((c) => {
          return (
            JSON.stringify((c.users as any).map((i) => i.id).sort()) ===
            JSON.stringify(exp.sort())
          );
        });

        t.assert(connection, "Connection was not included.");
      }
    } else {
      for (const person of people) {
        if (person.best_friend) {
          const connection = included_records.find((r) => {
            return (
              r.users &&
              (r.users as any).filter(
                (u) => u.id === person.iri || u.id === person.best_friend
              ).length === 2
            );
          });

          t.assert(connection, "connection was not included");
        }
      }
    }
  }

  // Filter.
  if (filter?.length) {
    t.assert(expected_people, "expected people");

    for (const person_iri of expected_people!) {
      const person = person_records.find((r) => r.id === person_iri);
      t.assert(person, `records include person with id ${person_iri}`);
    }
  }

  // Converse properties.
  for (const person of people) {
    for (const group_iri of person?.group_iris || []) {
      const group = group_records.find((g) => g.id === group_iri);
      t.assert(group, "group exists");

      const group_member = group?.has_members?.find((p) => p.id === person.iri);
      t.assert(group_member, "group has person");
    }
  }

  // Order by.
  const created_at = person_records.map((p) => p.created_at!);

  t.deepEqual(created_at, [...created_at].sort().reverse());
};

// TODO: This test always (or almost always?) fails on a second run because it
// appears not to be cleaned up properly.
test.skip("lists records of a given type from the default graph", (t) =>
  with_temp_resource({ endpoint }, (record1_iri) =>
    with_temp_resource({ endpoint }, (record2_iri) =>
      with_temp_resource({ endpoint }, (record3_iri) =>
        list_test(t, {
          people: [
            { iri: record1_iri },
            { iri: record2_iri },
            { iri: record3_iri },
          ],
          total_result_count: 3,
        })
      )
    )
  ));

test("lists records of a given type from a named graph", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        { iri: `${TEST_TYPES.Person.instance_namespace}Person1` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person2` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person3` },
      ],
      graph_iri,
    })
  ));

test("returns empty array when no matching records exist in the graph", (t) =>
  with_temp_named_graph(endpoint, async (graph_iri) => {
    const result = await list_records(
      TEST_TYPES,
      "Person",
      endpoint,
      { named_graph: graph_iri },
      {}
    );
    assert_success(t, result.context, "main operation");
    const records = result.value;
    t.log(records);
    t.is(records?.length, 0);
  }));

test("returns correct number of results when limited", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        { iri: `${TEST_TYPES.Person.instance_namespace}Person1` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person2` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person3` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person4` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person5` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person6` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person7` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person8` },
        { iri: `${TEST_TYPES.Person.instance_namespace}Person9` },
      ],
      graph_iri,
      limit: 3,
      total_result_count: 9,
    })
  ));

test("returns correct included records", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group2`,
          ],
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group3`],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [],
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person1`,
        },
      ],
      graph_iri,
      include_related: ["in_group", "has_connection"],
    })
  ));

test("returns expected records using simple relationship filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group2`,
          ],
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group3`],
        },
      ],
      graph_iri,
      filter: [
        [
          "=",
          ["this", "Person:best_friend"],
          ["id", `${TEST_TYPES.Person.instance_namespace}Person2`],
        ],
      ],
      expected_people: [`${TEST_TYPES.Person.instance_namespace}Person1`],
    })
  ));

test("returns expected records using complex relationship filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group1`],
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person3`,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group2`,
            `${TEST_TYPES.Group.instance_namespace}Group3`,
          ],
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person3`,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group3`],
        },
      ],
      graph_iri,
      filter: [
        [
          "=",
          ["this", "Person:best_friend"],
          ["id", `${TEST_TYPES.Person.instance_namespace}Person3`],
        ],
        // The `in_group` condition has no effect (test passes without it)
        [
          "in",
          ["this", "Person:in_group"],
          [
            ["id", `${TEST_TYPES.Group.instance_namespace}Group1`],
            ["id", `${TEST_TYPES.Group.instance_namespace}Group3`],
          ],
        ],
      ],
      expected_people: [
        `${TEST_TYPES.Person.instance_namespace}Person1`,
        `${TEST_TYPES.Person.instance_namespace}Person2`,
      ],
    })
  ));

test("returns expected records using complex relationship filter and limit", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          age: 11,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group1`],
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person3`,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          age: 22,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group2`,
            `${TEST_TYPES.Group.instance_namespace}Group3`,
          ],
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person3`,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          age: 33,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group3`],
        },
      ],
      graph_iri,
      filter: [
        [
          "=",
          ["this", "Person:best_friend"],
          ["id", `${TEST_TYPES.Person.instance_namespace}Person3`],
        ],
        [
          "in",
          ["this", "Person:in_group"],
          [
            ["id", `${TEST_TYPES.Group.instance_namespace}Group1`],
            ["id", `${TEST_TYPES.Group.instance_namespace}Group3`],
          ],
        ],
      ],
      limit: 1,
      total_result_count: 2,
      order: [{ property: "age" }],
      expected_people: [`${TEST_TYPES.Person.instance_namespace}Person1`],
    })
  ));

test("returns expected records using boolean filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group1`],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group3`],
          is_admin: true,
        },
      ],
      graph_iri,
      filter: [["=", ["this", "Person:is_admin"], true]],
      expected_people: [`${TEST_TYPES.Person.instance_namespace}Person3`],
    })
  ));

test("returns expected records using numerical filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group1`],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 24,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group3`],
        },
      ],
      graph_iri,
      filter: [["=", ["this", "Person:age"], 24]],
      expected_people: [`${TEST_TYPES.Person.instance_namespace}Person2`],
    })
  ));

test("returns expected records using string filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group1`],
          name: "John Smith",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group3`],
          name: "Mikel Blake",
        },
      ],
      graph_iri,
      filter: [
        [
          "or",
          ["match", ["this", "Person:name"], "Smith"],
          ["match", ["this", "Person:name"], "Mikel"],
        ],
      ],
      expected_people: [
        `${TEST_TYPES.Person.instance_namespace}Person1`,
        `${TEST_TYPES.Person.instance_namespace}Person3`,
      ],
    })
  ));

test("returns expected records using complex mixed filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group2`,
          ],
          name: "John Smith",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group2`,
          ],
          name: "mikel blake",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group2`,
          ],
          age: 25,
          is_admin: true,
          name: "john SmitH",
        },
      ],
      graph_iri,
      filter: [
        ["=", ["this", "Person:age"], 25],
        ["=", ["this", "Person:is_admin"], true],
        ["match", ["this", "Person:name"], "ith", "i"],
        [
          "in",
          ["this", "Person:in_group"],
          [
            ["id", `${TEST_TYPES.Group.instance_namespace}Group1`],
            ["id", `${TEST_TYPES.Group.instance_namespace}Group2`],
          ],
        ],
      ],
      expected_people: [`${TEST_TYPES.Person.instance_namespace}Person4`],
    })
  ));

test("returns expected records using nested filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group2`,
          ],
          name: "John Smith",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group2`,
          ],
          name: "Mikel Blake",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group3`,
          ],
          age: 25,
          is_admin: true,
          name: "John Smith",
        },
      ],
      graph_iri,
      filter: [
        ["=", ["this", ["/", "Person:in_group", "Group:name"]], "Group3"],
      ],
      expected_people: [`${TEST_TYPES.Person.instance_namespace}Person4`],
    })
  ));

test("returns expected records using nested converse filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group3`,
          ],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group2`,
          ],
          name: "Mikel Blake",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group3`],
          age: 25,
          is_admin: true,
          name: "John Smith",
        },
      ],
      graph_iri,
      filter: [
        [
          "=",
          ["this", ["/", "Person:in_group", "Group:has_members"]],
          ["id", `${TEST_TYPES.Person.instance_namespace}Person4`],
        ],
      ],
      expected_people: [
        `${TEST_TYPES.Person.instance_namespace}Person1`,
        `${TEST_TYPES.Person.instance_namespace}Person4`,
      ],
    })
  ));

test("returns expected records using complex nested converse filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group3`,
          ],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [
            `${TEST_TYPES.Group.instance_namespace}Group1`,
            `${TEST_TYPES.Group.instance_namespace}Group2`,
          ],
          name: "Mikel Blake",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group3`],
          age: 30,
          is_admin: true,
          name: "John Smith",
        },
      ],
      graph_iri,
      filter: [
        [
          "=",
          ["this", ["/", "Person:in_group", "Group:has_members", "Person:age"]],
          25,
        ],
      ],
      expected_people: [
        `${TEST_TYPES.Person.instance_namespace}Person2`,
        `${TEST_TYPES.Person.instance_namespace}Person3`,
      ],
    })
  ));

test("returns expected records using initial converse property filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
          name: "Don Donovon",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          name: "Mikel Blake",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          age: 30,
          is_admin: true,
          name: "John Smith",
        },
      ],
      graph_iri,
      filter: [
        [
          "=",
          ["this", ["/", "Person:has_connection", "Connection:users"]],
          ["id", `${TEST_TYPES.Person.instance_namespace}Person2`],
        ],
        [
          "match",
          [
            "this",
            ["/", "Person:has_connection", "Connection:users", "Person:name"],
          ],
          "Don",
        ],
      ],
      expected_people: [
        `${TEST_TYPES.Person.instance_namespace}Person1`,
        `${TEST_TYPES.Person.instance_namespace}Person2`,
      ],
    })
  ));

test("returns no records using initial converse property filter has not found name", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
          name: "Don Donovon",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          name: "Mikel Blake",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          age: 30,
          is_admin: true,
          name: "John Smith",
        },
      ],
      graph_iri,
      filter: [
        // This clause appears to be unnecessary
        [
          "=",
          ["this", ["/", "Person:has_connection", "Connection:users"]],
          ["id", `${TEST_TYPES.Person.instance_namespace}Person2`],
        ],
        [
          "match",
          [
            "this",
            ["/", "Person:has_connection", "Connection:users", "Person:name"],
          ],
          "Jake",
        ],
      ],
      expected_people: [],
      total_result_count: 0,
    })
  ));

test("returns expected records on passing complex relationship NOT filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
          name: "Don Donovon",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          name: "Mikel Blake",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
          age: 30,
          is_admin: true,
          name: "John Smith",
        },
      ],
      graph_iri,
      filter: [
        [
          "not",
          [
            "=",
            ["this", ["/", "Person:has_connection", "Connection:users"]],
            `${TEST_TYPES.Person.instance_namespace}Person2`,
          ],
        ],
      ],
      filter_old: {
        "!has_connection.Connection:users": `${TEST_TYPES.Person.instance_namespace}Person2`,
      },
      expected_people: [`${TEST_TYPES.Person.instance_namespace}Person3`],
    })
  ));

test("returns expected records on passing simple NOT filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
          name: "Don Donovon",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          name: "Mikel Blake",
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
          age: 30,
          is_admin: true,
          name: "John Smith",
        },
      ],
      graph_iri,
      // TODO: revisit, this doesn't mean what it did before
      filter: [["not", ["=", ["this", "Person:age"], 25]]],
      expected_people: [
        `${TEST_TYPES.Person.instance_namespace}Person1`,
        `${TEST_TYPES.Person.instance_namespace}Person4`,
      ],
    })
  ));

test("returns expected records on passing complex mixed NOT filter", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person4`,
          name: "Don Donovon",
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          name: "Mikel Blake",
          age: 25,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          age: 30,
          is_admin: true,
          name: "John Smith",
        },
      ],
      graph_iri,
      filter: [
        ["=", ["this", "Person:age"], 25],
        // TODO: what does not mean here? not exists?
        // currently this will fail if there's *any* value that's not equal
        // which is okay for functional properties not probably not the intent
        [
          "=",
          ["this", ["/", "Person:has_connection", "Connection:users"]],
          ["id", `${TEST_TYPES.Person.instance_namespace}Person4`],
        ],
      ],
      filter_old: {
        "!has_connection.Connection:users": `${TEST_TYPES.Person.instance_namespace}Person4`,
        age: "25",
      },
      expected_people: [
        `${TEST_TYPES.Person.instance_namespace}Person2`,
        `${TEST_TYPES.Person.instance_namespace}Person3`,
      ],
    })
  ));

// TODO: Unskip when permissions are being applied to related records.
test.skip("returns expected included values when passing permissions", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group1`],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group1`],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
        },
      ],
      graph_iri,
      ignore_permissions: false,
      authenticated_user_id: `${TEST_TYPES.Person.instance_namespace}Person1`,
      include_related: ["in_group"],
      expected_people: [
        `${TEST_TYPES.Person.instance_namespace}Person1`,
        `${TEST_TYPES.Person.instance_namespace}Person2`,
        `${TEST_TYPES.Person.instance_namespace}Person3`,
        `${TEST_TYPES.Person.instance_namespace}Person4`,
      ],
      expected_groups: [`${TEST_TYPES.Group.instance_namespace}Group2`],
    })
  ));

// TODO: Unskip when permissions are being applied to related records.
test.skip("returns expected included values via converse property when passing permissions", (t) =>
  with_temp_named_graph(endpoint, (graph_iri) =>
    list_test(t, {
      people: [
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person2`,
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person2`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person3`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group1`],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person3`,
          best_friend: `${TEST_TYPES.Person.instance_namespace}Person1`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group1`],
        },
        {
          iri: `${TEST_TYPES.Person.instance_namespace}Person4`,
          group_iris: [`${TEST_TYPES.Group.instance_namespace}Group2`],
        },
      ],
      graph_iri,
      ignore_permissions: false,
      authenticated_user_id: `${TEST_TYPES.Person.instance_namespace}Person1`,
      include_related: ["has_connection"],
      expected_people: [
        `${TEST_TYPES.Person.instance_namespace}Person1`,
        `${TEST_TYPES.Person.instance_namespace}Person2`,
        `${TEST_TYPES.Person.instance_namespace}Person3`,
        `${TEST_TYPES.Person.instance_namespace}Person4`,
      ],
      expected_connections: [
        [
          `${TEST_TYPES.Person.instance_namespace}Person1`,
          `${TEST_TYPES.Person.instance_namespace}Person2`,
        ],
        [
          `${TEST_TYPES.Person.instance_namespace}Person1`,
          `${TEST_TYPES.Person.instance_namespace}Person3`,
        ],
      ],
    })
  ));
