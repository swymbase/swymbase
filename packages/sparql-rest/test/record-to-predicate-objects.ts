import { InterfacesFrom, metatypes, Specs as $ } from "@swymbase/metatype-core";
import type { RDFTerm } from "@swymbase/sparql-protocol-client";
import { record_to_predicate_objects } from "@swymbase/sparql-rest";
import * as assert from "assert";
import test, { ExecutionContext } from "ava";

const FOAF = "http://xmlns.com/foaf/0.1/";
const RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
const RDF_TYPE = `${RDF}type`;
const RDF_JSON = `${RDF}JSON`;
const XSD = "http://www.w3.org/2001/XMLSchema#";
const XSD_BOOLEAN = `${XSD}boolean`;
const XSD_STRING = `${XSD}string`;
const XSD_FLOAT = `${XSD}float`;
const SKOS_LABEL = "http://www.w3.org/2004/02/skos/core#prefLabel";
const FOAF_PERSON = `${FOAF}Person`;
const FOAF_GROUP = `${FOAF}Group`;
const FOAF_NAME = `${FOAF}name`;
const FOAF_NICK = `${FOAF}nick`;
const FOAF_MEMBER = `${FOAF}member`;
const EX = "https://example.com/vocab#";
const EX_PUBLIC = `${EX}public`;
const EX_MOTHER = `${EX}mother`;
const EX_PASSCODE = `${EX}passcode`;
const EX_AMBIVALENT = `${EX}boolean`;
const EX_LEADER = `${EX}leader`;
const EX_LUCKY_NUMBER = `${EX}luckyNumber`;
const EX_BIRTH_LOCATION = `${EX}birthLocation`;
const EX_COLOR = `${EX}color`;

const TYPES = metatypes({
  Person: {
    "@type": FOAF_PERSON,
    instance_namespace: `https://example.com/entities/Person/`,
    properties: {
      name: { term: FOAF_NAME, type: "string" },
      aliases: { term: FOAF_NICK, type: "string", multivalued: true },
      ambivalent: { term: EX_AMBIVALENT, type: "boolean", multivalued: true },
      mother: { term: EX_MOTHER, type: "object", relationship: true },
      birthLocation: {
        term: EX_BIRTH_LOCATION,
        type: { spec: $.tuple($.number, $.number) },
      },
      luckyNumbers: {
        term: EX_LUCKY_NUMBER,
        type: "number",
        multivalued: true,
      },
    },
  },
  Group: {
    "@type": FOAF_GROUP,
    instance_namespace: `https://example.com/entities/Group/`,
    properties: {
      name: { term: SKOS_LABEL, type: "string" },
      public: { term: EX_PUBLIC, type: "boolean" },
      passcode: { term: EX_PASSCODE, type: "number" },
      leader: { term: EX_LEADER, type: "object", relationship: true },
      colors: {
        term: EX_COLOR,
        type: {
          spec: $.or(
            $.record({ r: $.number, g: $.number, b: $.number }),
            $.record({ h: $.number, s: $.number, l: $.number })
          ),
        },
        multivalued: true,
      },
      members: {
        term: FOAF_MEMBER,
        type: "object",
        relationship: true,
        multivalued: true,
      },
    },
  },
});

type Type = keyof Types;
type MetaTypes = typeof TYPES;
type Types = InterfacesFrom<MetaTypes>;

// Helpers for constructing RDF terms
const n = (value: string): RDFTerm => ({ type: "uri", value });
const l = (value: boolean | number | string): RDFTerm => ({
  type: "literal",
  value: value.toString(),
  datatype: {
    string: XSD_STRING,
    boolean: XSD_BOOLEAN,
    number: XSD_FLOAT,
  }[typeof value],
});
const j = (value: Object): RDFTerm => ({
  type: "literal",
  value: JSON.stringify(value),
  datatype: RDF_JSON,
});

interface TestCase<T extends Type = Type> {
  readonly type: T;
  readonly record: Types[T];

  /**
   * An unordered set of the predicate-object pairs imputed by the given record
   * when read as the given type.
   */
  readonly tuples: ReturnType<typeof record_to_predicate_objects>;
}

const check = <T extends Type>(t: ExecutionContext, spec: TestCase<T>) => {
  const { type, record, tuples } = spec;
  const type_spec = TYPES[type];
  const result = record_to_predicate_objects(type_spec, record);
  assert.deepStrictEqual(new Set(result), new Set(tuples));

  t.pass();
};

test("writes record type", (t) => {
  check(t, {
    type: "Group",
    record: {},
    tuples: [[n(RDF_TYPE), n(FOAF_GROUP)]],
  });
  check(t, {
    type: "Person",
    record: {},
    tuples: [[n(RDF_TYPE), n(FOAF_PERSON)]],
  });
});

test("ignores given type", (t) => {
  check(t, {
    type: "Group",
    record: { type: `${EX}Nonsense` },
    tuples: [[n(RDF_TYPE), n(FOAF_GROUP)]],
  });
  check(t, {
    type: "Person",
    record: { type: `${EX}Group` },
    tuples: [[n(RDF_TYPE), n(FOAF_PERSON)]],
  });
});

// PROVISIONAL: type will be made optional
// test.todo("type is optional in schema");

test("single-valued string", (t) => {
  check(t, {
    type: "Group",
    record: { name: "NRDC" },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(SKOS_LABEL), l("NRDC")],
    ],
  });
  check(t, {
    type: "Person",
    record: { name: "Bjork" },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(FOAF_NAME), l("Bjork")],
    ],
  });
});

test("multi-valued string", (t) => {
  check(t, {
    type: "Person",
    record: { aliases: [] },
    tuples: [[n(RDF_TYPE), n(FOAF_PERSON)]],
  });
  check(t, {
    type: "Person",
    record: { aliases: ["Will"] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(FOAF_NICK), l("Will")],
    ],
  });
  check(t, {
    type: "Person",
    record: { aliases: ["Will", "Bill"] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(FOAF_NICK), l("Will")],
      [n(FOAF_NICK), l("Bill")],
    ],
  });
});

test("single-valued boolean", (t) => {
  check(t, {
    type: "Group",
    record: { public: false },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(EX_PUBLIC), l(false)],
    ],
  });
  check(t, {
    type: "Group",
    record: { public: true },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(EX_PUBLIC), l(true)],
    ],
  });
});

// Can't think of a real example of a multi-valued boolean
test("multi-valued boolean", (t) => {
  check(t, {
    type: "Person",
    record: { ambivalent: [] },
    tuples: [[n(RDF_TYPE), n(FOAF_PERSON)]],
  });
  check(t, {
    type: "Person",
    record: { ambivalent: [true] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(EX_AMBIVALENT), l(true)],
    ],
  });
  check(t, {
    type: "Person",
    record: { ambivalent: [true, false] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(EX_AMBIVALENT), l(true)],
      [n(EX_AMBIVALENT), l(false)],
    ],
  });
});

test("single-valued number", (t) => {
  check(t, {
    type: "Group",
    record: { passcode: 0 },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(EX_PASSCODE), l(0)],
    ],
  });
  check(t, {
    type: "Group",
    record: { passcode: 1234 },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(EX_PASSCODE), l(1234)],
    ],
  });
  check(t, {
    type: "Group",
    record: { passcode: -9876 },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(EX_PASSCODE), l(-9876)],
    ],
  });
  check(t, {
    type: "Group",
    record: { passcode: 1234.5678 },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(EX_PASSCODE), l(1234.5678)],
    ],
  });
});

test("multi-valued number", (t) => {
  check(t, {
    type: "Person",
    record: { luckyNumbers: [] },
    tuples: [[n(RDF_TYPE), n(FOAF_PERSON)]],
  });
  check(t, {
    type: "Person",
    record: { luckyNumbers: [0] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(EX_LUCKY_NUMBER), l(0)],
    ],
  });
  check(t, {
    type: "Person",
    record: { luckyNumbers: [0, 3421, 1, 0.00005, 99999] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(EX_LUCKY_NUMBER), l(0)],
      [n(EX_LUCKY_NUMBER), l(1)],
      [n(EX_LUCKY_NUMBER), l(3421)],
      [n(EX_LUCKY_NUMBER), l(0.00005)],
      [n(EX_LUCKY_NUMBER), l(99999)],
    ],
  });
});

test("single-valued relationship", (t) => {
  check(t, {
    type: "Person",
    record: { mother: { id: `${EX}Alice` } },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(EX_MOTHER), n(`${EX}Alice`)],
    ],
  });
  check(t, {
    type: "Group",
    record: { leader: { id: `${EX}Harriet` } },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(EX_LEADER), n(`${EX}Harriet`)],
    ],
  });
});

test("multi-valued relationship", (t) => {
  check(t, {
    type: "Group",
    record: { members: [] },
    tuples: [[n(RDF_TYPE), n(FOAF_GROUP)]],
  });
  check(t, {
    type: "Group",
    record: { members: [{ id: `${EX}Groucho` }] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(FOAF_MEMBER), n(`${EX}Groucho`)],
    ],
  });
  check(t, {
    type: "Group",
    record: { members: [{ id: `${EX}John` }, { id: `${EX}Presper` }] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(FOAF_MEMBER), n(`${EX}John`)],
      [n(FOAF_MEMBER), n(`${EX}Presper`)],
    ],
  });
});

test("single-valued spec", (t) => {
  check(t, {
    type: "Person",
    record: { birthLocation: [34.34, -73.333] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(EX_BIRTH_LOCATION), j([34.34, -73.333])],
    ],
  });
  check(t, {
    type: "Person",
    record: { birthLocation: [0, 1] },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(EX_BIRTH_LOCATION), j([0, 1])],
    ],
  });
});

test("multi-valued spec", (t) => {
  check(t, {
    type: "Group",
    record: { colors: [] },
    tuples: [[n(RDF_TYPE), n(FOAF_GROUP)]],
  });
  check(t, {
    type: "Group",
    record: {
      colors: [
        { r: 255, g: 0, b: 0 },
        { r: 255, g: 255, b: 0 },
        { r: 0, g: 255, b: 0 },
      ],
    },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(EX_COLOR), j({ r: 255, g: 0, b: 0 })],
      [n(EX_COLOR), j({ r: 255, g: 255, b: 0 })],
      [n(EX_COLOR), j({ r: 0, g: 255, b: 0 })],
    ],
  });
});

test("multiple properties", (t) => {
  check(t, {
    type: "Group",
    record: {
      name: "The Founding Mothers",
      public: true,
      passcode: 1971,
      leader: { id: `${EX}Truth` },
      colors: [
        { h: 0, s: 128, l: 64 },
        { h: 42, s: 128, l: 255 },
      ],
      members: [
        { id: `${EX}NinaTotenberg` },
        { id: `${EX}SusanStamberg` },
        { id: `${EX}CokieRoberts` },
        { id: `${EX}LindaWertheimer` },
      ],
    },
    tuples: [
      [n(RDF_TYPE), n(FOAF_GROUP)],
      [n(SKOS_LABEL), l("The Founding Mothers")],
      [n(EX_PUBLIC), l(true)],
      [n(EX_PASSCODE), l(1971)],
      [n(EX_LEADER), n(`${EX}Truth`)],
      [n(FOAF_MEMBER), n(`${EX}NinaTotenberg`)],
      [n(FOAF_MEMBER), n(`${EX}SusanStamberg`)],
      [n(FOAF_MEMBER), n(`${EX}CokieRoberts`)],
      [n(FOAF_MEMBER), n(`${EX}LindaWertheimer`)],
      [n(EX_COLOR), j({ h: 0, s: 128, l: 64 })],
      [n(EX_COLOR), j({ h: 42, s: 128, l: 255 })],
    ],
  });
});

test("ignores undefined record properties", (t) => {
  check(t, {
    type: "Person",
    record: {
      name: "Shakespeare",
      aliases: ["Will", "The Bard of Avon"],
      ambivalent: [true],
      mother: { id: `${EX}MaryArden` },
      birthLocation: [55.66, -77.88],
      // @ts-ignore-error: this is the point
      father: { id: `${EX}JohnShakespeare` },
      luckyNumbers: [26, 23],
    },
    tuples: [
      [n(RDF_TYPE), n(FOAF_PERSON)],
      [n(FOAF_NAME), l("Shakespeare")],
      [n(FOAF_NICK), l("Will")],
      [n(FOAF_NICK), l("The Bard of Avon")],
      [n(EX_AMBIVALENT), l(true)],
      [n(EX_MOTHER), n(`${EX}MaryArden`)],
      [n(EX_LUCKY_NUMBER), l(23)],
      [n(EX_LUCKY_NUMBER), l(26)],
      [n(EX_BIRTH_LOCATION), j([55.66, -77.88])],
    ],
  });
});

test("ignores nullish values", (t) => {
  check(t, {
    type: "Group",
    record: {
      // @ts-expect-error: this is the point
      passcode: null,
    },
    tuples: [[n(RDF_TYPE), n(FOAF_GROUP)]],
  });
  check(t, {
    type: "Person",
    record: {
      // @ts-expect-error: this is the point
      mother: null,
    },
    tuples: [[n(RDF_TYPE), n(FOAF_PERSON)]],
  });
  // TS allows undefined because the types are partial.
  check(t, {
    type: "Group",
    record: { leader: undefined },
    tuples: [[n(RDF_TYPE), n(FOAF_GROUP)]],
  });
  check(t, {
    type: "Person",
    record: { luckyNumbers: undefined },
    tuples: [[n(RDF_TYPE), n(FOAF_PERSON)]],
  });
});

test("ignores non-arrays for multi-valued properties", (t) => {
  check(t, {
    type: "Person",
    record: {
      // @ts-expect-error: this is the point
      luckyNumbers: 0,
    },
    tuples: [[n(RDF_TYPE), n(FOAF_PERSON)]],
  });
});

// TODO:
// - numbers: infinity
// - numbers: nan
// - numbers: -0
// FUTURE: single-valued value-spec
// FUTURE: multivalued value-spec
// deduplicates? or just let sparql engine do that
