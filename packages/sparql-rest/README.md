# SPARQL REST interface

This package provides a typed, REST-like interface over a SPARQL service.

## Dependencies

- jsonld

## Testing

The tests require a `SPARQL_ENDPOINT` environment variable to be set.

## Goals

- mediate between knowledge base and (primarily JSON-based) clients
- support answering questions about the system
