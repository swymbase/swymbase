import { parse_s3_arn, s3_arn } from "@swymbase/media-interop";
import test from "ava";

test("detect that something is not an S3 ARN", (t) => {
  t.is(parse_s3_arn("http://example.com/something-else"), undefined);
});

test("parsing an S3 ARN yields the Bucket name and Key", (t) => {
  const { Bucket, Key } = parse_s3_arn("arn:aws:s3:::holey/to/the/kingdom")!;
  t.is(Bucket, "holey");
  t.is(Key, "to/the/kingdom");
});

test("convert S3 Bucket and Key to a valid ARN", (t) => {
  const arn = s3_arn({ Bucket: "exemplary", Key: "happenstance" });
  t.is(arn, "arn:aws:s3:::exemplary/happenstance");
});
