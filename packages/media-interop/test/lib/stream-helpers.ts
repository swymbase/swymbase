import { Readable } from "stream";

export const read_stream = async (stream: Readable): Promise<string> =>
  new Promise((resolve, reject) => {
    const buffer: string[] = [];
    stream.on("data", (chunk) => buffer.push(chunk.toString()));
    stream.on("end", () => resolve(buffer.join("")));
    stream.on("error", reject);
  });
