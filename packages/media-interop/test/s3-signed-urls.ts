import { presigned_get_url, presigned_put_url } from "@swymbase/media-interop";
import test from "ava";
import fetch from "node-fetch";
import { v4 as uuid } from "uuid";
import { read_stream } from "./lib/stream-helpers";
import { with_temp_s3_object } from "./lib/with_temp_s3_object";

test("creates a URL for uploading to the default bucket", (t) =>
  with_temp_s3_object(async ({ s3, Bucket, Key }) => {
    // === SETUP
    const body = `This is a test of upload URL's ${uuid()}`;

    // === OPERATION
    // Create an upload URL for the object
    const signed_url = await presigned_put_url({ Bucket, Key });

    // === POSTCONDITIONS
    // Upload something to the URL
    const put_response = await fetch(signed_url, {
      method: "PUT",
      body,
    });
    if (!put_response.ok) {
      t.log("PUT RESPONSE", put_response);
      const body = await put_response.text();
      t.log("PUT RESPONSE BODY", body);
      throw new Error(
        `PUT operation failed with status ${put_response.status}`
      );
    }

    // Fetch the object using plain SDK
    const get_response = await s3.getObject({ Bucket, Key });
    if (!get_response.Body) throw new Error("getObject response has no Body!");

    // Assert that it matches what was uploaded
    const got_body = await read_stream(get_response.Body);
    t.is(got_body, body);
  }));

test("creates a URL for downloading from the default bucket", (t) =>
  with_temp_s3_object(async ({ s3, Bucket, Key }) => {
    // === SETUP
    const body = `This is a test of download URL's ${uuid()}`;

    // Use plain SDK to put an object
    await s3.putObject({ Bucket, Key, Body: body });

    // === PRECONDITIONS
    // Ensure that the object is there
    const get_response = await s3.getObject({ Bucket, Key });
    if (!get_response.Body) {
      t.log("GET RESPONSE", get_response);
      throw new Error("getObject response has no Body!");
    }
    const got_body = await read_stream(get_response.Body);

    t.is(got_body, body);

    // === OPERATION
    // Get a download URL for the object
    const signed_url = await presigned_get_url({ Bucket, Key });

    // === POSTCONDITIONS
    // Get the object using plain HTTP
    const fetch_response = await fetch(signed_url);
    if (!fetch_response.ok) {
      t.log("FETCH RESPONSE", fetch_response);
      throw new Error(`Fetch failed with status ${fetch_response.status}`);
    }

    const got_text = await fetch_response.text();

    t.is(got_text, body, "Expect roundtripped content");
  }));

test("creates same URL for downloading for X amount of time", (t) =>
  with_temp_s3_object(async ({ s3, Bucket, Key }) => {
    // === SETUP
    const body = `This is a test of download URL's ${uuid()}`;

    // Use plain SDK to put an object
    await s3.putObject({ Bucket, Key, Body: body });
    // === PRECONDITIONS
    // Ensure that the object is there
    const get_response = await s3.getObject({ Bucket, Key });
    if (!get_response.Body) throw new Error("getObject response has no Body!");
    t.log(get_response.Body);
    const got_body = await read_stream(get_response.Body);

    t.is(got_body, body);

    // === OPERATION
    const get_signed_url = async () => {
      const expiresIn = 60 * 60; // secondsa

      const signingDate = new Date();

      // Round down to nearest 10 seconds for cacheable URLs.
      signingDate.setSeconds(
        signingDate.getSeconds() - (signingDate.getSeconds() % 10)
      );
      signingDate.setMilliseconds(0);

      // Get a download URL for the object
      return presigned_get_url({ Bucket, Key }, { expiresIn, signingDate });
    };

    // Get a download URL for the object
    const signed_url1 = await get_signed_url();

    // Wait 10 seconds to pass timeout.
    await new Promise((res) => setTimeout(res, 10000));

    // Get different download URL for the object
    const signed_url2 = await get_signed_url();

    // === POSTCONDITIONS
    t.not(signed_url1, signed_url2);
  }));
