import type { MediaRequest } from "@swymbase/media-interop";
import { process_media_request } from "@swymbase/media-interop";
import { metatypes } from "@swymbase/metatype-core";
import test from "ava";
import * as util from "util";

export const VOCAB = "https://example.com/vocab/v1#";
export const ENTITIES = "https://example.com/entities/";
const DEFAULT_MEDIA_EXPIRATION = 60 * 60 * 24 * 6;

// Copied / adapted from Thrive code
const TYPES = metatypes({
  User: {
    "@type": `${VOCAB}User`,
    comment: "A person with a registered account",
    instance_namespace: `${ENTITIES}User/`,
    properties: {
      email: {
        term: `${VOCAB}email`,
        type: "string",
      },
      first_name: {
        term: `${VOCAB}first_name`,
        type: "string",
      },
      last_name: {
        term: `${VOCAB}last_name`,
        type: "string",
      },
      profile_picture: {
        term: `${VOCAB}profile_picture`,
        type: "object",
        relationship: true,
        media: {
          url_property: "profile_picture_url",
          expiration: DEFAULT_MEDIA_EXPIRATION,
          processors: ["image"],
        },
      },
      profile_picture_url: {
        // TODO: support “computed” properties, which will not require term
        term: `${VOCAB}DUMMY`,
        type: "string",
      },
      created_at: {
        term: `${VOCAB}created_at`,
        type: "string",
      },
    },
  },
  Media: {
    "@type": `${VOCAB}Media`,
    // This is required in sparql-rest, but internally we will always specify an ID on record creation.
    instance_namespace: `${ENTITIES}Media/`,
    comment:
      "Container to store metadata about AWS S3 objects. The IRI should be an ARN.",
    properties: {
      video_duration: {
        term: `${VOCAB}video_duration`,
        type: "number",
        comment:
          "Video duration in milliseconds. Only has value when applicable.",
      },
      video_thumbnail: {
        term: `${VOCAB}video_thumbnail`,
        type: "object",
        relationship: true,
        media: {
          url_property: "video_thumbnail_url",
          expiration: DEFAULT_MEDIA_EXPIRATION,
        },
      },
      video_thumbnail_url: {
        term: `${VOCAB}video_thumbnail_url`,
        type: "string",
      },
      small: {
        term: `${VOCAB}small`,
        type: "object",
        relationship: true,
        media: {
          url_property: "small_url",
          expiration: DEFAULT_MEDIA_EXPIRATION,
        },
      },
      small_url: {
        term: `${VOCAB}small_url`,
        type: "string",
      },
      medium: {
        term: `${VOCAB}medium`,
        type: "object",
        relationship: true,
        media: {
          url_property: "medium_url",
          expiration: DEFAULT_MEDIA_EXPIRATION,
        },
      },
      medium_url: {
        term: `${VOCAB}medium_url`,
        type: "string",
      },
      large: {
        term: `${VOCAB}large`,
        type: "object",
        relationship: true,
        media: {
          url_property: "large_url",
          expiration: DEFAULT_MEDIA_EXPIRATION,
        },
      },
      large_url: {
        term: `${VOCAB}large_url`,
        type: "string",
      },
      xlarge: {
        term: `${VOCAB}xlarge`,
        type: "object",
        relationship: true,
        media: {
          url_property: "xlarge_url",
          expiration: DEFAULT_MEDIA_EXPIRATION,
        },
      },
      xlarge_url: {
        term: `${VOCAB}xlarge_url`,
        type: "string",
      },
      created_at: {
        term: `${VOCAB}created_at`,
        type: "string",
      },
    },
  },
});

util.inspect.defaultOptions.depth = 5;

test("create upload URL", async (t) => {
  // === SETUP
  const request: MediaRequest = {
    profile_picture: {
      media_type: "image/png",
    },
  };
  const record_iri = "http://example.com/anyrecord";

  // === OPERATION
  const result = await process_media_request(
    TYPES,
    "User",
    record_iri,
    "https://example.com/User/1",
    request
  );

  // === POSTCONDITIONS
  t.log(result);
  t.log(result.profile_picture);
  // Does the output metadata have a corresponding key with a download link
  t.assert(result.profile_picture);
  t.truthy(
    !result.profile_picture.errors || result.profile_picture.errors.length === 0
  );
  t.assert(result.profile_picture.upload_url);
  t.is(typeof result.profile_picture.upload_url, "string");
  // Should be a signed URL with metadata assertions, etc
});
