import { download_urls } from "@swymbase/media-interop";
import type { MetaProperties } from "@swymbase/metatype-core";
import test from "ava";
import { parse } from "url";

// Other cases:
// - check actual properties about the URL
//   - is it actually signed with key
//   - does it actually point to this bucket/key
//   - does it actually expire

const TEST_PROPERTIES: MetaProperties = {
  title: {
    term: "http://example.com/media-download/vocab#title",
    type: "string",
  },
  cover_image: {
    term: "http://example.com/media-download/vocab#coverImage",
    type: "object",
    media: { url_property: "cover_image_url", expiration: 120 },
  },
  cover_image_url: {
    term: "http://example.com/media-download/vocab#coverImageURL",
    type: "string",
  },
};

test("ignores missing source", async (t) => {
  const result = await download_urls(TEST_PROPERTIES, { title: "Lolita" });
  t.log(result);
  t.is(Object.values(result.properties).length, 0);
  t.is(Object.values(result.warnings).length, 0);
});

test("warns if source is not an object", async (t) => {
  const result = await download_urls(TEST_PROPERTIES, {
    cover_image: "arn:aws:s3:::imaginary-bucket/imaginary-resource",
  });
  t.is(Object.values(result.properties).length, 0);
  t.is(Object.values(result.warnings).length, 1);
  t.assert(result.warnings.cover_image_url);
});

test("warns if source does not have an id", async (t) => {
  const result = await download_urls(TEST_PROPERTIES, {
    cover_image: { "@id": "arn:aws:s3:::imaginary-bucket/imaginary-resource" },
  });
  t.is(Object.values(result.properties).length, 0);
  t.is(Object.values(result.warnings).length, 1);
  t.assert(result.warnings.cover_image_url);
});
test("warns if source IRI is not an S3 ARN", async (t) => {
  const result = await download_urls(TEST_PROPERTIES, {
    cover_image: { id: "arn:s3:::imaginary-bucket/imaginary-resource" },
  });
  t.is(Object.values(result.properties).length, 0);
  t.is(Object.values(result.warnings).length, 1);
  t.assert(result.warnings.cover_image_url);
});

test("provides download URL for media properties", async (t) => {
  const record = {
    title: "The Red Badge of Courage",
    cover_image: { id: "arn:aws:s3:::imaginary-bucket/imaginary-resource" },
  };

  // === OPERATION
  const result = await download_urls(TEST_PROPERTIES, record);

  // === POSTCONDITIONS
  t.log(result);
  t.is(Object.values(result.properties).length, 1);
  t.is(Object.values(result.warnings).length, 0);
  t.assert(result.properties.cover_image_url);
  t.is(typeof result.properties.cover_image_url, "string");
  const url = parse(result.properties.cover_image_url);
  t.is(url.protocol, "https:");
  t.assert(url.host);
});
