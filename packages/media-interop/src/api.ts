// Types defined by media subsystem

export interface MediaRequest {
  readonly [property: string]: {
    readonly media_type: string;
  };
}

export interface MediaResponse {
  [property: string]: {
    readonly upload_url?: string;
    readonly errors?: readonly { message: string }[];
  };
}

export interface MediaMetadataRequest {
  readonly media: MediaRequest;
}

export interface MediaMetadataResponse {
  readonly media: MediaResponse;
}

export interface MediaProcessing {
  readonly subject_iri: string;
  readonly type_name: string;
  readonly property_name: string;
  // TODO: This is domain/app specific.  Why is it needed, and how can this
  // context be generalized?
  readonly authenticated_user_id: string;
}

export interface MediaConfiguration {
  /** Name of the bucket used by this environment. */
  readonly SWYMBASE_S3_DEFAULT_BUCKET: string;
}
