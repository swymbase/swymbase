import type { MetaProperties } from "@swymbase/metatype-core";
import { parse_s3_arn, presigned_get_url } from "./s3/index";

/**
 * Return the S3 download URL's for a given record based on its media metadata.
 */
export const download_urls = async <T extends object>(
  meta_properties: MetaProperties,
  record: T
): Promise<{
  properties: { [key: string]: string };
  warnings: { [key: string]: string };
}> => {
  const properties = {};
  const warnings = {};

  for (const [key, { media }] of Object.entries(meta_properties)) {
    const source = record[key];

    if (source && media) {
      const { url_property, expiration } = media;

      // If the url property doesn't exist at all, ignore
      if (!url_property) continue;

      if (typeof source !== "object") {
        warnings[url_property] = `Unsupported source type: ${typeof source}`;
        continue;
      }

      const source_iri = source.id;
      if (!source_iri) {
        warnings[url_property] = `Source is missing 'id' property`;
        continue;
      }

      const s3_id = parse_s3_arn(source_iri);
      if (!s3_id) {
        warnings[url_property] = `Unsupported source format: ${source_iri}`;
        continue;
      }

      const signingDate = new Date();

      // Round down to nearest hour for cacheable URLs.
      if (expiration > 60 * 60) {
        signingDate.setMinutes(0);
        signingDate.setSeconds(0);
        signingDate.setMilliseconds(0);
      }

      properties[url_property] = await presigned_get_url(s3_id, {
        signingDate,
        expiresIn: expiration,
      });
    }
  }

  return { properties, warnings };
};
