import type { MediaConfiguration } from "./api";

/**
 * Synchronously get media subsystem configuration from a configuration
 * dictionary, which defaults to the process environment.
 *
 * Throws if any of the defined settings are missing in the source.
 *
 * This configuration will not change for the duration of the execution
 * environment, and thus only needs to be retrieved once.  That said, calling
 * this at the module level means that the package can only be loaded in
 * environments with a valid configuration (even if nothing is used).  For that
 * reason, this is not called at the module level within this package.
 */
export const get_config = (source = process.env): MediaConfiguration => {
  const setting = (key: string): string => {
    const value = source[key];
    if (!value) throw new Error(`Expected value for ${key}`);
    return value;
  };

  return {
    SWYMBASE_S3_DEFAULT_BUCKET: setting("SWYMBASE_S3_DEFAULT_BUCKET"),
  };
};
