import type { MetaInterfaces } from "@swymbase/metatype-core";
import type { MediaProcessing, MediaRequest, MediaResponse } from "./api";
import { get_config } from "./environment";
import { new_s3_key, presigned_put_url } from "./s3/index";

export const process_media_request = async <
  S extends MetaInterfaces,
  T extends keyof S
>(
  types: S,
  type_name: T,
  subject_iri: string,
  authenticated_user_id: string,
  request: MediaRequest
): Promise<MediaResponse> => {
  const s3_default_bucket = get_config().SWYMBASE_S3_DEFAULT_BUCKET;

  const type_spec = types[type_name];
  if (!type_spec) {
    throw new Error(`No spec for type: ${type_name}`);
  }

  const response: MediaResponse = {};
  for (const [property_name, { media_type }] of Object.entries(request)) {
    const property_spec = type_spec.properties[property_name];

    if (!property_spec) {
      response[property_name] = {
        errors: [{ message: `No spec for property: ${property_name}` }],
      };
      continue;
    }

    const Bucket = s3_default_bucket;
    const Key = new_s3_key();

    const assertions: MediaProcessing = {
      subject_iri,
      property_name,
      type_name: type_name as string,
      authenticated_user_id,
    };

    const upload_url = await presigned_put_url({
      Bucket,
      Key,
      Metadata: {
        "Content-Type": media_type,
        processing: JSON.stringify(assertions),
      },
    });

    response[property_name] = { upload_url };
  }
  return response;
};
