// https://docs.aws.amazon.com/AmazonS3/latest/dev/s3-arn-format.html

interface S3ObjectID {
  readonly Bucket: string;
  readonly Key: string;
}

const S3_ARN = /^arn:aws:s3:::(.+?)\/(.+)$/;

export const parse_s3_arn = (arn: string): S3ObjectID | undefined => {
  const [, Bucket, Key] = arn.match(S3_ARN) || [];
  if (Bucket && Key) return { Bucket, Key };
};

export const s3_arn = (id: S3ObjectID) => `arn:aws:s3:::${id.Bucket}/${id.Key}`;
