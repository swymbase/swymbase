import type { GetObjectRequest, PutObjectRequest } from "@aws-sdk/client-s3";
import { GetObjectCommand, PutObjectCommand } from "@aws-sdk/client-s3";
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";
import { RequestPresigningArguments } from "@aws-sdk/types";
import { get_s3 } from "./get-s3";

/**
 * Convenience function for getting pre-signed upload URL's for current AWS
 * settings.
 */
export const presigned_put_url = async (
  request: PutObjectRequest,
  options?: RequestPresigningArguments
): Promise<string> => {
  const s3 = get_s3();
  const command = new PutObjectCommand(request);
  const signed_url = await getSignedUrl(s3, command, options);
  return signed_url;
};

/**
 * Convenience function for getting pre-signed download URL's for current AWS
 * settings.
 */
export const presigned_get_url = async (
  request: GetObjectRequest,
  options?: RequestPresigningArguments
): Promise<string> => {
  const s3 = get_s3();
  const command = new GetObjectCommand(request);
  const signed_url = await getSignedUrl(s3, command, options);
  return signed_url;
};
