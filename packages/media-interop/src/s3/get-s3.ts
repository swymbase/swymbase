import { S3 } from "@aws-sdk/client-s3";

// TODO: get rid of this?  I was originally using it to marshal configuration of
// credentials from the environment.  But the SDK does that automatically.  So
// at this moment, there's no practical need to wrap this.  If we can support
// the idea that a default, vanilla S3 client can be used wherever one is
// needed, that would be a win.
export const get_s3 = (): S3 => {
  return new S3({});
};
