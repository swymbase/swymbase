# Swymbase media interop

This package contains code related to supporting media extensions for the
swymbase API.

A major reason for keeping this in a separate package is that it takes
dependencies on the AWS v3 SDK that swymbase would not otherwise have.
