import * as fs from "fs";
import * as path from "path";

export const get_document_asset = (name: string): string => {
  // The running code will be in a single-file bundle at `dist` relative to the
  // package root (not this file).
  const fullpath = path.join(__dirname, "../assets", name);
  return fs.readFileSync(fullpath, "utf-8");
};
