import * as aws_imagebuilder from "@aws-cdk/aws-imagebuilder";
import * as core from "@aws-cdk/core";
import { get_document_asset } from "./temp";

/**
 * General-purpose component for configuring the mount points of a machine
 * image.
 */
export class MountPointsComponent extends core.Construct {
  readonly document: aws_imagebuilder.CfnComponent;

  constructor(scope: core.Construct, id: string) {
    super(scope, id);

    // Construct a safe prefix according to the rules for component name.
    const namespace = this.node.path.replace(/[^-_A-Za-z0-9 ]/g, "--");

    const document = new aws_imagebuilder.CfnComponent(
      this,
      "ConfigureMountPointsDocument",
      {
        name: `${namespace}-ConfigureMountPoints`,
        platform: "Linux",
        description: "Optimizes mount points",
        version: "1.0.0",
        data: get_document_asset("mount-points.yml"),
      }
    );

    this.document = document;
  }
}
