import * as aws_imagebuilder from "@aws-cdk/aws-imagebuilder";
import * as core from "@aws-cdk/core";
import { get_document_asset } from "./temp";

/**
 * General-purpose component for adding logging agent to a machine image.
 */
export class FluentdComponent extends core.Construct {
  readonly document: aws_imagebuilder.CfnComponent;

  constructor(scope: core.Construct, id: string) {
    super(scope, id);

    // Construct a safe prefix according to the rules for component name.
    const namespace = this.node.path.replace(/[^-_A-Za-z0-9 ]/g, "--");

    const document = new aws_imagebuilder.CfnComponent(
      this,
      "InstallFluentdDocument",
      {
        name: `${namespace}-InstallFluentd`,
        description: "Installs td-agent",
        version: "1.0.0",
        platform: "Linux",
        data: get_document_asset("fluentd.yml"),
      }
    );

    this.document = document;
  }
}
