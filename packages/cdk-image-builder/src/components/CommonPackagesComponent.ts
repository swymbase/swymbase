import * as aws_imagebuilder from "@aws-cdk/aws-imagebuilder";
import * as core from "@aws-cdk/core";
import { get_document_asset } from "./temp";

/**
 * General-purpose component for adding common software packages to a machine
 * image.
 */
export class CommonPackagesComponent extends core.Construct {
  readonly document: aws_imagebuilder.CfnComponent;

  constructor(scope: core.Construct, id: string) {
    super(scope, id);

    // Construct a safe prefix according to the rules for component name.
    const namespace = this.node.path.replace(/[^-_A-Za-z0-9 ]/g, "--");

    const document = new aws_imagebuilder.CfnComponent(
      this,
      "InstallPackagesDocument",
      {
        name: `${namespace}-InstallPackages`,
        description: "Installs some needed common packages",
        version: "1.0.0",
        platform: "Linux",
        data: get_document_asset("common-packages.yml"),
      }
    );

    this.document = document;
  }
}
