import * as aws_iam from "@aws-cdk/aws-iam";
import * as aws_imagebuilder from "@aws-cdk/aws-imagebuilder";
import * as aws_s3 from "@aws-cdk/aws-s3";
import * as core from "@aws-cdk/core";
import type { WithArchetype } from "./api";

interface ImagePipelineConstructProps extends WithArchetype {
  readonly instanceSizeList: readonly string[];
  readonly image_recipe: aws_imagebuilder.CfnImageRecipe;
}

/**
 * A general-purpose construct for building a machine image from a given recipe.
 */
export class ImagePipelineConstruct extends core.Construct {
  constructor(
    scope: core.Construct,
    id: string,
    props: ImagePipelineConstructProps
  ) {
    super(scope, id);

    const { region } = core.Stack.of(this);

    // This is used to optionally tag the AMI.
    const app_name = this.node.tryGetContext("app_name");

    const { archetype, instanceSizeList, image_recipe } = props;

    const bucket = new aws_s3.Bucket(this, "logs", {
      encryption: aws_s3.BucketEncryption.KMS_MANAGED,
      blockPublicAccess: aws_s3.BlockPublicAccess.BLOCK_ALL,
    });

    // Managed Policies
    const managed_policies = [
      aws_iam.ManagedPolicy.fromAwsManagedPolicyName(
        "AmazonSSMManagedInstanceCore"
      ),
      aws_iam.ManagedPolicy.fromAwsManagedPolicyName(
        "EC2InstanceProfileForImageBuilder"
      ),
    ];

    // Configure a role that we can manually use on a web server instance.
    const instance_role = new aws_iam.Role(this, "image-builder-role", {
      assumedBy: new aws_iam.ServicePrincipal("ec2.amazonaws.com"),
      managedPolicies: managed_policies,
    });

    const instance_profile_name = `${archetype}-EC2ImageBuilder`;
    const instance_profile = new aws_iam.CfnInstanceProfile(
      this,
      "instance-profile",
      {
        roles: [instance_role.roleName],
        instanceProfileName: instance_profile_name,
      }
    );
    bucket.grantReadWrite(instance_role);

    const infra_config = new aws_imagebuilder.CfnInfrastructureConfiguration(
      this,
      "infrastructureconfig",
      {
        name: `${archetype}-infra-config`,
        instanceProfileName: instance_profile_name,
        instanceTypes: [...instanceSizeList],
        terminateInstanceOnFailure: true,
        // TS: The property also accepts `any`, so doesn't check this.
        logging: {
          s3Logs: {
            s3BucketName: bucket.bucketName,
            s3KeyPrefix: "imagebuilder",
          },
        } as aws_imagebuilder.CfnInfrastructureConfiguration.LoggingProperty,
      }
    );

    infra_config.node.addDependency(bucket);
    infra_config.node.addDependency(instance_profile);
    const distro_config = new aws_imagebuilder.CfnDistributionConfiguration(
      this,
      "distroconfig",
      {
        name: `${archetype}-distro-config`,
        distributions: [
          {
            region,
            amiDistributionConfiguration: {
              Name: `${archetype} {{ imagebuilder:buildDate }}`,
              AmiTags: {
                Client: app_name,
                Archetype: archetype,
                Author: "Mindgrub",
              },
            },
          },
        ],
      }
    );

    new aws_imagebuilder.CfnImagePipeline(this, "pipeline", {
      infrastructureConfigurationArn: infra_config.attrArn,
      distributionConfigurationArn: distro_config.attrArn,
      imageRecipeArn: image_recipe.attrArn,
      name: `${archetype}-pipeline`,
      description: `Builds the ${archetype} server AMI`,
      enhancedImageMetadataEnabled: true,
      imageTestsConfiguration: {
        imageTestsEnabled: true,
        timeoutMinutes: 60,
      },
    });
  }
}
