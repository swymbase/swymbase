export interface WithArchetype {
  /**
   * Application-defined Identifier of an image profile within an account.
   *
   * This identifer is used in the name properties of several constructs,
   * including the profile name of the infrastructure config, which can be used
   * to locate the resulting AMI.
   *
   * As such, `archetype` is expected to be unique within the account.
   */
  readonly archetype: string;
}
