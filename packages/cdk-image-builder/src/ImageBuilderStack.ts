import * as aws_imagebuilder from "@aws-cdk/aws-imagebuilder";
import * as core from "@aws-cdk/core";
import type { WithArchetype } from "./api";
import * as components from "./components/index";
import { ImagePipelineConstruct } from "./ImagePipelineConstruct";
import { ImageRecipeConstruct } from "./ImageRecipeConstruct";

export interface ImageBuilderStackProps extends core.StackProps, WithArchetype {
  readonly instanceSizeList: string[];
  readonly appComponents: aws_imagebuilder.CfnComponent[];
}

export class ImageBuilderStack extends core.Stack {
  constructor(
    scope: core.Construct,
    id: string,
    props: ImageBuilderStackProps
  ) {
    super(scope, id, props);

    const { archetype, instanceSizeList, appComponents } = props;

    const mount_points = new components.MountPointsComponent(
      this,
      "mount-points-doc"
    );

    const fluend = new components.FluentdComponent(this, "fluentd-doc");

    const common_packages = new components.CommonPackagesComponent(
      this,
      "common-packages-doc"
    );

    const image_recipe = new ImageRecipeConstruct(this, "image-recipe", {
      mount_points: mount_points.document,
      fluentd: fluend.document,
      common_packages: common_packages.document,
      appComponents,
    });

    new ImagePipelineConstruct(this, "image-pipeline", {
      image_recipe: image_recipe.image_recipe,
      archetype,
      instanceSizeList,
    });
  }
}
