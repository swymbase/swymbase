import * as aws_imagebuilder from "@aws-cdk/aws-imagebuilder";
import * as core from "@aws-cdk/core";

interface ImageRecipeConstructProps {
  readonly mount_points: aws_imagebuilder.CfnComponent;

  /**
   * A component describing the common software packages that are expected to be
   * present across machine images.
   */
  readonly common_packages: aws_imagebuilder.CfnComponent;

  /**
   * The domain-specific components of the image.  These are installed after the
   * common packages and before Fluentd.
   */
  readonly appComponents: readonly aws_imagebuilder.CfnComponent[];

  /**
   * A component describing the Fluentd logging configuration for the image.
   */
  readonly fluentd: aws_imagebuilder.CfnComponent;
}

export class ImageRecipeConstruct extends core.Construct {
  readonly image_recipe: aws_imagebuilder.CfnImageRecipe;

  constructor(
    scope: core.Construct,
    id: string,
    props: ImageRecipeConstructProps
  ) {
    super(scope, id);

    const { region } = core.Stack.of(this);

    const { common_packages, fluentd, appComponents, mount_points } = props;

    // Construct a safe name according to the rules for recipe name.
    const name = this.node.path.replace(/[^-_A-Za-z0-9 ]/g, "--");

    const image_recipe = new aws_imagebuilder.CfnImageRecipe(this, "recipe", {
      name,
      description: "Generic Image Recipe",
      version: "1.0.7",
      parentImage: `arn:aws:imagebuilder:${region}:aws:image/ubuntu-server-18-lts-x86/x.x.x`,
      components: [
        {
          componentArn: `arn:aws:imagebuilder:${region}:aws:component/simple-boot-test-linux/1.0.0/1`,
        },
        { componentArn: mount_points.attrArn },
        { componentArn: common_packages.attrArn },
        ...appComponents.map((_) => ({ componentArn: _.attrArn })),
        { componentArn: fluentd.attrArn },
      ],
      blockDeviceMappings: [
        {
          deviceName: "/dev/sda1",
          ebs: {
            deleteOnTermination: true,
            encrypted: true,
            volumeType: "gp2",
            volumeSize: 8,
          },
        },
      ],
    });

    this.image_recipe = image_recipe;
  }
}
