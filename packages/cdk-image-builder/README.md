# CDK — Image builder

This package defines a construct for creating pipelines to build custom machine
images.

## Usage

To use this construct, first import the package:

```typescript
import * as core from "@aws-cdk/core";
import { ImageBuilderStack } from "@swymbase/cdk-image-builder";

function main() {
  const app = new core.App();
  // TODO: should initialize properly with stage, etc
  new ImageBuilderStack(app, "image-builder");
}
```

## Roadmap

The cloud team has worked to add automatic OS updates to image pipelines. This
package should be updated to incorporate that work.

Upgrade CDK
