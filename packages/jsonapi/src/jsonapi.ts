import { KBInterfaceOptions, make_kb_interface } from "@swymbase/sparql-rest";
import type {
  JsonApiConfig,
  JsonApiHandlers,
  JsonApiOperationConfig,
} from "./api";
import * as ops from "./operations/index";

export const make_jsonapi = (config: JsonApiConfig): JsonApiHandlers => {
  const { schema } = config;

  // Currently, options besides `permissions` and `observers` are taken only
  // from environment.

  const endpoint = process.env.SWYMBASE_SPARQL_ENDPOINT;
  if (!endpoint) {
    throw new Error("MissingSparqlEndpointConfiguration");
  }

  const named_graph = process.env.SWYMBASE_PRIMARY_GRAPH;
  const sparql_engine = process.env.SWYMBASE_SPARQL_ENGINE;

  if (!(sparql_engine === undefined || sparql_engine === "neptune")) {
    throw new Error("UnsupportedSparqlEngine");
  }

  const effective_config: KBInterfaceOptions<any> = {
    ...config,
    named_graph,
    sparql_engine,
  };

  const kb = make_kb_interface(endpoint, schema, effective_config);

  const op_config: JsonApiOperationConfig = { kb, schema };

  return {
    record_list: ops.record_list(op_config),
    record_get: ops.record_get(op_config),
    // record_put: ops.record_put(op_config),
    record_post: ops.record_post(op_config),
    record_patch: ops.record_patch(op_config),
    record_delete: ops.record_delete(op_config),
    relationship_get: ops.relationship_get(op_config),
    relationship_post: ops.relationship_post(op_config),
    relationship_patch: ops.relationship_patch(op_config),
    relationship_delete: ops.relationship_delete(op_config),
  };
};
