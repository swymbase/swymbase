type ID = { readonly id: string };

export const subtract_records = <R extends ID>(
  left: readonly R[] | undefined,
  right: readonly R[] | undefined
): readonly R[] =>
  (left ?? []).filter((lr) => !(right ?? []).find((rr) => lr.id === rr.id));

export const union_records = <R extends ID>(
  left: readonly R[] | undefined,
  right: readonly R[] | undefined
): readonly R[] =>
  (left ?? [])
    .filter((l) => !(right ?? []).find((r) => l.id === r.id))
    .concat(right ?? []);
