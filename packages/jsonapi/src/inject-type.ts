import type {
  Preprocess,
  WithJsonBody,
  WithRecordType,
} from "@swymbase/api-gateway-interop";
import { MetaInterfaces } from "@swymbase/metatype-core";
import { ensure_type_single } from "@swymbase/metatype-jsonapi";

/**
 * Extend incoming JSON:API resource with record type from the context.
 *
 * See notes about “Contextual type” in `metatype-jsonapi`.
 */
export const with_inject_type =
  <T extends MetaInterfaces>(
    // So... we just ignore this?
    types: T
  ): Preprocess<WithJsonBody, WithJsonBody & WithRecordType<T>> =>
  (_event, context) => {
    const json_body = ensure_type_single(
      context.json_body,
      context.record_type_spec["@type"]
    );
    return { context: { json_body } };
  };
