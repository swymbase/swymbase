import * as prep from "@swymbase/api-gateway-interop";
import { record_list_to_document } from "@swymbase/metatype-jsonapi";
import type { JsonApiOperation } from "../api";

/**
 * Return a JSON:API handler for getting a collection of records of a given
 * type.
 */
export const record_list: JsonApiOperation = (config) => {
  const { kb, schema } = config;
  return prep.with_preprocessors(
    [
      prep.with_record_type_from(schema),
      prep.with_include_paths,
      prep.with_fieldset_params,
      prep.with_filter_expression,
      prep.with_paging_params,
      prep.with_sorting_params,
      prep.with_query_params,
    ],
    async (context) => {
      const headers = { "Content-type": "application/json" };

      const {
        record_type_name,
        include_paths,
        fields,
        filter,
        filters: filter_old,
        paging: { offset, limit },
        order_by,
        query,
      } = context;

      const records = await kb.list_records(record_type_name, context, {
        // TS: upstream type is too strict
        include_related: include_paths as any[],
        fields,
        filter,
        filter_old,
        offset,
        limit,
        order_by,
        include_count: query.get("include_count")?.[0] === "true",
      });

      if (!records.value) {
        console.log("TypeGetRecordsFailed", records.context);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "TypeGetRecordsFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      const doc = record_list_to_document(
        schema,
        records.value,
        records.included_values
      );

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          ...doc,
          meta: {
            total_result_count: records.total_result_count,
          },
        }),
      };
    }
  );
};
