import * as prep from "@swymbase/api-gateway-interop";
import type { MediaRequest } from "@swymbase/media-interop";
import { process_media_request } from "@swymbase/media-interop";
import { record_to_document } from "@swymbase/metatype-jsonapi";
import type { JsonApiOperation } from "../api";
import { with_inject_type } from "../inject-type";

/**
 * Return a JSON:API handler for patching a single record of a given type.
 */
export const record_patch: JsonApiOperation = (config) => {
  const { kb, schema } = config;
  return prep.with_preprocessors(
    [
      prep.with_record_type_from(schema),
      prep.with_record_id,
      prep.with_json_body,
      with_inject_type(schema),
      prep.with_json_api_document,
      prep.with_json_api_records_from(schema),
      prep.with_include_paths,
    ],
    async (context) => {
      const headers = { "Content-type": "application/json" };

      const {
        record_type_name,
        record_id,
        json_api_document,
        json_api_records,
        include_paths,
      } = context;

      const [given_data] = json_api_records;

      if (!(given_data || json_api_document.meta))
        return {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "DataOrMetaRequired",
                title: "A top-level `data` or `meta` key is required.",
              },
            ],
          }),
        };

      if (given_data) {
        const patch_result = await kb.patch_record(
          record_type_name,
          {
            ...given_data,
            id: record_id,
          },
          context
        );

        if (!patch_result.success) {
          console.log("TypePatchRecordFailed", patch_result);
          return {
            statusCode: 500,
            headers,
            body: JSON.stringify({
              errors: [
                {
                  code: "TypePatchRecordFailed",
                  title: "An unknown error has occurred",
                },
              ],
            }),
          };
        }
      }

      const read_back = await kb.get_record(record_type_name, record_id, {
        // TS: upstream type is too strict
        include_related: include_paths as any[],
      });

      if (!read_back.context.success || !read_back.value) {
        console.log("TypePatchReadRecordFailed", read_back.context);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "TypePatchReadRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      const record = read_back.value;
      const doc = record_to_document(schema, record, read_back.included_values);

      const media_request = json_api_document?.meta?.media as MediaRequest;
      const media_response =
        media_request &&
        (await process_media_request(
          schema,
          record_type_name,
          record.id!,
          // TS: TODO: This is a domain-specific context but needs to be
          // addressed in several places.
          context["authenticated_user_id"],
          media_request
        ));

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          ...doc,
          meta: media_request && { media: media_response },
        }),
      };
    }
  );
};
