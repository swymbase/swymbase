import * as prep from "@swymbase/api-gateway-interop";
import { record_list_to_document } from "@swymbase/metatype-jsonapi";
import type { JsonApiOperation } from "../api";

/**
 * Return a JSON:API handler for fetching all of a record's value for a
 * specified property.
 *
 * See https://jsonapi.org/format/#fetching-relationships
 */
export const relationship_get: JsonApiOperation = (config) => {
  const { kb, schema } = config;
  return prep.with_preprocessors(
    [
      prep.with_record_type_from(schema),
      prep.with_record_id,
      prep.with_relationship_from(schema),
    ],
    async (context) => {
      const headers = { "Content-type": "application/json" };

      const {
        record_type_name,
        record_id,
        relationship_name,
        relationship_spec,
      } = context;

      if (!relationship_spec.multivalued) {
        console.log("RelationshipGetInvalidToOne", relationship_spec);
        return {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "RelationshipGetInvalidToOne",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      // This unnecessarily pulls the parent record from the knowledge base.
      // TOOD: Replace with a more efficient approach.
      const get_record = await kb.get_record(record_type_name, record_id, {
        // TS: upstream type for this is a bit too strict now
        include_related: [relationship_name as any],
      });

      if (!get_record.context.success || !get_record.value) {
        console.log("RelationshipGetRecordFailed", get_record.context);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "RelationshipDeleteGetRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      const relationships = get_record.included_values ?? [];

      // Use included_values in place of records.
      const doc = record_list_to_document(schema, relationships);

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify(doc),
      };
    }
  );
};
