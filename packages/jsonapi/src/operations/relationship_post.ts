import * as prep from "@swymbase/api-gateway-interop";
import type { JsonApiOperation } from "../api";
import { union_records } from "../set-helpers";

/**
 * Return a JSON:API handler for adding add values to a given property of a
 * record.
 *
 * See https://jsonapi.org/format/#crud-updating-to-many-relationships
 */
export const relationship_post: JsonApiOperation = (config) => {
  const { kb, schema } = config;
  return prep.with_preprocessors(
    [
      prep.with_record_type_from(schema),
      prep.with_record_id,
      prep.with_relationship_from(schema),
      prep.with_json_body,
      prep.with_json_api_document,
      prep.with_resource_identifiers,
    ],
    async (context) => {
      const headers = { "Content-type": "application/json" };

      const {
        record_type_name,
        record_id,
        relationship_name,
        resource_identifiers,
        relationship_spec,
      } = context;

      if (!relationship_spec.multivalued) {
        console.log("RelationshipUpdateInvalidToOne", relationship_spec);
        return {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "RelationshipUpdateInvalidToOne",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      const get_record = await kb.get_record(record_type_name, record_id);

      if (!get_record.context.success || !get_record.value) {
        console.log("RelationshipPostGetRecordFailed", get_record.context);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "RelationshipPostGetRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      // @ts-expect-error
      // Type 'unknown' is not assignable to type 'readonly Readonly<Omit<ResourceIdentifierObject, "type">>[]'. [2345]
      const union = union_records(get_record.value[relationship_name] ?? [], [
        ...resource_identifiers,
      ]);

      const patch_record = await kb.patch_record(
        record_type_name,
        {
          id: record_id,
          [relationship_name]: union,
        },
        context
      );

      if (!patch_record.success) {
        console.log("RelationshipPostPatchRecordFailed", patch_record);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "RelationshipPostPatchRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      // The response document MUST include a representation of the updated relationship(s).
      // See https://jsonapi.org/format/#crud-updating-to-many-relationships
      const data = union;

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({ data }),
      };
    }
  );
};
