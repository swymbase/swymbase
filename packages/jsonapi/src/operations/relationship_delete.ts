import * as prep from "@swymbase/api-gateway-interop";
import { subtract_records } from "../set-helpers";
import type { JsonApiOperation } from "../api";

/**
 * Return a JSON:API handler for deleting a record's value for a given property.
 *
 * See https://jsonapi.org/format/#crud-updating-to-many-relationships
 */
export const relationship_delete: JsonApiOperation = (config) => {
  const { kb, schema } = config;
  return prep.with_preprocessors(
    [
      prep.with_record_type_from(schema),
      prep.with_record_id,
      prep.with_relationship_from(schema),
      prep.with_json_body,
      prep.with_json_api_document,
      prep.with_resource_identifiers,
    ],
    async (context) => {
      const headers = { "Content-type": "application/json" };

      const {
        record_type_name,
        record_id,
        relationship_name,
        relationship_spec,
        resource_identifiers,
      } = context;

      if (!relationship_spec.multivalued) {
        console.log("RelationshipUpdateInvalidToOne", relationship_spec);
        return {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "RelationshipUpdateInvalidToOne",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      const get_record = await kb.get_record(record_type_name, record_id);

      if (!get_record.context.success || !get_record.value) {
        console.log("RelationshipDeleteGetRecordFailed", get_record.context);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "RelationshipDeleteGetRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      const record = get_record.value;

      const subtracted = subtract_records(
        // TS: indexed value is `unknown`
        (record?.[relationship_name] as any[]) ?? [],
        resource_identifiers
      );

      const patch_record = await kb.patch_record(
        record_type_name,
        {
          id: record_id,
          [relationship_name]: subtracted,
        },
        context
      );

      if (!patch_record.success) {
        console.log("RelationshipDeletePatchRecordFailed", patch_record);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "RelationshipDeletePatchRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      // The response document MUST include a representation of the updated relationship(s).
      // See https://jsonapi.org/format/#crud-updating-to-many-relationships
      const data = subtracted;

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({ data }),
      };
    }
  );
};
