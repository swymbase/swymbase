import * as prep from "@swymbase/api-gateway-interop";
import type { MediaRequest } from "@swymbase/media-interop";
import { process_media_request } from "@swymbase/media-interop";
import { record_to_document } from "@swymbase/metatype-jsonapi";
import type { JsonApiOperation } from "../api";
import { with_inject_type } from "../inject-type";

/**
 * Return a JSON:API handler for posting a new record of a given type.
 */
export const record_post: JsonApiOperation = (config) => {
  const { kb, schema } = config;
  return prep.with_preprocessors(
    [
      prep.with_record_type_from(schema),
      prep.with_json_body,
      with_inject_type(schema),
      prep.with_json_api_document,
      prep.with_json_api_records_from(schema),
      prep.with_include_paths,
    ],
    async (context) => {
      const headers = { "Content-type": "application/json" };

      const {
        record_type_name,
        json_api_records,
        json_api_document,
        include_paths,
      } = context;

      if (json_api_records.length === 0) {
        return {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            errors: [{ code: "TypePostRequiresRecord", title: "Bad Request" }],
          }),
        };
      }

      // Do not accept ID when posting.
      const [{ id, ...incoming_record }] = json_api_records;

      const post_result = await kb.post_record(
        record_type_name,
        incoming_record,
        context
      );

      if (!post_result.success) {
        console.log("TypePostRecordFailed", post_result);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "TypePostRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      // TODO: bypass reader endpoint for read-back
      const read_back = await kb.get_record(
        record_type_name,
        post_result.new_id,
        {
          // TS: upstream type is too strict
          include_related: include_paths as any[],
        }
      );

      if (!read_back.context.success || !read_back.value) {
        console.log("TypePostReadRecordFailed", read_back.context);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "TypePostReadRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      const record = read_back.value;

      const doc = record_to_document(schema, record, read_back.included_values);

      const media_request = json_api_document?.meta?.media as MediaRequest;
      const media_response =
        media_request &&
        (await process_media_request(
          schema,
          record_type_name,
          record.id,
          // TS: TODO: This is a domain-specific context but needs to be
          // addressed in several places.
          context["authenticated_user_id"],
          media_request
        ));

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          ...doc,
          meta: media_request && { media: media_response },
        }),
      };
    }
  );
};
