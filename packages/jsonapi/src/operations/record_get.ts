import * as prep from "@swymbase/api-gateway-interop";
import { record_to_document } from "@swymbase/metatype-jsonapi";
import type { JsonApiOperation } from "../api";

/**
 * Return a JSON:API handler for getting a single record of a given type.
 */
export const record_get: JsonApiOperation = (config) => {
  const { kb, schema } = config;
  return prep.with_preprocessors(
    [
      prep.with_record_type_from(schema),
      prep.with_record_id,
      prep.with_include_paths,
      prep.with_fieldset_params,
    ],
    async (context) => {
      const headers = { "Content-type": "application/json" };

      const { record_type_name, record_id, include_paths, fields } = context;

      const got = await kb.get_record(record_type_name, record_id, {
        // TS: upstream type for this is too strict
        include_related: include_paths as any[],
        fields,
      });

      if (!got.context.success) {
        console.log("TypeGetRecordFailed", got.context);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "TypeGetRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      if (!got.value) {
        console.log("TypeGetRecordNotFound", got.context);
        return {
          statusCode: 404,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "TypeGetRecordNotFound",
                title: "Not found",
              },
            ],
          }),
        };
      }

      const doc = record_to_document(schema, got.value, got.included_values);

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify(doc),
      };
    }
  );
};
