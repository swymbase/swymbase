import * as prep from "@swymbase/api-gateway-interop";
import type { JsonApiOperation } from "../api";

/**
 * Return a JSON:API handler for replacing all of a record's values for a given
 * relationship.
 *
 * See https://jsonapi.org/format/#crud-updating-to-many-relationships
 */
export const relationship_patch: JsonApiOperation = (config) => {
  const { kb, schema } = config;
  return prep.with_preprocessors(
    [
      prep.with_record_type_from(schema),
      prep.with_record_id,
      prep.with_relationship_from(schema),
      prep.with_json_body,
      prep.with_json_api_document,
      prep.with_resource_identifiers,
    ],
    async (context) => {
      const headers = { "Content-type": "application/json" };

      const {
        record_type_name,
        record_id,
        relationship_name,
        relationship_spec,
        resource_identifiers,
      } = context;

      const members = relationship_spec.multivalued
        ? resource_identifiers
        : resource_identifiers[0];

      const patch_record = await kb.patch_record(
        record_type_name,
        {
          id: record_id,
          [relationship_name]: members,
        },
        context
      );

      if (!patch_record.success) {
        console.log("RelationshipPatchRecordFailed", patch_record);
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "RelationshipPatchRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      // The response document MUST include a representation of the updated relationship(s).
      // See https://jsonapi.org/format/#crud-updating-to-many-relationships
      const data = members;

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({ data }),
      };
    }
  );
};
