import * as prep from "@swymbase/api-gateway-interop";
import type { JsonApiOperation } from "../api";

/**
 * Return a JSON:API handler for deleting a single record of a given type.
 */
export const record_delete: JsonApiOperation = (config) => {
  const { kb, schema } = config;
  return prep.with_preprocessors(
    [prep.with_record_type_from(schema), prep.with_record_id],
    async (context) => {
      const headers = { "Content-type": "application/json" };

      const { record_type_name, record_id } = context;

      const result = await kb.delete_record(record_type_name, record_id, {});

      if (!result.success) {
        console.log(
          "TypeDeleteRecordFailed",
          result.code,
          result.error,
          result.http_response
        );
        return {
          statusCode: 500,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "TypeDeleteRecordFailed",
                title: "An unknown error has occurred",
              },
            ],
          }),
        };
      }

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({ data: null }),
      };
    }
  );
};
