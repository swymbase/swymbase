import type { SimpleHandler } from "@swymbase/api-gateway-interop";
import type { MetaInterfaces } from "@swymbase/metatype-core";
import type {
  KBRecordInterface,
  KBInterfaceOptions,
} from "@swymbase/sparql-rest";

export interface JsonApiConfig
  extends Pick<KBInterfaceOptions<any>, "permissions" | "observers"> {
  readonly schema: MetaInterfaces;
}

export const JSONAPI_OPERATIONS = [
  "record_list",
  "record_get",
  // "record_put",
  "record_post",
  "record_patch",
  "record_delete",
  "relationship_get",
  "relationship_post",
  "relationship_patch",
  "relationship_delete",
] as const;

export interface JsonApiOperationConfig {
  readonly kb: KBRecordInterface<any>;
  readonly schema: MetaInterfaces;
}

export type JsonApiOperation = (input: JsonApiOperationConfig) => SimpleHandler;

type JsonApiOperationName = typeof JSONAPI_OPERATIONS[any];

export type JsonApiHandlers = { [K in JsonApiOperationName]: SimpleHandler };
