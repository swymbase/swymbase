# Swymbase JSON:API

This package provides support for a JSON:API implementation backed by a
knowledge base interface.

The implementation targets an AWS API Gateway environment insofar as it produces
handlers expecting an `APIGatewayProxyEvent` and resolving to an
`APIGatewayProxyResult`. However, this is a fairly generic request-response data
interface, and no AWS-specific mechanisms are assumed. As such, it should be
substantially adaptable to other environments. In particular, it uses request
preprocessors (with help from
[`@swymbase/api-gateway-interop`](../api-gateway-interop)) to decouple the
handler function from the details of the request format.
