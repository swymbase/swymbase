import {
  configuration,
  make_kb,
  MediaProcessing,
  s3_arn,
} from "@swymbase/media-interop";
import { assert_success } from "@swymbase/sparql-protocol-client";
import { with_temp_named_graph } from "@swymbase/sparql-rest";
import test from "ava";
import { S3CreateEvent } from "aws-lambda";
import { handler_inner } from "../src/index";
import { with_temp_s3_object } from "./lib/with_temp_s3_object";

const { sparql_endpoint: endpoint } = configuration(
  process.env,
  "sparql_endpoint"
);

// Skip this test because, when AWS is pointing to a real environment, it can
// cause side effects in the remote system that don't get cleaned up (due to
// Lambda triggers on media operations).
test.skip("media handler", async (t) =>
  with_temp_s3_object(async ({ s3, Bucket, Key }) =>
    with_temp_named_graph(endpoint, async (graph_iri) => {
      //=== SETUP
      const kb = make_kb(endpoint, graph_iri);
      const arn = s3_arn({ Bucket, Key });

      const post_user = await kb.post_record(
        "User",
        { first_name: "Casey" },
        { kb }
      );
      assert_success(t, post_user, "Could not post user.");

      await s3.putObject({
        Bucket,
        Key,
        Body: "data",
        Metadata: {
          processing: JSON.stringify({
            subject_iri: post_user.new_id,
            type_name: "User",
            property_name: "profile_picture",
          } as MediaProcessing),
        },
      });

      const event = {
        Records: [
          {
            s3: {
              object: { key: Key },
              bucket: { name: Bucket },
            },
          },
        ],
      } as S3CreateEvent;

      // === OPERATION
      await handler_inner(event, kb);

      const get_media = await kb.get_record("Media", arn);
      assert_success(t, get_media.context, "Could not get Media record.");

      const get_user = await kb.get_record("User", post_user.new_id);
      assert_success(t, get_user.context, "Could not get User record.");

      // === POSTCONDITIONS
      const media = get_media.value!;
      const user = get_user.value!;

      // Media property was updated.
      t.deepEqual(user.profile_picture, { id: arn });

      // Media record was created.
      t.is(media.id, arn);
      t.is(media.created_at, new Date(media.created_at!).toISOString());
    })
  ));
