import type { S3 } from "@aws-sdk/client-s3";
import { get_config, get_s3 } from "@swymbase/media-interop";
import { v4 as uuid } from "uuid";

/**
 * Call the given function with an S3 identifier and delete any such object
 * aftwards.  This method doesn't create the object, but it deletes it if the
 * operation does.  Assumes no UUID collisions.
 */
export const with_temp_s3_object = async (
  operation: (info: { s3: S3; Bucket: string; Key: string }) => Promise<void>
) => {
  const Key = uuid();
  const config = get_config();
  const s3 = get_s3(config);
  const Bucket = config.SWYMBASE_S3_DEFAULT_BUCKET;
  try {
    await operation({ s3, Bucket, Key });
  } finally {
    await s3.deleteObject({ Bucket, Key });
  }
};
