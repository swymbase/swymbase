import test from "ava";
import pathToFfmpeg = require("ffmpeg-static");
import { pathToFileURL } from "url";
import { get_video_thumbnail } from "../src/lib";

// TODO: Write test that doesn't require externally hosted video.
test.skip("get video thumbnail", async (t) => {
  const url =
    "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4";

  const thumbnail = await get_video_thumbnail(url);

  t.assert(Buffer.isBuffer(thumbnail));
});
