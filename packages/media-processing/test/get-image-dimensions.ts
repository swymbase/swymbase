import test from "ava";
import { get_image_dimensions } from "../src/lib";

// TODO: Write test that doesn't require externally hosted image.
test.skip("get image dimensions", async (t) => {
  const dim = Math.floor(Math.random() * Math.floor(1000));
  const url = `https://via.placeholder.com/${dim}`;

  const dimensions = await get_image_dimensions(url);

  t.deepEqual(dimensions, [dim, dim]);
});
