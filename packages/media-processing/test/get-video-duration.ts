import test from "ava";
import { get_video_duration } from "../src/lib";

test.skip("get video duration", async (t) => {
  const url =
    "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4";

  const milliseconds = await get_video_duration(url);

  t.log(milliseconds);
  t.is(milliseconds, 30530);
});
