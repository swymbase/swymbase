import test from "ava";
import { resize_image } from "../src/lib";

// TODO: Write test that doesn't require externally hosted image.
test.skip("resize image", async (t) => {
  const url = "https://via.placeholder.com/1000";

  const resized = await resize_image(url, 500);

  t.assert(Buffer.isBuffer(resized));
});
