# Swymbase media processing

This package defines a Lambda function that helps coordinate media processing
and storage (in the knowledge base).

See additional notes at [media](/doc/media.md).

## Media Processing

The store processor is the default for every object and is executed first.

Additional processors are defined [here](./src/processors). A processor can be
associated with a metatype property in the schema by adding its key to the
`media.processors` array.
