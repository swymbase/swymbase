import * as path_to_ffmpeg from "ffmpeg-static";
import { spawn } from "./spawn";

/**
 * Reads raw video duration using ffprobe and returns parsed result in milliseconds.
 */
export const get_video_duration = async (url: string): Promise<number> => {
  const result = await spawn(path_to_ffmpeg, ["-i", url]);

  // Process w/ err b/c of it has no output file, just read metadata.
  const duration = result.stderr.toString().match(/Duration:[ \t]+([0-9:.]+)/);

  if (!duration || !duration[1]) {
    console.log("Failure to parse stderr ffmpeg duration", result);
    return 0;
  }

  const parts = duration[1].split(":");

  const hours = +parts[0] * 3600;
  const mins = +parts[1] * 60;
  const secs = +parts[2];

  return (hours + mins + secs) * 1000;
};

/**
 * Captures thumbnail from video in buffer.
 */
export const get_video_thumbnail = async (
  url: string
): Promise<Buffer | undefined> => {
  const result = await spawn(path_to_ffmpeg, [
    "-i",
    url,
    "-ss",
    "00:00:01.00",
    "-vframes",
    "1",
    "-c:v",
    "png",
    "-loglevel",
    "panic",
    "-hide_banner",
    "-f",
    "image2pipe",
    "-",
  ]);

  if (result.status != 0) {
    console.log(
      `Failure to execute ffmpeg: ${result.stderr.toString("utf-8")}`
    );
    return undefined;
  }

  return result.stdout;
};
