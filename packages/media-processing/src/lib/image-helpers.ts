import * as path_to_ffmpeg from "ffmpeg-static";
import { spawn } from "./spawn";

/**
 * Reads raw image dimensions using ffmpeg returns dimensions in [width, height].
 */
export const get_image_dimensions = async (
  url: string
): Promise<[number, number] | undefined> => {
  const result = await spawn(path_to_ffmpeg, ["-i", url]);

  // Process w/ err b/c of it has no output file, just read metadata.
  const dimensions = result.stderr
    .toString()
    .match(/Stream.*?([0-9]+)x([0-9]+)/);

  if (!dimensions || !dimensions[1]) {
    console.log("Failure to parse stderr ffmpeg duration", result);
    return undefined;
  }

  const [_, width, height] = dimensions;

  if (isNaN(+width) || isNaN(+height)) {
    return undefined;
  }

  return [+width, +height];
};

/**
 * Resizes image to specified max dimensions and returns in Buffer.
 */
export const resize_image = async (
  url: string,
  max_width: number
): Promise<Buffer | undefined> => {
  const scale = `w=${max_width}:h=-1:force_original_aspect_ratio=decrease`;
  const result = await spawn(path_to_ffmpeg, [
    "-i",
    url,
    "-vf",
    `scale=${scale}`,
    "-f",
    "image2pipe",
    "-",
  ]);

  if (result.status != 0) {
    console.log(
      `Failure to execute ffmpeg: ${result.stderr.toString("utf-8")}`
    );
    return undefined;
  }

  return result.stdout;
};
