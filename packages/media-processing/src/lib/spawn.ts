import * as child_process from "child_process";

interface ShellCommandResult {
  status: number | null;
  stdout: Buffer;
  stderr: Buffer;
}

/** Run a shell process with the given args and resolve to its stdout. */
export const spawn = async (command: string, args: any[] = []) =>
  new Promise<ShellCommandResult>((resolve, reject) => {
    const child = child_process.spawn(command, args);

    const stdout: Buffer[] = [];
    const stderr: Buffer[] = [];

    // Asserting that these are `Buffer` instead of `Buffer | string` because
    // encoding is not set.
    child.stdout.on("data", (data: Buffer) => stdout.push(data));
    child.stderr.on("data", (data: Buffer) => stderr.push(data));

    child.on("error", reject);
    child.on("close", (status) =>
      resolve({
        status,
        stdout: Buffer.concat(stdout),
        stderr: Buffer.concat(stderr),
      })
    );
  });
