import { parse_s3_arn, presigned_get_url } from "@swymbase/media-interop";

const MINUTE = 60;

// Get public URL for S3 resource.
export const get_public_url = async (arn: string): Promise<string> => {
  const parsed_arn = parse_s3_arn(arn);

  if (!parsed_arn) {
    throw new Error(`Could not parse S3 ARN: ${arn}`);
  }

  const url = await presigned_get_url(parsed_arn, {
    signingDate: new Date(),
    expiresIn: 5 * MINUTE,
  });

  return url;
};
