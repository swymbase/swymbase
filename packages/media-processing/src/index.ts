import { get_s3, MediaProcessing, s3_arn } from "@swymbase/media-interop";
import type { MetaInterfaces, MetaProperty } from "@swymbase/metatype-core";
import type {
  KBInterfaceOptions,
  KBRecordInterface,
} from "@swymbase/sparql-rest";
import { make_kb_interface } from "@swymbase/sparql-rest";
import type { Handler, S3CreateEvent } from "aws-lambda";
import { media_processors } from "./processors/index";

const handler_inner = async (
  // https://docs.aws.amazon.com/AmazonS3/latest/dev/notification-content-structure.html
  event: Parameters<Handler<S3CreateEvent, void>>[0],
  kb: KBRecordInterface<any>,
  schema: MetaInterfaces
) => {
  const [record] = event.Records;

  if (!record) {
    console.log(event);
    throw new Error("No record included in S3 event.");
  }

  const s3 = get_s3();
  const Bucket = record.s3.bucket.name;
  const Key = record.s3.object.key;
  const obj = await s3.getObject({ Bucket, Key });
  const arn = s3_arn({ Bucket, Key });
  const processing_raw = obj?.Metadata?.["processing"];

  if (!processing_raw) {
    throw new Error(`Missing processing request for object: ${arn}.`);
  }

  const request = JSON.parse(processing_raw) as MediaProcessing;

  const media_property_spec = schema[request.type_name]?.properties?.[
    request.property_name
  ] as MetaProperty;

  // Verify property is of media type.
  if (!media_property_spec?.relationship || !media_property_spec?.media) {
    console.log(processing_raw);
    throw new Error(`Invalid processing request for object: ${arn}.`);
  }

  // Every media object should be stored in the KB.
  // Note: 'store' first as subsequent processors may patch the record.
  const processors = [
    "store",
    ...(media_property_spec.media?.processors ?? []),
  ];

  // Handle processing.
  for (const key of processors) {
    if (!(key in media_processors)) {
      console.log(
        `Unknown media processor '${key}' encountered on property ${request.type_name}:${request.property_name}`
      );
      continue;
    }

    // Call processor.
    await media_processors[key](request, arn, kb);
  }

  const { type_name, subject_iri, property_name, authenticated_user_id } =
    request;

  // Lastly, write ARN to specified media property on record.
  const patch_result = await kb.patch_record(
    type_name,
    { id: subject_iri, [property_name]: { id: arn } },
    { kb, authenticated_user_id }
  );

  if (!patch_result.success) {
    console.log("FAILED to write arn to media property.", patch_result);
  }
};

// DUPLICATES similar code in jsonapi
//
// Which means that kb must be initialized identically in each place (jsonapi
// and media) if it's expected to work identically.

export interface MediaHandlerConfig
  extends Pick<KBInterfaceOptions<any>, "permissions" | "observers"> {
  readonly schema: MetaInterfaces;
}

export const make_handler = (
  config: MediaHandlerConfig
): Handler<S3CreateEvent, void> => {
  const { schema } = config;

  // Currently, options besides `permissions` and `observers` are taken only
  // from environment.

  const endpoint = process.env.SWYMBASE_SPARQL_ENDPOINT;
  if (!endpoint) {
    throw new Error("MissingSparqlEndpointConfiguration");
  }

  const named_graph = process.env.SWYMBASE_PRIMARY_GRAPH;
  const sparql_engine = process.env.SWYMBASE_SPARQL_ENGINE;

  if (!(sparql_engine === undefined || sparql_engine === "neptune")) {
    throw new Error("UnsupportedSparqlEngine");
  }

  const effective_config: KBInterfaceOptions<any> = {
    ...config,
    named_graph,
    sparql_engine,
  };

  const kb = make_kb_interface(endpoint, schema, effective_config);

  return async (event) => {
    return handler_inner(event, kb, schema);
  };
};
