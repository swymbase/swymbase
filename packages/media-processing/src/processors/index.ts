/**
 * This file includes all media processing logic.
 *
 * Note: Media processors should not knowingly throw errors or impede execution.
 */
import type { MediaProcessing } from "@swymbase/media-interop";
import type { KBRecordInterface } from "@swymbase/sparql-rest";
import { image_processor } from "./image";
import { storage_processor } from "./store";
import { video_processor } from "./video";

export type MediaProcessor = (
  request: MediaProcessing,
  arn: string,
  kb: KBRecordInterface<any>
) => void | Promise<void>;

export const media_processors: Record<string, MediaProcessor> = {
  store: storage_processor,
  video: video_processor,
  image: image_processor,
};
