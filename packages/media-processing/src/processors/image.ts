import {
  get_s3,
  new_s3_key,
  parse_s3_arn,
  s3_arn,
} from "@swymbase/media-interop";
import { get_image_dimensions, resize_image } from "../lib/image-helpers";
import { get_public_url } from "../lib/index";
import type { MediaProcessor } from "../processors";

type Size = { property: string; max_width: number };

const SIZES: Record<string, Size> = {
  small: {
    property: "small",
    max_width: 360,
  },
  medium: {
    property: "medium",
    max_width: 720,
  },
  large: {
    property: "large",
    max_width: 1080,
  },
  xlarge: {
    property: "xlarge",
    max_width: 1440,
  },
};

export const image_processor: MediaProcessor = async (_, arn, kb) => {
  const s3 = get_s3();
  const parsed_arn = parse_s3_arn(arn);

  if (!parsed_arn) {
    throw new Error(`Could not parse S3 ARN:
     ${arn}`);
  }

  const Bucket = parsed_arn.Bucket;
  const public_url = await get_public_url(arn);

  // Resize and upload images concurrently.
  const dimensions = await get_image_dimensions(public_url);

  // Determine which sizes are applicable.
  const sizes = Object.values(SIZES).filter((size) => {
    return !dimensions || dimensions[0] > size.max_width;
  });

  const results = await Promise.all(
    sizes.map(async (size) => {
      try {
        const resized = await resize_image(public_url, size.max_width);

        if (resized) {
          const Key = new_s3_key();

          await s3.putObject({
            Bucket,
            Key,
            Body: resized,
            ContentType: "image/png",
            // Do not specify processing request, do it below.
            Metadata: {},
          });

          // Return property to write.
          return { [size.property]: { id: s3_arn({ Bucket, Key }) } };
        }
      } catch (e) {
        console.log(`Error resizing image using ffmpeg for object: ${arn}`, e);
      }

      return {};
    }, {})
  );

  // If applicable, write properties to Media record.
  if (results.length > 0) {
    const properties = results.reduce((p, c) => Object.assign(p, c));

    const patch_result = await kb.patch_record(
      "Media",
      { id: arn, ...properties },
      {}
    );

    if (!patch_result.success) {
      console.log(`Unable to write size properties to media record: ${arn}`);
      console.log(patch_result.code, patch_result.error);
    }
  }
};
