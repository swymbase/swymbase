import type { MediaProcessor } from "../processors";

export const storage_processor: MediaProcessor = async (request, arn, kb) => {
  // Create Media record in KB for this object using S3 ARN as IRI.
  const post_result = await kb.post_record("Media", { id: arn }, {});

  if (!post_result.success) {
    console.log(
      "FAILED to write Media record to KB.",
      post_result.code,
      post_result.error,
      post_result.http_response
    );
  }
};
