import {
  get_s3,
  new_s3_key,
  parse_s3_arn,
  s3_arn,
} from "@swymbase/media-interop";
import {
  get_public_url,
  get_video_duration,
  get_video_thumbnail,
} from "../lib/index";
import type { MediaProcessor } from "../processors";

export const video_processor: MediaProcessor = async (_, arn, kb) => {
  const s3 = get_s3();
  const parsed_arn = parse_s3_arn(arn);

  if (!parsed_arn) {
    throw new Error(`Could not parse S3 ARN: ${arn}`);
  }

  let video_duration: number | undefined = undefined;
  let video_thumbnail: string | undefined = undefined;

  const public_url = await get_public_url(arn);

  try {
    // Capture video duration in milliseconds.
    video_duration = await get_video_duration(public_url);
  } catch (e) {
    console.log(`Error capturing duration using ffprobe for object: ${arn}`, e);
  }

  try {
    // Capture & write thumbnail.
    const thumbnail = await get_video_thumbnail(public_url);

    if (thumbnail) {
      // Save to same bucket as source.
      const Bucket = parsed_arn.Bucket;
      const Key = new_s3_key();

      await s3.putObject({
        Bucket,
        Key,
        Body: thumbnail,
        ContentType: "image/png",
        // Do not specify processing request, do it below.
        Metadata: {},
      });

      video_thumbnail = s3_arn({ Bucket, Key });
    }
  } catch (e) {
    console.log(`Error creating thumbnail using ffmpeg for object: ${arn}`, e);
  }

  // Write video duration to Media record.
  if (video_duration || video_thumbnail) {
    const patch_result = await kb.patch_record(
      "Media",
      {
        id: arn,
        video_duration,
        ...(video_thumbnail
          ? { video_thumbnail: { id: video_thumbnail } }
          : {}),
      },
      {}
    );

    if (!patch_result.success) {
      console.log(`Unable to write video duration to media record: ${arn}`);
      console.log(patch_result.code, patch_result.error);
    }
  }
};
