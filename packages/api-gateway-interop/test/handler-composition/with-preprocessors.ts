import test from "ava";
import type { APIGatewayProxyEvent as APIEvent, Context } from "aws-lambda";
import type { Preprocess, BaseHandler } from "@swymbase/api-gateway-interop";
import { with_preprocessors } from "@swymbase/api-gateway-interop";

test("calls base handler when there are no preprocessors", async (t) => {
  const base_handler: BaseHandler = async () => ({
    statusCode: 200,
    body: "value from handler",
  });

  const wrapped = with_preprocessors([], base_handler);
  const event = {} as unknown as APIEvent;
  const context = {} as Context;
  const result = await wrapped(event, context);
  t.deepEqual(result, { statusCode: 200, body: "value from handler" });
});

test("preprocessor can extend context", async (t) => {
  const add_user_id: Preprocess<{ user_id: string }> = () => ({
    context: { user_id: "john.conway" },
  });

  const base_handler: BaseHandler<{ user_id: string }> = (context) => ({
    statusCode: 200,
    body: `hello ${context.user_id}`,
  });

  const wrapped = with_preprocessors([add_user_id], base_handler);
  const event = {} as unknown as APIEvent;
  const context = {} as Context;
  const result = await wrapped(event, context);

  t.deepEqual(result, { statusCode: 200, body: "hello john.conway" });
});

test("preprocessor has access to event", async (t) => {
  const add_user_id: Preprocess<{ user_id: string }> = (event) => ({
    context: { user_id: event.queryStringParameters?.["user"]! },
  });

  const base_handler: BaseHandler<{ user_id: string }> = async (context) => ({
    statusCode: 200,
    body: `hello ${context.user_id}`,
  });

  const wrapped = with_preprocessors([add_user_id], base_handler);
  const event = {
    queryStringParameters: { user: "paul.allen" },
  } as unknown as APIEvent;
  const context = {} as Context;
  const result = await wrapped(event, context);

  t.deepEqual(result, { statusCode: 200, body: "hello paul.allen" });
});

test("preprocessor can return a response", async (t) => {
  const teapot: Preprocess<object> = () => ({
    response: { statusCode: 418, body: "I'm a teapot" },
  });

  const base_handler: BaseHandler<{ user_id: string }> = () => ({
    statusCode: 200,
    body: "you'll never reach me",
  });

  const wrapped = with_preprocessors([teapot], base_handler);
  const event = {} as unknown as APIEvent;
  const context = {} as Context;
  const result = await wrapped(event, context);

  t.deepEqual(result, { statusCode: 418, body: "I'm a teapot" });
});

test("preprocessor receive context from earlier preprocessors", async (t) => {
  const authenticate: Preprocess<{ user: string }> = (event) => ({
    context: { user: event.headers["X-User"] },
  });

  const authorize: Preprocess<{ permissions: string[] }> = (
    _,
    context: { user: string }
  ) => ({
    context: { permissions: [`${context.user}:invoke`] },
  });

  const base_handler: BaseHandler<{
    // TODO: It should be okay for this to be optional, or omitted altogether.
    // See note to `ComposeHandlers` in `preprocessors`.
    user: string;
    user_id: string;
    permissions: string[];
  }> = async (context) => ({ statusCode: 200, body: context.permissions[0] });

  const wrapped = with_preprocessors([authenticate, authorize], base_handler);

  const event = {
    headers: { "X-User": "steve.allen" },
  } as unknown as APIEvent;
  const context = {} as Context;
  const result = await wrapped(event, context);

  t.is(result.body, "steve.allen:invoke");
});

test("types are inferred when composing preprocessors", async (t) => {
  // This is the same as the previous test but with fewer annotations.

  const wrapped = with_preprocessors(
    [
      (e) => ({ context: { user: e.headers["X-User"] } }),
      // TS: For some reason context becomes `Partial<unknown>` when the first
      // preprocessor includes parameters.
      (_, context: { user: string }) => ({
        context: { permissions: [`${context.user}:invoke`] },
      }),
    ],
    (context) => ({ statusCode: 200, body: context.permissions[0] })
  );

  const event = {
    headers: { "X-User": "fran.allen" },
  } as unknown as APIEvent;
  const context = {} as Context;
  const result = await wrapped(event, context);

  t.is(result.body, "fran.allen:invoke");
});

test("async preprocessors are awaited in succession", async (t) => {
  const timeout = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  const wrapped = with_preprocessors(
    [
      async () => {
        await timeout(50);
        return { context: { user: "guest" } };
      },
      async (_, context) => {
        await timeout(50);
        return { context: { policy: `DENY ${context.user}:invoke` } };
      },
    ],
    (context) => ({ statusCode: 403, body: context.policy })
  );

  const event = {} as unknown as APIEvent;
  const context = {} as Context;
  const result = await wrapped(event, context);

  t.is(result.body, "DENY guest:invoke");
});
