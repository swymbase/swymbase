import test, { Macro, ExecutionContext } from "ava";
import { with_include_paths } from "@swymbase/api-gateway-interop";
import type { APIGatewayProxyEvent } from "aws-lambda";

interface BaseCase {
  readonly event: Partial<APIGatewayProxyEvent>;
  readonly include_paths: readonly (readonly string[])[];
}

type TestCase = readonly [
  param: readonly string[],
  include_paths: readonly (readonly string[])[],
  note?: string
];

const check_base = async (t: ExecutionContext, spec: BaseCase) => {
  const { event, include_paths } = spec;

  // === OPERATION
  // TS: preprocessor doesn't expect partial event
  const result = await with_include_paths(<any>event, {});

  // === POSTCONDITIONS

  if (!result) throw new Error("Result of operation was undefined");

  if (!("context" in result)) {
    t.log("RESULT", result);
    throw new Error("Expected context in result");
  }

  // In practice, order doesn't matter
  t.deepEqual(result.context.include_paths, include_paths);
};

const check: Macro<[TestCase]> = async (t, [param, include_paths]) =>
  check_base(t, {
    event: { multiValueQueryStringParameters: { include: param as string[] } },
    include_paths,
  });
check.title = (_, [input, output, note]) =>
  `${JSON.stringify(input)} => ${JSON.stringify(output)}` +
  (note ? ` (${note})` : "");

test("no query params", (t) => check_base(t, { event: {}, include_paths: [] }));
test("no relevant query params", (t) =>
  check_base(t, {
    event: { multiValueQueryStringParameters: { foo: [], fields: ["abc"] } },
    include_paths: [],
  }));

// Note that a query key will not contain an empty array.
test(check, [[""], []]);
test(check, [["foo,bar"], [["foo"], ["bar"]]]);
test(check, [["foo,bar,hello_world"], [["foo"], ["bar"], ["hello_world"]]]);
test(check, [["foo", ""], [["foo"]]]);
test(check, [["foo", "foo"], [["foo"], ["foo"]], "Does not deduplicate"]);
test(check, [
  ["foo,,bar"],
  [["foo"], ["bar"]],
  "Ignores empty values in a list",
]);
test(check, [
  ["foo", "bar"],
  [["foo"], ["bar"]],
]);
test(check, [
  ["foo", "bar,hello_world"],
  [["foo"], ["bar"], ["hello_world"]],
]);
// Paths
test(check, [["friend.image"], [["friend"], ["friend", "image"]]]);
test(check, [["foo,friend.image"], [["foo"], ["friend"], ["friend", "image"]]]);
test(check, [
  ["foo", "bar,deeply.included.Thing:path,baz"],
  [
    ["foo"],
    ["bar"],
    ["deeply"],
    ["deeply", "included"],
    ["deeply", "included", "Thing:path"],
    ["baz"],
  ],
]);
