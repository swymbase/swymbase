import test, { Macro } from "ava";
import { with_fieldset_params } from "@swymbase/api-gateway-interop";
import type { APIGatewayProxyEvent } from "aws-lambda";
import type { Fieldsets } from "@swymbase/sparql-rest";

interface TestCase {
  readonly event: Partial<APIGatewayProxyEvent>;
  readonly fields: Fieldsets<any>;
}

const check: Macro<[TestCase]> = async (t, spec) => {
  const { event, fields } = spec;

  // === OPERATION
  // TS: preprocessor doesn't expect partial event
  const result = await with_fieldset_params(<any>event, {});

  // === POSTCONDITIONS

  if (!result) throw new Error("Result of operation was undefined");

  if (!("context" in result)) {
    t.log("RESULT", result);
    throw new Error("Expected context in result");
  }

  // In practice, order doesn't matter
  t.deepEqual(result.context.fields, fields);
};

test("no query params", check, {
  event: {},
  fields: {},
});

test("no relevant query params", check, {
  event: { multiValueQueryStringParameters: { foo: [], fields: ["abc"] } },
  fields: {},
});

// Note that a query key will not contain an empty array.
test("one fields parameter with empty string", check, {
  event: { multiValueQueryStringParameters: { "fields[Alpha]": [""] } },
  fields: { Alpha: [] },
});

test("one type with one field", check, {
  event: { multiValueQueryStringParameters: { "fields[Alpha]": ["foo"] } },
  fields: { Alpha: ["foo"] },
});

test("one fields parameter with one field", check, {
  event: { multiValueQueryStringParameters: { "fields[Alpha]": ["foo"] } },
  fields: { Alpha: ["foo"] },
});

test("one fields parameter with multiple fields", check, {
  event: {
    multiValueQueryStringParameters: { "fields[Alpha]": ["foo,bar,baz"] },
  },
  fields: { Alpha: ["foo", "bar", "baz"] },
});

test("multiple fields parameters", check, {
  event: {
    multiValueQueryStringParameters: {
      "fields[Alpha]": ["foo"],
      "fields[Beta]": ["brains,brawn"],
      "fields[Delta]": [""],
    },
  },
  fields: { Alpha: ["foo"], Beta: ["brains", "brawn"], Delta: [] },
});

test("concatenates multiple values for same type", check, {
  event: {
    multiValueQueryStringParameters: {
      "fields[Alpha]": ["foo", "bar,baz", ""],
      "fields[Beta]": ["brains,brawn"],
    },
  },
  fields: { Alpha: ["foo", "bar", "baz"], Beta: ["brains", "brawn"] },
});
