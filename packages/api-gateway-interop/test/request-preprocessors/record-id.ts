import { with_record_id } from "@swymbase/api-gateway-interop";
import test, { Macro } from "ava";
import type { APIGatewayProxyEvent } from "aws-lambda";

interface TestCase {
  readonly event: Partial<APIGatewayProxyEvent>;
  readonly output:
    | { readonly ok: true; readonly record_id: string }
    | { readonly ok: false; readonly error: string };
}

const check: Macro<[TestCase]> = async (t, spec) => {
  const { event, output } = spec;

  // === OPERATION
  // TS: preprocessor doesn't expect partial event
  const result = await with_record_id(<any>event, {});

  // === POSTCONDITIONS

  if (!result) throw new Error("Result of operation was undefined");

  if (output.ok) {
    if (!("context" in result)) {
      t.log("RESULT", result);
      throw new Error("Expected context in result");
    }
    t.deepEqual(result, { context: { record_id: output.record_id } });
  } else {
    if (!("response" in result)) {
      t.log("RESULT", result);
      throw new Error("Expected response in result");
    }
    t.is(result.response.statusCode, 400);
    const obj = JSON.parse(result.response.body);
    t.is(obj.errors[0].code, output.error);
  }
};

test("no path params", check, {
  event: { queryStringParameters: { id: "foo" } },
  output: { ok: false, error: "MissingRecordIdentifier" },
});

test("no id path params", check, {
  event: { pathParameters: { type: "Volcano" } },
  output: { ok: false, error: "MissingRecordIdentifier" },
});

test("invalid ID path param", check, {
  event: { pathParameters: { id: "ElCapitan" } },
  output: { ok: false, error: "InvalidRecordIdentifier" },
});

test("qualified ID path param", check, {
  event: { pathParameters: { id: "ex:Alice" } },
  output: { ok: true, record_id: "ex:Alice" },
});

test("http path param", check, {
  event: { pathParameters: { id: "http://example.com/Bob" } },
  output: { ok: true, record_id: "http://example.com/Bob" },
});
