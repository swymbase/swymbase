# AWS Interop utilities

This package contains functions for defining endpoint handlers for
[API Gateway](https://aws.amazon.com/api-gateway/).

## Preprocessor composition

The package contains facilities for composing typed preprocessor pipelines.

See the [handler-composition](src/handler-composition) directory for details.

## Preprocessor library

This package also contains a selection of predefined preprocessors for common
requirements.

See the [request-preprocessors](src/request-preprocessors) directory for
details.
