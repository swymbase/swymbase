import { SimpleHandler } from "../handler-composition/index";

/**
 * Wrap a handler so that it includes specific CORS headers in the response.
 */
export const with_cors = (handler: SimpleHandler): SimpleHandler => {
  return async (...args) => {
    const response = await handler(...args);
    return {
      ...response,
      headers: {
        ...(response.headers || {}),
        // This could be changed to an environment-specific value.
        "Access-Control-Allow-Origin": "*",
      },
    };
  };
};
