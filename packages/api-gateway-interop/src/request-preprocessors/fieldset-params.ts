import type { Fieldsets } from "@swymbase/sparql-rest";
import type { Preprocess } from "../handler-composition/index";
import { query_from } from "../handler-composition/index";

export interface WithFieldsetParams {
  readonly fields: Fieldsets<any>;
}

const FIELDS_PARAM = /^fields\[(.+)\]$/;

/**
 * This preprocessor guarantees that the context will include a record of any
 * fieldsets requested for inclusion with the response.
 *
 * The list of properties is sourced from the query parameters in the form
 * `fields[TYPE]`, where `TYPE` is an alias of a record type from the schema,
 * interpreted according to the [“Sparse Fieldsets” section of the JSON:API
 * spec](https://jsonapi.org/format/#fetching-sparse-fieldsets).
 *
 * If multiple `fields` parameters are provided for the same type, all of the
 * indicated fields are included in the list.
 *
 * Note that neither the types nor the properties specified in the parameter are
 * validated against a schema.
 */
export const with_fieldset_params: Preprocess<WithFieldsetParams> = (event) => {
  const fields = Object.create(null);
  const query = query_from(event);
  for (const [key, values] of query) {
    const [, type] = key.match(FIELDS_PARAM) || [];
    if (type) {
      fields[type] ??= [];
      for (const value of values)
        for (const field of value.split(","))
          if (field) fields[type].push(field);
    }
  }

  return { context: { fields } };
};
