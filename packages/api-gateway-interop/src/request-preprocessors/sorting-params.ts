import type { Preprocess } from "../handler-composition/index";
import { query_from } from "../handler-composition/index";

export interface WithSortingParams {
  readonly order_by: readonly {
    readonly property: string;
    readonly descending?: boolean;
  }[];
}

/**
 * This preprocessor guarantees that the context will include a record of any
 * sort properties and directions specified in the request's query parameters.
 *
 * Sorting properties are sourced from the `sort` query parameter, with
 * multiple, directional fields interpreted according to the [“Sorting” section
 * of the JSON:API spec](https://jsonapi.org/format/#fetching-sorting).
 */
export const with_sorting_params: Preprocess<WithSortingParams> = (event) => {
  const query = query_from(event);

  const properties = query.get("sort")?.[0]?.split(",") ?? [];

  const order_by: WithSortingParams["order_by"] = properties.map((property) =>
    property.startsWith("-")
      ? { property: property.substr(1), descending: true }
      : { property }
  );

  return { context: { order_by } };
};
