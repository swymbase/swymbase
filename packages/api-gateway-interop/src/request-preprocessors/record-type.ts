import type { Preprocess } from "../handler-composition/index";
import type { MetaInterface, MetaInterfaces } from "@swymbase/metatype-core";

export interface WithRecordType<T extends MetaInterfaces> {
  record_type_name: keyof T;
  record_type_spec: MetaInterface;
}

/**
 * Given a metatype schema, return a preprocessor that guarantees that the
 * context will include an identifier and definition of one of the type aliases
 * defined by the schema.  The type identifier is sourced from the `type` key of
 * the event's path parameters.
 */
export const with_record_type_from =
  <T extends MetaInterfaces>(types: T): Preprocess<WithRecordType<T>> =>
  (event) => {
    const headers = { "Content-type": "application/json" };
    const type_name_raw = event.pathParameters?.["type"];

    if (!type_name_raw)
      return {
        response: {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            errors: [
              { code: "MissingRecordTypeIndicator", title: "Bad request" },
            ],
          }),
        },
      };

    const record_type_name = decodeURIComponent(type_name_raw);

    const record_type_spec = types[record_type_name];
    if (!record_type_spec)
      return {
        response: {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            errors: [
              { code: "UnrecognizedRecordTypeIndicator", title: "Bad request" },
            ],
          }),
        },
      };

    return { context: { record_type_name, record_type_spec } };
  };
