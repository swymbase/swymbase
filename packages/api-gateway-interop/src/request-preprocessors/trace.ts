import type { Preprocess } from "../handler-composition/index";
import { inspect } from "util";

interface TraceOptions {
  readonly prefix: string;
  readonly depth: number;
}

/**
 * Utility preprocessor for inspecting the context at this point in a pipeline.
 *
 * NOTE: This is intended for local debugging only and should not be used in
 * production.
 */
export const with_trace =
  (options?: Partial<TraceOptions>): Preprocess =>
  (_event, context) => {
    const prefix = options?.prefix ?? "CONTEXT";
    const depth = options?.depth;

    console.log(prefix, inspect(context, { depth }));
    return null;
  };
