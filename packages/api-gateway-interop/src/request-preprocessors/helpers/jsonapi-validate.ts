import type * as JSONAPI from "jsonapi-typescript";

// SHALLOW checks for various top-level JSON:API types
// Note that `type` is treated as optional.

/* === BEGIN WORKAROUND === */
// TODO: resolve upstream
// https://github.com/mike-north/jsonapi-typescript/issues/180 is resolved

export type JSONAPIPrimaryData = JSONAPI.PrimaryData | null;

export interface JSONAPIDocWithData extends JSONAPI.DocBase {
  data: JSONAPIPrimaryData;
  included?: JSONAPI.Included;
}

export type JSONAPIDocument = JSONAPI.Document | JSONAPIDocWithData;
/* === END WORKAROUND === */

const is_plain_object = (x: any) =>
  x !== null && typeof x === "object" && !Array.isArray(x);

/**
 * Regarding the optionality of `id`:
 *
 * > Exception: The id member is not required when the resource object
 * > originates at the client and represents a new resource to be created on the
 * > server.
 *
 * https://jsonapi.org/format/#document-resource-objects
 */
const is_resource = (x: any) =>
  is_plain_object(x) && (x.id === undefined || typeof x.id === "string");

// TODO: resolve upstream.  Predicate is consistent with spec but not with type.
// https://github.com/mike-north/jsonapi-typescript/issues/180
const is_primary_data = (x: any): x is JSONAPI.PrimaryData =>
  x === null ||
  is_resource(x) ||
  (Array.isArray(x) && x.every((item) => is_resource(item)));

export const is_jsonapi_collection_resource_document = (
  x: any
): x is JSONAPI.CollectionResourceDoc =>
  is_primary_data(x?.data) && Array.isArray(x?.data);

export const is_jsonapi_single_resource_document = (
  x: any
): x is JSONAPI.SingleResourceDoc =>
  is_primary_data(x?.data) && x !== null && !Array.isArray(x?.data);

export const is_jsonapi_data_document = (x: any): x is JSONAPIDocWithData =>
  is_primary_data(x?.data);

export const is_jsonapi_meta_document = (x: any): x is JSONAPI.DocWithMeta =>
  is_plain_object(x?.meta);

export const is_jsonapi_error_document = (x: any): x is JSONAPI.DocWithErrors =>
  Array.isArray(x?.errors);

export const is_jsonapi_document = (x: any): x is JSONAPIDocument =>
  is_jsonapi_data_document(x) ||
  is_jsonapi_meta_document(x) ||
  is_jsonapi_error_document(x);
