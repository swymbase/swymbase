// These intermediate helpers should not be exported.  They are needed only to
// avoid the circularity restriction that is already gone in the latest TS.
interface ReadonlyJSONArray extends ReadonlyArray<ReadonlyJSON> {}
interface ReadonlyJSONObject extends Readonly<Record<string, ReadonlyJSON>> {}

/**
 * Describes the value space that can be serialized as JSON — almost.
 * TypeScript can't constrain against cyclic object graphs, but they will throw
 * if you attempt to serialize them.  For *parsed* JSON values (using default
 * handler), this is an exact description.
 */
export type ReadonlyJSON =
  | null
  | boolean
  | number
  | string
  | ReadonlyJSONObject
  | ReadonlyJSONArray;

/**
 * Return the JSON value for the given lexical representation, or `undefined` if
 * it cannot be parsed.
 */
export const try_parse_json = (text: string): ReadonlyJSON | undefined => {
  try {
    return JSON.parse(text);
  } catch {
    // Ignore error, return undefined
  }
};
