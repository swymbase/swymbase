import type { Preprocess } from "../handler-composition/index";
import type { WithJsonBody } from "./json-body";
import { is_jsonapi_document } from "./helpers/jsonapi-validate";
import type { JSONAPIDocument } from "./helpers/jsonapi-validate";

export interface WithJsonApiDocument {
  /** The request body interpreted as a JSON:API document. */
  json_api_document: JSONAPIDocument;
}

/**
 * This preprocessor guarantees that the context will include a valid JSON:API
 * Document object as represented by the request body.
 *
 * Assumes a context containing a valid JSON object.
 */
export const with_json_api_document: Preprocess<
  WithJsonApiDocument,
  WithJsonBody
> = (_event, context) => {
  const headers = { "Content-type": "application/json" };

  const { json_body } = context;

  if (!is_jsonapi_document(json_body)) {
    return {
      response: {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          errors: [{ code: "BodyNotValidJsonApiFormat", title: "Bad Request" }],
        }),
      },
    };
  }

  return { context: { json_api_document: json_body } };
};
