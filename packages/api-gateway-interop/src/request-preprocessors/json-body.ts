import { try_parse_json } from "./helpers/json";
import type { ReadonlyJSON } from "./helpers/json";
import type { Preprocess } from "../handler-composition/index";

export interface WithJsonBody {
  /** The request body interpreted as a JSON value. */
  readonly json_body: ReadonlyJSON;
}

/**
 * This preprocessor guarantees that the context will include a valid JSON
 * object representing the content of the request body.  Note that this may
 * include `null` values.
 *
 * This should be used with handlers that expect a JSON-based media type;
 * however, it does *not* regard the request's `Content-Type` header.
 *
 * This should be used only when a non-empty request body is expected.  If the
 * request body is not a well-formed JSON value, the preprocessor will return a
 * Bad Request error.
 */
export const with_json_body: Preprocess<WithJsonBody> = (event) => {
  const headers = { "Content-type": "application/json" };

  const { body } = event;

  // Note that this also applies to empty strings.
  if (!body) {
    return {
      response: {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          errors: [{ code: "MissingRequestBody", title: "Bad Request" }],
        }),
      },
    };
  }

  const json_body = try_parse_json(body);

  if (json_body === undefined) {
    return {
      response: {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          errors: [{ code: "RequestBodyInvalidJSON", title: "Bad Request" }],
        }),
      },
    };
  }

  return { context: { json_body } };
};
