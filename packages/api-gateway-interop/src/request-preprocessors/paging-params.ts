import type { Preprocess } from "../handler-composition/index";
import { query_from } from "../handler-composition/index";

export interface WithPagingParams {
  readonly paging: {
    readonly offset?: number;
    readonly limit?: number;
  };
}

/**
 * This preprocessor guarantees that the context will include a record of any
 * numeric limit and offset specified in the request's query parameters.
 *
 * Paging properties are sourced from the `offset` and `limit` query parameters.
 */
export const with_paging_params: Preprocess<WithPagingParams> = (event) => {
  const query = query_from(event);
  const paging: WithPagingParams["paging"] = {};

  for (const key of ["offset", "limit"]) {
    const param = query.get(key)?.[0];

    if (param) {
      paging[key] = parseInt(param, 10);
    }
  }

  return { context: { paging } };
};
