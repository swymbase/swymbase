import type { Preprocess } from "../handler-composition/index";

export interface WithRecordID {
  /** The IRI of the record identified in the request (as part of path). */
  record_id: string;
}

// A loose definition of a valid IRI.  This is meant to filter out cases where,
// e.g. only the UUID of an entity is sent.
const IRI_PATTERN = /[a-z]+:.+/i;
const looks_like_iri = (s: string): boolean => IRI_PATTERN.test(s);

/**
 * This preprocessor guarantees that the context will include a record
 * identifier sourced from the `id` key of the event's path parameters, and that
 * the identifier has the general structure of an IRI (specifically, starts with
 * an alphabetic protocol followed by colon).
 */
export const with_record_id: Preprocess<WithRecordID> = (event) => {
  const headers = { "Content-type": "application/json" };
  const record_iri_raw = event.pathParameters?.["id"];

  if (!record_iri_raw)
    return {
      response: {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          errors: [{ code: "MissingRecordIdentifier", title: "Bad request" }],
        }),
      },
    };

  const record_id = decodeURIComponent(record_iri_raw);

  if (!looks_like_iri(record_id))
    return {
      response: {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          errors: [{ code: "InvalidRecordIdentifier", title: "Bad request" }],
        }),
      },
    };

  return { context: { record_id } };
};
