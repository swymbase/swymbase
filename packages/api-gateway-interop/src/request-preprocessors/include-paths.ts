import type { Preprocess } from "../handler-composition/index";
import { query_from } from "../handler-composition/index";

export interface WithIncludePaths {
  readonly include_paths: readonly (readonly string[])[];
}

/**
 * This preprocessor guarantees that the context will include a record of any
 * relationship properties requested for inclusion with the response.
 *
 * The list of properties is sourced from the `include` query parameter, with
 * multiple properties interpreted according to the [“Inclusion of Related
 * Resources” section of the JSON:API
 * spec](https://jsonapi.org/format/#fetching-includes).
 *
 * Note that the list will comprise intermediate paths, as per the spec:
 *
 * > Because compound documents require full linkage (except when relationship
 * > linkage is excluded by sparse fieldsets), intermediate resources in a
 * > multi-part path must be returned along with the leaf nodes. For example, a
 * > response to a request for `comments.author` should
 * > include `comments` as well as the `author` of each of those `comments`.
 */
export const with_include_paths: Preprocess<WithIncludePaths> = (event) => {
  const include_paths: string[][] = [];
  const query = query_from(event);
  const include = query.get("include");
  if (include) {
    for (const value of include) {
      for (const path of value.split(",")) {
        if (path) {
          const parts = path.split(".");
          for (let i = 1; i <= parts.length; i++) {
            include_paths.push(parts.slice(0, i));
          }
        }
      }
    }
  }

  return { context: { include_paths } };
};
