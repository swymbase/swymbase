import type * as JSONAPI from "jsonapi-typescript";
import type { Preprocess } from "../handler-composition/index";
import type { WithJsonApiDocument } from "./json-api-document";
import { is_jsonapi_data_document } from "./helpers/jsonapi-validate";

// See “Optionality of `type`” in `metatype-jsonapi` docs.
export type ResourceIdentifier = Readonly<
  Omit<JSONAPI.ResourceIdentifierObject, "type">
>;

export interface WithResourceIdentifiers {
  readonly resource_identifiers: readonly ResourceIdentifier[];
}

const ensure_array = <T>(t: T | readonly T[]): readonly T[] =>
  Array.isArray(t) ? t : t == null ? [] : [t];

/**
 * This preprocessor guarantees that the context will include an array
 * containing any JSON:API Resource Identifier Objects provided as the “primary
 * data” of a JSON:API request.  (See
 * https://jsonapi.org/format/#document-top-level)
 *
 * Assumes a context containing a valid JSON:API document.
 *
 * The preprocessor will fail with a Bad Request error if:
 *
 * - the JSON API document does not contain a top-level `data` key
 *
 * - the top-level `data` key is a non-`null` value containing any invalid
 *   resource identifiers
 *
 * Note that this does not guarantee that any resource identifiers are actually
 * provided.
 */
export const with_resource_identifiers: Preprocess<
  WithResourceIdentifiers,
  WithJsonApiDocument
> = async (_event, context) => {
  const headers = { "Content-type": "application/json" };

  const { json_api_document } = context;

  if (!is_jsonapi_data_document(json_api_document)) {
    return {
      response: {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          errors: [
            { code: "ResourceIdentifiersRequiresData", title: "Bad Request" },
          ],
        }),
      },
    };
  }

  const data = json_api_document.data ?? [];

  const ids = ensure_array(data);
  const resource_identifiers = ids
    .filter((i) => i?.id)
    // TS: doesn't realize the filter constrains to defined id's
    .map((i) => ({ id: i.id as string }));

  if (resource_identifiers.length !== ids.length) {
    console.log("InvalidResourceIdentifiersFormat", ids);
    return {
      response: {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          errors: [
            { code: "InvalidResourceIdentifiersFormat", title: "Bad Request" },
          ],
        }),
      },
    };
  }

  return { context: { resource_identifiers } };
};
