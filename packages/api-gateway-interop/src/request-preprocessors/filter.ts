import type { Preprocess } from "../handler-composition/index";
import type { Filter } from "@swymbase/sparql-rest";
import { query_from } from "../handler-composition/index";

export interface WithFilterExpression {
  readonly filter?: Filter | null;
  // DEPRECATED: This will be removed in a future version, once it is not in use
  // by clients.
  filters?: { [key: string]: string[] };
}

/**
 * This preprocessor guarantees that the context will include a record of any
 * filter expression specified by the request.
 *
 * The filter expression is sourced from the `filter` query parameter, as
 * recommended in the [“Filtering” section of the JSON:API
 * spec](https://jsonapi.org/format/#fetching-filtering).
 *
 * The content of the filter parameter is interpreted according to the Filter
 * feature defined in the `sparql-rest` package.
 */
export const with_filter_expression: Preprocess<WithFilterExpression> = (
  event
) => {
  const query = query_from(event);
  let filters: { [key: string]: string[] } | undefined = undefined;
  let filter: Filter | null = null;

  try {
    const raw = query.get("filter")?.[0];
    filter =
      raw && typeof raw === "string" ? (JSON.parse(raw) as Filter) : null;
  } catch (error) {
    throw new Error(`filter is not well-formed JSON`);
  }

  for (const [key, values] of query) {
    const matches = key.match(/filter\[(.+)\]/);

    if (matches && matches.length === 2) {
      const field = matches[1];
      const value = values?.[0];

      if (value) {
        if (!filters) filters = {};
        filters[field] = value.split(",");
      }
    }
  }

  return { context: { filter, filters } };
};
