import type { Preprocess } from "../handler-composition/index";
import type { MetaProperty, MetaInterfaces } from "@swymbase/metatype-core";
import type { WithRecordType } from "./record-type";

export interface WithRelationship {
  relationship_name: string;
  relationship_spec: MetaProperty;
}

/**
 * Given a metatype schema, return a preprocessor that guarantees that the
 * context will include an identifier and definition of one of the properties
 * defined on a type in the schema.  The property identifier is sourced from the
 * `relationship_name` key of the event's path parameters.
 *
 * Assumes a context containing a record type.
 */
export const with_relationship_from =
  <T extends MetaInterfaces>(
    types: T
  ): Preprocess<WithRelationship, WithRecordType<T>> =>
  (event, context) => {
    const headers = { "Content-type": "application/json" };
    const name_raw = event.pathParameters?.["relationship_name"];
    const { record_type_name } = context;

    if (!name_raw)
      return {
        response: {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "MissingRelationshipNameIndicator",
                title: "Bad request",
              },
            ],
          }),
        },
      };

    const relationship_name = decodeURIComponent(name_raw);

    const relationship_spec = types[record_type_name]?.properties?.[
      relationship_name
    ] as MetaProperty | undefined;

    if (!relationship_spec?.relationship)
      return {
        response: {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            errors: [
              {
                code: "UnrecognizedRelationshipTypeIndicator",
                title: "Bad request",
              },
            ],
          }),
        },
      };

    return { context: { relationship_name, relationship_spec } };
  };
