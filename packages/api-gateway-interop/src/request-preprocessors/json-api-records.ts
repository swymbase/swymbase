import type { MetaInterfaces, InterfaceFrom } from "@swymbase/metatype-core";
import {
  collection_document_to_records,
  document_to_record,
} from "@swymbase/metatype-jsonapi";
import type { Preprocess } from "../handler-composition/index";
import type { WithJsonApiDocument } from "./json-api-document";
import {
  is_jsonapi_collection_resource_document,
  is_jsonapi_single_resource_document,
} from "./helpers/jsonapi-validate";

export interface WithJsonApiRecords {
  /**
   * The request body's primary data interpreted as a collection of records
   * according to the available schema.
   */
  readonly json_api_records: readonly InterfaceFrom<any>[];
}

/**
 * This preprocessor guarantees that the context will include an array of the
 * records that could be interpreted from the primary data of the request body.
 *
 * Assumes a context containing a valid JSON:API document.
 *
 * If no Resource Objects are provided (i.e. primary data is missing, `null`, or
 * an empty array), then an empty array is returned.  This allows the
 * preprocessor to be used with handlers that may recognize `meta` documents, in
 * which case `data` is not provided.
 *
 * Resource Objects that cannot be interpreted as records according to the
 * available schema are ignored.
 */
export const with_json_api_records_from =
  <T extends MetaInterfaces>(
    types: T
  ): Preprocess<WithJsonApiRecords, WithJsonApiDocument> =>
  (_event, context) => {
    const { json_api_document } = context;

    const records = ((doc) => {
      if (is_jsonapi_collection_resource_document(doc))
        return collection_document_to_records(types, doc);

      if (is_jsonapi_single_resource_document(doc)) {
        const record = document_to_record(types, doc);
        if (record) return [record];
      }

      return [];
    })(json_api_document);

    return { context: { json_api_records: records } };
  };
