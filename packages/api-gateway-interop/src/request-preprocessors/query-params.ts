import type { Preprocess, QueryParams } from "../handler-composition/index";
import { query_from } from "../handler-composition/index";

export interface WithQueryParams {
  readonly query: QueryParams;
}

/**
 * Add normalized query to context.
 */
export const with_query_params: Preprocess<WithQueryParams> = (event) => {
  const query = query_from(event);

  return { context: { query } };
};
