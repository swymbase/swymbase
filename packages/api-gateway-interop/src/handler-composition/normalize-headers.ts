/**
 * Given an HTTP headers object as received by a handler, return an object
 * extended to include equivalent values for lowercased header names, to support
 * case-insensitive header processing as required by the HTTP spec.  See
 * https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-known-issues.html
 */
export const normalize_headers = (
  headers: { [name: string]: string } | undefined
): Record<string, string> => {
  const normalized: Record<string, string> = {};
  if (headers && typeof headers === "object")
    for (const [key, value] of Object.entries(headers)) {
      normalized[key] = value;
      normalized[key.toLowerCase()] = value;
    }
  return normalized;
};
