import type { APIGatewayProxyEvent } from "aws-lambda";

type APIGatewayQueryV1 = Pick<
  APIGatewayProxyEvent,
  "queryStringParameters" | "multiValueQueryStringParameters"
>;

// A map is preferred to a dictionary (e.g. with an index signature) so it's
// always obvious in code that these are not predefined record fields.  In other
// words `query.foo` is misleading compared to `query.get("foo")`.
export type QueryParams = ReadonlyMap<string, readonly string[]>;

/**
 * Given (the relevant part of) an API Gateway Proxy event, return a uniform map
 * of the decoded query parameter values.
 *
 * This deals with two or three problems:
 *
 * - Query values in the request object are still URL encoded, which makes them
 *   unsuitable for most processing.
 *
 * - There is more than one event format (see notes following).
 *
 * - See note below re the possible inconsistency between the single- and
 *   multi-valued properties of the incoming event.  May or may not be a thing.
 *
 * Currently supports version 1 of the payload format. Version 2 completely
 * changes the representation of query values in the request format.
 *
 * This could be extended to
 * support version 2 without change to call sites by reading the payload's
 * `version` property.  See the section “Payload Format Version” here:
 * https://docs.aws.amazon.com/apigateway/latest/developerguide/http-api-develop-integrations-lambda.html
 */
export const query_from = (event: APIGatewayQueryV1): QueryParams => {
  const normalized = new Map<string, string[]>();

  const single = event.queryStringParameters ?? {};
  const multi = event.multiValueQueryStringParameters ?? {};

  const add = (key: string, value: string) => {
    const decoded_key = decodeURIComponent(key);
    if (!normalized.has(decoded_key)) normalized.set(decoded_key, []);
    normalized.get(decoded_key)!.push(value);
  };

  for (const [key, values] of Object.entries(multi)) {
    for (const value of values) {
      add(key, decodeURIComponent(value));
    }
  }

  // This may not be necessary.  However, I *think* that we have run into issues
  // where values provided as single values were not included here.
  for (const [key, value] of Object.entries(single)) {
    const decoded = decodeURIComponent(value);
    if (!normalized.get(decodeURIComponent(key))?.includes(decoded)) {
      add(key, decoded);
    } else {
      console.log("warning: inconsistent query:", single, multi);
    }
  }

  return normalized;
};
