// Context preprocessors.  Supports separation of concerns in API Gateway
// handlers.
//
// Preprocessor composition does not care about the actual structure of either
// the requests or the responses, and so in principle could be defined without
// reference to API Gateway types.  However, mainly because the handler *also*
// accepts a (rarely-used) Context argument, the types are still a bit tangled.
// I suppose I could replace the request type with a tuple representing all of
// the arguments.
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  Context,
} from "aws-lambda";
import { normalize_headers } from "./normalize-headers";

type MaybeAsync<T> = T | Promise<T>;

// like `APIGatewayProxyHandler` but
// - omit callback argument
// - don't accept `void` return
export type SimpleHandler<
  Event = APIGatewayProxyEvent,
  Result = APIGatewayProxyResult
> = (event: Event, context: Context) => Promise<Result>;

/**
 * Composable event handler with options to respond now or extend context with
 * `Out`.
 *
 * A `null` return value signals a no-op (no immediate response and no change to
 * context).  This is preferred over allowing `undefined` to avoid
 * unintentionally skipping return value in implementation.
 */
export type Preprocess<
  Out = {},
  In = object,
  Request = APIGatewayProxyEvent,
  Response = APIGatewayProxyResult
> = (
  event: Request,
  context: In
) => MaybeAsync<{ context: Out } | { response: Response } | null>;

// Note that this receives the arguments in the opposite order to the
// preprocesses.  This helps with inference and aligns with the fact that most
// handlers should not need the event (because everything will have been put
// into context), while most preprocessors will work from the event (because
// they are (typically) agnostic of other preprocessors).
export type BaseHandler<T = object, Res = APIGatewayProxyResult> = (
  context: T,
  event: APIGatewayProxyEvent
) => MaybeAsync<Res>;

/**
 * Custom compose signature for handler preprocessors.
 *
 * This signature asserts that the aggregation of the contexts provided by all
 * of the preprocessors will be provided to the handler (when it is reached).
 *
 * Note that as currently written, this signature requires the handler to
 * require all of those contexts.  In principle, the handler *may* accept a
 * partial map of that aggregation.  However, I don't know how to allow for that
 * without undermining the guarantee (i.e. by saying that it accepts a
 * `Partial`).
 *
 * Each preprocessor may include its own output context as input, even when no
 * prior processor provides it.  For example, a context item can be set
 * conditionally based on the incoming context:
 *
 *     const with_foo: Preprocess<{ foo: Foo }, { foo?: Foo }> = (_, context) => ({
 *       context: { foo: context.foo ?? new Foo() },
 *     });
 *
 * Since contexts can be passed directly to the composed handler, this can be
 * used to allow such arguments to preempt the preprocessor.
 */
interface ComposeHandlers<Req, Res> {
  <A, B, C, D, E, F, G, H, I, J>(
    preprocessors:
      | []
      | [Preprocess<A, Partial<A>, Req, Res>]
      | [
          Preprocess<A, Partial<A>, Req, Res>,
          Preprocess<B, Partial<A & B>, Req, Res>
        ]
      | [
          Preprocess<A, Partial<A>, Req, Res>,
          Preprocess<B, Partial<A & B>, Req, Res>,
          Preprocess<C, Partial<A & B & C>, Req, Res>
        ]
      | [
          Preprocess<A, Partial<A>, Req, Res>,
          Preprocess<B, Partial<A & B>, Req, Res>,
          Preprocess<C, Partial<A & B & C>, Req, Res>,
          Preprocess<D, Partial<A & B & C & D>, Req, Res>
        ]
      | [
          Preprocess<A, Partial<A>, Req, Res>,
          Preprocess<B, Partial<A & B>, Req, Res>,
          Preprocess<C, Partial<A & B & C>, Req, Res>,
          Preprocess<D, Partial<A & B & C & D>, Req, Res>,
          Preprocess<E, Partial<A & B & C & D & E>, Req, Res>
        ]
      | [
          Preprocess<A, Partial<A>, Req, Res>,
          Preprocess<B, Partial<A & B>, Req, Res>,
          Preprocess<C, Partial<A & B & C>, Req, Res>,
          Preprocess<D, Partial<A & B & C & D>, Req, Res>,
          Preprocess<E, Partial<A & B & C & D & E>, Req, Res>,
          Preprocess<F, Partial<A & B & C & D & E & F>, Req, Res>
        ]
      | [
          Preprocess<A, Partial<A>, Req, Res>,
          Preprocess<B, Partial<A & B>, Req, Res>,
          Preprocess<C, Partial<A & B & C>, Req, Res>,
          Preprocess<D, Partial<A & B & C & D>, Req, Res>,
          Preprocess<E, Partial<A & B & C & D & E>, Req, Res>,
          Preprocess<F, Partial<A & B & C & D & E & F>, Req, Res>,
          Preprocess<G, Partial<A & B & C & D & E & F & G>, Req, Res>
        ]
      | [
          Preprocess<A, Partial<A>, Req, Res>,
          Preprocess<B, Partial<A & B>, Req, Res>,
          Preprocess<C, Partial<A & B & C>, Req, Res>,
          Preprocess<D, Partial<A & B & C & D>, Req, Res>,
          Preprocess<E, Partial<A & B & C & D & E>, Req, Res>,
          Preprocess<F, Partial<A & B & C & D & E & F>, Req, Res>,
          Preprocess<G, Partial<A & B & C & D & E & F & G>, Req, Res>,
          Preprocess<H, Partial<A & B & C & D & E & F & G & H>, Req, Res>
        ]
      | [
          Preprocess<A, Partial<A>, Req, Res>,
          Preprocess<B, Partial<A & B>, Req, Res>,
          Preprocess<C, Partial<A & B & C>, Req, Res>,
          Preprocess<D, Partial<A & B & C & D>, Req, Res>,
          Preprocess<E, Partial<A & B & C & D & E>, Req, Res>,
          Preprocess<F, Partial<A & B & C & D & E & F>, Req, Res>,
          Preprocess<G, Partial<A & B & C & D & E & F & G>, Req, Res>,
          Preprocess<H, Partial<A & B & C & D & E & F & G & H>, Req, Res>,
          Preprocess<I, Partial<A & B & C & D & E & F & G & H & I>, Req, Res>
        ]
      | [
          Preprocess<A, Partial<A>, Req, Res>,
          Preprocess<B, Partial<A & B>, Req, Res>,
          Preprocess<C, Partial<A & B & C>, Req, Res>,
          Preprocess<D, Partial<A & B & C & D>, Req, Res>,
          Preprocess<E, Partial<A & B & C & D & E>, Req, Res>,
          Preprocess<F, Partial<A & B & C & D & E & F>, Req, Res>,
          Preprocess<G, Partial<A & B & C & D & E & F & G>, Req, Res>,
          Preprocess<H, Partial<A & B & C & D & E & F & G & H>, Req, Res>,
          Preprocess<I, Partial<A & B & C & D & E & F & G & H & I>, Req, Res>,
          Preprocess<
            J,
            Partial<A & B & C & D & E & F & G & H & I & J>,
            Req,
            Res
          >
        ],
    handler: BaseHandler<A & B & C & D & E & F & G & H & I & J>
  ): SimpleHandler<Req, Res>;
}

/**
 * Create a composite handler from the given preprocessors and base handler.
 * Preprocessors may terminate the pipeline early by returning a response or
 * extend the context.  Each preprocessor receives the context provided by the
 * previous ones.  The final handler can assume all of the contexts provided by
 * all of the preprocessors.
 */
export const with_preprocessors: ComposeHandlers<
  APIGatewayProxyEvent,
  APIGatewayProxyResult
> =
  (preprocessors, handler: BaseHandler<any>) =>
  async (event, original_context) => {
    event.headers = normalize_headers(event.headers); // which see

    let context = original_context;

    // TS: minimal cast for harmony with overloads
    for (const preprocessor of preprocessors as Preprocess[]) {
      const result = await preprocessor(event, context);

      // If the preprocessor indicated a no-op, advance to next one
      if (result === null) continue;

      // If the preprocessor provided a response, return it now
      if ("response" in result) return result.response;

      // If the preprocessor contributes to the context, mix it in
      if ("context" in result) context = { ...context, ...result.context };
    }

    return handler(context, event);
  };
