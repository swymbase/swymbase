# Typescript source

## Project configuration

Since you can't put comments in JSON (strictly speaking: TS actually ignores
comments in `tsconfig` files), here are any notes about the shared `tsconfig`
files in this directory.

## Preserve symlinks

The [base tsconfig.json file](./tsconfig-base.json) contains the compiler
setting

```json
  "preserveSymlinks" : true
```

Without it, you can get errors like the following during CI builds:

```
packages/some-package/src/some-module.ts(42,14): error TS2742: The inferred type of 'XYZ' cannot be named without a reference to '../../../../../../../../usr/local/src/build-deps/node_modules/foo/bar. This is likely not portable. A type annotation is necessary.
```

These can arise in the CI environment even when the local build succeeds,
because the `node_modules` folder is symlinked from the project directory to a
location in the runner's container (see
[build-packages.sh](../ci/build-packages.sh)). This can cause types to appear
incompatible when referenced from multiple places. The
[compiler setting](https://www.typescriptlang.org/tsconfig#preserveSymlinks)
causes them to be treated as relative to the original location (not the resolved
one).
