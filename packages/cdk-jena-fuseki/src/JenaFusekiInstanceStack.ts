import * as aws_autoscaling from "@aws-cdk/aws-autoscaling";
import * as aws_backup from "@aws-cdk/aws-backup";
import * as aws_ec2 from "@aws-cdk/aws-ec2";
import * as aws_efs from "@aws-cdk/aws-efs";
import * as aws_elasticloadbalancingv2 from "@aws-cdk/aws-elasticloadbalancingv2";
import * as aws_iam from "@aws-cdk/aws-iam";
import * as aws_s3 from "@aws-cdk/aws-s3";
import * as aws_logs from "@aws-cdk/aws-logs";
import * as aws_s3_deployment from "@aws-cdk/aws-s3-deployment";
import * as aws_secretsmanager from "@aws-cdk/aws-secretsmanager";
import * as core from "@aws-cdk/core";
import * as path from "path";

// The declarative part of the stack inputs.
export interface JenaFusekiInstanceStackConfig {
  // The outward facing port for the database?
  readonly databasePort: number;
  readonly instanceSize: string;

  /**
   * Lookup pattern for the machine image to use for the server.  May be an
   * exact name or may include wildcards.
   *
   * A machine image with specific requirements is expected.  A compatible image
   * can be created using the `JenaFusekiConstruct` class.
   */
  readonly machineNamePattern: string;

  /**
   * Gigabytes of RAM to allocate to the Jena Fuseki service.
   */
  readonly jenaMemory: number;
}

export interface JenaFusekiInstanceStackProps
  extends core.StackProps,
    JenaFusekiInstanceStackConfig {
  // VPC in which the instance (and EFS storage) will live.
  readonly vpc: aws_ec2.IVpc;
}

export class JenaFusekiInstanceStack extends core.Stack {
  // Underspecifying, as the only usage is for ALB.
  readonly autoScalingGroup: aws_elasticloadbalancingv2.IApplicationLoadBalancerTarget;

  constructor(
    scope: core.Construct,
    id: string,
    props: JenaFusekiInstanceStackProps
  ) {
    super(scope, id, props);

    const { region } = core.Stack.of(this);

    const {
      vpc,
      instanceSize,
      machineNamePattern,
      databasePort,
      jenaMemory,
    } = props;

    // Stage is used to help identify the ASG during startup check.
    const stage = this.node.tryGetContext("stage");

    // Create Jena Admin Secret
    //
    // In principle, the user of this construct could want to provide the
    // secret.
    const admin_secret = new aws_secretsmanager.Secret(
      this,
      "jena-admin-secret",
      {
        secretName: `${this.node.path}-fuseki-admin`,
        description: `Admin credentials for Fuseki admin (${this.node.path})`,
        generateSecretString: {
          excludePunctuation: true,
          generateStringKey: "password",
          includeSpace: false,
          secretStringTemplate: '{"username":"admin"}',
        },
      }
    );

    // === File system

    const file_system = new aws_efs.FileSystem(this, "filesystem", {
      vpc,
      encrypted: true,
      performanceMode: aws_efs.PerformanceMode.GENERAL_PURPOSE,
      throughputMode: aws_efs.ThroughputMode.BURSTING,
    });

    // === Backups

    // Construct a safe prefix according to the rules for backup vault name.
    const namespace = this.node.path.replace(/[^-_A-Za-z0-9 ]/g, "--");

    const vault = new aws_backup.BackupVault(this, "filesystem-vault", {
      backupVaultName: `${namespace}-vault`,
    });

    const plan = aws_backup.BackupPlan.dailyWeeklyMonthly5YearRetention(
      this,
      "filesystem-backupplan",
      vault
    );

    plan.addSelection(`${this.node.path}-filesystem-selection`, {
      resources: [aws_backup.BackupResource.fromEfsFileSystem(file_system)],
    });

    plan.addRule(aws_backup.BackupPlanRule.daily());

    const file_system_hostname = `${file_system.fileSystemId}.efs.${region}.amazonaws.com`;

    // === Auto Scaling Group

    const instance_role = new aws_iam.Role(this, "instance-role", {
      assumedBy: new aws_iam.ServicePrincipal("ec2.amazonaws.com"),
      managedPolicies: [
        aws_iam.ManagedPolicy.fromManagedPolicyArn(
          this,
          `${this.node.path}-policy-ssm`,
          "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
        ),
        aws_iam.ManagedPolicy.fromManagedPolicyArn(
          this,
          `${this.node.path}-policy-cw`,
          "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
        ),
      ],
      inlinePolicies: {
        logs: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams",
              ],
              resources: ["*"],
            }),
          ],
        }),
        ssm: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: [
                "ssmmessages:*",
                "ssm:DescribeAssociation",
                "ssm:GetDeployablePatchSnapshotForInstance",
                "ssm:GetDocument",
                "ssm:DescribeDocument",
                "ssm:GetManifest",
                "ssm:GetParameter",
                "ssm:GetParameters",
                "ssm:ListAssociations",
                "ssm:ListInstanceAssociations",
                "ssm:PutInventory",
                "ssm:PutComplianceItems",
                "ssm:PutConfigurePackageResult",
                "ssm:UpdateAssociationStatus",
                "ssm:UpdateInstanceAssociationStatus",
                "ssm:UpdateInstanceInformation",
                "ec2messages:*",
              ],
              resources: ["*"],
            }),
          ],
        }),
        s3: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: ["s3:*"],
              resources: ["*"],
            }),
          ],
        }),
        ec2: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: [
                "ec2:DescribeSubnets",
                "ec2:CreateNetworkInterface",
                "ec2:DescribeNetworkInterfaces",
                "autoscaling:DescribeAutoScalingInstances",
              ],
              resources: ["*"],
            }),
          ],
        }),
        efs: new aws_iam.PolicyDocument({
          statements: [
            new aws_iam.PolicyStatement({
              effect: aws_iam.Effect.ALLOW,
              actions: [
                "elasticfilesystem:CreateFileSystem",
                "elasticfilesystem:CreateMountTarget",
              ],
              resources: ["*"],
            }),
          ],
        }),
      },
    });

    // === Config bucket

    const config_bucket = new aws_s3.Bucket(this, "config-bucket", {
      encryption: aws_s3.BucketEncryption.KMS_MANAGED,
      blockPublicAccess: aws_s3.BlockPublicAccess.BLOCK_ALL,
    });
    const config_bucket_name = config_bucket.bucketName;

    // The running code will be in a single-file bundle at `dist` relative to
    // the package root (not this file).
    const config_dir = path.join(__dirname, "../assets/bucket-deploy-config");
    new aws_s3_deployment.BucketDeployment(this, "config-bucket-deploy", {
      sources: [aws_s3_deployment.Source.asset(config_dir)],
      destinationBucket: config_bucket,
    });

    // EC2 instance for Jena

    const jena_image = aws_ec2.MachineImage.lookup({
      name: machineNamePattern,
      owners: ["self"],
    });

    // HTTP/HTTPS = 80, 443
    // SMTP = 25, 465, 587
    // DNS = 53
    // DHCP = 67, 68
    // NTP = 123
    // NFS = 2049
    // Jena = 3030
    const vpc_cidr_block = vpc.vpcCidrBlock;
    const inbound_tcp_ports = [80, 22, databasePort];
    const inbound_udp_ports = [68];
    const inbound_tcp_ports_joined = inbound_tcp_ports.join(",");
    const inbound_udp_ports_joined = inbound_udp_ports.join(",");
    const outbound_tcp_ports = [80, 443, 53, 25, 465, 587, 2049, databasePort];
    const outbound_udp_ports = [53, 67, 123];
    const outbound_tcp_ports_joined = outbound_tcp_ports.join(",");
    const outbound_udp_ports_joined = outbound_udp_ports.join(",");

    // === Jena image startup

    const user_data = aws_ec2.UserData.forLinux();
    user_data.addCommands(
      "set -x",
      "iptables -A INPUT -i lo -j ACCEPT",
      "iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT",
      `iptables -A INPUT -p tcp -s ${vpc_cidr_block} --match multiport --dports 0:65535 -j ACCEPT`,
      `iptables -A INPUT -p tcp --match multiport --dports ${inbound_tcp_ports_joined} -j ACCEPT`,
      `iptables -A INPUT -p udp --match multiport --dports ${inbound_udp_ports_joined} -j ACCEPT`,
      "iptables -A INPUT -j REJECT",
      "iptables -A OUTPUT -o lo -p all -j ACCEPT",
      "iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT",
      `iptables -A OUTPUT -p tcp -d ${vpc_cidr_block} -j ACCEPT`,
      `iptables -A OUTPUT -p tcp --match multiport --dports ${outbound_tcp_ports_joined} -j ACCEPT`,
      `iptables -A OUTPUT -p udp --match multiport --dports ${outbound_udp_ports_joined} -j ACCEPT`,
      "iptables -A OUTPUT -p icmp -j ACCEPT",
      "iptables -A OUTPUT -j REJECT",
      "iptables-save | sudo tee /etc/sysconfig/iptables",
      "myInstanceId=$(curl http://instance-data/latest/meta-data/instance-id)",
      "myAvailabilityZone=$(curl http://instance-data/latest/meta-data/placement/availability-zone)",
      "myRegion=${myAvailabilityZone%?}",
      "myNetworkMac=$(curl http://instance-data/latest/meta-data/network/interfaces/macs/)",
      "myVpcId=$(curl http://instance-data/latest/meta-data/network/interfaces/macs/${myNetworkMac}/vpc-id)",
      "myHostNameLocal=$(curl \"http://instance-data/latest/meta-data/network/interfaces/macs/${myNetworkMac}local-hostname\" | head -n 1 | cut -d ' ' -f2)",
      'myHostIpLocal=$(curl "http://instance-data/latest/meta-data/network/interfaces/macs/${myNetworkMac}local-ipv4s")',
      'echo "AWS_AVAILABILITY_ZONE=$myAvailabilityZone" >> /etc/environment',
      'echo "AWS_REGION=$myRegion" >> /etc/environment',
      'echo "AWS_VPC_ID=$myVpcId" >> /etc/environment',
      'echo "AWS_INSTANCE_ID=$myInstanceId" >> /etc/environment',
      'echo "AWS_HOSTNAME_PRIVATE=$myHostNameLocal" >> /etc/environment',
      'echo "AWS_IPV4_PRIVATE=$myHostIpLocal" >> /etc/environment',
      `sed -i \"/Service/ a Environment=JVM_ARGS='-Xmx${jenaMemory}G'\" /etc/systemd/system/jena-fuseki.service`,
      "systemctl daemon-reload",
      "systemctl start jena-fuseki.service",
      "while [[ $(curl -I http://localhost:3030 2>/dev/null | head -n 1 | cut -d$' ' -f2) != '200' ]]; do sleep 1s; done",
      'systemctl stop jena-fuseki.service',
      `mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${file_system_hostname}:/ /opt/jena/run`,
      `aws s3 cp s3://${config_bucket_name}/jena/shiro.ini /opt/jena/run/shiro.ini`,
      `aws s3 sync s3://${config_bucket_name}/etc/td-agent /etc/td-agent`,
      `aws s3 sync s3://${config_bucket_name}/etc/rsyslog.d /etc/rsyslog.d`,
      `ADMIN_PASSWORD=$(aws --region ${region} secretsmanager get-secret-value --secret-id ${admin_secret.secretArn} --query "SecretString" --output text | jq -r ".password")`,
      'sed -i "s/^admin=.*/admin=$ADMIN_PASSWORD/" /opt/jena/run/shiro.ini',
      `while [[ $(aws autoscaling describe-auto-scaling-instances --region ${region} --query 'AutoScalingInstances[?contains(AutoScalingGroupName,\`jena\`) && contains(AutoScalingGroupName, \`${stage}\`) && InstanceId !=\`'$myInstanceId'\`].InstanceId' --output text) ]]; do echo "Waiting on ASG" && sleep 30s; done`,
      // ASSUMES a database name of `kb`.  It is still a manual step to set up this db.
      // This kills a leftover lock file from any previous abnormal termination.
      `find /opt/jena/run/databases/kb/ -name tdb.lock -delete`,
      'systemctl start jena-fuseki.service',
      `echo "{\"agent\": {\"metrics_collection_interval\": 60,"run_as_user": "root"},\"logs\": {\"logs_collected\": {\"files\": {\"collect_list\": [{\"file_path\": \"/var/log/syslog\",\"log_group_name\": \"${this.node.path}-logs\",\"log_stream_name\": \"{hostname}/syslog\",\"timestamp_format\" :\"%b %d %H:%M:%S\"}]}}}}">/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json`,
      'systemctl enable amazon-cloudwatch-agent.service',
      'service amazon-cloudwatch-agent start',
    );

    const autoScalingGroup = new aws_autoscaling.AutoScalingGroup(
      this,
      "asg",
      {
        instanceType: new aws_ec2.InstanceType(instanceSize),
        vpc,
        vpcSubnets: { subnets: vpc.privateSubnets },
        machineImage: jena_image,
        minCapacity: 1,
        maxCapacity: 1,
        healthCheck: aws_autoscaling.HealthCheck.ec2(),
        role: instance_role,
        userData: user_data,
      }
    );

    file_system.connections.allowDefaultPortFrom(autoScalingGroup);
    admin_secret.grantRead(autoScalingGroup);

    const sec_group = new aws_ec2.SecurityGroup(this, `security-group`, {
      vpc,
      description: `ASG for VPC (${this.node.path})`,
      allowAllOutbound: true,
    });

    sec_group.addIngressRule(
      aws_ec2.Peer.ipv4(vpc.vpcCidrBlock),
      aws_ec2.Port.allTcp(),
      "vpc-in"
    );

    autoScalingGroup.addSecurityGroup(sec_group);

    new aws_logs.LogGroup(this, `log-group`, {
        logGroupName: `${this.node.path}-logs`,
      }
    );

    this.autoScalingGroup = autoScalingGroup;
  }
}
