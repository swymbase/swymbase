import * as core from "@aws-cdk/core";
import { ImageBuilderStack } from "@swymbase/cdk-image-builder";
import { JenaFusekiImageComponent } from "./JenaFusekiImageComponent";

export interface JenaFusekiImageBuilderStackProps extends core.StackProps {
  readonly archetype?: string;

  readonly instanceSizeList: string[];
}

export class JenaFusekiImageBuilderStack extends core.Stack {
  constructor(
    scope: core.Construct,
    id: string,
    props: JenaFusekiImageBuilderStackProps
  ) {
    super(scope, id, props);

    const { instanceSizeList } = props;

    const jena_fuseki = new JenaFusekiImageComponent(this, "jena-fuseki");

    const archetype = props.archetype ?? "jena";

    new ImageBuilderStack(this, "image-builder", {
      archetype,
      instanceSizeList,
      appComponents: [jena_fuseki.document],
    });
  }
}
