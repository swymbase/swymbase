import * as aws_imagebuilder from "@aws-cdk/aws-imagebuilder";
import * as core from "@aws-cdk/core";

/**
 * A component for building a machine image for running a Jena Fuseki service.
 */
export class JenaFusekiImageComponent extends core.Construct {
  readonly document: aws_imagebuilder.CfnComponent;

  constructor(scope: core.Construct, id: string) {
    super(scope, id);

    // Construct a safe prefix according to the rules for component name.
    const namespace = this.node.path.replace(/[^-_A-Za-z0-9 ]/g, "--");

    const document = new aws_imagebuilder.CfnComponent(
      this,
      "InstallJenaFusekiDocument",
      {
        name: `${namespace}-InstallJenaFuseki`,
        description: "Installs for Jena Fuseki",
        version: "1.0.7",
        platform: "Linux",
        data: `
        name: InstallJenaFusekiDocument
        description: Installs for Jena Fuseki
        schemaVersion: 1.0

        phases:
          - name: build
            steps:
              - name: InstallPackages
                action: ExecuteBash
                inputs:
                  commands:
                    - apt update
                    - apt install -y --no-install-recommends aptitude
                    - LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
                    - aptitude update
                    - DEBIAN_FRONTEND=noninteractive aptitude -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" safe-upgrade
                    - DEBIAN_FRONTEND=noninteractive aptitude install -y --without-recommends default-jre iptables-persistent ca-certificates nfs-common git-core ruby build-essential python-pip python-setuptools python-wheel mysql-client-5.7 redis-tools jq
                    - pip install awscli
                    - chmod +x /usr/local/bin/aws /usr/local/bin/aws_completer
                    - curl --location --output /tmp/fuseki.tar.gz http://archive.apache.org/dist/jena/binaries/apache-jena-fuseki-3.17.0.tar.gz
                    - tar zxf /tmp/fuseki.tar.gz -C /tmp
                    - mkdir -p /opt/jena
                    - cp -a /tmp/apache-jena-fuseki-3.17.0/. /opt/jena/
                    - rm /opt/jena/fuseki.war
                    - chmod 755 /opt/jena/fuseki-server
              - name: ConfigureJenaService
                action: ExecuteBash
                inputs:
                  commands:
                    - touch /etc/systemd/system/jena-fuseki.service
                    - echo "[Unit]" > /etc/systemd/system/jena-fuseki.service
                    - echo "Description=Jena Fuseki" >> /etc/systemd/system/jena-fuseki.service
                    - echo "After=network.target" >> /etc/systemd/system/jena-fuseki.service
                    - echo "[Service]" >> /etc/systemd/system/jena-fuseki.service
                    - echo "Environment=FUSEKI_HOME=/opt/jena" >> /etc/systemd/system/jena-fuseki.service
                    - echo "Environment=FUSEKI_BASE=/opt/jena/run" >> /etc/systemd/system/jena-fuseki.service
                    - echo "Type=simple" >> /etc/systemd/system/jena-fuseki.service
                    - echo "Restart=on-failure" >> /etc/systemd/system/jena-fuseki.service
                    - echo "RestartSec=5s" >> /etc/systemd/system/jena-fuseki.service
                    - echo "ExecStart=/opt/jena/fuseki-server" >> /etc/systemd/system/jena-fuseki.service
                    - echo "[Install]" >> /etc/systemd/system/jena-fuseki.service
                    - echo "WantedBy=multi-user.target" >> /etc/systemd/system/jena-fuseki.service
                    - systemctl daemon-reload
`,
      }
    );

    this.document = document;
  }
}
