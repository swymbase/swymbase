# CDK — Jena Fuseki

A construct for creating a server running
[Apache Jena Fuseki](https://jena.apache.org/documentation/fuseki2/index.html).

## Roadmap

The cloud team has updated a Jena instance to include logging to CloudWatch from
a Jena instance. This package should be updated to incorporate that work.

Fix Jena User Data Script
