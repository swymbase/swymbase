import type { EntityFrom, MetaInterfaces } from "@swymbase/metatype-core";
import type * as JSONAPI from "jsonapi-typescript";
import { defined, find_type } from "../helpers";
import { record_to_resource_object } from "./record-to-resource-object";

/**
 * Map the given records to an array of JSON API Resource Objects using the
 * given metadata.  Records are matched to schema using their `type` property.
 */
export const records_to_resource_objects = (
  schema: MetaInterfaces,
  records: readonly EntityFrom<any>[]
): JSONAPI.ResourceObject[] => {
  return records
    .map((item) => {
      if (item?.type) {
        const spec = find_type(schema, item.type);
        if (spec) return record_to_resource_object(spec, item);
      }
    })
    .filter(defined);
};
