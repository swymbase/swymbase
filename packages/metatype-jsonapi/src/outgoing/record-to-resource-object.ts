import type { EntityFrom, MetaInterface } from "@swymbase/metatype-core";
import type * as JSONAPI from "jsonapi-typescript";
import { resource_linkage } from "./resource-linkage";

/**
 * Map the given record to a JSON API Resource Object using the given metadata.
 */
export const record_to_resource_object = (
  type_spec: MetaInterface,
  record: EntityFrom<any>
): JSONAPI.ResourceObject => {
  let attributes: {} | undefined = undefined;
  let relationships: {} | undefined = undefined;

  for (const [key, { relationship }] of Object.entries(type_spec.properties)) {
    if (relationship) {
      const data = resource_linkage(type_spec, key, record);
      // See note to `resource-linkage`
      if (data !== undefined) {
        if (!relationships) relationships = {};
        relationships[key] = { data };
      }
    } else if (record.hasOwnProperty(key)) {
      if (!attributes) attributes = {};
      attributes[key] = record[key];
    }
  }

  return {
    id: record.id,
    type: record.type,
    ...(attributes ? { attributes } : {}),
    ...(relationships ? { relationships } : {}),
  };
};
