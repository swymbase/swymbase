import type { EntityFrom, MetaInterfaces } from "@swymbase/metatype-core";
import type * as JSONAPI from "jsonapi-typescript";
import { find_type } from "../helpers";
import { record_to_resource_object } from "./record-to-resource-object";
import { records_to_resource_objects } from "./records-to-resource-objects";

/**
 * Map the given record to a JSON API document using the given metadata.
 *
 * Returns `undefined` if the given record does not indicate a `type` that is
 * defined in the schema.
 *
 * @param include Additional records to include, which will be mapped to JSON
 * API Resource Objects using the given schema using their `type` value.
 */
export const record_to_document = (
  schema: MetaInterfaces,
  record: EntityFrom<any>,
  include: readonly EntityFrom<any>[] = []
): JSONAPI.SingleResourceDoc | undefined => {
  if (record?.type) {
    const main_spec = find_type(schema, record.type);
    if (main_spec) {
      return {
        data: record_to_resource_object(main_spec, record),
        included: records_to_resource_objects(schema, include),
      };
    }
  }
};
