import type * as JSONAPI from "jsonapi-typescript";

/**
 * Return just the JSON API Resource Identifier Object from a given object, or
 * `undefined` for non-conforming values.
 *
 * See https://jsonapi.org/format/#document-resource-identifier-objects
 */
export const resource_identifier = (
  value: any
): JSONAPI.ResourceIdentifierObject | undefined => {
  if (value) {
    const { id, type } = value;
    if (typeof id === "string")
      // @ts-expect-error: `type` is optional in metatype
      return { id, ...(type ? { type } : {}) };
  }
};
