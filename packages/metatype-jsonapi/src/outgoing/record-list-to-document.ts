import type { EntityFrom, MetaInterfaces } from "@swymbase/metatype-core";
import type * as JSONAPI from "jsonapi-typescript";
import { records_to_resource_objects } from "./records-to-resource-objects";

/**
 * Map the given records to a JSON API resource collection document using the
 * given metadata.
 */
export const record_list_to_document = (
  schema: MetaInterfaces,
  records: readonly EntityFrom<any>[],
  include: readonly EntityFrom<any>[] = []
): JSONAPI.CollectionResourceDoc => {
  const data = records_to_resource_objects(schema, records);
  const included = records_to_resource_objects(schema, include);

  return { data, ...(included.length > 0 ? { included } : {}) };
};
