import type { EntityFrom, MetaInterface } from "@swymbase/metatype-core";
import type * as JSONAPI from "jsonapi-typescript";
import { defined } from "../helpers";
import { resource_identifier } from "./resource-identifier";

/**
 * Return a JSON API Resource Linkage for the given property against the given
 * record.  Returns `undefined` if any expectations fail.
 *
 * Assumes that the property is an object-valued property.
 *
 * Assumes that values for multivalued properties are arrays (if present).
 *
 * See https://jsonapi.org/format/#document-resource-object-linkage
 */
export const resource_linkage = (
  type_spec: MetaInterface,
  key: string,
  record: EntityFrom<any> | undefined
): JSONAPI.ResourceLinkage | undefined => {
  const property_spec = type_spec.properties[key];

  if (!record || !property_spec) return undefined;

  const property_value = record[key];

  /** “`null` for empty to-one relationships” */
  if (property_value === null) return null;

  // According to the spec, an “empty” to-one relationship must be represented
  // with `null`.  We currently treat an explicit `null` in the record as
  // signaling an empty relationship.  Other values return `undefined`.
  // `record-to-resource-object.ts` uses this distinction to omit undefined
  // values while allowing explicit “empty” signals to be forwarded through
  // records (even though they don't represent facts as such).  This is useful
  // for, e.g. PATCH operations, which need to negate an existing fact.
  //
  // A strict interpretation of the spec would appear to require that we emit
  // `null` whenever there is no value for a defined relationship property.  We
  // don't do this now.  Doing so should not be a breaking change (as clients
  // must already tolerate “missing” null values).  And note that when we add
  // support for sparse fieldsets, it may become necessary to emit said nulls;
  // otherwise readers could not tell the difference between a relationship that
  // has no value and a relationship that wasn't included in the projection.
  //
  // Finally, note that this condition applies to both single- and multi-valued
  // relationships at present.
  if (!property_value || typeof property_value !== "object") return undefined;

  /** “an empty array (`[]`) for empty to-many relationships.” */
  /** “an array of resource identifier objects for non-empty to-many relationships” */
  if (property_spec.multivalued) {
    return (Array.isArray(property_value) ? property_value : [])
      .map(resource_identifier)
      .filter(defined);
  }

  // Reject array values for non-multivalued properties
  // THIS will happen implicitly via below anyway
  if (Array.isArray(record[key])) return undefined;

  /** “a single resource identifier object for non-empty to-one relationships” */
  return resource_identifier(property_value);
};
