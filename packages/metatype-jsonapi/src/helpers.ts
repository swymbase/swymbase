import type { MetaInterfaces } from "@swymbase/metatype-core";

export const defined = <T>(value: T | null | undefined): value is T =>
  value != null;

export const find_type = (schema: MetaInterfaces, type: string) =>
  Object.values(schema).find((spec) => spec["@type"] === type);

export const is_plain_object = (x: any): x is object =>
  x && typeof x === "object" && !Array.isArray(x);
