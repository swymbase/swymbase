import { is_plain_object } from "../helpers";

/**
 * Ensure that a given JSON:API document has a `type` value on the single
 * resource object it represents.  If the given object doesn't appear to be a
 * single resource document, or already has a `type`, it is returned as-is.
 *
 * See notes in `README` For situations in which `type` is not required on
 * incoming objects, it may be necessary to add a type to a document that is not
 * strictly valid.
 */
export const ensure_type_single = <T>(doc: T, type: string): T => {
  const data = doc?.["data"];
  return is_plain_object(data) && !("type" in data)
    ? { ...doc, data: { ...data, type } }
    : doc;
};
