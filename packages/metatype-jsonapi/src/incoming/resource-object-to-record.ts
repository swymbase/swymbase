import type { InterfaceFrom, MetaInterface } from "@swymbase/metatype-core";
import type * as JSONAPI from "jsonapi-typescript";

const is_relationships_with_data = (
  x: JSONAPI.RelationshipObject
): x is JSONAPI.RelationshipsWithData => "data" in x;

/**
 * Map the given JSON API Resource Object to a record using the given metadata.
 *
 * Only properties recognized in the schema are mapped to the record.  Moreover,
 * properties designated as relationships must come from the `relationships`
 * map, and others must come from `attributes`.
 */
export const resource_object_to_record = (
  type_spec: MetaInterface,
  resource_object: JSONAPI.ResourceObject
): InterfaceFrom<any> => {
  const { id, type, attributes, relationships } = resource_object;

  const properties = {};

  if (attributes) {
    for (const [key, value] of Object.entries(attributes)) {
      const property_spec = type_spec.properties[key];
      if (property_spec && !property_spec.relationship) {
        properties[key] = value;
      }
    }
  }

  if (relationships) {
    for (const [key, relationship] of Object.entries(relationships)) {
      if (is_relationships_with_data(relationship)) {
        const property_spec = type_spec.properties[key];
        if (property_spec && property_spec.relationship) {
          properties[key] = relationship.data;
        }
      }
    }
  }

  // TODO: See note to `TypeFrom` definition.  This is not assignable when
  // `$error` type is in union.
  return { ...(id ? { id } : {}), ...(type ? { type } : {}), ...properties };
};
