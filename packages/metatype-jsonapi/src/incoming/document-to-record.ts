import type { InterfaceFrom, MetaInterfaces } from "@swymbase/metatype-core";
import type * as JSONAPI from "jsonapi-typescript";
import { find_type } from "../helpers";
import { resource_object_to_record } from "./resource-object-to-record";

/**
 * Map the given JSON API document to a record using the given metadata.
 *
 * Returns `undefined` if the given document does not indicate a `type` that is
 * defined in the schema.
 *
 * It is assumed that incoming JSON API documents may originate at the client,
 * in which case “the `id` member is not required” according to the spec's
 * definition of [Resource Objects](https://jsonapi.org/format/#document-resource-objects).
 * The record type is thus `InterfaceFrom` instead of `EntityFrom`, in contrast
 * to `record_to_document` which assumes that records originate from the data
 * source.
 */
export const document_to_record = (
  schema: MetaInterfaces,
  document: JSONAPI.SingleResourceDoc
): InterfaceFrom<any> | undefined => {
  if (document?.data?.type) {
    const main_spec = find_type(schema, document.data.type);
    if (main_spec) {
      return resource_object_to_record(main_spec, document.data);
    }
  }
};
