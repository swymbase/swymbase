import type { InterfaceFrom, MetaInterfaces } from "@swymbase/metatype-core";
import type * as JSONAPI from "jsonapi-typescript";
import { find_type } from "../helpers";
import { resource_object_to_record } from "./resource-object-to-record";

/**
 * Map the given JSON API collection document to records using the given
 * metadata.  The items may be of heterogeneous types.
 *
 * Returns an empty array if the input document's `data` is not an array.
 *
 * Data items that do not indicate a `type` that is defined in the schema are
 * ignored.
 */
// TODO: this should be a generator, eh?
// TODO: and if so, what about the outgoing case?
export const collection_document_to_records = (
  schema: MetaInterfaces,
  document: JSONAPI.CollectionResourceDoc
): readonly InterfaceFrom<any>[] => {
  const records: InterfaceFrom<any>[] = [];
  for (const item of Array.isArray(document?.data) ? document.data : []) {
    const type_spec = find_type(schema, item.type);
    if (type_spec) {
      records.push(resource_object_to_record(type_spec!, item));
    }
  }
  return records;
};
