import type { EntitiesFrom } from "@swymbase/metatype-core";
import { metatypes } from "@swymbase/metatype-core";
import {
  record_to_resource_object,
  resource_object_to_record,
} from "@swymbase/metatype-jsonapi";
import test from "ava";
import type * as JSONAPI from "jsonapi-typescript";

const TEST_TYPES = metatypes({
  Person: {
    "@type": "http://example.com/Person",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
    },
  },
  Book: {
    "@type": "http://example.com/Book",
    properties: {
      title: {
        term: "http://example.com/title",
        type: "string",
      },
      publication_year: {
        term: "http://example.com/publication_year",
        type: "number",
      },
      author: {
        term: "http://example.com/author",
        type: "object",
        relationship: true,
      },
    },
  },
});

type Types = EntitiesFrom<typeof TEST_TYPES>;

interface TestCase<K extends keyof Types> {
  readonly type: K;
  readonly record: Types[K];
  readonly resource_object: JSONAPI.ResourceObject;
  readonly direction?: "incoming" | "outgoing";
}

// See note to resource-to-document
const _test = <K extends keyof Types>(title: string, spec: TestCase<K>) => {
  if (spec.direction !== "incoming") {
    test(`outgoing: ${title}`, (t) => {
      const { type, record, resource_object } = spec;

      const result = record_to_resource_object(TEST_TYPES[type], record);
      t.log("RESULT", result);

      t.deepEqual(result, resource_object);
    });
  }

  if (spec.direction !== "outgoing") {
    test(`incoming: ${title}`, (t) => {
      const { type, record, resource_object } = spec;

      const result = resource_object_to_record(
        TEST_TYPES[type],
        resource_object
      );
      t.log("RESULT", result);

      t.deepEqual(result, record);
    });
  }
};

_test("includes the primitive property values as attributes", {
  type: "Person",
  record: {
    id: "http://example.com/Melville",
    type: TEST_TYPES.Person["@type"],
    name: "Herman Melville",
  },
  resource_object: {
    id: "http://example.com/Melville",
    type: TEST_TYPES.Person["@type"],
    attributes: {
      name: "Herman Melville",
    },
  },
});

_test("includes related resource values as relationships", {
  type: "Book",
  record: {
    id: "http://example.com/Omoo",
    type: TEST_TYPES.Book["@type"],
    author: { id: "http://example.com/Melville" },
  },
  resource_object: {
    id: "http://example.com/Omoo",
    type: TEST_TYPES.Book["@type"],
    relationships: {
      author: {
        // @ts-expect-error: `type` is optional
        //
        // This implementation is not strictly compliant, as it doesn't include
        // `type` on linked resources.  The error message is rather misleading
        // (presumably because of the union).  This checks if you add a `type:
        // string` to the `data` object.  See also note in `resource_identifier`.
        //
        // Type '{ id: string; }' is not assignable to type 'ResourceLinkage'.
        //   Type '{ id: string; }' is not assignable to type 'null'. [2322]
        data: { id: "http://example.com/Melville" },
      },
    },
  },
});

_test("includes all recognized properties of given record", {
  type: "Book",
  record: {
    id: "http://example.com/Bartleby",
    type: TEST_TYPES.Book["@type"],
    title: "Bartleby, the Scrivener",
    publication_year: 1853,
    author: { id: "http://example.com/Melville" },
  },
  resource_object: {
    id: "http://example.com/Bartleby",
    type: TEST_TYPES.Book["@type"],
    attributes: {
      title: "Bartleby, the Scrivener",
      publication_year: 1853,
    },
    relationships: {
      author: {
        // @ts-expect-error: see above
        data: { id: "http://example.com/Melville" },
      },
    },
  },
});

_test("represents a null to-one relationship with null data", {
  type: "Book",
  record: {
    id: "http://example.com/PrimaryColors",
    type: TEST_TYPES.Book["@type"],
    title: "Primary Colors",
    publication_year: 1996,
    // TS: record types don't currently allow `null` (but probably should for
    // relationship types)
    author: null as any,
  },
  resource_object: {
    id: "http://example.com/PrimaryColors",
    type: TEST_TYPES.Book["@type"],
    attributes: { title: "Primary Colors", publication_year: 1996 },
    relationships: { author: { data: null } },
  },
});

_test("uses empty array for to-many relationships with null value", {
  type: "Book",
  record: {
    id: "http://example.com/PrimaryColors",
    type: TEST_TYPES.Book["@type"],
    title: "Primary Colors",
    publication_year: 1996,
  },
  resource_object: {
    id: "http://example.com/PrimaryColors",
    type: TEST_TYPES.Book["@type"],
    attributes: { title: "Primary Colors", publication_year: 1996 },
  },
});

_test("excludes missing to-one relationships", {
  type: "Book",
  record: {
    id: "http://example.com/PrimaryColors",
    type: TEST_TYPES.Book["@type"],
    title: "Primary Colors",
    publication_year: 1996,
  },
  resource_object: {
    id: "http://example.com/PrimaryColors",
    type: TEST_TYPES.Book["@type"],
    attributes: { title: "Primary Colors", publication_year: 1996 },
  },
});

_test("discards unrecognized properties of given record", {
  direction: "outgoing",
  type: "Book",
  record: {
    id: "http://example.com/Typee",
    type: TEST_TYPES.Book["@type"],
    title: "Typee",
    publication_year: 1846,
    // @ts-expect-error: testing an undefined property
    page_count: 123,
    author: { id: "http://example.com/Melville" },
  },
  resource_object: {
    id: "http://example.com/Typee",
    type: TEST_TYPES.Book["@type"],
    attributes: {
      title: "Typee",
      publication_year: 1846,
    },
    relationships: {
      author: {
        // @ts-expect-error: see above
        data: { id: "http://example.com/Melville" },
      },
    },
  },
});
