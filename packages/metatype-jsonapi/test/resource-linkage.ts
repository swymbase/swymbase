import type { EntitiesFrom } from "@swymbase/metatype-core";
import { metatypes } from "@swymbase/metatype-core";
import { resource_linkage } from "@swymbase/metatype-jsonapi";
import test from "ava";
import type * as JSONAPI from "jsonapi-typescript";

const TEST_TYPES = metatypes({
  Player: {
    "@type": "http://example.com/Player",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
      plays_for: {
        term: "http://exampls.com/plays_for",
        type: "object",
        relationship: true,
      },
      played_for: {
        term: "http://exampls.com/played_for",
        type: "object",
        relationship: true,
        multivalued: true,
      },
    },
  },
  Team: {
    "@type": "http://example.com/Team",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
      rank: {
        term: "http://example.com/rank",
        type: "number",
      },
    },
  },
});

type Types = EntitiesFrom<typeof TEST_TYPES>;

interface TestCase<K extends keyof Types> {
  readonly type: K;
  readonly record: Types[K];
  readonly property: string;
  readonly resource_linkage: JSONAPI.ResourceLinkage | undefined;
}

// See note to resource-to-document
const _test = <K extends keyof Types>(title: string, spec: TestCase<K>) => {
  test(title, (t) => {
    const { type, record, property, resource_linkage: expect } = spec;

    const result = resource_linkage(TEST_TYPES[type], property, record);
    t.log("RESULT", result);

    t.deepEqual(result, expect);
  });
};

_test("returns an object for single-valued properties", {
  type: "Player",
  property: "plays_for",
  record: {
    id: "http://example.com/Flacco",
    name: "Joe Flacco",
    plays_for: { id: "http://example.com/Jets" },
    played_for: [
      { id: "http://example.com/Ravens" },
      { id: "http://example.com/Broncos" },
    ],
  },
  // @ts-expect-error: type is optional; See `record-to-resource-object` tests
  resource_linkage: { id: "http://example.com/Jets" },
});

_test("returns an array for multivalued relations", {
  type: "Player",
  property: "played_for",
  record: {
    id: "http://example.com/Flacco",
    name: "Joe Flacco",
    plays_for: { id: "http://example.com/Jets" },
    played_for: [
      { id: "http://example.com/Ravens" },
      { id: "http://example.com/Broncos" },
    ],
  },
  // Assumes order
  resource_linkage: [
    // @ts-expect-error: type is optional; See `record-to-resource-object` tests
    { id: "http://example.com/Ravens" },
    // @ts-expect-error: type is optional; See `record-to-resource-object` tests
    { id: "http://example.com/Broncos" },
  ],
});

_test("treats non-array values for multivalued properties as if missing", {
  type: "Player",
  property: "played_for",
  record: {
    id: "http://example.com/Flacco",
    name: "Joe Flacco",
    plays_for: { id: "http://example.com/Jets" },
    // @ts-expect-error: testing non-conformant value
    played_for: { id: "http://example.com/Ravens" },
  },
  resource_linkage: [],
});

_test("returns undefined for properties with missing (undefined) values", {
  type: "Player",
  property: "plays_for",
  record: {
    id: "http://example.com/Kaepernick",
    name: "Colin Kaepernick",
    played_for: [{ id: "http://example.com/49ers" }],
  },
  resource_linkage: undefined,
});

_test("returns null for properties with null value", {
  type: "Player",
  property: "plays_for",
  record: {
    id: "http://example.com/Kaepernick",
    name: "Colin Kaepernick",
    // TS: see note to similar cast in record-to-resource-object
    plays_for: null as any,
    played_for: [{ id: "http://example.com/49ers" }],
  },
  resource_linkage: null,
});

_test("includes only identifier from related values", {
  type: "Player",
  property: "plays_for",
  record: {
    id: "http://example.com/Brees",
    name: "Drew Brees",
    plays_for: {
      id: "http://example.com/Saints",
      // TODO: This shouldn't be expected by type, right?
      type: "http://example.com/Team",
      // @ts-expect-error: generated interfaces don't expect these properties
      name: "Saints",
    },
  },
  resource_linkage: {
    id: "http://example.com/Saints",
    type: "http://example.com/Team",
  },
});
