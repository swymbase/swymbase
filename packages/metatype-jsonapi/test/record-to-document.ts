import type { EntitiesFrom, EntityFrom } from "@swymbase/metatype-core";
import { metatypes } from "@swymbase/metatype-core";
import {
  document_to_record,
  record_to_document,
} from "@swymbase/metatype-jsonapi";
import test from "ava";
import type * as JSONAPI from "jsonapi-typescript";

const TEST_TYPES = metatypes({
  Person: {
    "@type": "http://example.com/Person",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
      knows: {
        term: "http://example.com/knows",
        type: "object",
        relationship: true,
      },
      born_in: {
        term: "http://example.com/born_in",
        type: "object",
        relationship: true,
      },
      has_visited: {
        term: "http://example.com/has_visited",
        type: "object",
        relationship: true,
        multivalued: true,
      },
    },
  },
  Country: {
    "@type": "http://example.com/Country",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
      in_continent: {
        term: "http://example.com/in_continent",
        type: "object",
        relationship: true,
      },
    },
  },
  Continent: {
    "@type": "http://example.com/Continent",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
    },
  },
});

type Types = EntitiesFrom<typeof TEST_TYPES>;

interface TestCase<K extends keyof Types> {
  readonly type: K;
  readonly record: Types[K];
  readonly include?: readonly EntityFrom<any>[];
  readonly document: JSONAPI.SingleResourceDoc;
}

// Using wrapper because you can't (currently) check the generically correlated
// parts of the test spec using Macro.  May break special error reporting that
// looks for `test` call.
const _test = <K extends keyof Types>(title: string, spec: TestCase<K>) => {
  test(`outgoing: ${title}`, (t) => {
    const { record, include, document } = spec;

    const result = record_to_document(TEST_TYPES, record, include);
    t.log("RESULT", result);

    t.deepEqual(result, document);
  });

  test(`incoming: ${title}`, (t) => {
    const { record, document } = spec;

    const result = document_to_record(TEST_TYPES, document);
    t.log("RESULT", result);

    t.deepEqual(result, record);
  });
};

_test("returns the JSON document for a given record", {
  type: "Person",
  record: {
    id: "http://example.com/Stewart",
    type: "http://example.com/Person",
    name: "Jon Stewart",
  },
  document: {
    data: {
      id: "http://example.com/Stewart",
      type: "http://example.com/Person",
      attributes: {
        name: "Jon Stewart",
      },
    },
    included: [],
  },
});

_test("includes requested resources", {
  type: "Person",
  record: {
    id: "http://example.com/TomWaits",
    type: TEST_TYPES.Person["@type"],
    name: "Tom Waits",
    has_visited: [
      { id: "http://example.com/Japan", type: TEST_TYPES.Country["@type"] },
    ],
  },
  include: [
    {
      id: "http://example.com/Japan",
      type: TEST_TYPES.Country["@type"],
      name: "Japan",
      in_continent: {
        id: "http://example.com/Asia",
        type: TEST_TYPES.Continent["@type"],
      },
    },
    {
      id: "http://example.com/KB",
      type: TEST_TYPES.Person["@type"],
      name: "Kathleen Brennan",
    },
    {
      id: "http://example.com/Springsteen",
      type: TEST_TYPES.Person["@type"],
      name: "Bruce Springsteen",
      born_in: {
        id: "http://example.com/USA",
        type: TEST_TYPES.Country["@type"],
      },
    },
  ],
  document: {
    data: {
      id: "http://example.com/TomWaits",
      type: TEST_TYPES.Person["@type"],
      attributes: { name: "Tom Waits" },
      relationships: {
        has_visited: {
          data: [
            {
              id: "http://example.com/Japan",
              type: TEST_TYPES.Country["@type"],
            },
          ],
        },
      },
    },
    included: [
      {
        id: "http://example.com/Japan",
        type: TEST_TYPES.Country["@type"],
        attributes: { name: "Japan" },
        relationships: {
          in_continent: {
            data: {
              id: "http://example.com/Asia",
              type: TEST_TYPES.Continent["@type"],
            },
          },
        },
      },
      {
        id: "http://example.com/KB",
        type: TEST_TYPES.Person["@type"],
        attributes: { name: "Kathleen Brennan" },
      },
      {
        id: "http://example.com/Springsteen",
        type: TEST_TYPES.Person["@type"],
        attributes: { name: "Bruce Springsteen" },
        relationships: {
          born_in: {
            data: {
              id: "http://example.com/USA",
              type: TEST_TYPES.Country["@type"],
            },
          },
        },
      },
    ],
  },
});
