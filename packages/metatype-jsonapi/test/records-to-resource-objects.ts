import type { EntitiesFrom } from "@swymbase/metatype-core";
import { metatypes } from "@swymbase/metatype-core";
import {
  collection_document_to_records,
  records_to_resource_objects,
} from "@swymbase/metatype-jsonapi";
import test from "ava";
import type * as JSONAPI from "jsonapi-typescript";

const TEST_TYPES = metatypes({
  Person: {
    "@type": "http://example.com/Person",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
      knows: {
        term: "http://example.com/knows",
        type: "object",
        relationship: true,
      },
      worked_at: {
        term: "http://example.com/worked_at",
        type: "object",
        relationship: true,
        multivalued: true,
      },
    },
  },
  Company: {
    "@type": "http://example.com/Company",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
      headquartered_in: {
        term: "http://example.com/headquartered_in",
        type: "object",
        relationship: true,
      },
    },
  },
  City: {
    "@type": "http://example.com/City",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
      in_state: {
        term: "http://example.com/in_state",
        type: "object",
        relationship: true,
      },
    },
  },
  State: {
    "@type": "http://example.com/State",
    properties: {
      name: {
        term: "http://example.com/name",
        type: "string",
      },
      postal_abbreviation: {
        term: "http://example.com/postal_abbreviation",
        type: "string",
      },
    },
  },
});

type Types = EntitiesFrom<typeof TEST_TYPES>;

// See note to resource-to-document.  This one is not polymorphic, but using
// same structure for symmetry.
interface TestCase {
  readonly records: Types[keyof Types][];
  readonly resource_objects: JSONAPI.ResourceObject[];
  readonly direction?: "incoming" | "outgoing";
}

const _test = (title: string, spec: TestCase) => {
  if (spec.direction !== "incoming") {
    test(`outgoing: ${title}`, (t) => {
      const { records, resource_objects } = spec;

      const result = records_to_resource_objects(TEST_TYPES, records);
      t.log("RESULT", result);

      t.deepEqual(result, resource_objects);
    });
  }

  if (spec.direction !== "outgoing") {
    test(`incoming: ${title}`, (t) => {
      const { records, resource_objects: data } = spec;

      const result = collection_document_to_records(TEST_TYPES, { data });
      t.log("RESULT", result);

      t.deepEqual(result, records);
    });
  }
};

_test("returns an array of resource objects", {
  records: [
    {
      id: "http://example.com/BillG",
      type: TEST_TYPES.Person["@type"],
      name: "Bill Gates",
      knows: {
        id: "http://example.com/SteveB",
        type: TEST_TYPES.Person["@type"],
      },
      worked_at: [
        {
          id: "http://example.com/Microsoft",
          type: TEST_TYPES.Company["@type"],
        },
      ],
    },
    {
      id: "http://example.com/Microsoft",
      type: TEST_TYPES.Company["@type"],
      name: "Microsoft",
      headquartered_in: {
        id: "http://example.com/Seattle",
        type: TEST_TYPES.City["@type"],
      },
    },
  ],
  resource_objects: [
    {
      id: "http://example.com/BillG",
      type: TEST_TYPES.Person["@type"],
      attributes: { name: "Bill Gates" },
      relationships: {
        knows: {
          data: {
            id: "http://example.com/SteveB",
            type: TEST_TYPES.Person["@type"],
          },
        },
        worked_at: {
          data: [
            {
              id: "http://example.com/Microsoft",
              type: TEST_TYPES.Company["@type"],
            },
          ],
        },
      },
    },
    {
      id: "http://example.com/Microsoft",
      type: TEST_TYPES.Company["@type"],
      attributes: { name: "Microsoft" },
      relationships: {
        headquartered_in: {
          data: {
            id: "http://example.com/Seattle",
            type: TEST_TYPES.City["@type"],
          },
        },
      },
    },
  ],
});

_test("skips records of unrecognized type", {
  direction: "outgoing",
  records: [
    {
      id: "http://example.com/PaulA",
      type: TEST_TYPES.Person["@type"],
      name: "Paul Allen",
      worked_at: [
        {
          id: "http://example.com/Microsoft",
          type: TEST_TYPES.Company["@type"],
        },
      ],
    },
    {
      id: "http://example.com/Windows",
      type: "http://example.com/Product",
      name: "Windows",
    },
  ],
  resource_objects: [
    {
      id: "http://example.com/PaulA",
      type: TEST_TYPES.Person["@type"],
      attributes: { name: "Paul Allen" },
      relationships: {
        worked_at: {
          data: [
            {
              id: "http://example.com/Microsoft",
              type: TEST_TYPES.Company["@type"],
            },
          ],
        },
      },
    },
  ],
});
