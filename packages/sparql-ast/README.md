# SPARQL AST

Status: WIP. Everything is subject to change. This is currently a quick & dirty
implementation of the minimal subset of SPARQL needed to implement the filter
language in `sparql-rest`. The intent is to eventually support typed, in-memory
representation of the full _abstract_ SPARQL abstract algebra (not preserving
insignificant syntactical details). See
[§18 “Definition of SPARQL”](https://www.w3.org/TR/sparql11-query/#sparqlDefinition)
and in particular
[§18.2 “Translation to the SPARQL Algebra”](https://www.w3.org/TR/sparql11-query/#sparqlQuery)

This is not a parser but would be suitable for use as a parser output format in
cases where exact syntax does not need to be preserved.

## Motivation

Provide a basis for type safe operations on and serialization of SPARQL
expressions. Intended applications include term rewriting systems.

## Features

The package provides:

- [types](src/api): types for representing abstract SPARQL expressions

- [serialization](src/outgoing) : safe sparql serialization mapping JSON API
  documents and resources to record-type objects

- possible TBD's: equivalency tests, common transforms (e.g. skolemization or
  other ops on open expressions)

## Design

TBD

## Roadmap

Should use a reified grammar (i.e. concrete data that can generate TS types,
_versus_ using TS as source of truth).

## Related work

The [SPIN vocabulary](https://spinrdf.org/spin.html) is a SPARQL-in-RDF
representation, which should be consulted.

- [SPARQLAlgebra.js](https://github.com/joachimvh/SPARQLAlgebra.js): a "SPARQL
  to SPARQL Algebra converter"
