import * as ser from "@swymbase/sparql-ast";
import test from "ava";

const r = String.raw;

test("serialize string", (t) => {
  t.is(ser.serialize_string(r``), r`""`, "empty string");
  t.is(ser.serialize_string(r`a`), `"a"`, "one character");
  t.is(ser.serialize_string(r`banana`), r`"banana"`, "multiple characters");
  t.is(ser.serialize_string(r`foo bar`), r`"foo bar"`, "medial whitespace");
  t.is(ser.serialize_string(r`foo\ bar`), r`"foo\\ bar"`, "escape backslash");
  t.is(ser.serialize_string(r`can't`), r`"can\'t"`, "escape single quote");
  t.is(
    ser.serialize_string(r`can't won't don't`),
    r`"can\'t won\'t don\'t"`,
    "multiple escapes"
  );
  t.is(
    ser.serialize_string(r`a's "b's"\c`),
    r`"a\'s \"b\'s\"\\c"`,
    "mixed escapes"
  );
  t.is(
    ser.serialize_string(r`she said
he said`),
    r`"she said\nhe said"`,
    "multiline"
  );
  t.is(
    ser.serialize_string("tab: \t; back: \b; feed: \f; crlf; \r\n"),
    r`"tab: \t; back: \b; feed: \f; crlf; \r\n"`,
    "control characters"
  );
});

test("serialize number", (t) => {
  t.is(ser.serialize_number(42), `42`);
  t.is(ser.serialize_number(5.67), `5.67`);
  t.is(ser.serialize_number(0), `0`);
  t.is(ser.serialize_number(0.1), `0.1`);
});

test("serialize boolean", (t) => {
  t.is(ser.serialize_boolean(true), `true`);
  t.is(ser.serialize_boolean(false), `false`);
});

test("serialize named node reference", (t) => {
  t.is(
    ser.serialize_named_node(["id", "http://example.com"]),
    `<http://example.com>`
  );
});

test("serialize any literal", (t) => {
  t.is(ser.serialize_literal("orange"), `"orange"`);
  t.is(ser.serialize_literal(41), "41");
  t.is(ser.serialize_literal(false), "false");

  // May drop the boxed versions
  t.is(ser.serialize_literal(["string", "orange"]), `"orange"`);
  t.is(ser.serialize_literal(["number", 41]), "41");
  t.is(ser.serialize_literal(["boolean", false]), "false");

  t.is(ser.serialize_literal(["id", "http://ex"]), "<http://ex>");
});

test("serialize variable", (t) => {
  t.is(ser.serialize_variable(["var", "thing"]), "?thing");
});

test("serialize property path", (t) => {
  t.is(
    ser.serialize_property_path(["id", "http://foo"]),
    "<http://foo>",
    "basic path"
  );
  t.is(
    ser.serialize_property_path(["^", ["id", "http://foo"]]),
    "^(<http://foo>)",
    "inverse path"
  );
  t.is(
    ser.serialize_property_path(["/", ["id", "http://foo"]]),
    "(<http://foo>)",
    "sequence path (one element)"
  );
  t.is(
    ser.serialize_property_path([
      "/",
      ["id", "http://foo"],
      ["id", "http://bar"],
    ]),
    "(<http://foo>/<http://bar>)",
    "sequence path (two elements)"
  );
  t.is(
    ser.serialize_property_path(["|", ["id", "http://foo"]]),
    "(<http://foo>)",
    "alternative path (one element)"
  );
  t.is(
    ser.serialize_property_path([
      "|",
      ["id", "http://foo"],
      ["id", "http://bar"],
    ]),
    "(<http://foo>|<http://bar>)",
    "alternative path (two elements)"
  );
});

test("serialize triple pattern", (t) => {
  t.is(
    ser.serialize_triple_pattern([
      "pattern",
      ["id", "http://ex/subject"],
      ["id", "http://ex/predicate"],
      ["id", "http://ex/object"],
    ]),
    `<http://ex/subject> <http://ex/predicate> <http://ex/object> .`,
    "all named nodes"
  );
  t.is(
    ser.serialize_triple_pattern([
      "pattern",
      ["id", "http://ex/subject"],
      ["^", ["id", "http://ex/predicate"]],
      ["id", "http://ex/object"],
    ]),
    `<http://ex/subject> ^(<http://ex/predicate>) <http://ex/object> .`,
    "with property path"
  );
  t.is(
    ser.serialize_triple_pattern([
      "pattern",
      ["id", "http://ex/subject"],
      ["var", "prop"],
      ["id", "http://ex/object"],
    ]),
    `<http://ex/subject> ?prop <http://ex/object> .`,
    "with variable property"
  );
});

test("serialize logical expression", (t) => {
  t.is(
    ser.serialize_logical_expression(["!", true]),
    "!(true)",
    "simple 'not' expression"
  );
  t.is(
    ser.serialize_logical_expression(["||", true, false]),
    "(true || false)",
    "simple 'or' expression"
  );
  t.is(
    ser.serialize_logical_expression(["&&", "a", 1]),
    `("a" && 1)`,
    "simple 'and' expression"
  );
  t.is(
    ser.serialize_logical_expression(["!", ["||", ["var", "x"], false]]),
    "!((?x || false))",
    "negated 'or' expression"
  );
});

test("serialize comparison expression", (t) => {
  t.is(
    ser.serialize_comparison_expression(["=", ["var", "a"], 1]),
    "(?a = 1)",
    "equals"
  );
  t.is(
    ser.serialize_comparison_expression([">", ["var", "a"], 1]),
    "(?a > 1)",
    "greater than"
  );
});

test("serialize expression filter", (t) => {
  t.is(
    ser.serialize_expression_filter(["filter", ["var", "a"]]),
    "FILTER (?a)"
  );
});

test("serialize exists filter", (t) => {
  t.is(
    ser.serialize_exists_filter([
      "exists",
      ["basic", ["pattern", ["var", "a"], ["var", "b"], ["var", "c"]]],
    ]),
    `FILTER EXISTS { ?a ?b ?c . }
`
  );
});

test("serialize not exists filter", (t) => {
  t.is(
    ser.serialize_not_exists_filter([
      "not-exists",
      ["basic", ["pattern", ["var", "a"], ["var", "b"], ["var", "c"]]],
    ]),
    `FILTER NOT EXISTS { ?a ?b ?c . }
`
  );
});

test("serialize basic graph pattern", (t) => {
  t.is(
    ser.serialize_basic_graph_pattern([
      "basic",
      ["pattern", ["var", "a"], ["var", "b"], "c"],
    ]),
    `?a ?b "c" .`,
    "one pattern"
  );
  t.is(
    ser.serialize_basic_graph_pattern([
      "basic",
      ["pattern", ["var", "a"], ["var", "b"], ["var", "c"]],
      ["pattern", ["var", "d"], ["var", "e"], ["var", "f"]],
    ]),
    `?a ?b ?c .
?d ?e ?f .`,
    "two patterns"
  );
  t.is(
    ser.serialize_basic_graph_pattern([
      "basic",
      ["pattern", ["var", "a"], ["var", "b"], ["var", "c"]],
      ["filter", ["=", 1, 2]],
    ]),
    `?a ?b ?c .
FILTER ((1 = 2))`,
    "pattern and filter"
  );
});

test("serialize group graph pattern", (t) => {
  t.is(
    ser.serialize_group_graph_pattern([
      "group",
      ["basic", ["pattern", ["var", "a"], ["var", "b"], ["var", "c"]]],
    ]),
    `{ ?a ?b ?c . }
`,
    "one basic pattern"
  );
  t.is(
    ser.serialize_group_graph_pattern([
      "group",
      ["basic", ["pattern", ["var", "a"], ["var", "b"], ["var", "c"]]],
      ["basic", ["pattern", ["var", "d"], ["var", "e"], ["var", "f"]]],
    ]),
    `{ ?a ?b ?c .
?d ?e ?f . }
`,
    "two basic patterns"
  );
  t.is(
    ser.serialize_group_graph_pattern([
      "group",
      ["group", ["basic", ["pattern", ["var", "a"], ["var", "b"], 1]]],
      ["group", ["basic", ["pattern", ["var", "d"], ["var", "e"], 2]]],
    ]),
    `{ { ?a ?b 1 . }

{ ?d ?e 2 . }
 }
`,
    "two simple group patterns"
  );
});

test("serialize in list", (t) => {
  t.is(
    ser.serialize_in_list(["in", ["var", "x"], "apple", "banana", "pear"]),
    `?x IN ("apple", "banana", "pear")`
  );
});

test("serialize regex match expression", (t) => {
  t.is(ser.serialize_call(["call", "REGEX", "a", "b"]), `REGEX("a", "b")`);
});

test("serialize regex match expression with option", (t) => {
  t.is(
    ser.serialize_call(["call", "REGEX", "a", "b", "i"]),
    `REGEX("a", "b", "i")`
  );
});

// TODO: may still need to adjust grammar for these cases.  currently allows you
// to mix the order of patterns and clauses.  Right thing would be to emit the
// patterns then the filters, regardless of the order they appear here.
test.skip("serialize multiple successive basic patterns", (t) => {
  t.is(
    ser.serialize_group_graph_pattern([
      "group",
      [
        "basic",
        ["pattern", ["var", "p"], ["var", "x"], 0],
        ["filter", ["=", 1, 0]],
      ],
      ["basic", ["pattern", ["var", "q"], ["var", "y"], 1]],
    ]),
    `{ ?p ?x 0 .
FILTER ((1 = 0))
?q ?y 1 }`
  );
});
