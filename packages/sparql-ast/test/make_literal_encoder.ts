import { make_literal_encoder } from "@swymbase/sparql-ast";
import test, { Macro } from "ava";

const check: Macro<[Record<string, string>, string, string]> = (t, ...test) => {
  const [dictionary, input, expected] = test;
  const encode = make_literal_encoder(dictionary);
  const actual = encode(input);
  t.is(actual, expected);
};

test("replaces in initial position", check, { a: "z" }, "abc", "zbc");
test("replaces in medial position", check, { b: "z" }, "abc", "azc");
test("replaces in terminal position", check, { c: "z" }, "abc", "abz");
test("replaces in multiple positions", check, { a: "z" }, "alfalfa", "zlfzlfz");
test("replaces multiple tokens", check, { a: "y", b: "z" }, "abc", "yzc");
test("multi-chararacter terms", check, { abc: "q" }, "abc", "q");
test("multi-chararacter replacement", check, { a: "xyz" }, "abc", "xyzbc");

test(
  "multi-chararacter term and replacement",
  check,
  { a: "pq", b: "xyz" },
  "abc",
  "pqxyzc"
);

test(
  "regex pattern operators are not special 1",
  check,
  { "+": "plus", ".": "dot" },
  "1.2 + 3.4",
  "1dot2 plus 3dot4"
);
test(
  "regex pattern operators are not special 2",
  check,
  { plus: "+", dot: "." },
  "1dot2 plus 3dot4",
  "1.2 + 3.4"
);
test(
  "regex substitution operators are not special 1",
  check,
  { DOLLAR: "$$", MATCH: "$&", "GROUP 1": "$1" },
  "to write $ write DOLLAR; for the match write MATCH : for group 1 write GROUP 1",
  "to write $ write $$; for the match write $& : for group 1 write $1"
);
test(
  "regex substitution operators are not special 2",
  check,
  { $$: "DOLLAR", "$&": "MATCH", $1: "GROUP 1" },
  "to write $ write $$; for the match write $& : for group 1 write $1",
  "to write $ write DOLLAR; for the match write MATCH : for group 1 write GROUP 1"
);

test(
  "unicode terms",
  check,
  { "♚": "law", "⚖": "king" },
  "the ♚ is the ⚖",
  "the law is the king"
);
test("unicode replacement", check, { K: "♚" }, "Ke1", "♚e1");
test("unicode replacements", check, { K: "♚", Q: "♛" }, "Ke1 Qf2", "♚e1 ♛f2");

test(
  "control character terms",
  check,
  { "\b": "BACKSPACE", "\f": "FORMFEED" },
  "goo\bd",
  "gooBACKSPACEd"
);
test(
  "control character replacements",
  check,
  { BACKSPACE: "\b", FORMFEED: "\f" },
  "gooBACKSPACEd",
  "goo\bd"
);

test(
  "no recursion 1",
  check,
  { "&": "&amp;", "<": "&lt;" },
  "1<3 & 3<4",
  "1&lt;3 &amp; 3&lt;4"
);
test(
  "no recursion 2",
  check,
  { "<": "&lt;", "&": "&amp;" },
  "1<3 & 3<4",
  "1&lt;3 &amp; 3&lt;4"
);

test(
  "order matters 1",
  check,
  { a: "one", aa: "two" },
  "abc aaron",
  "onebc oneoneron"
);
test(
  "order matters 2",
  check,
  { aa: "two", a: "one" },
  "abc aaron",
  "onebc tworon"
);

test(
  "xml text example",
  check,
  { "&": "&amp;", "<": "&lt;", ">": "&gt;" },
  "x > 5 & y < 3",
  "x &gt; 5 &amp; y &lt; 3"
);

test(
  "xml attributes example",
  check,
  { "'": "&apos;", '"': "&quot;" },
  `never say "can't"`,
  `never say &quot;can&apos;t&quot;`
);
