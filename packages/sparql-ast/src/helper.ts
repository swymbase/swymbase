/**
 * Helper for exhaustive type checking.
 *
 * @example
 *
 *     function offset(chan: "r" | "g" | "b"): number {
 *       if (chan === "r") return 8;
 *       if (chan === "g") return 4;
 *       if (chan === "b") return 0;
 *
 *        // Type of `chan` should be `never` here:
 *       return assert_unreachable(chan, "channel")
 *     }
 */
export function assert_unreachable(_: never, name?: string): never {
  throw new Error(
    name
      ? `Unrecognized ${name}: ${JSON.stringify(_)}.`
      : `Assert failed: unreachable code reached.`
  );
}
