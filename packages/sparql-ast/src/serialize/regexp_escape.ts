/**
 * A regular expression matching characters that have special meaning in
 * ECMAScript regular expressions.
 *
 * This selection of characters is based on an escape function in
 * [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping).
 *
 * See also [this post and discussion](https://stackoverflow.com/a/3561711).
 */
const REGEXP_SPECIAL = /[.*+?^${}()|[\]\\]/g;

/**
 * Return a regular expression that matches only and exactly the given input.
 *
 * @param literal: text that you want to be treated literally in a regular
 * expression
 *
 * @returns a regular expression with no special characters
 */
export const regexp_escape = (literal: string): string =>
  literal.replace(REGEXP_SPECIAL, "\\$&");
