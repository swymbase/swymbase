// Efficient dictionary-based string replacement.

import { regexp_escape } from "./regexp_escape";

/**
 * Create a replacement function that maps a set of terms to a corresponding set
 * of substitutions in an input string.
 *
 * Only one pass is made over the input.  If a substitution results in the
 * creation of a new term in the output, that term will not be substituted.
 *
 * Substitutions are performed in dictionary order.  If there are terms that are
 * prefixes of other terms, the longer terms need to be defined earlier in order
 * to match.
 *
 * This implementation has the same complexity as a single call to
 * `String.replace` with one alternation construct containing only literals.  In
 * cases where multiple substitutions may be made, this approach is more
 * efficient in both time and space than multiple `replace` operations.
 *
 * @example
 *
 *     const encode_xml_text = make_literal_encoder({
 *       "&": "&amp;",
 *       "<": "&lt;",
 *       ">": "&gt;",
 *     });
 *     const encoded = encode_xml_text("x > 5 & y < 3");
 *     // "x &gt; 5 &amp; y &lt; 3"
 *
 * @param dictionary A record whose keys are strings to match in the input and
 * whose values are their respective replacement strings.
 *
 * @returns A function that applies the defined substitutions.
 */
export const make_literal_encoder = (
  dictionary: Readonly<Record<string, string>>
): ((match: string) => string) => {
  const terms = Object.keys(dictionary);
  const pattern = new RegExp(`(${terms.map(regexp_escape).join("|")})`, "g");
  const replacer = (match: string) => dictionary[match];
  return (text) => text.replace(pattern, replacer);
};
