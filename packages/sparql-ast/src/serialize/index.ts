import * as AST from "../ast";
import { assert_unreachable } from "../helper";
import { make_literal_encoder } from "./make_literal_encoder";

const { raw } = String;

/**
 * Special escape sequences for SPARQL string literals.
 *
 * See https://www.w3.org/TR/2013/REC-sparql11-query-20130321/#grammarEscapes
 *
 * Note that all of these escape sequences are defined identically for
 * Javascript strings.
 */
const ESCAPES = {
  "\u0009": raw`\t`, // tab
  "\u000A": raw`\n`, // line feed
  "\u000D": raw`\r`, // carriage return
  "\u0008": raw`\b`, // backspace
  "\u000C": raw`\f`, // form feed
  "\u0022": raw`\"`, // quotation mark, double quote mark
  "\u0027": raw`\'`, // apostrophe-quote, single quote mark
  "\u005C": raw`\\`, // backslash
};

const escape_string_for_double_quote = make_literal_encoder(ESCAPES);

export const serialize_group_graph_pattern = (expr: AST.GroupGraphPattern) => {
  const [, ...parts] = expr;
  return `{ ${parts.map(serialize_group_graph_pattern_part).join("\n")} }\n`;
};

export const serialize_group_graph_pattern_part = (
  expr: AST.GroupGraphPatternPart
) => {
  if (expr[0] === "group") return serialize_group_graph_pattern(expr);
  if (expr[0] === "basic") return serialize_basic_graph_pattern(expr);
  if (expr[0] === "union") return serialize_union_graph_pattern(expr);

  return assert_unreachable(expr, "group graph pattern part");
};

export const serialize_basic_graph_pattern = (
  expr: AST.BasicGraphPattern
): string => {
  const [, ...parts] = expr;
  return parts.map(serialize_basic_graph_pattern_part).join("\n");
};

export const serialize_basic_graph_pattern_part = (
  expr: AST.BasicGraphPatternPart
): string => {
  if (expr[0] === "pattern") return serialize_triple_pattern(expr);
  return serialize_filter(expr);
};

export const serialize_union_graph_pattern = (
  expr: AST.UnionGraphPattern
): string => {
  const [, ...parts] = expr;
  return parts.map(serialize_group_graph_pattern).join("\nUNION");
};

export const serialize_filter = (expr: AST.FilterClause): string => {
  if (expr[0] === "filter") return serialize_expression_filter(expr);
  if (expr[0] === "exists") return serialize_exists_filter(expr);
  if (expr[0] === "not-exists") return serialize_not_exists_filter(expr);

  return assert_unreachable(expr, "filter");
};

export const serialize_expression_filter = (
  filter: AST.ExpressionFilter
): string => {
  const [, expr] = filter;
  return `FILTER (${serialize_value_expression(expr)})`;
};

export const serialize_exists_filter = (expr: AST.ExistsFilter): string => {
  const [, ...parts] = expr;
  return `FILTER EXISTS ${serialize_group_graph_pattern(["group", ...parts])}`;
};

export const serialize_not_exists_filter = (
  expr: AST.NotExistsFilter
): string => {
  const [, ...parts] = expr;
  return `FILTER NOT EXISTS ${serialize_group_graph_pattern([
    "group",
    ...parts,
  ])}`;
};

// The trailing `.` is not part of a single triple pattern *per se*, but
// emitting here allows these to be composed with less concern for context.
export const serialize_triple_pattern = (expr: AST.TriplePattern): string => {
  const [, s, p, o] = expr;
  return [
    serialize_pattern_subject(s),
    serialize_pattern_predicate(p),
    serialize_pattern_object(o),
    ".",
  ].join(" ");
};

export const serialize_pattern_subject = (expr: AST.PatternSubject): string => {
  if (expr[0] === "id") return serialize_named_node(expr);
  if (expr[0] === "var") return serialize_variable(expr);

  return assert_unreachable(expr, "pattern subject");
};

export const serialize_pattern_predicate = (
  expr: AST.PatternPredicate
): string => {
  if (expr[0] === "id") return serialize_named_node(expr);
  if (expr[0] === "var") return serialize_variable(expr);
  return serialize_composite_path(expr);
};

export const serialize_pattern_object = (expr: AST.PatternObject): string => {
  // TS: Array.isArray not narrowing as expected
  if (typeof expr !== "object") return serialize_literal(expr);
  if (expr[0] === "id") return serialize_named_node(expr);
  if (expr[0] === "var") return serialize_variable(expr);
  return serialize_literal(expr);
};

export const serialize_literal = (literal: AST.LiteralTerm): string => {
  if (typeof literal === "string") return serialize_string(literal);
  if (typeof literal === "boolean") return serialize_boolean(literal);
  if (typeof literal === "number") return serialize_number(literal);

  if (literal[0] === "string") return serialize_string(literal[1]);
  if (literal[0] === "boolean") return serialize_boolean(literal[1]);
  if (literal[0] === "number") return serialize_number(literal[1]);

  if (literal[0] === "id") return serialize_iri(literal[1]);

  return assert_unreachable(literal, "literal");
};

export const serialize_property_path = (path: AST.PropertyPath): string => {
  if (path[0] === "id") return serialize_named_node(path);
  return serialize_composite_path(path);
};

export const serialize_composite_path = (path: AST.CompositePath): string => {
  if (path[0] === "^") return serialize_inverse_path(path);
  if (path[0] === "/") return serialize_sequence_path(path);
  if (path[0] === "|") return serialize_alternative_path(path);

  return assert_unreachable(path, "composite path");
};

export const serialize_inverse_path = (path: AST.InversePath): string => {
  const [, elt] = path;
  return `^(${serialize_property_path(elt)})`;
};

export const serialize_sequence_path = (path: AST.SequencePath): string => {
  const [, ...elts] = path;
  return `(${elts.map(serialize_property_path).join("/")})`;
};

export const serialize_alternative_path = (
  path: AST.AlternativePath
): string => {
  const [, ...elts] = path;
  return `(${elts.map(serialize_property_path).join("|")})`;
};

export const serialize_string = (literal: string): string => {
  return `"${escape_string_for_double_quote(literal)}"`;
};

export const serialize_boolean = (literal: boolean): string => {
  return literal ? "true" : "false";
};

export const serialize_number = (literal: number): string => {
  // What else to do here?
  return literal.toString();
};

export const serialize_iri = (iri: string): string => {
  // What else to do here?
  return `<${iri}>`;
};

export const serialize_named_node = (expr: AST.NamedNodeTerm): string => {
  const [, iri] = expr;
  return serialize_iri(iri);
};

// should parenthesizing be done here?
export const serialize_logical_expression = (
  expr: AST.LogicalExpression
): string => {
  if (expr[0] === "!") return `!(${serialize_value_expression(expr[1])})`;

  const [op, lhs, rhs] = expr;
  if (expr[0] === "&&" || expr[0] === "||")
    return `(${serialize_value_expression(
      lhs
    )} ${op} ${serialize_value_expression(rhs)})`;

  return assert_unreachable(expr, "logical expression");
};

// should parenthesizing be done here?
export const serialize_comparison_expression = (
  expr: AST.ComparisonExpression
): string => {
  const [op, lhs, rhs] = expr;
  return `(${serialize_value_expression(
    lhs
  )} ${op} ${serialize_value_expression(rhs)})`;
};

export const serialize_value_expression = (
  expr: AST.ValueExpression
): string => {
  // TS: Array.isArray not narrowing as expected
  if (typeof expr !== "object") return serialize_literal(expr);

  switch (expr[0]) {
    case "!":
    case "&&":
    case "||":
      return serialize_logical_expression(expr);

    case "=":
    case "<":
    case ">":
    case "<=":
    case ">=":
      return serialize_comparison_expression(expr);

    case "string":
    case "number":
    case "boolean":
    case "id":
      return serialize_literal(expr);

    case "var":
      return serialize_variable(expr);

    case "call":
      return serialize_call(expr);

    case "in":
      return serialize_in_list(expr);

    case "not-in":
      return serialize_not_in_list(expr);

    default:
      return assert_unreachable(expr, "value expression");
  }
};

export const serialize_variable = (term: AST.VariableTerm): string => {
  // TODO: sanitize/validate name
  const [, name] = term;
  return `?${name}`;
};

export const serialize_call = (expr: AST.FunctionExpression): string => {
  const [, fn, ...args] = expr;
  return `${fn}(${args.map(serialize_value_expression).join(", ")})`;
};

export const serialize_in_list = (expr: AST.InExpression): string => {
  const [, operand, ...items] = expr;
  return `${serialize_value_expression(operand)} IN (${items
    .map(serialize_value_expression)
    .join(", ")})`;
};

export const serialize_not_in_list = (expr: AST.NotInExpression): string => {
  const [, operand, ...items] = expr;
  return `${serialize_value_expression(operand)} NOT IN (${items
    .map(serialize_value_expression)
    .join(", ")})`;
};
