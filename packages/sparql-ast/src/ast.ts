// like I said, quick & dirty
export type GroupGraphPattern = readonly ["group", ...GroupGraphPatternPart[]];
export type GroupGraphPatternPart =
  | GroupGraphPattern
  | BasicGraphPattern
  | UnionGraphPattern;
export type BasicGraphPattern = readonly ["basic", ...BasicGraphPatternPart[]];
export type BasicGraphPatternPart = TriplePattern | FilterClause;
export type TriplePattern = readonly [
  "pattern",
  PatternSubject,
  PatternPredicate,
  PatternObject
];
export type UnionGraphPattern = readonly ["union", ...GroupGraphPattern[]];
export type PatternSubject = NamedNodeTerm | VariableTerm; // | BlankNodeTerm;
export type NamedNodeTerm = readonly ["id", string];
// use "?" for tag?  But `?` is also used for optional path
export type VariableTerm = readonly ["var", string];
// export type BlankNodeTerm = readonly ["bnode", string?];
export type PatternPredicate = PropertyPath | VariableTerm;
export type PropertyPath = PredicatePath | CompositePath;
export type PredicatePath = NamedNodeTerm;
export type CompositePath = InversePath | SequencePath | AlternativePath;
export type InversePath = readonly ["^", PropertyPath];
export type SequencePath = readonly ["/", ...PropertyPath[]];
export type AlternativePath = readonly ["|", ...PropertyPath[]];
export type PatternObject = NamedNodeTerm | VariableTerm | LiteralTerm;
export type LiteralTerm =
  | BooleanLiteral
  | NumberLiteral
  | StringLiteral
  | NamedNodeTerm;
// Not yet
// | BlankNodeTerm
export type StringLiteral = PlainStringLiteral;
// Not yet
//  | LanguageString
//  | DatatypedString;
// export type LanguageString = ["lang", string, string];
// export type DatatypedString = ["typed", string, string];
// The boxed versions may not be needed
export type PlainStringLiteral = string | readonly ["string", string];
export type NumberLiteral = number | readonly ["number", number];
export type BooleanLiteral = boolean | readonly ["boolean", boolean];
export type FilterClause = PatternFilter | ExpressionFilter;
export type PatternFilter = ExistsFilter | NotExistsFilter;
export type ExistsFilter = readonly ["exists", ...GroupGraphPatternPart[]];
// not crazy about separate `not-` term... this is simpler for now
export type NotExistsFilter = readonly [
  "not-exists",
  ...GroupGraphPatternPart[]
];
export type ExpressionFilter = readonly ["filter", ValueExpression];
export type ValueExpression =
  | LiteralTerm
  | VariableTerm
  | FunctionExpression
  | LogicalExpression
  | ComparisonExpression
  | InExpression
  | NotInExpression;
// really function *call*, but that's the only kind in sparql
// hmm but here we run out of gas & don't type the args
export type FunctionExpression = readonly [
  "call",
  string,
  ...ValueExpression[]
];
export type LogicalExpression = AndExpression | OrExpression | NotExpression;
export type AndExpression = readonly ["&&", ...ValueExpression[]];
export type OrExpression = readonly ["||", ...ValueExpression[]];
export type NotExpression = readonly ["!", ValueExpression];
export type InExpression = readonly [
  "in",
  ValueExpression,
  ...ValueExpression[]
];
// see NotExistsFilter
export type NotInExpression = readonly [
  "not-in",
  ValueExpression,
  ...ValueExpression[]
];
export type ComparisonExpression = readonly [
  "=" | "<" | ">" | "<=" | ">=",
  ValueExpression,
  ValueExpression
];
