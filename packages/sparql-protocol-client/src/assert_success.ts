// This is a test helper that is used in the tests of several other packages.
// This package should not take a dependency on AVA...
import type { ExecutionContext } from "ava";
import type {
  SparqlResponse,
  SuccessResponse,
} from "@swymbase/sparql-protocol-client";

export function assert_success(
  t: ExecutionContext,
  response: SparqlResponse,
  label: string
): asserts response is SuccessResponse {
  if (!response.success) {
    t.log(
      `Context: ${label}`,
      response.code,
      response.error,
      response.http_response,
      response.request.operation
    );
    throw new Error(`Failed: ${label}`);
  }
}
