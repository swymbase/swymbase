/*
 * Types for this API.  These types are not based a standard protocol.
 */

import { SparqlRequest } from "./sparql-protocol";
import { SparqlJsonResults, SparqlSelectResults } from "./sparql-json-results";

// TODO: Use `import type` when next Prettier is released
import { Request, Response } from "node-fetch";

// Dictionary of possible response types and associated codes.
// This type could be extensible.
export interface SparqlResponseFormats {
  unknown: unknown;
  boolean: boolean;
  json_ld: object; // use for graph
  results: SparqlJsonResults;
  select_results: SparqlSelectResults;
  // Binary RDF Results Table.  This format returns a buffer directly to the
  // client.  It can be used with the `read_brtr` function in `./brtr.ts`.
  brtr: Uint8Array;
  // csv, xml, etc
  // Could also be special implementation-specific types
}

// Tagged union type based on response type dictionary.
export type FormattedResponse<
  T = SparqlResponseFormats,
  K = keyof T
> = K extends keyof T ? { readonly format: K; readonly value: T[K] } : never;

// These keys can be used to indicate preferred as well as actual response type.
export type SparqlResponseFormat = keyof SparqlResponseFormats;

// Successful SPARQL response along with request context.
export interface SuccessResponse {
  readonly success: true;
  readonly request: SparqlRequest;
  readonly http_request: Request;
  readonly http_response: Response;
  readonly response: FormattedResponse;
}

// Unsuccessful SPARQL response along with request context.
export interface FailureResponse {
  readonly success: false;
  readonly request: SparqlRequest;
  readonly http_request: Request;
  readonly http_response?: Response;
  readonly code: string; // specify internal codes
  readonly error: any;
}

export type SparqlResponse = SuccessResponse | FailureResponse;
