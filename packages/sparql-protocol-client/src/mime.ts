// RDF (CONSTRUCT/DESCRIBE)
export const NQUADS = "application/n-quads";
export const JSON_LD = "application/ld+json";
export const N_TRIPLES = "application/n-triples";
export const TURTLE = "text/turtle";
export const N3 = "text/n3";

// SELECT/ASK
export { SPARQL_JSON_RESULTS } from "./api/sparql-json-results";
export const CSV = "text/csv";
export const TSV = "text/tab-separated-values";
export const BRTR = "application/x-binary-rdf-results-table";

// ASK
export const BOOLEAN = "text/boolean";

// GENERAL
export const JSON = "application/json";

const IS_JSON_MIME = /application\/(.*\+)?json/;
export const is_json_mime = (mime: string): boolean => IS_JSON_MIME.test(mime);
