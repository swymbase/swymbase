/**
 * Metadata describing the defined errors returned from Neptune query endpoints.
 *
 * Source: [“Graph Engine Query
 * Errors”](https://docs.aws.amazon.com/neptune/latest/userguide/errors-engine-codes.html)
 */
export const QUERY_ERRORS = {
  BadRequestException: {
    status: 400,
    ok_to_retry: false,
    description: "The request could not be completed.",
  },
  InternalFailureException: {
    status: 500,
    ok_to_retry: true,
    description: "The request processing has failed.",
  },
  CancelledByUserException: {
    status: 500,
    ok_to_retry: true,
    description:
      "The request processing was cancelled by an authorized client.",
  },
  InvalidParameterException: {
    status: 400,
    ok_to_retry: false,
    description:
      "An invalid or out-of-range value was supplied for some input parameter or invalid syntax in a supplied RDF file.",
  },
  MissingParameterException: {
    status: 400,
    ok_to_retry: false,
    description:
      "A required parameter for the specified action is not supplied.",
  },
  ReadOnlyViolationException: {
    status: 400,
    ok_to_retry: false,
    description:
      "The request is rejected because it violates some read-only restriction, such as a designation of a replica as read-only.",
  },
  MalformedQueryException: {
    status: 400,
    ok_to_retry: false,
    description:
      "The request is rejected because it contains a query that is syntactically incorrect or does not pass additional validation.",
  },
  // NOTE: The source doc has two entries for `BadRequestException`
  BadRequestException2: {
    status: 400,
    ok_to_retry: false,
    description: "Request size exceeds max allowed value of 157286400 bytes.",
  },
  MemoryLimitExceededException: {
    status: 500,
    ok_to_retry: true,
    description:
      "The request processing did not succeed due to lack of memory, but can be retried when the server is less busy.",
  },
  QueryLimitException: {
    status: 400,
    ok_to_retry: false,
    description: "Size of query exceeds system limit.",
  },
  TimeLimitExceededException: {
    status: 500,
    ok_to_retry: true,
    description: "The request processing timed out.",
  },
  UnsupportedOperationException: {
    status: 400,
    ok_to_retry: false,
    description:
      "The request uses a currently unsupported feature or construct.",
  },
  ConstraintViolationException: {
    status: 400,
    ok_to_retry: true,
    description:
      "The query engine discovered, during the execution of the request, that the completion of some operation is impossible without violating some data integrity constraints, such as persistence of in- and out-vertices while adding an edge. Such conditions are typically observed if there are concurrent modifications to the graph, and are transient. The client should retry the request.",
  },
  QueryLimitExceededException: {
    status: 500,
    ok_to_retry: true,
    description:
      "The request processing did not succeed due to the lack of a limited resource, but can be retried when the server is less busy.",
  },
  ConcurrentModificationException: {
    status: 500,
    ok_to_retry: true,
    description:
      "The request processing did not succeed due to a modification conflict. The client should retry the request.",
  },
  QueryTooLargeException: {
    status: 400,
    ok_to_retry: false,
    description: "The request was rejected because its body is too large.",
  },
  MethodNotAllowedException: {
    status: 405,
    ok_to_retry: false,
    description:
      "The request is rejected because the chosen HTTP method is not supported by the used endpoint.",
  },
  AccessDeniedException: {
    status: 403,
    ok_to_retry: false,
    description: "Authentication or authorization failure.",
  },
  TooManyRequestsException: {
    status: 429,
    ok_to_retry: true,
    description: "Too many requests.",
  },
  ThrottlingException: { status: 500, ok_to_retry: true, description: "" },
} as const;

export type QueryErrorCode = keyof typeof QUERY_ERRORS;
