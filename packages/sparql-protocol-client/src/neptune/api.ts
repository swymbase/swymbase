/*
 * Types related to Neptune-specific implementation.  Not currently used.
 */

// import { QueryErrorCode } from "./query-errors";

/** Structure of JSON response from SPARQL `INSERT` query. */
// Or update?
// this is based on an example response
// looks like it's an array of some discriminated union
interface UpdateEvent {
  readonly type: "UpdateEvent";
  readonly totalElapsedMillis: number;
  readonly elapsedMillis: number;
  readonly connFlush: number;
  readonly batchResolve: number;
  readonly whereClause: number;
  readonly deleteClause: number;
  readonly insertClause: number;
}

interface Commit {
  readonly type: "Commit";
  readonly totalElapsedMillis: number;
}

export type NeptuneResponseRecord = UpdateEvent | Commit;

export type NeptuneInsertResponse = readonly NeptuneResponseRecord[];

export interface NeptuneSparqlError {
  /** A UUID identifying this error instance. */
  readonly requestId: string;

  // MalformedQueryException
  // One of the values in
  readonly code: string;

  // Human-readable details of specific error instance
  readonly detailedMessage: string;
}

/*

https://docs.aws.amazon.com/neptune/latest/userguide/sparql-media-type-support.html

RDF media-types that SPARQL SELECT can output from Neptune

- SPARQL JSON Results (This is the default)
- SPARQL XML Results
- Binary Results Table (media type: application/x-binary-rdf-results-table)
- Comma-Separated Values (CSV)
- Tab-Separated Values (TSV)

RDF media-types that SPARQL ASK can output from Neptune

- SPARQL JSON Results (This is the default)
- SPARQL XML Results
- Boolean (media type: text/boolean, meaning "true" or "false")

RDF media-types that SPARQL CONSTRUCT can output from Neptune

- N-Quads (This is the default)
- RDF/XML
- JSON-LD
- N-Triples
- Turtle
- N3
- TriX
- TriG
- SPARQL JSON Results

RDF media-types that SPARQL DESCRIBE can output from Neptune
(same as above for CONSTRUCT)

- N-Quads (This is the default)
- RDF/XML
- JSON-LD
- N-Triples
- Turtle
- N3
- TriX
- TriG
- SPARQL JSON Results

*/
