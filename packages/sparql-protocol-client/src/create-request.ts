import { Request, Headers } from "node-fetch";
import { SparqlRequest } from "./api";
import { encode_query_via, encode_update_via } from "./encode-operation";

/**
 * Translate a SPARQL request into an HTTP request according to SPARQL Protocol.
 */
export const create_http_request = (request: SparqlRequest): Request => {
  const { operation, method, accept } = request;
  const encoder = "query" in operation ? encode_query_via : encode_update_via;
  const the = encoder[method || "post_direct"](operation);

  return new Request(request.endpoint + (the.query ? `?${the.query}` : ""), {
    method: the.http_method,
    body: the.body,
    headers: new Headers([
      ...(the.content_type ? [["Content-type", the.content_type]] : []),
      ...(accept ? accept.map((mime) => ["Accept", mime]) : []),
    ]),
  });
};
