import fetch, { Response } from "node-fetch";
import {
  SparqlJsonResults,
  SparqlRequest,
  SparqlResponse,
  SparqlResponseFormat,
  SparqlResponseFormats,
} from "./api/index";
import { create_http_request } from "./create-request";
import * as MIME from "./mime";

export async function send(request: SparqlRequest): Promise<SparqlResponse> {
  const http_request = create_http_request(request);
  let http_response: Response;

  const fail_with = (code: string, error: any) => ({
    success: false as const,
    request,
    http_request,
    http_response,
    code,
    error,
  });

  try {
    http_response = await fetch(http_request);
  } catch (error) {
    return {
      success: false,
      request,
      http_request,
      code: "fetch_error",
      error,
    };
  }

  if (!http_response.ok)
    return {
      success: false,
      request,
      http_request,
      http_response,
      code: "http_error",
      error: {
        http_status: http_response.status,
        message: http_response.statusText,
      },
    };

  const content_type = http_response.headers.get("Content-Type");
  const [mime_type] = content_type?.split(";") || [];

  // Binary results
  if (mime_type === MIME.BRTR) {
    const buffer = await http_response.arrayBuffer();
    return {
      success: true,
      request,
      http_request,
      http_response,
      response: {
        format: "brtr",
        value: new Uint8Array(buffer),
      },
    };
  }

  // JSON-based results
  if (MIME.is_json_mime(mime_type)) {
    try {
      const parsed = (await http_response.json()) as SparqlJsonResults;

      // Neptune specific
      if ("code" in parsed) return fail_with("engine", parsed);

      return {
        success: true,
        request,
        http_request,
        http_response,
        response:
          mime_type === MIME.SPARQL_JSON_RESULTS
            ? // Note that this is just `results` for both ‘select’ and
              // ‘boolean’ results. `formatted` will return the more specific
              // type, but only when it was requested explicitly.
              { format: "results", value: parsed }
            : { format: "json_ld", value: parsed },
      };
    } catch (error) {
      return fail_with("json", error);
    }
  }

  let text: string | undefined;
  try {
    text = await http_response.text();
  } catch {}

  return {
    success: true,
    request,
    http_request,
    http_response,
    response: { format: "unknown", value: text },
  };
}

// Extract the formatted result and return as typed.  Include response.
export async function formatted<T extends SparqlResponseFormat>(
  format: T,
  request: SparqlRequest
): Promise<{
  readonly context: SparqlResponse;
  readonly value: SparqlResponseFormats[T] | undefined;
}> {
  let { accept } = request;

  if (format === "json_ld") accept = [MIME.JSON_LD];

  const context = await send({ ...request, accept });

  if (context.success) {
    const { response } = context;

    if (response.format === format)
      return { context, value: response.value as SparqlResponseFormats[T] };

    if (
      format === "select_results" &&
      response.format === "results" &&
      !("boolean" in response.value)
    )
      // TS: doesn't narrow format to "boolean"
      return { context, value: response.value as SparqlResponseFormats[T] };

    if (
      format === "boolean" &&
      response.format === "results" &&
      "boolean" in response.value
    )
      // TS: doesn't narrow format to "boolean"
      return {
        context,
        value: response.value.boolean as SparqlResponseFormats[T],
      };
  }
  return { context, value: undefined };
}
