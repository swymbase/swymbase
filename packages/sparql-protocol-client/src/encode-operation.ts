import {
  UpdateOperation,
  UpdateHttpMethod,
  SparqlQueryOperation,
  QueryHttpMethod,
} from "./api";

interface HttpPartial<Method extends string = string> {
  readonly http_method: Method;
  readonly content_type?: string;
  readonly query?: string;
  readonly body?: string;
}

// Note that a dictionary cannot be used because keys may be repeated.
type Param = readonly [string, string];

export const url_encode = (params: Iterable<Param>) =>
  Array.from(
    params,
    ([name, value]) =>
      `${encodeURIComponent(name)}=${encodeURIComponent(value)}`
  ).join("&");

function* list_of<T>(value: undefined | T | T[]): IterableIterator<T> {
  if (Array.isArray(value)) yield* value;
  else if (value !== undefined) yield value;
}

// Assumes a naming convention mapping
export const as_params = (dict: object): Iterable<Param> => {
  const out: Param[] = [];
  for (const [key, values] of Object.entries(dict))
    for (const value of list_of(values))
      out.push([key.replace(/_/g, "-"), value]);
  return out;
};

/*
 * Encode a query request according to one of the protocol options
 * https://www.w3.org/TR/sparql11-protocol/#query-operation */

/**
 * The query operation **MUST** be invoked with either the HTTP GET or HTTP POST
 * method.
 */
type QueryHttpRequest = HttpPartial<"GET" | "POST">;
type QueryHttpEncoder = (request: SparqlQueryOperation) => QueryHttpRequest;

export const encode_query_via: Record<QueryHttpMethod, QueryHttpEncoder> = {
  /**
   * Protocol clients **may** send protocol requests via the HTTP GET method.
   *
   * https://www.w3.org/TR/sparql11-protocol/#query-via-get
   */
  get: (request) => ({
    http_method: "GET",

    /* When using the GET method, clients **must** URL percent encode all
     * parameters and include them as query parameter strings with the names
     * given above [RFC3986]. */
    query: url_encode(as_params(request)),
  }),

  /**
   * Protocol clients **may** send protocol requests via the HTTP POST method by
   * URL encoding the parameters.
   *
   * https://www.w3.org/TR/sparql11-protocol/#query-via-post-urlencoded
   */
  post_with_encoded_params: (request) => ({
    http_method: "POST",

    /* When using this method, clients **must** URL percent encode [RFC3986] all
     * parameters and include them as parameters within the request body */
    body: url_encode(as_params(request)),

    /* The content type header of the HTTP request **must** be set to
     * `application/x-www-form-urlencoded`. */
    content_type: "application/x-www-form-urlencoded",
  }),

  /**
   * Protocol clients **may** send protocol requests via the HTTP POST method by
   * including the query directly and unencoded as the HTTP request message body.
   *
   * https://www.w3.org/TR/sparql11-protocol/#query-via-post-direct
   */
  post_direct: ({ query, ...rest }) => ({
    http_method: "POST",

    /* Clients **may** include the optional `default-graph-uri` and
     * `named-graph-uri` parameters as HTTP query string parameters in the
     * request URI. */
    query: url_encode(as_params(rest)),

    /* Clients **must** set the content type header of the HTTP request to
     * application/sparql-query. */
    content_type: "application/sparql-query",

    /* When using this approach, clients **must** include the SPARQL query
     * string, unencoded, and nothing else as the message body of the
     * request. */
    body: query,
  }),
};

/*
 * Encode an update request according to one of the protocol options
 * https://www.w3.org/TR/sparql11-protocol/#update-operation */

/**
 * The update operation **must** be invoked using the HTTP POST method.
 */
type UpdateHttpRequest = HttpPartial<"POST">;
type UpdateHttpEncoder = (request: UpdateOperation) => UpdateHttpRequest;

export const encode_update_via: Record<UpdateHttpMethod, UpdateHttpEncoder> = {
  /**
   * Protocol clients **may** send update protocol requests via the HTTP POST
   * method by URL encoding the parameters.
   *
   * https://www.w3.org/TR/sparql11-protocol/#update-via-post-urlencoded
   */
  post_with_encoded_params: (request) => ({
    http_method: "POST",

    /* When using this approach, clients **must** URL percent encode [RFC3986]
     * all parameters and include them as parameters within the request body */
    body: url_encode(as_params(request)),

    /* The content type header of the HTTP request **must** be set to
     * `application/x-www-form-urlencoded`. */
    content_type: "application/x-www-form-urlencoded",
  }),

  /**
   * Protocol clients **may** send update protocol requests via the HTTP POST
   * method by including the update request directly and unencoded as the HTTP
   * request message body.
   *
   * https://www.w3.org/TR/sparql11-protocol/#update-via-post-direct
   */
  post_direct: ({ update, ...rest }) => ({
    http_method: "POST",

    /* Clients **may** include the optional `using-graph-uri` and
     * `using-named-graph-uri` parameters as HTTP query string parameters in the
     * request URI. */
    query: url_encode(as_params(rest)),

    /* Clients **must** set the content type header of the HTTP request to
     * `application/sparql-update`. */
    content_type: "application/sparql-update",

    /* When using this approach, clients **must** include the SPARQL update
     * request string, unencoded, and nothing else as the message body of the
     * request. */
    body: update,
  }),
};
