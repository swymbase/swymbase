// Binary RDF Table Results reader.  See documentation at `../brtr`
import { TextDecoder } from "util";
import type { RDFTerm, SparqlSelectResults } from "./api";

type SparqlResultsBinding = SparqlSelectResults["results"]["bindings"][any];

const EOF: unique symbol = Symbol("EOF");
const REPEAT: unique symbol = Symbol("REPEAT");

// Adapted from https://www.w3.org/2011/08/BinaryTableResults/BRTparser.cpp
const enum RecordTypeMarker {
  NULL = 0,
  REPEAT,
  NAMESPACE,
  QNAME,
  URI,
  BNODE,
  PLAIN_LITERAL,
  LANG_LITERAL,
  DATATYPE_LITERAL,
  ERROR = 126,
  TABLE_END = 127,
}

function* range(n: number) {
  for (let i = 0; i < n; i++) yield i;
}

/**
 * Reader for SPARQL Binary RDF Table Results format.
 */
class SparqlBrtrReader {
  private readonly decoder = new TextDecoder("utf-8");
  readonly #namespaces: Map<number, string> = new Map();
  /** Version of the protocol. (Not currently used) */
  readonly #version: number;
  readonly #column_count: number;
  readonly #keys: readonly string[];
  readonly #data: Uint8Array;

  #pos: number = 0;

  public get keys(): readonly string[] {
    return [...this.#keys];
  }

  constructor(data: Uint8Array) {
    this.#data = data;

    // Read header as soon as constructed
    const marker_bytes = this.read(4);
    const marker = this.decoder.decode(marker_bytes);
    if (marker != "BRTR") {
      throw new Error("First 4 bytes in should be BRTR");
    }
    this.#version = this.read_int();
    this.#column_count = this.read_int();
    this.#keys = [...range(this.#column_count)].map(() => this.read_string());
  }

  // All reads go through here.
  private read(size: number): Uint8Array {
    if (this.#pos >= this.#data.length) {
      throw new Error("ReadPastEOF");
    }
    const bytes = this.#data.subarray(this.#pos, this.#pos + size);
    this.#pos += size; // MUTATION
    return bytes;
  }

  // Read the rows
  *parse(): IterableIterator<SparqlResultsBinding> {
    const values = Object.create(null);
    while (true) {
      for (const i of range(this.#column_count)) {
        const value = this.read_value();
        if (value === null) {
          delete values[this.#keys[i]];
          continue;
        }
        if (value === REPEAT) {
          // REPEAT here is like skip...the val is already in this.values[i]
          continue;
        }
        if (value === EOF) {
          return;
        }
        values[this.#keys[i]] = value;
      }
      yield { ...values };
    }
  }

  // ====== PRIVATE IMPLEMENTATION

  private read_byte(): number {
    const bytes = this.read(1);
    return bytes[0];
  }

  /** Read a 32-bit big-endian integer. */
  private read_int(): number {
    const [b3, b2, b1, b0] = this.read(4);
    const value = (b3 << 24) | (b2 << 16) | (b1 << 8) | b0;
    return value;
  }

  /** Read a length-prefixed UTF-8 string. */
  private read_string(): string {
    const length = this.read_int();
    const bytes = this.read(length);
    const text = this.decoder.decode(bytes);
    return text;
  }

  private read_value(): null | RDFTerm | typeof REPEAT | typeof EOF {
    // The loop is used only because namespace definitions may appear any number
    // of times before a value.
    while (true) {
      const record_type = this.read_byte();
      switch (record_type) {
        case RecordTypeMarker.NULL:
          return null;

        case RecordTypeMarker.REPEAT:
          return REPEAT;

        case RecordTypeMarker.QNAME:
          return {
            type: "uri",
            value: this.#namespaces.get(this.read_int()) + this.read_string(),
          };

        case RecordTypeMarker.URI:
          return {
            type: "uri",
            value: this.read_string(),
          };

        case RecordTypeMarker.BNODE:
          return {
            type: "bnode",
            value: this.read_string(),
          };

        case RecordTypeMarker.PLAIN_LITERAL:
          return {
            type: "literal",
            value: this.read_string(),
          };

        case RecordTypeMarker.LANG_LITERAL:
          return {
            type: "literal",
            value: this.read_string(),
            "xml:lang": this.read_string(),
          };

        case RecordTypeMarker.DATATYPE_LITERAL:
          const literal_value = this.read_string();
          const datatype = this.read_value() as RDFTerm;
          return {
            type: "literal",
            value: literal_value,
            // The JSON format doesn't include datatype when it's string.
            ...(datatype &&
              datatype.value !== "http://www.w3.org/2001/XMLSchema#string" && {
                datatype: datatype.value,
              }),
          };

        case RecordTypeMarker.ERROR:
          const error_type = this.read_byte();
          const error_text = this.read_string();
          if (error_type === 1) {
            throw new Error(`Malformed query: ${error_text}`);
          }
          if (error_type === 2) {
            throw new Error(`Query evaluation error: ${error_text}`);
          }
          throw new Error(error_text);

        case RecordTypeMarker.TABLE_END:
          return EOF;

        case RecordTypeMarker.NAMESPACE:
          this.#namespaces.set(this.read_int(), this.read_string());
          break;

        default:
          throw new Error(`Undefined record type: ${record_type}`);
      }
    }
  }
}

/**
 * Read data from the SPARQL Binary RDF Table Results (BRTR) format into SPARQL
 * JSON Select results form.
 */
export const read_brtr = (data: Uint8Array): SparqlSelectResults => {
  const reader = new SparqlBrtrReader(data);
  const bindings = [...reader.parse()];
  return { head: { vars: reader.keys }, results: { bindings } };
};
