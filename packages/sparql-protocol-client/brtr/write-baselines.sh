#!/bin/sh

# Create a set of baseline tests for a SPARQL BRTR reader.
#
# RESETS all content in the output directory.
#
# Requires a connection to a SPARQL engine that supports this output format.


set -e

endpoint="${1:-$SPARQL_ENDPOINT}"

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
queries_dir="$this_script_dir/queries"
results_dir="$this_script_dir/baselines"

if [ -z "$endpoint" ]; then
  >&2 echo 'create-baselines: abort: a SPARQL endpoint is required.'
  exit 1
fi

rm -rf "$results_dir"
mkdir -p "$results_dir"

do_update() {
  local name="$1"
  local file="$this_script_dir/$name.sparql"
  local update=$(cat "$file")
  curl \
    --silent \
    --data-urlencode "update=$update" \
    "$endpoint"
}

do_query() {
  local file="$1"
  local name="${file##*/}"
  local root="${name%.sparql}"
  local json_file="$results_dir/$root.json"
  local binary_file="$results_dir/$root.brtr"
  local query=$(cat "$file")
  
  curl \
    --silent \
    --data-urlencode "query=$query" \
    --header 'Accept: application/sparql-results+json' \
    "$endpoint" \
    > "$json_file"
  
  curl \
    --silent \
    --data-urlencode "query=$query" \
    --header 'Accept: application/x-binary-rdf-results-table' \
    "$endpoint" \
    > "$binary_file"
}

do_update 'setup'

for file in "$queries_dir"/*.sparql; do
  echo "$file"
  do_query "$file"
done

do_update 'cleanup'

exit 0
