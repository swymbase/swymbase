# SPARQL Binary RDF Results Table

This directory contains assets used to create baseline tests for decoding the
`application/x-binary-rdf-results-table` format (henceforth, “BRTR”).

## Background

SPARQL `SELECT` results are available in several standard formats, including
[JSON](https://www.w3.org/TR/sparql11-results-json/) and
[CSV/TSV](https://www.w3.org/TR/2013/REC-sparql11-results-csv-tsv-20130321/).
These formats are straightforward to process because of widely-available
platform support for their syntax.

However, these text-based formats are not optimal for transmission. The CSV and
TSV formats are lossy (in that they don't capture all details of a term), and so
are not usable in many situations. The JSON format is highly verbose, and
although compression can alleviate this, clients cannot always guarantee that
the server will apply a given transfer encoding.

Support for a binary format is therefore desirable, particularly for large
result sets.

## Documentation

BRTR does not appear to be an official format.

The closest thing to official documentation appears to be the page
[“Implementing SPARQL Binary Table Results Format”](https://www.w3.org/2011/08/BinaryTableResults/),
(which points to
[a C++ reader](https://www.w3.org/2011/08/BinaryTableResults/BRTparser.cpp)).
This in turn appears to be based on a “Sesame” feature.
([archive link](https://web.archive.org/web/20080724192943/http://www.openrdf.org/doc/sesame/api/org/openrdf/sesame/query/BinaryTableResultConstants.html))

It is mentioned as being
[supported by Wikidata](https://en.wikibooks.org/wiki/SPARQL/Wikidata_Query_Service#Supported_formats),
though without a reference to the basis of the format.

The present implementation is adapted from
[a Python implementation in the rdfalchemy package](https://rdfalchemy.readthedocs.io/en/latest/_modules/rdfalchemy/sparql/parsers.html#_BRTRSPARQLHandler).

### Additional resources

- [RDF Binary using Apache Thrift](https://jena.apache.org/documentation/io/rdf-binary.html).
  The parent document
  [Reading and Writing RDF in Apache Jena](https://jena.apache.org/documentation/io/)
  does list “RDF Binary” as a “format supported by Jena.” However, it's not
  clear whether Fuseki supports any binary output formats, nor how such would be
  specified.

- `w3c/sparql-12`
  [Binary encodings for SPARQL results #82](https://github.com/w3c/sparql-12/issues/82)

- [RDF4J Binary RDF Format](https://rdf4j.org/documentation/reference/rdf4j-binary/)
  describes a “a custom binary RDF serialization format”. Notably, this
  accomodates RDF\* by supporting structured triples as values.

  - Also from RDF4J but apparently unrelated is
    [this class](https://rdf4j.org/javadoc/latest/org/eclipse/rdf4j/query/resultio/binary/BinaryQueryResultConstants.html),
    which looks very much like BRTR, except that it uses a 13-byte header
    (adding a byte 9 for “flags”).

- [“Binary RDF Representation for Publication and Exchange (HDT)”](https://www.w3.org/Submission/HDT/),
  a 2011 W3C member submission. I found this by way of the link to
  http://www.rdfhdt.org/ (“Header Dictionary Triples”) at the above thread on
  SPARQL-12, but I have not seen it mentioned by any implementations.

## Testing

Amazon Neptune supports this output format, but (to wit) Jena does not. Since
this project's tests usually run against Jena, a more engine-independent method
of confirming the reader implementation is needed.

This directory contains a script for producing query results using an available
Neptune instance. Those results serve as the basis for test cases that can be
run offline.

The strategy is:

- create queries that will return a variety of binding results
- run each query to both the binary format and the standard JSON results format
- save the results to a location usable by the test program
- include a test that validates the binary reader's results against the JSON
  results

For most tests, it's not necessary to write to the Neptune instance, since
`SELECT` queries can provide their own bindings from literal data. However,
blank nodes cannot be produced from literal bindings. For that reason, the
`write-baselines` script executes a [setup command](./setup.sparql) before the
test is run. All test triples are confined to a named graph, which is deleted
afterwards by a [cleanup command](./cleanup.sparql).
