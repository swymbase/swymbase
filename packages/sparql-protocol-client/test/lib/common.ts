import ava, { Implementation, TestInterface } from "ava";
import { send } from "@swymbase/sparql-protocol-client";
import { get_config } from "./config";

const test = ava as TestInterface<KnowledgeBaseTestContext>;

// Context shared by all tests in this package?  or just those that use this
export interface KnowledgeBaseTestContext {
  /** Generated IRI for named graph to contain data for this test run. */
  readonly graph_name: string;

  readonly sparql_endpoint: string;

  // write();
}

// Helpers related to named graph
const { SPARQL_ENDPOINT } = get_config("SPARQL_ENDPOINT");

const pseudo_uuid = () => Math.floor(1e12 * Math.random());

export const get_endpoint = () => SPARQL_ENDPOINT;

export const get_test_namespace = () => `http://test.com/${pseudo_uuid()}/`;

const generate_test_graph_name = () => `http://example.com/${pseudo_uuid()}`;

export const create_test_context = (): KnowledgeBaseTestContext => {
  return {
    sparql_endpoint: SPARQL_ENDPOINT,
    graph_name: generate_test_graph_name(),
  };
};

const knowledge_before: Implementation<KnowledgeBaseTestContext> = async (
  t
) => {
  t.context = create_test_context();
};

const knowledge_after: Implementation<KnowledgeBaseTestContext> = async (t) => {
  const drop = `DROP GRAPH <${t.context.graph_name}>`;
  const drop_result = await send({
    endpoint: t.context.sparql_endpoint,
    operation: { update: drop },
  });

  if (!drop_result.success) {
    t.log(t.context);
    t.log(drop_result.error);
    throw new Error(`Cleanup failed for ${t.context.graph_name}`);
  }
};

test.beforeEach(knowledge_before);
test.afterEach.always(knowledge_after);

export { test };
