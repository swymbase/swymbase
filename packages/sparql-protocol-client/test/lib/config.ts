import { dictionary_from } from "./dictionary_from";
import { assert } from "./assert";

function get_env(key: string): string {
  const value = process.env[key];
  assert(value, `Expected a definition for ${key}`);
  return value;
}

export const get_config = <K extends string>(
  ...keys: K[]
): Readonly<Record<K, string>> => dictionary_from(keys, get_env);
