import { ExecutionContext } from "ava";

// function assert_defined<T>(input: T | undefined): asserts input is T {
//   if (input === undefined) throw new Error("Expected a definition for");
// }

export function assert(value: any, message?: string): asserts value {
  if (!value) throw new Error(message || `Expected truthy, got ${value}`);
}

// Temporary alternatives to AVA assert functions.  These functions include
// assert signatures.  See https://github.com/avajs/ava/issues/2411

export function tassert(
  t: ExecutionContext<any>,
  value: any,
  message?: string
): asserts value {
  t.assert(value, message);
}
