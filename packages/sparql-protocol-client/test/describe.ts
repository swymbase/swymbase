import { send } from "@swymbase/sparql-protocol-client";
import * as jsonld from "jsonld";
import { tassert } from "./lib/assert";
import { get_test_namespace, test } from "./lib/common";

test("DESCRIBE query", async (t) => {
  const endpoint = t.context.sparql_endpoint;
  const graph_iri = t.context.graph_name;
  const NAMESPACE = get_test_namespace();
  const PREAMBLE = `prefix : <${NAMESPACE}> \n `;

  const given = `
    GRAPH <${graph_iri}> {
      :Bruce a :Dog ; :label "Brucie" ; :owner :Marcus ; .
    }
  `;

  // Write assumptions
  const update = `${PREAMBLE} INSERT DATA { ${given} }`;
  const wrote = await send({ operation: { update }, endpoint });
  if (!wrote.success) t.log(wrote);
  t.assert(wrote.success, "Failed to write prerequisites");

  // Read it back
  const query = `${PREAMBLE} DESCRIBE :Bruce`;
  const queried = await send({
    operation: { query },
    endpoint,
    accept: ["application/ld+json"],
  });
  if (!queried.success) t.log(queried);
  tassert(t, queried.success);

  // Validate response expectations
  t.assert(queried.response);
  t.assert(queried.response.format);
  t.assert(queried.response.format === "json_ld");

  t.log(queried.response);

  const result = queried.response.value as object;
  const expanded = await jsonld.expand(result);
  t.is(expanded.length, 1);
  const [record] = expanded;
  t.is(record["@id"], `${NAMESPACE}Bruce`);
  t.deepEqual(record["@type"], [`${NAMESPACE}Dog`]);
  t.deepEqual(record[`${NAMESPACE}label`], [{ "@value": "Brucie" }]);
  t.deepEqual(record[`${NAMESPACE}owner`], [{ "@id": `${NAMESPACE}Marcus` }]);
});
