import { formatted, send } from "@swymbase/sparql-protocol-client";
import { deepStrictEqual } from "assert";
import { test } from "./lib/common";

test("returns select bindings for `select_results` format", async (t) => {
  // === SETUP
  // Write test triples to a test graph
  const endpoint = t.context.sparql_endpoint;
  const graph_iri = t.context.graph_name;
  const update = `
BASE <http://example.com/select_results/>
INSERT DATA {
  GRAPH <${graph_iri}> {
    <Bob> <loves> <Alice> .
    <Alice> <loves> <Carol> .
    <Carol> <loves> <Bob> .
    <Alice> <likes> <kale> .
  }
}`;
  try {
    const insert_result = await send({ endpoint, operation: { update } });
    if (!insert_result.success) {
      t.log(insert_result);
      throw new Error("operation failed");
    }

    // === OPERATION
    // Select with bindings
    const query = `
BASE <http://example.com/select_results/>
SELECT * WHERE { GRAPH <${graph_iri}> { ?s <loves> ?o } }`;
    const result = await formatted("select_results", {
      endpoint,
      operation: { query },
    });
    if (!result.value) throw new Error("Response value is undefined!");

    deepStrictEqual(
      new Set(result.value.results.bindings),
      new Set([
        {
          s: { type: "uri", value: "http://example.com/select_results/Alice" },
          o: { type: "uri", value: "http://example.com/select_results/Carol" },
        },
        {
          s: { type: "uri", value: "http://example.com/select_results/Bob" },
          o: { type: "uri", value: "http://example.com/select_results/Alice" },
        },
        {
          s: { type: "uri", value: "http://example.com/select_results/Carol" },
          o: { type: "uri", value: "http://example.com/select_results/Bob" },
        },
      ])
    );
  } finally {
    // === CLEANUP
    // Delete test graph
    const cleanup_result = await send({
      endpoint,
      operation: { update: `DROP GRAPH <${graph_iri}>` },
    });
    if (!cleanup_result.success) throw new Error("cleanup failed");
  }

  t.pass("Test completed with native assertions.");
});
