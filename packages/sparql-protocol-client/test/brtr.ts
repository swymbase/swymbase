// Use of BRTR reader
import {
  read_brtr,
  send,
  SparqlSelectResults,
} from "@swymbase/sparql-protocol-client";
import { deepStrictEqual } from "assert";
import { test } from "./lib/common";

// This test depends on an engine that supports BRTR.
test("returns expected BRTR buffer when acceptable", async (t) => {
  // === SETUP
  // Write test triples to a test graph
  const endpoint = t.context.sparql_endpoint;
  const graph_iri = t.context.graph_name;
  const update = `
BASE <http://example.com/BRTR/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA {
  GRAPH <${graph_iri}> {
    <Alice> a foaf:Person ; foaf:name "Alice" ; foaf:knows <Bob> .
    <Bob> a foaf:Person ; foaf:name "Bob" ; foaf:knows <Alice> .
  }
}`;
  try {
    const insert_result = await send({ endpoint, operation: { update } });
    if (!insert_result.success) {
      t.log(insert_result);
      throw new Error("operation failed");
    }

    // === OPERATION
    // Select with bindings
    const query = `
BASE <http://example.com/select_results/>
SELECT * WHERE { GRAPH <${graph_iri}> { ?s ?p ?o } }`;
    const result = await send({
      accept: [
        "application/x-binary-rdf-results-table",
        "application/sparql-results+json",
      ],
      endpoint,
      operation: { query },
    });
    if (!result.success) {
      t.log(result);
      throw new Error("Response value is undefined!");
    }

    let select_results: SparqlSelectResults;
    const { response } = result;
    if (response.format === "brtr") {
      const { value } = response;
      select_results = read_brtr(value);
    } else {
      console.warn("WARN: Skipping test: engine does not support BRTR");
      if (response.format !== "results" || "boolean" in response.value) {
        throw new Error(
          "Expected JSON Select results format when BRTR not supported"
        );
      }
      select_results = response.value;
    }

    t.deepEqual(select_results.head, { vars: ["s", "p", "o"] });

    deepStrictEqual(
      new Set(select_results.results.bindings),
      new Set([
        {
          s: { type: "uri", value: "http://example.com/BRTR/Alice" },
          p: {
            type: "uri",
            value: "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
          },
          o: { type: "uri", value: "http://xmlns.com/foaf/0.1/Person" },
        },
        {
          s: { type: "uri", value: "http://example.com/BRTR/Alice" },
          p: { type: "uri", value: "http://xmlns.com/foaf/0.1/name" },
          o: { type: "literal", value: "Alice" },
        },
        {
          s: { type: "uri", value: "http://example.com/BRTR/Alice" },
          p: { type: "uri", value: "http://xmlns.com/foaf/0.1/knows" },
          o: { type: "uri", value: "http://example.com/BRTR/Bob" },
        },
        {
          s: { type: "uri", value: "http://example.com/BRTR/Bob" },
          p: {
            type: "uri",
            value: "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
          },
          o: { type: "uri", value: "http://xmlns.com/foaf/0.1/Person" },
        },
        {
          s: { type: "uri", value: "http://example.com/BRTR/Bob" },
          p: { type: "uri", value: "http://xmlns.com/foaf/0.1/name" },
          o: { type: "literal", value: "Bob" },
        },
        {
          s: { type: "uri", value: "http://example.com/BRTR/Bob" },
          p: { type: "uri", value: "http://xmlns.com/foaf/0.1/knows" },
          o: { type: "uri", value: "http://example.com/BRTR/Alice" },
        },
      ])
    );
  } finally {
    // === CLEANUP
    // Delete test graph
    const cleanup_result = await send({
      endpoint,
      operation: { update: `DROP GRAPH <${graph_iri}>` },
    });
    if (!cleanup_result.success) throw new Error("cleanup failed");
  }

  t.pass("Test completed with native assertions.");
});
