import { formatted, send } from "@swymbase/sparql-protocol-client";
import { ExecutionContext } from "ava";
import { tassert } from "./lib/assert";
import {
  get_endpoint,
  get_test_namespace,
  KnowledgeBaseTestContext,
  test,
} from "./lib/common";

const ask = async (
  t: ExecutionContext<KnowledgeBaseTestContext>,
  spec: { given: string; query: string; expect: boolean }
) => {
  const { given, query, expect } = spec;
  const endpoint = get_endpoint();
  const NAMESPACE = get_test_namespace();
  const PREFIX = `prefix : <${NAMESPACE}> \n `;

  // WRITE the given to a named graph
  const written = await send({
    operation: {
      update: `${PREFIX} INSERT DATA { GRAPH <${t.context.graph_name}> { ${given} } }`,
    },
    endpoint,
  });
  t.assert(written.success, "Failed to write prerequisites");

  const answer = await formatted("boolean", {
    operation: {
      query: `${PREFIX} ASK { GRAPH <${t.context.graph_name}> { ${query} } }`,
    },
    endpoint,
  });

  if (answer.value === undefined) {
    const backup = await send({ operation: { query }, endpoint });
    t.log(backup);
  }

  t.is(answer.value, expect);
};

test("ASK query returning false", ask, {
  given: ":Roses :are :Red",
  query: ":Roses :are :Blue",
  expect: false,
});

test("ASK query returning true", ask, {
  given: ":Roses :are :Red",
  query: ":Roses :are :Red",
  expect: true,
});

test("ASK query returning JSON results object", async (t) => {
  const endpoint = get_endpoint();
  const NAMESPACE = get_test_namespace();
  const PREFIX = `prefix : <${NAMESPACE}> \n `;

  const given = `:Bob :loves :Alice . :Alice :loves :Carol`;

  const write_response = await send({
    operation: {
      update: `${PREFIX} INSERT DATA { GRAPH <${t.context.graph_name}> { ${given} } }`,
    },
    endpoint,
  });
  t.assert(write_response.success, "Failed to write prerequisites");

  const read_response = await send({
    operation: { query: `${PREFIX} ASK { :Alice :loves :Bob }` },
    endpoint,
  });
  tassert(t, read_response.success);
  tassert(t, read_response.response.format === "results");
  tassert(t, "boolean" in read_response.response.value);
  t.is(typeof read_response.response.value.boolean, "boolean");
  t.false(read_response.response.value.boolean);
});
