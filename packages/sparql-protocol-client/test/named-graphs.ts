import { send } from "@swymbase/sparql-protocol-client";
import { assert } from "./lib/assert";
import { test } from "./lib/common";

// Low-level tests.  These write to specifically-named graphs, which may have
// had data before.  We could delete any previous things by this name.

// This does not in fact work.  The quad syntax is permitted, but the graph
// identifier appears to be ignored.
test.skip("write and read a named graph using quad", async (t) => {
  const update = `
    INSERT DATA {
      <http://example.com/Subject_1q>
      <http://example.com/Predicate_1q>
      <http://example.com/Object_1q>
      <${t.context.graph_name}> .
    }
  `;

  const insert_result = await send({
    operation: { update },
    endpoint: t.context.sparql_endpoint,
  });
  t.assert(insert_result.success);

  const query = `
    SELECT *
    WHERE { GRAPH <${t.context.graph_name}> { ?s ?p ?o } }
    LIMIT 50
  `;
  const select_result = await send({
    operation: { query },
    endpoint: t.context.sparql_endpoint,
  });

  if (!select_result.success) t.log(select_result.code, select_result.error);
  assert(select_result.success);
  const { response } = select_result;
  if (response.format === "results") {
    if ("results" in response.value) {
      const { results } = response.value;
      const { bindings } = results;

      t.is(bindings.length, 1);
      t.deepEqual(bindings, [
        {
          s: { type: "uri", value: "http://example.com/Subject_1q" },
          p: { type: "uri", value: "http://example.com/Predicate_1q" },
          o: { type: "uri", value: "http://example.com/Object_1q" },
          g: { type: "uri", value: "http://example.com/Graph_1q" },
        },
      ]);
    }
  }
});

test("write and read using a named graph reference in query", async (t) => {
  const update = `
    INSERT DATA { 
      GRAPH <${t.context.graph_name}> {
        <http://example.com/Test1Subject>
        <http://example.com/Test1Predicate> 
        <http://example.com/Test1Object>
        .
      }
    }
  `;

  const insert_result = await send({
    operation: { update },
    endpoint: t.context.sparql_endpoint,
  });
  t.assert(insert_result.success);

  const query = `
    SELECT *
    WHERE { GRAPH <${t.context.graph_name}> { ?s ?p ?o } }
    LIMIT 50
  `;
  const select_result = await send({
    operation: { query },
    endpoint: t.context.sparql_endpoint,
  });

  assert(select_result.success);
  const { response } = select_result;
  if (response.format !== "results")
    t.fail("Expected `results` to be response type");
  else if (!("results" in response.value)) t.fail("Expected SELECT results");
  else {
    const { results } = response.value;
    const { bindings } = results;

    t.is(bindings.length, 1);
    t.deepEqual(bindings, [
      {
        s: { type: "uri", value: "http://example.com/Test1Subject" },
        p: { type: "uri", value: "http://example.com/Test1Predicate" },
        o: { type: "uri", value: "http://example.com/Test1Object" },
      },
    ]);
  }
});

// This doesn't work as such either.
test.skip("write and read using a protocol graph reference", async (t) => {
  const update = `
    INSERT DATA { 
      <http://example.com/Subject_1p>
      <http://example.com/Predicate_1p> 
      <http://example.com/Object_1p>
      <${t.context.graph_name}>
      .
    }
  `;

  const insert_result = await send({
    operation: { update, using_named_graph_uri: t.context.graph_name },
    endpoint: t.context.sparql_endpoint,
  });
  t.assert(insert_result.success);

  const query = `SELECT * WHERE { GRAPH <${t.context.graph_name}> { ?s ?p ?o } } LIMIT 50`;
  const select_result = await send({
    operation: { query },
    endpoint: t.context.sparql_endpoint,
  });

  assert(select_result.success);
  const { response } = select_result;
  if (response.format !== "results")
    t.fail("Expected `results` to be response type");
  else if (!("results" in response.value)) t.fail("Expected SELECT results");
  else {
    const { results } = response.value;
    const { bindings } = results;

    if (bindings.length !== 1) t.log(select_result);
    t.is(bindings.length, 1);
    t.deepEqual(bindings, [
      {
        s: { type: "uri", value: "http://example.com/Subject_1p" },
        p: { type: "uri", value: "http://example.com/Predicate_1p" },
        o: { type: "uri", value: "http://example.com/Object_1p" },
      },
    ]);
  }
});
