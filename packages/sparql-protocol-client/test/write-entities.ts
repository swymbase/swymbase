import { send } from "@swymbase/sparql-protocol-client";
import { tassert } from "./lib/assert";
import { test } from "./lib/common";

test("write entities", async (t) => {
  const insert_query_1 = `
    prefix : <http://example.com/>
    INSERT DATA {
      GRAPH <${t.context.graph_name}> {
        :a :p :b .
        :b :p :c .
        :c :p :a .
        :d :p :a .
        :e :p :a .
      }
    }
  `;

  const result = await send({
    operation: { update: insert_query_1 },
    endpoint: t.context.sparql_endpoint,
  });
  t.assert(result.success);

  // Now query and make sure that you get that back

  const selected = await send({
    operation: {
      query: `
        prefix : <http://example.com/>
        select ?s where { graph <${t.context.graph_name}> {?s :p ?o} } limit 10
      `,
    },
    endpoint: t.context.sparql_endpoint,
  });

  tassert(t, selected.success);
  const { response } = selected;
  if (response.format !== "results")
    t.fail("Expected `results` to be response type");
  else if (!("results" in response.value)) t.fail("Expected SELECT results");
  else {
    const { head, results } = response.value;

    t.is(typeof head, "object");
    t.assert(Array.isArray(head.vars));
    t.deepEqual(head.vars, ["s"]);

    const { bindings } = results;
    t.assert(Array.isArray(bindings));
    t.assert(bindings.length > 0);
    t.is(bindings.length, 5);

    // Doesn't work because still treats as ordered
    /*
    t.deepEqual(
      new Set(bindings),
      new Set([
        { s: { type: "uri", value: "http://example.com/a" } },
        { s: { type: "uri", value: "http://example.com/b" } },
        { s: { type: "uri", value: "http://example.com/c" } }
      ])
    );
    */

    // Unfortunate alternative to above
    for (const letter of ["a", "b", "c", "d", "e"])
      t.assert(
        bindings.find(
          ({ s }: any) =>
            s && s.type === "uri" && s.value === `http://example.com/${letter}`
        )
      );
  }
});
