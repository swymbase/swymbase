import {
  create_http_request,
  SparqlRequest,
} from "@swymbase/sparql-protocol-client";
import test, { ExecutionContext } from "ava";
import { URL } from "url";

const as_array = <T>(t: T | readonly T[] | undefined): readonly T[] =>
  Array.isArray(t) ? t : t === undefined ? [] : [t];

// Test all invariants
const roundtrip = (t: ExecutionContext, sparql: SparqlRequest) => {
  const request = create_http_request(sparql);

  if (sparql.method === "get") t.is(request.method, "GET");
  if (sparql.method === "post_direct") t.is(request.method, "POST");
  if (sparql.method === "post_with_encoded_params")
    t.is(request.method, "POST");

  const url = new URL(request.url);
  const [path] = url.pathname.split("?");
  t.is(
    url.origin + path,
    sparql.endpoint,
    "HTTP request must be addressed to endpoint"
  );

  // Method-specific
  if (sparql.method === "get") {
    t.false(request.headers.has("Content-Type"));
  } else if (sparql.method === "post_with_encoded_params") {
    t.is(
      request.headers.get("Content-Type"),
      "application/x-www-form-urlencoded"
    );
  }

  // Query-specific
  if ("query" in sparql.operation) {
    const { operation } = sparql;

    if (sparql.method === "get")
      t.is(url.searchParams.get("query"), operation.query);

    if (sparql.method === "post_direct")
      t.is(request.headers.get("Content-Type"), "application/sparql-query");

    // TODO: Here and in update doesn't decode body for POST direct

    if (
      sparql.method === "get" ||
      sparql.method === "post_with_encoded_params"
    ) {
      t.deepEqual(
        url.searchParams.getAll("default-graph-uri"),
        as_array(operation.default_graph_uri)
      );
      t.deepEqual(
        url.searchParams.getAll("named-graph-uri"),
        as_array(operation.named_graph_uri)
      );
    }
  }

  // Update-specific
  if ("update" in sparql.operation) {
    const { operation } = sparql;

    if (sparql.method === "get")
      t.is(url.searchParams.get("update"), operation.update);

    if (sparql.method === "post_direct")
      t.is(request.headers.get("Content-Type"), "application/sparql-update");

    if (
      sparql.method === "get" ||
      sparql.method === "post_with_encoded_params"
    ) {
      t.deepEqual(
        url.searchParams.getAll("using-graph-uri"),
        as_array(operation.using_graph_uri)
      );
      t.deepEqual(
        url.searchParams.getAll("using-named-graph-uri"),
        as_array(operation.using_named_graph_uri)
      );
    }
  }
};

const endpoint = "http://example.com/sparql";
const query = `SELECT * WHERE { ?s <http://example.com/foobar> "test"@en  }`;
const update = `INSERT DATA { _:b1 <http://example.com/foobar> "test"@en  }`;

test("query with default method", roundtrip, {
  operation: { query },
  endpoint,
});

test("update with default method", roundtrip, {
  operation: { update },
  endpoint,
});

test("query with GET", roundtrip, {
  operation: { query },
  endpoint,
  method: "get",
});

test("query using POST direct", roundtrip, {
  endpoint,
  operation: { query },
  method: "post_direct",
});

test("update using POST direct", roundtrip, {
  endpoint,
  operation: { update },
  method: "post_direct",
});

test("query using POST with params", roundtrip, {
  endpoint,
  operation: { query },
  method: "post_with_encoded_params",
});

test("update using POST with params", roundtrip, {
  endpoint,
  operation: { update },
  method: "post_with_encoded_params",
});

test("query with default graph URI", roundtrip, {
  endpoint,
  operation: { query, default_graph_uri: "http://default.com/" },
});

test("query using GET with multiple default graph URI's", roundtrip, {
  endpoint,
  operation: {
    query,
    default_graph_uri: ["http://default.com/1", "http://default.com/2"],
  },
  method: "get",
});

test("query using GET with named graph URI", roundtrip, {
  endpoint,
  operation: { query, named_graph_uri: "http://default.com/" },
  method: "get",
});

test("query using GET with multiple named graph URI's", roundtrip, {
  endpoint,
  operation: {
    query,
    named_graph_uri: ["http://default.com/1", "http://default.com/2"],
  },
  method: "get",
});

test("query using GET with default and named graph URI's", roundtrip, {
  endpoint,
  operation: {
    query,
    default_graph_uri: "http://default.com/",
    named_graph_uri: "http://named.com/",
  },
  method: "get",
});

test("query using POST with default graph URI", roundtrip, {
  endpoint,
  operation: { query, default_graph_uri: "http://default.com/" },
  method: "post_direct",
});

test("query using POST with multiple default graph URI's", roundtrip, {
  endpoint,
  operation: {
    query,
    default_graph_uri: ["http://default.com/1", "http://default.com/2"],
  },
  method: "post_direct",
});

test("query using POST with named graph URI", roundtrip, {
  endpoint,
  operation: { query, named_graph_uri: "http://default.com/" },
  method: "post_direct",
});

test("query using POST with multiple named graph URI's", roundtrip, {
  endpoint,
  operation: {
    query,
    named_graph_uri: ["http://example.com/1", "http://example.com/2"],
  },
  method: "post_direct",
});

test("query using POST with default and named graph URI's", roundtrip, {
  endpoint,
  operation: {
    query,
    default_graph_uri: "http://default.com/",
    named_graph_uri: "http://named.com/",
  },
  method: "post_direct",
});
