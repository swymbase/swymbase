import { test } from "./lib/common";

test("test context is provided", (t) => {
  t.is(typeof t.context.sparql_endpoint, "string");
  t.is(typeof t.context.graph_name, "string");
});
