import { send } from "@swymbase/sparql-protocol-client";
import { assert } from "./lib/assert";
import { test } from "./lib/common";

test("query specifying default graph via protocol", async (t) => {
  const insert_with_named = `
    INSERT DATA { 
      GRAPH <${t.context.graph_name}> {
        <http://example.com/Test1Subject>
        <http://example.com/Test1Predicate> 
        <http://example.com/Test1Object>
        .
      }
    }
  `;

  const insert_result = await send({
    operation: { update: insert_with_named },
    endpoint: t.context.sparql_endpoint,
  });
  t.assert(insert_result.success);

  const select_result = await send({
    operation: {
      query: `select * where { ?s ?p ?o } limit 10`,
      default_graph_uri: t.context.graph_name,
    },
    endpoint: t.context.sparql_endpoint,
  });

  if (!select_result.success) {
    t.log(select_result.code, select_result.error);
    t.fail("Couldn't get result of SELECT");
  }
  t.log("SUCC", select_result);

  assert(select_result.success);
  const { response } = select_result;
  if (response.format === "results") {
    if ("results" in response.value) {
      const { results } = response.value;
      const { bindings } = results;
      t.log(bindings);

      t.is(bindings.length, 1);
      t.deepEqual(bindings, [
        {
          s: { type: "uri", value: "http://example.com/Test1Subject" },
          p: { type: "uri", value: "http://example.com/Test1Predicate" },
          o: { type: "uri", value: "http://example.com/Test1Object" },
        },
      ]);
    }
  }

  const {} = select_result;
});
