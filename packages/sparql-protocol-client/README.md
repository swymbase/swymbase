# TypeScript SPARQL protocol client

This package defines a
[SPARQL Protocol](https://www.w3.org/TR/sparql11-protocol/) client in
TypeScript. This is a low-level interface, which marshals between various RDF
syntaxes and HTTP messages.

## Dependencies

- node-fetch

## Motivation

This fills a niche not currently occupied by any available package:

- provide first-class TypeScript interface
- hew very close to spec
- PROVISIONAL: special support for Amazon Neptune implementation where necessary
  (e.g. detailed error code definitions).

## Usage

`send` is a low-level function that formats and sends a request to a SPARQL
Protocol service. It returns a response object with the result in an indicated
format.

```typescript
import { send } from "../sparql-protocol-client/src/index";

const result = await send({
  request: {
    query: "SELECT ?s WHERE { ?s ?p ?o } LIMIT 10",
  },
  endpoint: "http://example.com/sparql",
});

if (result.success && result.response.format === "results")
  log(result.response.value);
```

`formatted` is a somewhat higer-level function that ensures that the response
matches a given format (and otherwise does the same thing as `send`).

```typescript
import { formatted } from "../sparql-protocol-client/src/index";

const result = await formatted("select_results", {
  request: {
    query: "SELECT ?s WHERE { ?s ?p ?o } LIMIT 10",
  },
  endpoint: "http://example.com/sparql",
});

if (result) {
  // This is known to be formed as SELECT results bindings
  log(result);
}
```

## Prior art

The [sparql-http-client](https://github.com/zazuko/sparql-http-client) by Thomas
Bergwinkl is a partial implementation of a SPARQL Protocol client in plain
JavaScript.

## Roadmap

Consider a Neptune extension for requesting
[execution plans](https://docs.aws.amazon.com/neptune/latest/userguide/sparql-explain.html),
which is done by adding an `explain={mode}`

### Use dependency injection for HTTP client

This package uses a
[Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) client
with the idea that it could be usable from browser environments. However, as
things stand, it currently takes a hard dependency on
[`node-fetch`](https://www.npmjs.com/package/node-fetch).

Related to this, it should also be possible to provide some of the core
functionality as mappings between SPARL Protocol requests and HTTP requests in
an implementation-agnostic form.
