# JSON:API client

This package provides a browser-compatible (fetch-based) JSON:API client that
implements a def.codes record store interface.

## Roadmap

Use dependency injection to provide Fetch implementation. The dependency on
global fetch is the reason [this package's tsconfig](./tsconfig.json) currently
includes the `DOM` lib.

## Known issues

### Multi-valued media properties

Currently, the swymbase API only supports media interop for single-valued
properties. This API client has the same limitation.

If swymbase were to add media support for multi-valued properties, it would
likely mean a change (or at least an extension) to the API and adding support
would require a corresponding extension here. For example, the `meta.media`
object could be extended to allow array values, which would have to specify the
IRI (in practice, ARN) of the property value being targeted.
