import type { EntityFrom, MetaInterfaces } from "@swymbase/metatype-core";

export interface TypedRecord {
  readonly type: string;
  [property: string]: any;
}

export interface IdRecord {
  readonly id: string;
  [property: string]: any;
}

// Other than `data`, this is very similar to the `meta.media` entry
// recognized by the swymbase JSON API.
//
// Not JSON compatible because of Blob
export interface MediaRequest {
  // Key is the record property
  readonly [property: string]: {
    readonly media_type: string;
    readonly data: Blob;
  };
}

// Would this be different for POST/PUT than for PATCH, where there may or may
// not already be images?
export interface WithMedia {
  // Optional attachments.
  readonly media?: MediaRequest;
}

// See also @def.codes/core
export interface ErrorResponse {
  type: "ErrorResponse";
}

export interface GetRecordRequest<T = string> {
  readonly ofType: T;
  readonly target: string;
  // Default, empty array (no includes)
  // NAME: should just be `include`?
  readonly includeRelated?: readonly string[];
}
export interface GetRecordResult<T extends keyof S, S extends MetaInterfaces> {
  readonly type: "GetRecordResult";
  // In practice, we know `type` will always be set in response.
  readonly record: { readonly type: T } & EntityFrom<S[T]>;
  readonly included?: readonly {
    id: string;
    type: string;
    [prop: string]: any;
  }[];
}

export interface ListRecordsRequest<
  T extends keyof S,
  S extends MetaInterfaces
> {
  readonly ofType: T;
  // Default, empty array (no includes)
  // NAME: should just be `include`?
  readonly includeRelated?: readonly string[];
  // Default, unfiltered
  readonly filter?: readonly any[][]; // definition in sparql-rest
  // Default false
  readonly includeCount?: boolean;
  // Default... 25?
  readonly limit?: number;
  // Default 0
  readonly offset?: number;
  // Default, unspecified
  readonly orderBy?: string;
  // Default, asc
  readonly orderDescending?: boolean;
  // Default, everything
  readonly fields?: Readonly<
    Record<keyof S, readonly (keyof S[T]["properties"])[]>
  >;
}
export interface ListRecordsResult<
  T extends keyof S,
  S extends MetaInterfaces
> {
  type: "ListRecordsResult";
  records: readonly EntityFrom<S[T]>[];
}

export interface PutRecordRequest extends WithMedia {
  readonly ofType: string;
  readonly target: string;
  readonly record: object;
}
export interface PutRecordResult {
  type: "PutRecordResult";
  // TODO: check JSON:API spec, is this expected?
  readonly record: object;
}

export interface PatchRecordRequest extends WithMedia {
  readonly ofType: string;
  readonly target: string;
  readonly record: object;
}
export interface PatchRecordResult {
  type: "PatchRecordResult";
  readonly record: object;
}

export interface PostRecordRequest extends WithMedia {
  readonly ofType: string;
  readonly record: TypedRecord;
}
export interface PostRecordResult {
  type: "PostRecordResult";
  readonly record: IdRecord & TypedRecord;
  // May need to modify this to add a stream for progress of media upload.
}

export interface DeleteRecordRequest {
  // readonly type: "DeleteRecordRequest";
  readonly ofType: string;
  readonly target: string;
}
export interface DeleteRecordResult {
  readonly type: "DeleteRecordResult";
}

type Res<T> = Promise<T | ErrorResponse>;

export interface IJsonApiClient<S extends MetaInterfaces> {
  getRecord<T extends keyof S & string>(
    request: GetRecordRequest<T>
  ): Res<GetRecordResult<T, S>>;

  listRecords<T extends keyof S & string>(
    request: ListRecordsRequest<T, S>
  ): Res<ListRecordsResult<T, S>>;

  patchRecord(request: PatchRecordRequest): Res<PatchRecordResult>;
  postRecord(request: PostRecordRequest): Res<PostRecordResult>;
  putRecord(request: PutRecordRequest): Res<PutRecordResult>;
  deleteRecord(request: DeleteRecordRequest): Res<DeleteRecordResult>;
}
