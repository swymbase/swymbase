import type { EntityFrom, MetaInterfaces } from "@swymbase/metatype-core";
import {
  collection_document_to_records,
  document_to_record,
  records_to_resource_objects,
  record_to_document,
} from "@swymbase/metatype-jsonapi";
import type * as API from "./api";

type Res<T> = Promise<T | API.ErrorResponse>;

/**
 * An asynchronous request-response interface that provides record-store
 * operations over a JSON:API.
 *
 * To some degree this is a general JSON:API client.  The special types of
 * several query parameters (e.g. `filter` and `include`) describe
 * @swymbase-specific usages.  However, the client is not generally in the
 * business of interpreting the parameters.  (`filter` is JSON encoded.)
 *
 * You can use this with a static record schema (à la @def.codes).  When
 * provided, the operations on this interface are typed according to the given
 * schema.  This works best when the same schema is also used by the target API.
 *
 * It supports media attachments using a swymbase-specific interpretation of
 * `meta.media`.
 *
 * Makes assumptions about the URL structure:
 * - that the base endpoint plus the type alias is the way to list for a type
 * - that the type endpoint plus the ID of a resource is the way to get it
 *
 * In general:
 * - map request into an (JSON:API) HTTP request
 *   - with JSON:API document as body (for write operations)
 * - execute the HTTP request
 * - read response body as JSON:API document
 * - map response document to response type
 *
 * Note that `ofType` refers to type *alias* here, that is, the key in the
 * schemata, as opposed to the IRI.
 *
 * You also need a way to get tokens for making the request.
 */
export class JsonApiClient<S extends MetaInterfaces>
  implements API.IJsonApiClient<S>
{
  protected readonly _schema: MetaInterfaces;

  // Assumes this is the `type` endpoint
  protected readonly _endpoint: string;
  protected readonly _get_token: () => Promise<string>;

  // `endpoint` is the URL of the service that handles type requests for this schema
  constructor(schema: S, endpoint: string, get_token: () => Promise<string>) {
    this._schema = schema;
    this._endpoint = endpoint;
    this._get_token = get_token;
  }

  async #base(headers?: Record<string, string>): Promise<RequestInit> {
    return {
      mode: "cors",
      headers: new Headers({
        Authorization: `Bearer ${await this._get_token()}`,
        Accept: "application/json",
        ...(headers ?? {}),
      }),
    };
  }

  public async getRecord<T extends keyof S & string>(
    request: API.GetRecordRequest<T>
  ): Res<API.GetRecordResult<T, S>> {
    const { ofType, target, includeRelated } = request;

    if (!ofType) throw new Error("MissingType");
    if (!target) throw new Error("MissingTarget");

    const url_base = this.get_resource_url(ofType, target);
    const params = new URLSearchParams();
    if (includeRelated) {
      params.append("include", includeRelated.join(","));
    }
    const url = `${url_base}?${params}`;
    const response = await fetch(url, await this.#base());
    const doc = await response.json();

    // Assumes a non-error response.
    // What does this return in the event of 404? 500, etc

    const record = document_to_record(this._schema, doc);

    if (!record) throw new Error("InvalidJsonResponse");

    const included = records_to_resource_objects(this._schema, doc?.included);

    return {
      type: "GetRecordResult",
      // TS: trying to assign general JSON object to InterfaceFrom<any>
      record: record as any,
      // TS: JSONAPI type for some reason has `id` as optional, which this doesn't
      included: included as any,
    };
  }

  public async deleteRecord(
    request: API.DeleteRecordRequest
  ): Res<API.DeleteRecordResult> {
    const { ofType, target } = request;

    const base = await this.#base();
    const url_base = this.get_resource_url(ofType, target);
    const url = `${url_base}`;
    const response = await fetch(url, { ...base, method: "DELETE" });
    if (!response.ok) {
      return { type: "ErrorResponse" };
    }

    return { type: "DeleteRecordResult" };
  }

  public async listRecords<T extends keyof S & string>(
    request: API.ListRecordsRequest<T, S>
  ): Res<API.ListRecordsResult<T, S>> {
    const { ofType, ...options } = request;
    const { fields, filter, includeCount, includeRelated, ...paging } = options;
    const { limit, offset } = paging;
    // const { limit, offset, orderBy, orderDescending } = paging;

    const base = await this.#base();
    const url_base = this.get_type_url(ofType);
    const params = new URLSearchParams({
      ...(filter && { filter: JSON.stringify(filter) }),
      ...(limit && { limit: limit.toString() }),
      ...(offset && { offset: offset.toString() }),
      ...(fields &&
        Object.fromEntries(
          Object.entries(fields).map(([type_alias, fields]) => [
            `fields[${type_alias}]`,
            fields.join(","),
          ])
        )),
    });
    const url = `${url_base}?${params}`;
    const response = await fetch(url, base);

    const document = await response.json();

    // TS: the JSON API mapping doesn't assume that the things have ID's, which
    // would be valid for processing incoming documents with multiple entities.
    // Yet JSON:API post doesn't accept multiple records, so it should probably
    // be changed upstream to indicate that ID's are expected.
    const records = collection_document_to_records(
      this._schema,
      document
    ) as EntityFrom<S[T]>[];

    // const { included } = document;
    return { type: "ListRecordsResult", records };
  }

  public async patchRecord(
    request: API.PatchRecordRequest
  ): Res<API.PatchRecordResult> {
    const { ofType, target, record } = request;

    if (!ofType) throw new Error("Type is required");
    if (!target) throw new Error("Target is required");
    if (!record) throw new Error("Record is required");

    const meta = this._request_meta(request);

    const url = this.get_resource_url(ofType, target);
    // TS: record_to_document expects `id`, which perhaps it should not
    const document = record_to_document(this._schema, <any>record);
    const body = JSON.stringify({ ...document, ...(meta && { meta }) });
    const base = await this.#base({ "Content-type": "application/json" });
    const response = await fetch(url, { ...base, method: "PATCH", body });

    const json_response = await response.json();
    const response_record = document_to_record(this._schema, json_response);

    await this._process_response_meta(request, json_response);

    return {
      type: "PatchRecordResult",
      // TS: trying to assign general JSON object to InterfaceFrom<any>
      record: <any>response_record,
    };
  }

  // The given record is expected to already have a `type` matching the
  // indicated type.
  public async postRecord(
    request: API.PostRecordRequest
  ): Res<API.PostRecordResult> {
    const { ofType, record } = request;

    if (!ofType) throw new Error("Type is required");
    if (!record) throw new Error("Record is required");

    const meta = this._request_meta(request);

    const url_base = this.get_type_url(ofType);
    const document = record_to_document(this._schema, <any>record);
    const body = JSON.stringify({ ...document, ...(meta && { meta }) });
    const base = await this.#base({ "Content-type": "application/json" });
    const response = await fetch(url_base, { ...base, method: "POST", body });

    const json_response = await response.json();
    const response_record = document_to_record(this._schema, json_response);

    await this._process_response_meta(request, json_response);

    return {
      type: "PostRecordResult",
      // TS: trying to assign general JSON object to InterfaceFrom<any>
      record: <any>response_record,
    };
  }

  public async putRecord(
    _request: API.PutRecordRequest
  ): Res<API.PutRecordResult> {
    throw new Error("not implemented");
  }

  // ====== Protected interface

  protected get_resource_url(type_alias: string, id: string): string {
    return `${this.get_type_url(type_alias)}/${encodeURIComponent(id)}`;
  }

  protected get_type_url(type_alias: string): string {
    return `${this._endpoint}/${encodeURIComponent(type_alias)}`;
  }

  protected _request_meta(request: API.WithMedia): object | undefined {
    let meta: object | undefined = undefined;

    const media_request = request.media;
    if (media_request) {
      // The initial request just indicates the mediate type and doesn't include
      // the data.
      meta = {
        media: Object.fromEntries(
          Object.entries(media_request).map(([property, spec]) => [
            property,
            { media_type: spec.media_type },
          ])
        ),
      };
      // But you also need to hang around afterwards and upload the actual data.
    }

    return meta;
  }
  protected async _process_response_meta(
    request: API.WithMedia,
    jsonapi_response: object
  ): Promise<void> {
    // If there were media requests, we should now have upload URL's for them.
    const media_request = request.media;
    const media_response = (<any>jsonapi_response)?.meta?.media;
    if (media_request && media_response) {
      for (const [property, info] of Object.entries(media_response)) {
        const upload_url = (<any>info)?.upload_url;
        if (!upload_url) {
          console.warn("MediaResponseMissingUploadUrl", { property, info });
          continue;
        }
        const request_item = media_request[property];
        if (!request_item) continue;

        const { data, media_type } = request_item;
        if (upload_url && data) {
          const put_response = await fetch(upload_url, {
            method: "put",
            mode: "cors",
            body: data,
            headers: new Headers({ "content-type": media_type }),
          });
          if (!put_response.ok) {
            console.warn("MediaPutFailed", put_response);
          }
        }
      }
    }
  }
}
