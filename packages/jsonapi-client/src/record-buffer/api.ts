import type { MetaInterfaces } from "@swymbase/metatype-core";
import type { ISubscribable } from "@thi.ng/rstream";
import type { IJsonApiClient, TypedRecord } from "../api";

/*
type Difference =
  | { type: "single"; a: any; b: any }
  | {
      type: "multi";
      a_count: number;
      b_count: number;
      a_unique: any[];
      b_unique: any[];
    };

type Differences = Record<string, Difference>;
*/

// Transitions:
// I drew these somewhere...
// I'm pretty sure there were error states in the drawing
// new => creating
// creating => read ??
// deleting => deleted
export type RecordCrudState =
  | "new"
  | "creating"
  | "reading"
  | "read"
  | "updating"
  | "deleting"
  | "deleted";

// A temporary holding place for a record and related resources
//
// everything you need so that a record form can be created as a shallow layer on top
// with some consideration for preact/update requirements
//
// not meant for use with domain-specific code (hence not generic)
export interface IRecordBuffer {
  // a fixed property of the object
  readonly type: string;

  // the ID of the record, unless the operation is to create a record
  // always immutable for existing records
  // set-once then immutable for new records
  readonly target: string | undefined;

  // the current value of the record
  // TBD: will *not* necessarily be a new object each time the record changes
  value(): TypedRecord;
  valueStream(): ISubscribable<TypedRecord>;

  // the buffer is a state machine
  // why is value a method and this is not?
  readonly state: RecordCrudState;
  stateStream(): ISubscribable<RecordCrudState>;

  // create or update
  save(): Promise<void>;

  // applicable only after the record has been saved
  reread(): void;

  // applicable only after the record has been saved
  delete(): void;

  // set the properties in the object
  edit(partial: object): void;

  // get the current value for the given property, if any
  property(property: string): any | undefined;
  // subscribe to property
  propertyStream(property: string): ISubscribable<any>;

  getFile(property: string): Blob | undefined;
  setFile(property: string, data: Blob): void;
  unsetFile(property: string): void;
  fileStream(property: string): ISubscribable<Blob | undefined>;

  // Collection operations (index-based add/remove) for multivalued properties.
  //
  // These operations are index-based, although really the collection is a set.
  // In practice, actual editors need “slots” which may temporarily contain
  // duplicate values.  Otherwise, items even briefly having the same value will
  // be treated as one item and edited in unison, which is unwanted.
  // Deduplication will occur when the record is saved.  (Currently this
  // deduplication is not broadcast as a change.)
  //
  // Add a value to a multi-valued property
  addValue(property: string, value: any): void;
  // Remove a value from a multi-valued property
  removeValue(property: string, index: number): void;
  // Replace a value in a multi-valued property
  // this is very derivative but I somehow want it
  replaceValue(property: string, index: number, replacement: any): void;

  // compare with the last-saved state
  // applicable only when the record has been saved
  /*
  diff(): Iterable<Differences>;
  */

  dispose(): void;

  // something about cache?
}

export interface RecordBufferInit {
  // the interface to the backing store
  readonly store: IJsonApiClient<any>;
  // assume you'll need access to the entire schema
  // (not just the schema for this record's type)
  readonly schemata: MetaInterfaces;
  readonly type: string;
  // If this is not provided, then the operation is create
  readonly id?: string;
}
export interface IRecordBufferConstructor {
  new (init: RecordBufferInit): IRecordBuffer;
}
