# Record buffer interface

A record buffer is an in-memory representation of a record of a given type and
related resources, with a connection to a backing store.

## Objective

Create an object interface to represent the operations needed by a form in the
context of a persistent data source.

- testable
- data-based (can dump/serialize needed state)
- can
  - make edits
  - commit edits
  - roll back edits
  - answer
    - what properties have changed values since
    - what events have occurred on the buffer, and when

## Assumptions

- works with in the assumptions of def.codes schema
- recognizes a specific set of schema extensions?
  - but not UI-specific stuff, talking about interface stuff
- one record at a time
- two operations/modes
  - update :: the record was read from a source. it already “existed”
  - create :: a new record should be created, and it doesn't correspond to any
    existing thing yet
- could use a local cache
  - everything was fetched at some past point, i.e. has some degree of staleness
    - this isn't strictly about the buffer, though, so much as the store
  - can't know about everything that may happen with this record (at the
    _source_) outside of the scope. Creator of buffer should be able to control
    access to it.
- could connect to sockets, though, when available
  - i.e. in principle, edits could originate from other than the user interface
- make no guarantees about order of values in multivalued property
  - even as to their consistent ordering from state to state (when unchanged)
  - or that “add” operation always goes to the end

## Questions

What about PUT vs POST?

- i.e. what if we want the user to be able to
  - supply an ID for new records
  - change ID of existing records?? this is even beyond PUT/POST and should
    generally be allowed
    - though could be done via DELETE + PUT
- and within that, what about instance namespace, or rules for IRI?

What about permissions? e.g. how would we know (and signal) when a record cannot
be deleted? Or a property edited?

## Concepts

The interface has a backing store.

The interface is always “about one thing”:

- for new records (`create` operation), it's about the buffered record that is
  modified through the interface, which doesn't correspond to anything anywhere
  else.

## Considerations

The backing store may use a cache.

## Operations

### Save

An async operation to write the contents of the record in its current state to
the associated location.

### Diff

A sync operation to get the set of changes for the current record.

If none, then the record is understood as functionally identical to its initial
state.
