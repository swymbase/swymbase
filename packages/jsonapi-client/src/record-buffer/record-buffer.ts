import { MetaInterface } from "@swymbase/metatype-core";
import type { ISubscribable, Subscription } from "@thi.ng/rstream";
import { CloseMode, stream } from "@thi.ng/rstream";
import type { IJsonApiClient, TypedRecord, WithMedia } from "../api";
import type {
  IRecordBuffer,
  IRecordBufferConstructor,
  RecordBufferInit,
  RecordCrudState,
} from "./api";

type Store = IJsonApiClient<any>;

/**
 * Schema-aware, API-aware, mutable in-memory storage for a record and its
 * related resources.
 */
// NOTE: Uses type aliases
export const RecordBuffer: IRecordBufferConstructor = class
  implements IRecordBuffer
{
  readonly #store: Store;
  readonly #state_stream = stream<RecordCrudState>();
  readonly #value_stream = stream<TypedRecord>();
  readonly #type_alias: string;
  readonly #type_iri: string;
  readonly #revisions: TypedRecord[];
  readonly #schema: MetaInterface;
  // See package README about multi-valued media properties
  readonly #files = new Map<string, Blob>();
  readonly #property_streams = new Map<string, Subscription<any, any>>();
  readonly #file_streams = new Map<
    string,
    Subscription<Blob | undefined, Blob | undefined>
  >();
  #target: string | undefined;

  constructor(init: RecordBufferInit) {
    const { type, id } = init;
    this.#store = init.store;
    this.#type_alias = type;
    this.#schema = init.schemata[type];
    this.#type_iri = this.#schema["@type"];
    if (!this.#schema) {
      console.error("RecordBufferNoSuchType", init);
      throw new Error("RecordBufferNoSuchType");
    }
    if (id) {
      this.#target = id;
      this.#revisions = [{ type: this.#type_iri, id }];
      // Note this will also immediately set state
      this.#read();
    } else {
      this.#state_stream.next("new");
      this.#revisions = [{ type: this.#type_iri }];
    }
  }

  get target() {
    return this.#target;
  }

  get state() {
    // TS: the stream will always have a value
    return this.#state_stream.deref()!;
  }
  stateStream() {
    return this.#state_stream;
  }

  get type() {
    return this.#type_alias;
  }

  value() {
    return this.#revisions[this.#revisions.length - 1];
  }
  valueStream() {
    return this.#value_stream;
  }

  reread() {
    this.#read();
  }

  async delete() {
    const { target } = this;
    if (!target) {
      throw new Error("AttemptedDeleteWithoutTarget");
    }

    this.#state_stream.next("deleting");
    const response = await this.#store.deleteRecord({
      ofType: this.#type_alias,
      target,
    });

    if (response.type === "ErrorResponse") {
      // re-read here?
    } else {
      this.#state_stream.next("deleted");
    }
  }

  async save() {
    const { target } = this;
    const ofType = this.#type_alias;
    const record = this.value();
    if (!record) {
      // Is this an assert fail?  when would the record be legitimately undefined?
      throw new Error("AttemptedSaveWithoutRecord");
    }
    if (target) {
      this.#state_stream.next("updating");
      const response = await this.#store.patchRecord({
        ofType,
        target,
        record,
        ...this.#maybe_media(),
      });

      if (response.type === "ErrorResponse") {
        // re-read here?
      } else {
        // It's “read” because the operation returns the updated version
        // TODO: but... we also need to update the record
        this.#state_stream.next("read");
      }
    } else {
      this.#state_stream.next("creating");
      // if client can specify ID, then we need PUT here
      const response = await this.#store.postRecord({
        ofType,
        record,
        ...this.#maybe_media(),
      });
      if (response.type === "ErrorResponse") {
        // re-read here?
      } else {
        // It's “read” because the operation returns the updated version
        //
        // Except... this isn't a "revision" as such
        this.#revisions.push(response.record);
        this.#state_stream.next("read");
        // This should only be assigned once
        this.#target = response.record.id;
      }
    }
  }

  edit(partial: object) {
    // check schema here?
    const new_value = { ...this.value(), ...partial };
    this.#revisions.push(new_value);
    this.#update_property_streams(partial);
    this.#value_stream.next(new_value);
  }

  // Get a the current value in the record buffer for the given property.
  property(name: string): any | undefined {
    // TS: wanted this to have an index signature
    return (this.value() as any)?.[name];
  }

  // Multivalued property operations

  addValue(property: string, value: any) {
    if (!this.#is_multivalued(property)) {
      console.warn("ExpectedMultivaluedProperty", property);
      return;
    }
    const before = this.property(property);
    if (before === undefined) {
      this.edit({ [property]: [value] });
    } else if (Array.isArray(before)) {
      this.edit({ [property]: [...before, value] });
    } else {
      console.warn("AddValueExpectedArray", before);
    }
  }

  removeValue(property: string, index: number) {
    if (!this.#is_multivalued(property)) {
      console.warn("ExpectedMultivaluedProperty", property);
      return;
    }
    const before = this.property(property);
    if (Array.isArray(before)) {
      this.edit({
        [property]: before.filter((_item, i) => i !== index),
      });
    }
  }

  replaceValue(property: string, index: number, replacement: any) {
    if (!this.#is_multivalued(property)) {
      console.warn("ExpectedMultivaluedProperty", property);
      return;
    }
    const before = this.property(property);
    if (Array.isArray(before)) {
      this.edit({
        [property]: before.map((item, i) => (i === index ? replacement : item)),
      });
    }
  }

  // Get a memoized reference to a value stream for the given property.
  propertyStream(property: string): any | undefined {
    if (this.#property_streams.has(property))
      return this.#property_streams.get(property);
    const new_stream = stream({ closeOut: CloseMode.NEVER });
    this.#property_streams.set(property, new_stream);
    const value = this.property(property);

    // Send current value immediately
    if (value === undefined) {
      const empty_value = this.#is_multivalued(property) ? [] : null;
      new_stream.next(empty_value);
    } else {
      new_stream.next(value);
    }
    return new_stream;
  }

  getFile(property: string): Blob | undefined {
    return this.#files.get(property);
  }
  setFile(property: string, data: Blob): void {
    this.#files.set(property, data);
    this.#file_streams.get(property)?.next(data);
  }
  unsetFile(property: string): void {
    this.#files.delete(property);
    this.#file_streams.get(property)?.next(undefined);
  }
  fileStream(property: string): ISubscribable<Blob | undefined> {
    if (this.#file_streams.has(property))
      return this.#file_streams.get(property)!;
    const new_stream = stream<Blob | undefined>({ closeOut: CloseMode.NEVER });
    this.#file_streams.set(property, new_stream);
    const value = this.getFile(property);

    // Send current value immediatel
    if (value === undefined) {
      new_stream.next(undefined);
    } else {
      new_stream.next(value);
    }
    return new_stream;
  }

  /*
  diff() {
    return [];
  }
  */

  // Should put the object into a terminal state where no operations are
  // allowed.
  dispose() {
    this.#state_stream.done();
    this.#value_stream.done();
    for (const sub of this.#property_streams.values()) sub.done();
  }

  // ================================

  async #read() {
    const { target } = this;
    if (!target) {
      throw new Error("AttemptedReadWithoutTarget");
    }
    this.#state_stream.next("reading");
    const response = await this.#store.getRecord({
      ofType: this.#type_alias,
      target,
    });
    if (response.type === "ErrorResponse") {
      // re-read? at least need to update state
    } else {
      // Except... this isn't a "revision" as such
      this.#revisions.push(response.record);
      this.#update_property_streams(response.record);
      this.#state_stream.next("read");
    }
  }

  // Not doing comparison of multivalued properties yet, but may for diff
  /*
    if (multivalued) {
      if (a == null && b == null) return true;
      if (!Array.isArray(a) || !Array.isArray(b) || a.length !== b.length)
        return false;
      // This gets expensive... if you're making no assumptions about order.
      // return a.
    }
*/

  #update_property_streams(partial: object) {
    for (const [key, value] of Object.entries(partial)) {
      if (this.#schema.properties.hasOwnProperty(key)) {
        // dedupe?
        // we would need an equals method that supports multivalue
        this.#property_streams.get(key)?.next(value);
      }
    }
  }

  #is_multivalued(property: string): boolean {
    return !!this.#schema.properties[property]?.multivalued;
  }

  #maybe_media(): WithMedia | undefined {
    if (this.#files.size) {
      return {
        media: Object.fromEntries(
          Array.from(this.#files, ([k, data]) => [
            k,
            { data, media_type: data.type },
          ])
        ),
      };
    }
  }
};
