import type { ExecutionContext } from "ava";

type Awaited<T> = T extends PromiseLike<infer U> ? U : T;

type CallAndResult<T, K = keyof T> = K extends string & keyof T
  ? T[K] extends (...args: infer A) => infer R
    ? { method: K; args: A; result: Awaited<R> }
    : never
  : never;

const memoize = <K, V>(fn: (key: K) => V) => {
  const cache = new Map();
  return (key: K) => {
    if (cache.has(key)) return cache.get(key);
    const value = fn(key);
    cache.set(key, value);
    return value;
  };
};

// Generic mock interface for testing known call & response expectations
export const mock_interface = <T>(t: ExecutionContext) => {
  const expecting: CallAndResult<T>[] = [];

  const get = memoize((prop: string) => (...args: any[]) => {
    const expect = expecting.shift();
    if (!expect) throw new Error("Unexpected call!");
    t.is(expect.method, prop, `Method`);
    t.deepEqual(expect.args, args, `Args`);
    return expect.result;
  });

  return {
    proxy: new Proxy({}, { get: (_target, prop) => get(<string>prop) }) as T,

    // Run the given procedure and confirm that calls to the proxy are exactly
    // the expected ones.
    expecting<R>(procedure: () => R, ...calls: CallAndResult<T>[]) {
      expecting.push(...calls);
      const result = procedure();
      t.is(expecting.length, 0, "Expected empty call queue");
      return result;
    },
  };
};
