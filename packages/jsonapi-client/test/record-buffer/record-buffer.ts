import type { IJsonApiClient } from "@swymbase/jsonapi-client";
import { RecordBuffer } from "@swymbase/jsonapi-client";
import { metatypes } from "@swymbase/metatype-core";
import type * as rs from "@thi.ng/rstream";
import test from "ava";
import { mock_interface } from "./lib/mock-interface";

type API = IJsonApiClient<any>;

// All of these tests share the same schema
const schemata = metatypes({
  Alpha: {
    "@type": "ex:Alpha",
    properties: {
      name: {
        term: "ex:name",
        type: "string",
      },
      alias: {
        term: "ex:alias",
        type: "string",
        multivalued: true,
      },
    },
  },
});

// Some helpers for streams

// Promise next value from stream
// TODO: I don't think this quite works right though
const next = <T>(source: rs.ISubscribable<T>, n = 1) =>
  new Promise<T | undefined>((resolve, reject) => {
    /* const sub = */ source.subscribe({
      next(value: T) {
        n--;
        if (n === 0) resolve(value);
        // Umm.... how to initialize?  This can actually run sync before above assignment
        // console.log("sub", sub);
        // setTimeout(() => sub.unsubscribe(), 1);
      },
      error(error: any) {
        reject(error);
      },
      done() {
        resolve(undefined);
      },
    });
  });

const expect_next = <T>(source: rs.ISubscribable<T>, expected: T) => {
  return new Promise((resolve, reject) => {
    let sub: rs.ISubscribable<T> | undefined = undefined;
    sub = source.subscribe({
      next(value) {
        if (value !== expected) {
          throw new Error(`Got ${value}; expected ${expected}`);
        }
        resolve(undefined);
        setTimeout(() => sub?.unsubscribe(), 1);
      },
      error: reject,
      done() {
        reject("Closed before getting a value");
      },
    });
  });
};

test("new without ID", (t) => {
  const { expecting, proxy: store } = mock_interface<API>(t);
  const type_alias = "Alpha";

  const buffer = expecting(
    () => new RecordBuffer({ store, schemata, type: type_alias })
  );

  t.is(buffer.state, "new", "Initial state without ID is `new`");
  t.deepEqual(buffer.value(), { type: "ex:Alpha" }, "Initial new record");
});

// What should getRecord return for a 404?
test.skip("new with not found id", (t) => {
  const { expecting, proxy: store } = mock_interface<API>(t);
  const type_alias = "Alpha";
  const id = "ex:Alice";

  expecting(() => new RecordBuffer({ store, schemata, type: type_alias, id }), {
    method: "getRecord",
    args: [{ ofType: type_alias, target: id }],
    result: { type: "ErrorResponse" },
  });
});

test("new with id of existing record", async (t) => {
  const { expecting, proxy: store } = mock_interface<API>(t);
  const type_alias = "Alpha";
  const id = "ex:Alice";

  const buffer = expecting(
    () => new RecordBuffer({ store, schemata, type: type_alias, id }),
    {
      method: "getRecord",
      args: [{ ofType: type_alias, target: id }],
      result: {
        type: "GetRecordResult",
        record: { id, type: "ex:Alpha", name: "Alice" },
      },
    }
  );

  t.is(buffer.state, "reading", "Initial state with ID is `reading`");

  const states = buffer.stateStream();
  const first_state = await next(states);
  // Note we don't get this until after the record state change.
  t.deepEqual(
    buffer.value(),
    { id, type: "ex:Alpha", name: "Alice" },
    "Record after read"
  );

  const second_state = await next(states);
  t.is(first_state, "reading");
  t.is(second_state, "read");
});

test("`save` changes state from `new` to `creating`, issuing a post call", (t) => {
  const { expecting, proxy: store } = mock_interface<API>(t);
  const type_alias = "Alpha";

  const buffer = new RecordBuffer({ store, schemata, type: type_alias });
  t.is(buffer.state, "new");

  expecting(() => buffer.save(), {
    method: "postRecord",
    args: [{ ofType: type_alias, record: { type: "ex:Alpha" } }],
    result: {
      type: "PostRecordResult",
      record: { id: "ex:Alice", type: "ex:Alpha" },
    },
  });

  t.is(buffer.state, "creating");
});

test("state changes to `read` after a successful save", async (t) => {
  const type_alias = "Alpha";
  const { expecting, proxy: store } = mock_interface<API>(t);
  const buffer = new RecordBuffer({ store, schemata, type: type_alias });

  const save_request = expecting(() => buffer.save(), {
    method: "postRecord",
    args: [{ ofType: type_alias, record: { type: "ex:Alpha" } }],
    result: {
      type: "PostRecordResult",
      record: { id: "ex:Alice", type: "ex:Alpha" },
    },
  });

  t.is(buffer.state, "creating");
  await save_request;
  t.is(buffer.state, "read");
});

test("edits are reflected in value", async (t) => {
  const { expecting, proxy: store } = mock_interface<API>(t);
  const type_alias = "Alpha";
  const buffer = new RecordBuffer({ store, schemata, type: type_alias });

  buffer.edit({ name: "Jake" });
  t.deepEqual(buffer.value(), { type: "ex:Alpha", name: "Jake" }, "Initial");

  const save_request = expecting(() => buffer.save(), {
    method: "postRecord",
    args: [{ ofType: type_alias, record: { type: "ex:Alpha", name: "Jake" } }],
    result: {
      type: "PostRecordResult",
      record: { id: "ex:12345", type: "ex:Alpha", name: "Jake" },
    },
  });
  t.is(buffer.state, "creating");

  await save_request;
  t.deepEqual(
    buffer.value(),
    { id: "ex:12345", type: "ex:Alpha", name: "Jake" },
    "Value after save"
  );

  t.is(buffer.state, "read");
});

// TODO: this test isn't what it says
test.skip("provides value updates as stream", async (t) => {
  const { expecting, proxy: store } = mock_interface<API>(t);
  const type_alias = "Alpha";

  const buffer = new RecordBuffer({ store, schemata, type: type_alias });

  buffer.edit({ name: "Jake" });
  t.deepEqual(buffer.value(), { type: "ex:Alpha", name: "Jake" });

  const save_request = expecting(() => buffer.save(), {
    method: "postRecord",
    args: [{ ofType: type_alias, record: { type: "ex:Alpha", name: "Jake" } }],
    result: {
      type: "PostRecordResult",
      record: { id: "ex:12345", type: "ex:Alpha", name: "Jake" },
    },
  });
  t.is(buffer.state, "creating");

  // Subscribe to edits.  Check that the stream emits the expected values

  await save_request;
  t.deepEqual(
    buffer.value(),
    { id: "ex:12345", type: "ex:Alpha", name: "Jake" },
    "Value after save"
  );

  t.is(buffer.state, "read");
});

test("add and remove multi-valued property values", (t) => {
  const { expecting, proxy: store } = mock_interface<API>(t);
  const type_alias = "Alpha";

  const buffer = new RecordBuffer({ store, schemata, type: type_alias });

  t.deepEqual(buffer.value()["alias"], undefined, "Zero");
  expecting(() => buffer.addValue("alias", "solar"));
  t.deepEqual(buffer.value()["alias"], ["solar"], "One");
  expecting(() => buffer.addValue("alias", "PV"));
  // Except we don't guarantee order
  t.deepEqual(buffer.value()["alias"], ["solar", "PV"], "Two");
  expecting(() => buffer.addValue("alias", "Array"));
  t.deepEqual(buffer.value()["alias"], ["solar", "PV", "Array"], "Three");
  expecting(() => buffer.removeValue("alias", 10));
  t.deepEqual(buffer.value()["alias"], ["solar", "PV", "Array"], "Three again");
  expecting(() => buffer.removeValue("alias", 1));
  t.deepEqual(buffer.value()["alias"], ["solar", "Array"], "Two again");
  expecting(() => buffer.removeValue("alias", 0));
  t.deepEqual(buffer.value()["alias"], ["Array"], "One again");
  expecting(() => buffer.replaceValue("alias", 0, "module"));
  t.deepEqual(buffer.value()["alias"], ["module"], "Changed");
});

test("property stream works on initialize", async (t) => {
  const { expecting, proxy: store } = mock_interface<API>(t);
  const type_alias = "Alpha";
  const id = "ex:Alice";

  const buffer = expecting(
    () => new RecordBuffer({ store, schemata, type: type_alias, id }),
    {
      method: "getRecord",
      args: [{ ofType: type_alias, target: id }],
      result: {
        type: "GetRecordResult",
        record: { id, type: "ex:Alpha", name: "Alice" },
      },
    }
  );

  const state_stream = buffer.stateStream();
  const name_stream = buffer.propertyStream("name");

  t.is(buffer.state, "reading");
  t.is(buffer.property("name"), undefined, "Initial name");
  await expect_next(state_stream, "reading");
  await expect_next(name_stream, "Alice");

  await expect_next(state_stream, "read");
  t.is(buffer.state, "read");
});

// test.todo("provides state updates as a stream");
// test.todo("delete & state");
// test.todo("supports undo");
