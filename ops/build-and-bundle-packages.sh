#!/bin/bash

# Build all packages.
#
# ASSUMES this is run from the project root.
#
# ASSUMES all dependencies are installed (where the runner image put them).

set -e

# Because this project uses workspaces, all dependencies are installed at the
# root.  (See `node-runner/Dockerfile`) .  Symlinking allows us to use modules
# from the container without copying them to the host.
echo 'Linking dependencies...'
ln -s /usr/local/src/build-deps/node_modules node_modules

# This is kind of a hack that allows package tests to run from the CI.  Without
# this, Node is unable to resolve intra-project module references.  This is only
# a problem when (the root) `node_modules` is itself symlinked, which is why it
# isn't a problem in local development.
echo 'Linking local packages...'
mkdir packages/node_modules
ln -s `realpath packages` packages/node_modules/@swymbase

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

echo 'Building packages...'
"$this_script_dir"/build.sh

echo 'Bundling packages...'
"$this_script_dir"/bundle.sh

exit 0
