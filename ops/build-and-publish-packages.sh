#!/bin/bash

# Build packages and publish to this project's registry.
#
# ASSUMES all dependencies are already installed.
#
# ASSUMES several Gitlab CI variables are set.
#
# ASSUMES packages are located in `packages/*` directories at the project root.

set -e

REPOSITORY_USERNAME="${1}"
REPOSITORY_TOKEN="${2}"

if [ -z "$REPOSITORY_USERNAME" ]; then
  >&2 echo 'build-and-publish-packages: abort: `REPOSITORY_USERNAME` must be set'
  exit 1
fi

if [ -z "$REPOSITORY_TOKEN" ]; then
  >&2 echo 'build-and-publish-packages: abort: `REPOSITORY_TOKEN` must be set'
  exit 1
fi

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

# === Build

"$this_script_dir"/build-and-bundle-packages.sh

# === Publish

echo "//registry.npmjs.org/:_authToken=$NPM_PUBLISH_TOKEN" > .npmrc

# Get out of detached head state, necessary for creating a new revision.
git checkout "${CI_BUILD_REF_NAME}"

# Commit requires these config settings.
git config --global user.name "${GITLAB_USER_NAME:-GitLab CI}"
git config --global user.email "${GITLAB_USER_EMAIL}"

# Bump version numbers.  This can also be done as part of `lerna publish`.
# Preferring a separate command just for clarity & debug.
# 
# Using '[skip ci]' helps prevent a CI loop.  Note that the commit message
# will also include a list of the updated packages and their versions.
npx lerna version patch --exact --no-push --yes --message 'Publish [skip ci]'

# DEBUG
git status

# Publish all updated packages to the registry.
# --no-verify-access is needed for use with NPM access tokens.  See
# https://github.com/lerna/lerna/issues/2788#issuecomment-774265338
npx lerna publish from-git --yes --no-verify-access

# If publish succeeded, push revision back to original branch.
# Note that if nothing changed, then this is a no-op.
git remote set-url --push origin "${CI_REPOSITORY_URL}"
git push \
    --follow-tags \
    "https://${REPOSITORY_USERNAME}:${REPOSITORY_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git" \
    "${CI_BUILD_REF_NAME}"

exit 0
