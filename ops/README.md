# Operational resources

This directory contains resources that are needed for non-local operating
environments. The main CI is located in the normal place at the root
[.gitlab-ci.yml](../.gitlab-ci.yml).

Scripts that are needed only on local workstations are located in the
[dev](../dev) directory. Those scripts should _not_ be used from other
environments.

## Access tokens

The CI performs several operations that require permissions that are not
automatically available in the CI's job context. The following table summarizes
these operations, the permissions they require, and how they map to CI variables
assumed by the current configuration.

| Operation                                   | required permissions | Token location            |
| ------------------------------------------- | -------------------- | ------------------------- |
| Push build runner to container registry     | api                  | `GITLAB_RUNNER_API_TOKEN` |
| Create or update CI variable for runner tag | api                  | `GITLAB_RUNNER_API_TOKEN` |
| Push “Publish” commit to repository         | write repository     | `GITLAB_COMMIT_TOKEN`     |
| Publish to NPM registry                     | Automation (NPM)     | `NPM_PUBLISH_TOKEN`       |

The tokens are stored in CI variables of the same name.
