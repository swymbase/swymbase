#!/bin/sh

# Build all defined packages, passing any given arguments.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
project_dir="$this_script_dir/.."

cd "$project_dir"

npx tsc --build "$@"

exit 0
