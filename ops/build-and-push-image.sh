#!/bin/sh

# **Note**: this script is copied verbatim from the definition at:
# https://git.mindgrub.net/gcannizzaro/ci-tests/-/blob/develop/custom_runner/ci/build-and-push-image.sh
# 
# This script builds and pushes an image to the current project's container
# registry and records the resulting tag in a CI variable.  The identifier of
# the current pipeline is used as a unique tag for the new image.
#
# This script is run in the context of a Gitlab CI job.  The working directory
# is the same as that of the job (not the directory containing this script).
# Besides the standard CI variables, the script assumes the following are set:
#
# - IMAGE_NAME: A path-friendly name uniquely identifying this container
#   definition within this project.
#
# - IMAGE_INPUTS: A space-delimited list of paths to the files or directories
#   that image needs.  This will generally correspond to what is `ADD`ed in the
#   Dockerfile.  This list may contain 
#
# - DOCKER_FOLDER: The path (relative to the working directory) where the
#   Dockerfile is located.  The Docker build is done with this as context.
#
# - IMAGE_TAG_CI_VAR_NAME: The name of a variable in the CI environment that
#   stores the latest tag for this image.  This variable may be updated in the
#   course of the job.

set -e

# A Gitlab token with API permission on this project.
API_TOKEN="$1"

IMAGE="$CI_REGISTRY_IMAGE/$IMAGE_NAME"

# This variable accepts a list (multiple tokens), so don't quote it.
# 
# `cpio` is preferred over `cp` because it can create all directories as needed.
# This allows you to specify a wider variety of inputs, e.g. using glob patterns
# (though note that glob patterns specified when setting `IMAGE_INPUTS` in the
# extending job will already be expanded by the time we get here).  `pax` can do
# this slightly more simply, but isn't included in this image.  The `cpio` in
# this distribution for some reason only knows the short forms of the
# `--pass-through` and `--make-directories` options.
echo $IMAGE_INPUTS \
  | tr ' ' '\n' \
  | cpio -pd "$DOCKER_FOLDER"

echo 'Logging into Docker...'
docker login -u "$CI_REGISTRY_USER" -p "$CI_JOB_TOKEN" "$CI_REGISTRY"

echo 'Building Docker image...'
docker build -t "$IMAGE:$CI_PIPELINE_ID" "$DOCKER_FOLDER"

echo 'Pushing Docker image...'
docker push "$IMAGE:$CI_PIPELINE_ID"


# We're going to use the Gitlab HTTP API to add or update the environment
# variable with the new tag.
# 
# We don't want to proceed with jobs that depend on this image unless the var
# update succeeds.  `--fail` makes `curl` exit with an non-zero status if the
# response is an HTTP error.
# 
# We want to check the value of the variable whose *name* is
# $IMAGE_TAG_CI_VAR_NAME, not that variable itself.  `eval` is used because the
# `sh` we're running here doesn't support “indirect expansion” a la ${!NAME}
runner_tag=$(eval echo \$$IMAGE_TAG_CI_VAR_NAME)

# `curl` is not included in `docker:latest`.
apk add curl

# If there is no runner tag 
if [ -z "$runner_tag" ]; then
  curl \
    --fail \
    --request POST \
    --header "PRIVATE-TOKEN: ${API_TOKEN}" \
    --form "key=${IMAGE_TAG_CI_VAR_NAME}" \
    --form "value=${CI_PIPELINE_ID}" \
    --form 'variable_type=env_var' \
    --form 'protected=false' \
    --form 'masked=false' \
    --form 'environment_scope=*' \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables/"
else
  curl \
    --fail \
    --request PUT \
    --header "PRIVATE-TOKEN: ${API_TOKEN}" \
    --form "value=${CI_PIPELINE_ID}" \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables/${IMAGE_TAG_CI_VAR_NAME}"
fi


exit 0
