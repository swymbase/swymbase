#!/bin/bash

# Build packages and run all tests.
#
# ASSUMES only unit tests are defined (not integration tests).

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

# === Build

"$this_script_dir"/build-and-bundle-packages.sh

# === Test

# Skip tests on CI, as nearly all require a database.
#
# echo 'Running tests...'
# npm test


exit 0
