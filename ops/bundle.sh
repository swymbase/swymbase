#!/bin/sh

# Run bundler on compiled packages, passing on given arguments.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
project_dir="$this_script_dir/.."

cd "$project_dir"

npx rollup -c "$@"

exit 0
