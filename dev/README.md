# Development scripts

This directory contains scripts and other resources intended for use in local
development.

Scripts needed in other environments are in the [ops](../ops) directory. These
scripts may call those scripts as needed.
