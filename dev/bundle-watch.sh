#!/bin/sh

# Run bundler in watch mode.
#
# ASSUMES relative location of ops script dir.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
ops_dir="$this_script_dir/../ops"

"$ops_dir"/bundle.sh --watch

exit 0
