#!/bin/sh

# Run the package build in watch mode.
#
# ASSUMES relative location of ops script dir.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
ops_dir="$this_script_dir/../ops"

"$ops_dir"/build.sh \
          --watch \
          --preserveWatchOutput \
          --assumeChangesOnlyAffectDirectDependencies

exit 0
