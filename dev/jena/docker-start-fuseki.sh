#!/bin/sh

# Run a local RDF SPARQL server using a public Docker image.
#
# The `mode` argument can be used to run Jena in disk mode or in-memory mode
# (the default) .  In-memory mode is faster and results in less disk thrashing.
# (I want to say it is more stable, but I don't have evidence of that.)
# However, it is still valuable to run with a TDB database, as the results will
# sometimes differ (structurally but in semantically equivalent ways).

set -e

port="${1:-${PORT:-3030}}"
mode="${2:-mem}"                # anything else will use disk mode
dataset='swymbase'
container='swymbase_dev_fuseki'
ui_password='password'

echo 'Checking for an existing container...'
existing=$(docker ps --all --quiet --filter "name=$container" --filter status=running)

run_jena() {
  local jena_args="$1"
  local docker_args="$2"

  # Always use `strict` mode.  I don't see this option documented anywhere, but
  # it is described in the `--help` text of `fuseki-server` as:
  #
  # > Operate in strict SPARQL mode (no extensions of any kind)
  #
  # This prevents the inadvertent use of non-standard SPARQL constructs (which
  # has happened).
  docker run \
         -d \
         --name "$container" \
         -p "$port:3030" \
         -e ADMIN_PASSWORD="$ui_password" \
         $docker_args \
         stain/jena-fuseki /jena-fuseki/fuseki-server --strict $jena_args
}

if [ -z "$existing" ]; then
  echo "No running '$container' container found.  Creating one."

  # In case the container wasn't removed, remove it now.
  set +e
  docker rm "$container" 2>&1 > /dev/null
  set -e
  
  if [ "$mode" == 'mem' ]; then
    run_jena "--mem /$dataset"
  else
    run_jena '' "-e TDB=2 -e FUSEKI_DATASET_1=$dataset"
  fi
fi

exit 0
