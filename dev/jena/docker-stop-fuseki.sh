#!/bin/bash

# Stop any container started by `docker-start-fuseki.sh`.

set -e

container='swymbase_dev_fuseki'

echo 'Checking for an existing container...'
existing=$(docker ps --all --quiet --filter "name=$container")

if [ ! -z "$existing" ]; then
  docker stop -t 0 "$container"
  docker rm "$container"
fi

exit 0
