# Apache Jena and Fuseki

[Apache Jena](https://jena.apache.org/index.html) is “a free and open source
Java framework for building Semantic Web and Linked Data applications.”

Specifically, it includes an RDF Dataset storage and a SPARQL 1.1 compliant
query engine, as well as a web service
([Fuseki](https://jena.apache.org/documentation/fuseki2/index.html)) that
implements the SPARQL query and update protocols.

While this application uses Amazon Neptune in production, it can also be used
with Apache Jena for local development.

This directory contains resources for using Jena and Fuseki for the
application's knowledge base.

## Quick start

The [docker-start-fuseki.sh](./docker-start-fuseki.sh) script in this directory
will launch a Docker container in the background running a Fuseki service with
default settings.

The `docker-stop-fuseki.sh` script will stop the service.

## Web UI

Fuseki also includes a web-based user interface, which you can use to run
queries interactively.

You can access it at the same port where the protocol service is running, e.g.
http://localhost:3030

Credentials are required to access the datasets via the UI. The password for the
`admin` user is `password`. If needed, the password can be changed in
[docker-start-fuseki.sh](docker-start-fuseki.sh).

## Running Fuseki manually

It is very easy to get Fuseki up and running.

### via Java

If you have `java` installed, you can install Jena by downloading and extracting
the Jena and Fuseki bundles, setting `JENA_HOME`, and adding them to your
`PATH`.

```sh
fuseki-server --update --mem /swymbase
```

This starts an in-memory database service at `localhost:3030/swymbase`.

### via Docker

Alternatively, if you have Docker installed, you can run the same service with
no further installation using the following command:

```sh
docker run -p 3030:3030 atomgraph/fuseki --update --mem /swymbase
```
