#!/bin/sh

# Start a transient Fuseki server in the foreground using a local installation.
# Note that this will create a `run` subdirectory in the current directory.

set -e

port="${1:-${PORT:-3030}}"
dataset='swymbase'

if [ -z "$JENA_HOME" ]; then
  >&2 echo 'abort: JENA_HOME must be set.  Is Jena installed?'
  exit 1
fi

if ! which fuseki-server; then
  >&2 echo 'abort: fuseki-server command is not in path.  Is Fuseki installed?'
  exit 1
fi

fuseki-server --update --mem --port "$port" "/$dataset"

exit 0
