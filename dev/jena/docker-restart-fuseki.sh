#!/bin/bash

# Take down and bring back up a local Fuseki container.
# 
# NOTE!  This clears any data in the instance.

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"

"$script_dir"/docker-stop-fuseki.sh
"$script_dir"/docker-start-fuseki.sh "$@"

exit 0
