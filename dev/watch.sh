#!/bin/sh

# Run script, bundle, and test watch from same shell.
#
# ASSUMES relative location of ops script dir.

set -e

trap 'kill %1; kill %2' SIGINT

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
scripts="$this_script_dir"
ops_dir="$this_script_dir/../ops"

# First do a build so that test files are found.
"$ops_dir"/build.sh

"$scripts"/build-watch.sh \
	& "$scripts"/bundle-watch.sh \
  & "$scripts"/test-watch.sh \
  & wait

exit 0
