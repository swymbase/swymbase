#!/bin/sh

# Run the tests in the current configuration, passing any given arguments.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
. "$this_script_dir"/config.sh

"$this_script_dir"/check-database.sh

# You can uncomment and modify this (temporarily!) to target tests.
# TEST_SCOPE='packages/sparql-ast/dist/test/**/*'

# AVA complains when an empty argument is passed.
if [ -z "$TEST_SCOPE" ]; then
  npx ava "$@"
else
  npx ava "$@" "$TEST_SCOPE"
fi

exit 0
