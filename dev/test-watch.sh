#!/bin/sh

# Run the test runner in watch mode.
# ASSUMES the build is being updated (else code changes will have no effect).

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

"$this_script_dir"/test.sh --watch

exit 0
