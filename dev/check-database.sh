#!/bin/sh

# Verify that a SPARQL protocol service is reachable at the configured endpoint.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
. "$this_script_dir"/config.sh

if [ -z "$SPARQL_ENDPOINT" ]; then
  >&2 echo "abort: a SPARQL_ENDPOINT must be set."
  exit 1
fi

set +e
output=`curl -H 'Accept: application/json' -d 'SELECT * WHERE {}' "$SPARQL_ENDPOINT" 2>&1`
status="$?"
set -e

if [ "$status" != "0" ]; then
  >&2 echo "abort: could not confirm connection to SPARQL service at ${SPARQL_ENDPOINT}:"
  >&2 echo "$output"
  exit 1
fi

exit 0
