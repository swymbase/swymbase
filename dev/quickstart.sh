#!/bin/sh

# Get a workstation up and running.

set -e

port="${1:-${PORT:-3030}}"

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
project_dir="$this_script_dir/.."

write_env() {
    echo "export SPARQL_ENDPOINT='http://localhost:$port/swymbase'"
    echo "export PRIMARY_GRAPH='http://example.com/swymbase/dev/'"
    echo '
# Prevent X-Ray instrumentation from crashing certain tests
export AWS_XRAY_CONTEXT_MISSING="LOG_ERROR"
export AWS_XRAY_LOG_LEVEL="silent"
'
}

echo 'Checking environment configuration...'
local_env="$project_dir/.env.local"
if [ ! -f "$local_env" ]; then
    echo 'No environment configuration found, creating a default...'
    write_env > "$local_env"
fi

echo 'Starting a database...'
"$this_script_dir"/jena/docker-start-fuseki.sh "$port"

echo 'Installing dependencies...'
npm install

echo 'Building packages...'
npm run build

echo 'Pausing before tests...'
sleep 5

"$this_script_dir"/test.sh

echo "Quick start is complete!"
echo "You can now start a workflow by running"
echo "dev/watch.sh"

exit 0
