#!/bin/bash

# Add a dependency to an NPM workspace.  This should be provided by `npm`, but
# is currently not as such.
#
# ASSUMES the parent directory is the project root.

set -e

workspace="$1"
package="$2"
dev="$3"

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
project_dir="$this_script_dir/.."

cd "$project_dir"

if [ -z "$workspace" ]; then
  >&2 echo 'abort: workspace is required'
  exit 1
fi

if [ -z "$package" ]; then
  >&2 echo 'abort: package is required'
  exit 1
fi

if [ ! -d "$workspace" ]; then
  >&2 echo "abort: no such workspace: ‘${workspace}’"
  exit 1
fi

if [ "$dev" == 'dev' ]; then
  maybe_save_dev='--save-dev'
fi

npm install \
    --prefix "$workspace" \
    --save-exact \
    $maybe_save_dev \
    --package-lock-only \
    --no-package-lock \
    "$package"
npm install

exit 0
