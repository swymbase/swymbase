#!/bin/sh

# Remove all build artifacts.  Does not remove installed dependencies.
# ASSUMES the parent directory is the project root.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
project_dir="$this_script_dir/.."

# Remove all generated files.
rm -rf packages/*/dist

exit 0
