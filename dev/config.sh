# Source this script to apply environment configuration to current shell.

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
project_dir="$this_script_dir/.."
config_file="$project_dir/.env.local"

if [ -f "$config_file" ]; then
  . "$config_file"
else
  >&2 echo "No config file found at $config_file"
  # Exit here would exit the outer (sourcing) shell, probably not what you want.
fi
