#!/bin/sh

# Format source code or validate formatting.

set -e

command="${1:-write}"

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
project_dir="$this_script_dir/.."

# Opt into file list.
npx prettier \
    "--$command" \
    "$project_dir/**/*.{ts,tsx,json,md}"

exit 0
