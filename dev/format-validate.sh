#!/bin/sh

# Confirm that code matches automated formatting or fail with error status.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

"$this_script_dir"/format-code.sh list-different

exit 0
