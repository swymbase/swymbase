# Swymbase extension and deployment

This document describes the design strategy for supporting the automated
deployment of customized swymbase instances.

## Background

Swymbase provides common low-level functionality with extension points for
domain-specific requirements. Swymbase also provides facilities for deploying an
API instance to an AWS environment via the CDK. As such, a swymbase API uses a
mixture of its own code and code provided by the application.

In order to package and deliver the required code, swymbase must know where to
locate its own code (which should be easy!) and where to locate the
application's extension points. In JavaScript, packages are the idiomatic way of
providing and consuming collections of functionality.

## Interface extension package

Extension of a swymbase instance is done by way of an “interface extension
package.” An interface extension package is a directory containing JavaScript
modules that provide well-known extension points.

When deploying to the CDK, all necessary modules are incorporated into the
Lambda functions.

The directory is expected to have the following layout

```
interface
media
```

### Declarative extensions

The interface supports data-based extension.

Note that although these extensions use only (JSON-serializable) data, they may
be computed dynamically and exported from modules.

#### Schema

All interface operations require a [schema](./schema.md). This schema is
provided in the form of a dictionary of record type descriptions.

#### Environment

Provisional. Applications commonly use values from the process environment.
However, it's not clear how this would matter to a swymbase instance as such.

### Functional extensions

In addition to data-based extension, the interface supports code-based extension
points.

#### Observers

Per-type observers can be provided.

#### Permissions

Per-type permissions can be provided.

#### Preprocessors and context

API handlers can use custom preprocessors for various purposes:

- providing context to interface operations, such as
  - extracting authentication info from request
- validating request or otherwise enforcing invariants

#### Additional API handlers

Applications may need to provide additional handlers on the API besides the ones
provided by swymbase. It should be possible to define such handlers in any way
the appication sees fit.

It should be possible to reference the API Gateway instance created by swymbase
to provide such extension points.

Such handlers would not need to conform to JSON:API; however, if they did, it
should be possible to use swymbase's support for building them.

### Media

Media processors, possibly custom.

## Roadmap

This section discusses possible growth directions for swymbase extensions.

### JSON:API `meta` processing

The `data` property of the JSON:API is predominantly governed by the JSON:API
spec. However, the `meta` property can be used in any way that applications see
fit.

Future extension points may include the ability to provide custom `meta`
processing for JSON:API responses.
