# null

This document is about a potential use of `null` values as a signal in
@swymbase, where their presence currently has no defined semantics.

# background

> I'm not sure all this is relevant

Proposition stores tend to focus on _positive assertions_. In the open world, we
avoid statements about what _isn't_ true.

Likewise, there is no need to assert the lack of knowledge about (what would
otherwise be) some complete fact.

Blank nodes can appear like a way of asserting ignorance, since you're saying
something like “Alice loves X”, where X is simply unknown.

But even this is making a positive assertion.

@swymbase uses an open-world assumption as a starting point.

No properties are required on records except `id` and (almost always) `type`.

When a predicate lacks a value, we do not assert this.

# proposal

`null` is already used by _incoming_ records to signal the intent to erase a
value (as is done in JSON:API). `null` is thus distinct from `undefined` for the
purpose of _writing_ values.

`null` in _outgoing_ records signals the absence of a value _when we checked_.
(As opposed to, it's missing because it wasn't included in the projection.)

`null` values could serve as a weak but possibly convenient signal that a value
is “in fact” not defined.

A downside of this would be that responses would become more verbose, by
spelling out “information” that is otherwise implicit.

@swymbase doesn't seek to dictate the usage of `null` by applications. They are
usually treated as indicating the absence of a fact.

## prior art

“Maybe Not”, Rich Hickey
