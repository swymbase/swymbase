# Swymbase schema

Documentation TBD.

Swymbase schema is essentially a dictionary of metatype record schemata.

Currently, swymbase assumes a hardcoded schema.

This schema serves as the basis for a number of interpretations, including a set
of TypeScript types.
