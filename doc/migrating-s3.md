# Migrating media from one swymbase instance to another

This can come up when migrating a knowledge base between environments. For
example, if you have data in a development environment that you want to move to
production or _vice versa_, you may want to copy a knowledge base wholesale.

Migrating media between swymbase instances involves mainly two tasks:

- copying the contents of the S3 media bucket
- updating any necessary references

If the instances are in different AWS accounts, you'll need to

- https://aws.amazon.com/premiumsupport/knowledge-center/copy-s3-objects-account/

```sh
aws s3 sync \
  's3://app_name-from_stage-media' \
  's3://app_name-to_stage-media' \
  --acl bucket-owner-full-control
```

In general, you can copy the RDF data from one instance to another by
serializing to Turtle (or some other format) and loading the file in the target
instance.

However, if the facts include references to media objects, these will likely
include environment-specific bucket names. You must replace these to match the
target environment.
