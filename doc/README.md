# SWYMbase

Clearinghouse for docs. Status obvious.

## big ideas

- RDF is bytecode

- knowledge + knowledge -> new knowledge

  - what something “means” is what you can infer from it

- all computation is interpretation of a value in a context

- all application runtimes should have a knowledge base

- model interpretation is truth maintenance

- say anything about anything, in the same space

## application as knowledge

- entire application goes into knowledge base

  - store code modules, functions, or expressions as values

    - and say things about them

## the meaning sieve

What is stopping us from being able to say what a piece of data means?

is it that:

- there is no meaning?

- it doesn't mean anything?

- we don't know what it means?

- we don't know how to say what it means?

- we don't have a formal way to say what it means?

- our languages don't support formalisms for saying what things mean?

- our languages don't support formalisms for talking about the relevant domains?

## more specific points

```turtle
<mod1> ex:hasCode """
import { subscription } from "@thi.ng/rstream"

export const whatever = "something"
"""^^def:es2015
```

things you can put into the knowledge base

- data (endurant descriptions of things in your domain)

- metadata (endurant descriptions of types from your domain)

- code artifacts: modules, functions

- rules

  - inference rules

  - permissions

  - rules about how things should be displayed

## use cases

- pick something up here and put it over there without losing meaning

- persist runtime objects

  - cf
    [DynamoDB object persistence](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/HigherLevelInterfaces.html)

- above two are mostly about (wire) format
