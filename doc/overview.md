# SWYMbase overview

Status: WIP

## Motivation

Core dump --- I will clean this up later:

- we already have (RDS-based) information storage and retrieval systems

- but putting information into RDS systems doesn't amount to _saying_ anything

  - because it's like filling out a form where you must know _a priori_ the
    meaning of each location in the form

- applications need to talk about an open set of domains

  - including meta-domains, i.e. saying things about other domains and the
    relations between them

- RDF/Linked Data/Semantic Web already deal with these problems

- but SPARQL is too low-level for applications to use directly

  - no permissions/access model

  - no events model

  - unfamiliar naming requirements

- applications are still dominated by a record-oriented worldview

  - JSON-LD is a good bridge, but as a mapping DSL it's not designed for talking
    about domains as such

  - JSON:API is a good bridge, but can't be supported out-of-the-box by SPARQL
    providers without special (application-specific) mapping
